/*
 * 
 */
package spl.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.parsers.EnumParser;

import spl.SplPackage;
import spl.diagram.edit.parts.CMconstraintsOprEditPart;
import spl.diagram.edit.parts.CdwellformednessSourceTargetEditPart;
import spl.diagram.edit.parts.ChoiceNameVarTypeEditPart;
import spl.diagram.edit.parts.ClassNameInterfcEditPart;
import spl.diagram.edit.parts.DecMappingOprLHSOprRHSEditPart;
import spl.diagram.edit.parts.EntryNameEditPart;
import spl.diagram.edit.parts.FMconstraintsOprEditPart;
import spl.diagram.edit.parts.FeatMappingOprLHSOprRHSEditPart;
import spl.diagram.edit.parts.FeatureNameVartypeEditPart;
import spl.diagram.edit.parts.GuardNameEditPart;
import spl.diagram.edit.parts.InterfcNameEditPart;
import spl.diagram.edit.parts.LabelNameEditPart;
import spl.diagram.edit.parts.MapPresenceEditPart;
import spl.diagram.edit.parts.MapToEntityPresenceEditPart;
import spl.diagram.edit.parts.MapdecPresenceEditPart;
import spl.diagram.edit.parts.MaptoPresenceEditPart;
import spl.diagram.edit.parts.PropertyNameTextEditPart;
import spl.diagram.edit.parts.RelationshipNameLabInMulOutMulEditPart;
import spl.diagram.edit.parts.ScwellformednessSourceTargetEditPart;
import spl.diagram.edit.parts.StateChartNameEditPart;
import spl.diagram.edit.parts.StateNameEntryEditPart;
import spl.diagram.edit.parts.TransitionNameTriggerGrdTransitioEditPart;
import spl.diagram.edit.parts.TransitionalBehaviorNameEditPart;
import spl.diagram.edit.parts.TriggerNameEditPart;
import spl.diagram.parsers.MessageFormatParser;
import spl.diagram.part.SplVisualIDRegistry;

/**
 * @generated
 */
public class SplParserProvider extends AbstractProvider implements IParserProvider {

	/**
	* @generated
	*/
	private IParser propertyNameText_5022Parser;

	/**
	* @generated
	*/
	private IParser getPropertyNameText_5022Parser() {
		if (propertyNameText_5022Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getProperty_Name(),
					SplPackage.eINSTANCE.getProperty_Text() };
			MessageFormatParser parser = new MessageFormatParser(features);
			propertyNameText_5022Parser = parser;
		}
		return propertyNameText_5022Parser;
	}

	/**
	* @generated
	*/
	private IParser stateChartName_5031Parser;

	/**
	* @generated
	*/
	private IParser getStateChartName_5031Parser() {
		if (stateChartName_5031Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getStateChart_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			stateChartName_5031Parser = parser;
		}
		return stateChartName_5031Parser;
	}

	/**
	* @generated
	*/
	private IParser choiceNameVarType_5001Parser;

	/**
	* @generated
	*/
	private IParser getChoiceNameVarType_5001Parser() {
		if (choiceNameVarType_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getChoice_Name(),
					SplPackage.eINSTANCE.getChoice_VarType() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditPattern("{0}:{1}"); //$NON-NLS-1$
			choiceNameVarType_5001Parser = parser;
		}
		return choiceNameVarType_5001Parser;
	}

	/**
	* @generated
	*/
	private IParser cMconstraintsOpr_5032Parser;

	/**
	* @generated
	*/
	private IParser getCMconstraintsOpr_5032Parser() {
		if (cMconstraintsOpr_5032Parser == null) {
			EAttribute editableFeature = SplPackage.eINSTANCE.getCMconstraints_Opr();
			EnumParser parser = new EnumParser(editableFeature);
			cMconstraintsOpr_5032Parser = parser;
		}
		return cMconstraintsOpr_5032Parser;
	}

	/**
	* @generated
	*/
	private IParser featureNameVartype_5006Parser;

	/**
	* @generated
	*/
	private IParser getFeatureNameVartype_5006Parser() {
		if (featureNameVartype_5006Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getFeature_Name(),
					SplPackage.eINSTANCE.getFeature_Vartype() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditPattern("{0}:{1}"); //$NON-NLS-1$
			featureNameVartype_5006Parser = parser;
		}
		return featureNameVartype_5006Parser;
	}

	/**
	* @generated
	*/
	private IParser fMconstraintsOpr_5019Parser;

	/**
	* @generated
	*/
	private IParser getFMconstraintsOpr_5019Parser() {
		if (fMconstraintsOpr_5019Parser == null) {
			EAttribute editableFeature = SplPackage.eINSTANCE.getFMconstraints_Opr();
			EnumParser parser = new EnumParser(editableFeature);
			fMconstraintsOpr_5019Parser = parser;
		}
		return fMconstraintsOpr_5019Parser;
	}

	/**
	* @generated
	*/
	private IParser featMappingOprLHSOprRHS_5036Parser;

	/**
	* @generated
	*/
	private IParser getFeatMappingOprLHSOprRHS_5036Parser() {
		if (featMappingOprLHSOprRHS_5036Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getfeatMapping_OprLHS(),
					SplPackage.eINSTANCE.getfeatMapping_OprRHS() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditPattern("{0}:{1}"); //$NON-NLS-1$
			featMappingOprLHSOprRHS_5036Parser = parser;
		}
		return featMappingOprLHSOprRHS_5036Parser;
	}

	/**
	* @generated
	*/
	private IParser mapPresence_5034Parser;

	/**
	* @generated
	*/
	private IParser getMapPresence_5034Parser() {
		if (mapPresence_5034Parser == null) {
			EAttribute editableFeature = SplPackage.eINSTANCE.getMap_Presence();
			EnumParser parser = new EnumParser(editableFeature);
			mapPresence_5034Parser = parser;
		}
		return mapPresence_5034Parser;
	}

	/**
	* @generated
	*/
	private IParser maptoPresence_5035Parser;

	/**
	* @generated
	*/
	private IParser getMaptoPresence_5035Parser() {
		if (maptoPresence_5035Parser == null) {
			EAttribute editableFeature = SplPackage.eINSTANCE.getMapto_Presence();
			EnumParser parser = new EnumParser(editableFeature);
			maptoPresence_5035Parser = parser;
		}
		return maptoPresence_5035Parser;
	}

	/**
	* @generated
	*/
	private IParser decMappingOprLHSOprRHS_5039Parser;

	/**
	* @generated
	*/
	private IParser getDecMappingOprLHSOprRHS_5039Parser() {
		if (decMappingOprLHSOprRHS_5039Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getDecMapping_OprLHS(),
					SplPackage.eINSTANCE.getDecMapping_OprRHS() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditPattern("{0}:{1}"); //$NON-NLS-1$
			decMappingOprLHSOprRHS_5039Parser = parser;
		}
		return decMappingOprLHSOprRHS_5039Parser;
	}

	/**
	* @generated
	*/
	private IParser mapdecPresence_5037Parser;

	/**
	* @generated
	*/
	private IParser getMapdecPresence_5037Parser() {
		if (mapdecPresence_5037Parser == null) {
			EAttribute editableFeature = SplPackage.eINSTANCE.getMapdec_Presence();
			EnumParser parser = new EnumParser(editableFeature);
			mapdecPresence_5037Parser = parser;
		}
		return mapdecPresence_5037Parser;
	}

	/**
	* @generated
	*/
	private IParser mapToEntityPresence_5038Parser;

	/**
	* @generated
	*/
	private IParser getMapToEntityPresence_5038Parser() {
		if (mapToEntityPresence_5038Parser == null) {
			EAttribute editableFeature = SplPackage.eINSTANCE.getMapToEntity_Presence();
			EnumParser parser = new EnumParser(editableFeature);
			mapToEntityPresence_5038Parser = parser;
		}
		return mapToEntityPresence_5038Parser;
	}

	/**
	* @generated
	*/
	private IParser interfcName_5023Parser;

	/**
	* @generated
	*/
	private IParser getInterfcName_5023Parser() {
		if (interfcName_5023Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getInterfc_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			interfcName_5023Parser = parser;
		}
		return interfcName_5023Parser;
	}

	/**
	* @generated
	*/
	private IParser transitionalBehaviorName_5024Parser;

	/**
	* @generated
	*/
	private IParser getTransitionalBehaviorName_5024Parser() {
		if (transitionalBehaviorName_5024Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getTransitionalBehavior_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			transitionalBehaviorName_5024Parser = parser;
		}
		return transitionalBehaviorName_5024Parser;
	}

	/**
	* @generated
	*/
	private IParser guardName_5025Parser;

	/**
	* @generated
	*/
	private IParser getGuardName_5025Parser() {
		if (guardName_5025Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getGuard_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			guardName_5025Parser = parser;
		}
		return guardName_5025Parser;
	}

	/**
	* @generated
	*/
	private IParser labelName_5026Parser;

	/**
	* @generated
	*/
	private IParser getLabelName_5026Parser() {
		if (labelName_5026Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getLabel_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			labelName_5026Parser = parser;
		}
		return labelName_5026Parser;
	}

	/**
	* @generated
	*/
	private IParser entryName_5027Parser;

	/**
	* @generated
	*/
	private IParser getEntryName_5027Parser() {
		if (entryName_5027Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getEntry_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			entryName_5027Parser = parser;
		}
		return entryName_5027Parser;
	}

	/**
	* @generated
	*/
	private IParser triggerName_5028Parser;

	/**
	* @generated
	*/
	private IParser getTriggerName_5028Parser() {
		if (triggerName_5028Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getTrigger_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			triggerName_5028Parser = parser;
		}
		return triggerName_5028Parser;
	}

	/**
	* @generated
	*/
	private IParser classNameInterfc_5009Parser;

	/**
	* @generated
	*/
	private IParser getClassNameInterfc_5009Parser() {
		if (classNameInterfc_5009Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getClass_Name(),
					SplPackage.eINSTANCE.getClass_Interfc() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditPattern("{0}:{1}"); //$NON-NLS-1$
			classNameInterfc_5009Parser = parser;
		}
		return classNameInterfc_5009Parser;
	}

	/**
	* @generated
	*/
	private IParser cdwellformednessSourceTarget_5020Parser;

	/**
	* @generated
	*/
	private IParser getCdwellformednessSourceTarget_5020Parser() {
		if (cdwellformednessSourceTarget_5020Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getcdwellformedness_Source(),
					SplPackage.eINSTANCE.getcdwellformedness_Target() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}=>{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}=>{1}"); //$NON-NLS-1$
			parser.setEditPattern("{0}=>{1}"); //$NON-NLS-1$
			cdwellformednessSourceTarget_5020Parser = parser;
		}
		return cdwellformednessSourceTarget_5020Parser;
	}

	/**
	* @generated
	*/
	private IParser stateNameEntry_5010Parser;

	/**
	* @generated
	*/
	private IParser getStateNameEntry_5010Parser() {
		if (stateNameEntry_5010Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getState_Name(),
					SplPackage.eINSTANCE.getState_Entry() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditPattern("{0}:{1}"); //$NON-NLS-1$
			stateNameEntry_5010Parser = parser;
		}
		return stateNameEntry_5010Parser;
	}

	/**
	* @generated
	*/
	private IParser scwellformednessSourceTarget_5021Parser;

	/**
	* @generated
	*/
	private IParser getScwellformednessSourceTarget_5021Parser() {
		if (scwellformednessSourceTarget_5021Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getscwellformedness_Source(),
					SplPackage.eINSTANCE.getscwellformedness_Target() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}=>{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}=>{1}"); //$NON-NLS-1$
			parser.setEditPattern("{0}=>{1}"); //$NON-NLS-1$
			scwellformednessSourceTarget_5021Parser = parser;
		}
		return scwellformednessSourceTarget_5021Parser;
	}

	/**
	* @generated
	*/
	private IParser transitionNameTriggerGrdTransitionbehavior_6009Parser;

	/**
	* @generated
	*/
	private IParser getTransitionNameTriggerGrdTransitionbehavior_6009Parser() {
		if (transitionNameTriggerGrdTransitionbehavior_6009Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getTransition_Name(),
					SplPackage.eINSTANCE.getTransition_Trigger(), SplPackage.eINSTANCE.getTransition_Grd(),
					SplPackage.eINSTANCE.getTransition_Transitionbehavior() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}-{1}:{2}/{3}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}-{1}:{2}/{3}"); //$NON-NLS-1$
			parser.setEditPattern("{0}-{1}:{2}/{3}"); //$NON-NLS-1$
			transitionNameTriggerGrdTransitionbehavior_6009Parser = parser;
		}
		return transitionNameTriggerGrdTransitionbehavior_6009Parser;
	}

	/**
	* @generated
	*/
	private IParser relationshipNameLabInMulOutMul_6010Parser;

	/**
	* @generated
	*/
	private IParser getRelationshipNameLabInMulOutMul_6010Parser() {
		if (relationshipNameLabInMulOutMul_6010Parser == null) {
			EAttribute[] features = new EAttribute[] { SplPackage.eINSTANCE.getRelationship_Name(),
					SplPackage.eINSTANCE.getRelationship_Lab(), SplPackage.eINSTANCE.getRelationship_InMul(),
					SplPackage.eINSTANCE.getRelationship_OutMul() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}:{1}:{2}:{3}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}:{1}:{2}:{3}"); //$NON-NLS-1$
			parser.setEditPattern("{0}:{1}:{2}:{3}"); //$NON-NLS-1$
			relationshipNameLabInMulOutMul_6010Parser = parser;
		}
		return relationshipNameLabInMulOutMul_6010Parser;
	}

	/**
	* @generated
	*/
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case PropertyNameTextEditPart.VISUAL_ID:
			return getPropertyNameText_5022Parser();
		case StateChartNameEditPart.VISUAL_ID:
			return getStateChartName_5031Parser();
		case ChoiceNameVarTypeEditPart.VISUAL_ID:
			return getChoiceNameVarType_5001Parser();

		case CMconstraintsOprEditPart.VISUAL_ID:
			return getCMconstraintsOpr_5032Parser();
		case FeatureNameVartypeEditPart.VISUAL_ID:
			return getFeatureNameVartype_5006Parser();

		case FMconstraintsOprEditPart.VISUAL_ID:
			return getFMconstraintsOpr_5019Parser();
		case FeatMappingOprLHSOprRHSEditPart.VISUAL_ID:
			return getFeatMappingOprLHSOprRHS_5036Parser();

		case MapPresenceEditPart.VISUAL_ID:
			return getMapPresence_5034Parser();

		case MaptoPresenceEditPart.VISUAL_ID:
			return getMaptoPresence_5035Parser();
		case DecMappingOprLHSOprRHSEditPart.VISUAL_ID:
			return getDecMappingOprLHSOprRHS_5039Parser();

		case MapdecPresenceEditPart.VISUAL_ID:
			return getMapdecPresence_5037Parser();

		case MapToEntityPresenceEditPart.VISUAL_ID:
			return getMapToEntityPresence_5038Parser();
		case InterfcNameEditPart.VISUAL_ID:
			return getInterfcName_5023Parser();
		case TransitionalBehaviorNameEditPart.VISUAL_ID:
			return getTransitionalBehaviorName_5024Parser();
		case GuardNameEditPart.VISUAL_ID:
			return getGuardName_5025Parser();
		case LabelNameEditPart.VISUAL_ID:
			return getLabelName_5026Parser();
		case EntryNameEditPart.VISUAL_ID:
			return getEntryName_5027Parser();
		case TriggerNameEditPart.VISUAL_ID:
			return getTriggerName_5028Parser();
		case ClassNameInterfcEditPart.VISUAL_ID:
			return getClassNameInterfc_5009Parser();
		case CdwellformednessSourceTargetEditPart.VISUAL_ID:
			return getCdwellformednessSourceTarget_5020Parser();
		case StateNameEntryEditPart.VISUAL_ID:
			return getStateNameEntry_5010Parser();
		case ScwellformednessSourceTargetEditPart.VISUAL_ID:
			return getScwellformednessSourceTarget_5021Parser();
		case TransitionNameTriggerGrdTransitioEditPart.VISUAL_ID:
			return getTransitionNameTriggerGrdTransitionbehavior_6009Parser();
		case RelationshipNameLabInMulOutMulEditPart.VISUAL_ID:
			return getRelationshipNameLabInMulOutMul_6010Parser();
		}
		return null;
	}

	/**
	* Utility method that consults ParserService
	* @generated
	*/
	public static IParser getParser(IElementType type, EObject object, String parserHint) {
		return ParserService.getInstance().getParser(new HintAdapter(type, object, parserHint));
	}

	/**
	* @generated
	*/
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(SplVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(SplVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	* @generated
	*/
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (SplElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	* @generated
	*/
	private static class HintAdapter extends ParserHintAdapter {

		/**
		* @generated
		*/
		private final IElementType elementType;

		/**
		* @generated
		*/
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		* @generated
		*/
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
