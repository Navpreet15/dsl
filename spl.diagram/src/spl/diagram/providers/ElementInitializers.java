/*
 * 
 */
package spl.diagram.providers;

import spl.diagram.part.SplDiagramEditorPlugin;

/**
 * @generated
 */
public class ElementInitializers {

	protected ElementInitializers() {
		// use #getInstance to access cached instance
	}

	/**
	* @generated
	*/
	public static ElementInitializers getInstance() {
		ElementInitializers cached = SplDiagramEditorPlugin.getInstance().getElementInitializers();
		if (cached == null) {
			SplDiagramEditorPlugin.getInstance().setElementInitializers(cached = new ElementInitializers());
		}
		return cached;
	}
}
