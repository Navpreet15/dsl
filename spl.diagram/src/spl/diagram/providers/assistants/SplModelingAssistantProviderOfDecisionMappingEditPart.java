/*
 * 
 */
package spl.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import spl.diagram.providers.SplElementTypes;
import spl.diagram.providers.SplModelingAssistantProvider;

/**
 * @generated
 */
public class SplModelingAssistantProviderOfDecisionMappingEditPart extends SplModelingAssistantProvider {

	/**
	* @generated
	*/
	@Override

	public List<IElementType> getTypesForPopupBar(IAdaptable host) {
		List<IElementType> types = new ArrayList<IElementType>(1);
		types.add(SplElementTypes.DecMapping_3022);
		return types;
	}

}
