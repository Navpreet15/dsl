/*
 * 
 */
package spl.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import spl.diagram.providers.SplElementTypes;
import spl.diagram.providers.SplModelingAssistantProvider;

/**
 * @generated
 */
public class SplModelingAssistantProviderOfDesignChoicesEditPart extends SplModelingAssistantProvider {

	/**
	* @generated
	*/
	@Override

	public List<IElementType> getTypesForPopupBar(IAdaptable host) {
		List<IElementType> types = new ArrayList<IElementType>(8);
		types.add(SplElementTypes.ChoiceModel_2001);
		types.add(SplElementTypes.FeatureModel_2011);
		types.add(SplElementTypes.FeatureMapping_2005);
		types.add(SplElementTypes.DecisionMapping_2006);
		types.add(SplElementTypes.Property_2008);
		types.add(SplElementTypes.Variabls_2009);
		types.add(SplElementTypes.ClassDiagram_2013);
		types.add(SplElementTypes.StateChart_2014);
		return types;
	}

}
