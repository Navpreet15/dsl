/*
 * 
 */
package spl.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import spl.diagram.providers.SplElementTypes;
import spl.diagram.providers.SplModelingAssistantProvider;

/**
 * @generated
 */
public class SplModelingAssistantProviderOfVariablsEditPart extends SplModelingAssistantProvider {

	/**
	* @generated
	*/
	@Override

	public List<IElementType> getTypesForPopupBar(IAdaptable host) {
		List<IElementType> types = new ArrayList<IElementType>(6);
		types.add(SplElementTypes.Interfc_3012);
		types.add(SplElementTypes.TransitionalBehavior_3013);
		types.add(SplElementTypes.Guard_3014);
		types.add(SplElementTypes.Label_3015);
		types.add(SplElementTypes.Entry_3016);
		types.add(SplElementTypes.Trigger_3017);
		return types;
	}

}
