/*
 * 
 */
package spl.diagram.providers;

import org.eclipse.gmf.tooling.runtime.providers.DefaultEditPartProvider;

import spl.diagram.edit.parts.DesignChoicesEditPart;
import spl.diagram.edit.parts.SplEditPartFactory;
import spl.diagram.part.SplVisualIDRegistry;

/**
 * @generated
 */
public class SplEditPartProvider extends DefaultEditPartProvider {

	/**
	* @generated
	*/
	public SplEditPartProvider() {
		super(new SplEditPartFactory(), SplVisualIDRegistry.TYPED_INSTANCE, DesignChoicesEditPart.MODEL_ID);
	}

}
