/*
 * 
 */
package spl.diagram.providers;

import org.eclipse.gmf.runtime.common.ui.services.icon.IIconProvider;
import org.eclipse.gmf.tooling.runtime.providers.DefaultElementTypeIconProvider;

/**
 * @generated
 */
public class SplIconProvider extends DefaultElementTypeIconProvider implements IIconProvider {

	/**
	* @generated
	*/
	public SplIconProvider() {
		super(SplElementTypes.TYPED_INSTANCE);
	}

}
