/*
 * 
 */
package spl.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import spl.SplPackage;
import spl.diagram.edit.parts.CMconstraintsEditPart;
import spl.diagram.edit.parts.CdwellformednessEditPart;
import spl.diagram.edit.parts.ChoiceEditPart;
import spl.diagram.edit.parts.ChoiceModelEditPart;
import spl.diagram.edit.parts.ClassDiagramEditPart;
import spl.diagram.edit.parts.ClassEditPart;
import spl.diagram.edit.parts.DecMappingEditPart;
import spl.diagram.edit.parts.DecisionMappingEditPart;
import spl.diagram.edit.parts.DesignChoicesEditPart;
import spl.diagram.edit.parts.EntryEditPart;
import spl.diagram.edit.parts.FMconstraintsEditPart;
import spl.diagram.edit.parts.FeatMappingEditPart;
import spl.diagram.edit.parts.FeatureEditPart;
import spl.diagram.edit.parts.FeatureMappingEditPart;
import spl.diagram.edit.parts.FeatureModelEditPart;
import spl.diagram.edit.parts.GuardEditPart;
import spl.diagram.edit.parts.InterfcEditPart;
import spl.diagram.edit.parts.LabelEditPart;
import spl.diagram.edit.parts.MapEditPart;
import spl.diagram.edit.parts.MapToEntityEditPart;
import spl.diagram.edit.parts.MapdecEditPart;
import spl.diagram.edit.parts.MaptoEditPart;
import spl.diagram.edit.parts.PropertyEditPart;
import spl.diagram.edit.parts.RelationshipEditPart;
import spl.diagram.edit.parts.ScwellformednessEditPart;
import spl.diagram.edit.parts.StateChartEditPart;
import spl.diagram.edit.parts.StateEditPart;
import spl.diagram.edit.parts.TransitionEditPart;
import spl.diagram.edit.parts.TransitionalBehaviorEditPart;
import spl.diagram.edit.parts.TriggerEditPart;
import spl.diagram.edit.parts.VariablsEditPart;
import spl.diagram.part.SplDiagramEditorPlugin;

/**
 * @generated
 */
public class SplElementTypes {

	/**
	* @generated
	*/
	private SplElementTypes() {
	}

	/**
	* @generated
	*/
	private static Map<IElementType, ENamedElement> elements;

	/**
	* @generated
	*/
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(
			SplDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory());

	/**
	* @generated
	*/
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	* @generated
	*/
	public static final IElementType DesignChoices_1000 = getElementType("spl.diagram.DesignChoices_1000"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType ChoiceModel_2001 = getElementType("spl.diagram.ChoiceModel_2001"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType FeatureModel_2011 = getElementType("spl.diagram.FeatureModel_2011"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType FeatureMapping_2005 = getElementType("spl.diagram.FeatureMapping_2005"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType DecisionMapping_2006 = getElementType("spl.diagram.DecisionMapping_2006"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Property_2008 = getElementType("spl.diagram.Property_2008"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Variabls_2009 = getElementType("spl.diagram.Variabls_2009"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType ClassDiagram_2013 = getElementType("spl.diagram.ClassDiagram_2013"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType StateChart_2014 = getElementType("spl.diagram.StateChart_2014"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Choice_3001 = getElementType("spl.diagram.Choice_3001"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType CMconstraints_3018 = getElementType("spl.diagram.CMconstraints_3018"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Feature_3004 = getElementType("spl.diagram.Feature_3004"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType FMconstraints_3009 = getElementType("spl.diagram.FMconstraints_3009"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType FeatMapping_3021 = getElementType("spl.diagram.FeatMapping_3021"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Map_3019 = getElementType("spl.diagram.Map_3019"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Mapto_3020 = getElementType("spl.diagram.Mapto_3020"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType DecMapping_3022 = getElementType("spl.diagram.DecMapping_3022"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Mapdec_3023 = getElementType("spl.diagram.Mapdec_3023"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType MapToEntity_3024 = getElementType("spl.diagram.MapToEntity_3024"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Interfc_3012 = getElementType("spl.diagram.Interfc_3012"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType TransitionalBehavior_3013 = getElementType(
			"spl.diagram.TransitionalBehavior_3013"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Guard_3014 = getElementType("spl.diagram.Guard_3014"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Label_3015 = getElementType("spl.diagram.Label_3015"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Entry_3016 = getElementType("spl.diagram.Entry_3016"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Trigger_3017 = getElementType("spl.diagram.Trigger_3017"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Class_3007 = getElementType("spl.diagram.Class_3007"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Cdwellformedness_3010 = getElementType("spl.diagram.Cdwellformedness_3010"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType State_3008 = getElementType("spl.diagram.State_3008"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Scwellformedness_3011 = getElementType("spl.diagram.Scwellformedness_3011"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Transition_4001 = getElementType("spl.diagram.Transition_4001"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Relationship_4005 = getElementType("spl.diagram.Relationship_4005"); //$NON-NLS-1$

	/**
	* @generated
	*/
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	* @generated
	*/
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	* @generated
	*/
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	* @generated
	*/
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	* Returns 'type' of the ecore object associated with the hint.
	* 
	* @generated
	*/
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(DesignChoices_1000, SplPackage.eINSTANCE.getDesignChoices());

			elements.put(ChoiceModel_2001, SplPackage.eINSTANCE.getChoiceModel());

			elements.put(FeatureModel_2011, SplPackage.eINSTANCE.getFeatureModel());

			elements.put(FeatureMapping_2005, SplPackage.eINSTANCE.getFeatureMapping());

			elements.put(DecisionMapping_2006, SplPackage.eINSTANCE.getDecisionMapping());

			elements.put(Property_2008, SplPackage.eINSTANCE.getProperty());

			elements.put(Variabls_2009, SplPackage.eINSTANCE.getvariabls());

			elements.put(ClassDiagram_2013, SplPackage.eINSTANCE.getClassDiagram());

			elements.put(StateChart_2014, SplPackage.eINSTANCE.getStateChart());

			elements.put(Choice_3001, SplPackage.eINSTANCE.getChoice());

			elements.put(CMconstraints_3018, SplPackage.eINSTANCE.getCMconstraints());

			elements.put(Feature_3004, SplPackage.eINSTANCE.getFeature());

			elements.put(FMconstraints_3009, SplPackage.eINSTANCE.getFMconstraints());

			elements.put(FeatMapping_3021, SplPackage.eINSTANCE.getfeatMapping());

			elements.put(Map_3019, SplPackage.eINSTANCE.getMap());

			elements.put(Mapto_3020, SplPackage.eINSTANCE.getMapto());

			elements.put(DecMapping_3022, SplPackage.eINSTANCE.getDecMapping());

			elements.put(Mapdec_3023, SplPackage.eINSTANCE.getMapdec());

			elements.put(MapToEntity_3024, SplPackage.eINSTANCE.getMapToEntity());

			elements.put(Interfc_3012, SplPackage.eINSTANCE.getInterfc());

			elements.put(TransitionalBehavior_3013, SplPackage.eINSTANCE.getTransitionalBehavior());

			elements.put(Guard_3014, SplPackage.eINSTANCE.getGuard());

			elements.put(Label_3015, SplPackage.eINSTANCE.getLabel());

			elements.put(Entry_3016, SplPackage.eINSTANCE.getEntry());

			elements.put(Trigger_3017, SplPackage.eINSTANCE.getTrigger());

			elements.put(Class_3007, SplPackage.eINSTANCE.getClass_());

			elements.put(Cdwellformedness_3010, SplPackage.eINSTANCE.getcdwellformedness());

			elements.put(State_3008, SplPackage.eINSTANCE.getState());

			elements.put(Scwellformedness_3011, SplPackage.eINSTANCE.getscwellformedness());

			elements.put(Transition_4001, SplPackage.eINSTANCE.getTransition());

			elements.put(Relationship_4005, SplPackage.eINSTANCE.getRelationship());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	* @generated
	*/
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	* @generated
	*/
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(DesignChoices_1000);
			KNOWN_ELEMENT_TYPES.add(ChoiceModel_2001);
			KNOWN_ELEMENT_TYPES.add(FeatureModel_2011);
			KNOWN_ELEMENT_TYPES.add(FeatureMapping_2005);
			KNOWN_ELEMENT_TYPES.add(DecisionMapping_2006);
			KNOWN_ELEMENT_TYPES.add(Property_2008);
			KNOWN_ELEMENT_TYPES.add(Variabls_2009);
			KNOWN_ELEMENT_TYPES.add(ClassDiagram_2013);
			KNOWN_ELEMENT_TYPES.add(StateChart_2014);
			KNOWN_ELEMENT_TYPES.add(Choice_3001);
			KNOWN_ELEMENT_TYPES.add(CMconstraints_3018);
			KNOWN_ELEMENT_TYPES.add(Feature_3004);
			KNOWN_ELEMENT_TYPES.add(FMconstraints_3009);
			KNOWN_ELEMENT_TYPES.add(FeatMapping_3021);
			KNOWN_ELEMENT_TYPES.add(Map_3019);
			KNOWN_ELEMENT_TYPES.add(Mapto_3020);
			KNOWN_ELEMENT_TYPES.add(DecMapping_3022);
			KNOWN_ELEMENT_TYPES.add(Mapdec_3023);
			KNOWN_ELEMENT_TYPES.add(MapToEntity_3024);
			KNOWN_ELEMENT_TYPES.add(Interfc_3012);
			KNOWN_ELEMENT_TYPES.add(TransitionalBehavior_3013);
			KNOWN_ELEMENT_TYPES.add(Guard_3014);
			KNOWN_ELEMENT_TYPES.add(Label_3015);
			KNOWN_ELEMENT_TYPES.add(Entry_3016);
			KNOWN_ELEMENT_TYPES.add(Trigger_3017);
			KNOWN_ELEMENT_TYPES.add(Class_3007);
			KNOWN_ELEMENT_TYPES.add(Cdwellformedness_3010);
			KNOWN_ELEMENT_TYPES.add(State_3008);
			KNOWN_ELEMENT_TYPES.add(Scwellformedness_3011);
			KNOWN_ELEMENT_TYPES.add(Transition_4001);
			KNOWN_ELEMENT_TYPES.add(Relationship_4005);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	* @generated
	*/
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case DesignChoicesEditPart.VISUAL_ID:
			return DesignChoices_1000;
		case ChoiceModelEditPart.VISUAL_ID:
			return ChoiceModel_2001;
		case FeatureModelEditPart.VISUAL_ID:
			return FeatureModel_2011;
		case FeatureMappingEditPart.VISUAL_ID:
			return FeatureMapping_2005;
		case DecisionMappingEditPart.VISUAL_ID:
			return DecisionMapping_2006;
		case PropertyEditPart.VISUAL_ID:
			return Property_2008;
		case VariablsEditPart.VISUAL_ID:
			return Variabls_2009;
		case ClassDiagramEditPart.VISUAL_ID:
			return ClassDiagram_2013;
		case StateChartEditPart.VISUAL_ID:
			return StateChart_2014;
		case ChoiceEditPart.VISUAL_ID:
			return Choice_3001;
		case CMconstraintsEditPart.VISUAL_ID:
			return CMconstraints_3018;
		case FeatureEditPart.VISUAL_ID:
			return Feature_3004;
		case FMconstraintsEditPart.VISUAL_ID:
			return FMconstraints_3009;
		case FeatMappingEditPart.VISUAL_ID:
			return FeatMapping_3021;
		case MapEditPart.VISUAL_ID:
			return Map_3019;
		case MaptoEditPart.VISUAL_ID:
			return Mapto_3020;
		case DecMappingEditPart.VISUAL_ID:
			return DecMapping_3022;
		case MapdecEditPart.VISUAL_ID:
			return Mapdec_3023;
		case MapToEntityEditPart.VISUAL_ID:
			return MapToEntity_3024;
		case InterfcEditPart.VISUAL_ID:
			return Interfc_3012;
		case TransitionalBehaviorEditPart.VISUAL_ID:
			return TransitionalBehavior_3013;
		case GuardEditPart.VISUAL_ID:
			return Guard_3014;
		case LabelEditPart.VISUAL_ID:
			return Label_3015;
		case EntryEditPart.VISUAL_ID:
			return Entry_3016;
		case TriggerEditPart.VISUAL_ID:
			return Trigger_3017;
		case ClassEditPart.VISUAL_ID:
			return Class_3007;
		case CdwellformednessEditPart.VISUAL_ID:
			return Cdwellformedness_3010;
		case StateEditPart.VISUAL_ID:
			return State_3008;
		case ScwellformednessEditPart.VISUAL_ID:
			return Scwellformedness_3011;
		case TransitionEditPart.VISUAL_ID:
			return Transition_4001;
		case RelationshipEditPart.VISUAL_ID:
			return Relationship_4005;
		}
		return null;
	}

	/**
	* @generated
	*/
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(elementTypeImages) {

		/**
		* @generated
		*/
		@Override

		public boolean isKnownElementType(IElementType elementType) {
			return spl.diagram.providers.SplElementTypes.isKnownElementType(elementType);
		}

		/**
		* @generated
		*/
		@Override

		public IElementType getElementTypeForVisualId(int visualID) {
			return spl.diagram.providers.SplElementTypes.getElementType(visualID);
		}

		/**
		* @generated
		*/
		@Override

		public ENamedElement getDefiningNamedElement(IAdaptable elementTypeAdapter) {
			return spl.diagram.providers.SplElementTypes.getElement(elementTypeAdapter);
		}
	};

}
