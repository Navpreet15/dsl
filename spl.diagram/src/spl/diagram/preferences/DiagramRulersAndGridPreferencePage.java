/*
 * 
 */
package spl.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;

import spl.diagram.part.SplDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage {

	/**
	* @generated
	*/
	public DiagramRulersAndGridPreferencePage() {
		setPreferenceStore(SplDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
