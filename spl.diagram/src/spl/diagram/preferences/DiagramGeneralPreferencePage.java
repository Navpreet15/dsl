/*
 * 
 */
package spl.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.DiagramsPreferencePage;

import spl.diagram.part.SplDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramGeneralPreferencePage extends DiagramsPreferencePage {

	/**
	* @generated
	*/
	public DiagramGeneralPreferencePage() {
		setPreferenceStore(SplDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
