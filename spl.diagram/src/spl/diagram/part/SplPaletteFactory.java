
/*
 * 
 */
package spl.diagram.part;

import java.util.Collections;

import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.tooling.runtime.part.DefaultLinkToolEntry;
import org.eclipse.gmf.tooling.runtime.part.DefaultNodeToolEntry;

import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class SplPaletteFactory {

	/**
	* @generated
	*/
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createObjects1Group());
		paletteRoot.add(createConnections2Group());
	}

	/**
	* Creates "Objects" palette tool group
	* @generated
	*/
	private PaletteContainer createObjects1Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(Messages.Objects1Group_title);
		paletteContainer.setId("createObjects1Group"); //$NON-NLS-1$
		paletteContainer.add(createCMconstraints1CreationTool());
		paletteContainer.add(createChoice2CreationTool());
		paletteContainer.add(createChoiceModel3CreationTool());
		paletteContainer.add(createClass4CreationTool());
		paletteContainer.add(createClassDiagram5CreationTool());
		paletteContainer.add(createDecMapping6CreationTool());
		paletteContainer.add(createDecisionMapping7CreationTool());
		paletteContainer.add(createEntry8CreationTool());
		paletteContainer.add(createFMconstraints9CreationTool());
		paletteContainer.add(createFeature10CreationTool());
		paletteContainer.add(createFeatureMapping11CreationTool());
		paletteContainer.add(createFeatureModel12CreationTool());
		paletteContainer.add(createGuard13CreationTool());
		paletteContainer.add(createInterfc14CreationTool());
		paletteContainer.add(createLabel15CreationTool());
		paletteContainer.add(createMap16CreationTool());
		paletteContainer.add(createMapToEntity17CreationTool());
		paletteContainer.add(createMapdec18CreationTool());
		paletteContainer.add(createMapto19CreationTool());
		paletteContainer.add(createProperty20CreationTool());
		paletteContainer.add(createState21CreationTool());
		paletteContainer.add(createStateChart22CreationTool());
		paletteContainer.add(createTransitionalBehavior23CreationTool());
		paletteContainer.add(createTrigger24CreationTool());
		paletteContainer.add(createCdwellformedness25CreationTool());
		paletteContainer.add(createFeatMapping26CreationTool());
		paletteContainer.add(createScwellformedness27CreationTool());
		paletteContainer.add(createVariabls28CreationTool());
		return paletteContainer;
	}

	/**
	* Creates "Connections" palette tool group
	* @generated
	*/
	private PaletteContainer createConnections2Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(Messages.Connections2Group_title);
		paletteContainer.setId("createConnections2Group"); //$NON-NLS-1$
		paletteContainer.add(createRelationship1CreationTool());
		paletteContainer.add(createTransition2CreationTool());
		return paletteContainer;
	}

	/**
	* @generated
	*/
	private ToolEntry createCMconstraints1CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.CMconstraints1CreationTool_title,
				Messages.CMconstraints1CreationTool_desc,
				Collections.singletonList(SplElementTypes.CMconstraints_3018));
		entry.setId("createCMconstraints1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.CMconstraints_3018));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createChoice2CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Choice2CreationTool_title,
				Messages.Choice2CreationTool_desc, Collections.singletonList(SplElementTypes.Choice_3001));
		entry.setId("createChoice2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Choice_3001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createChoiceModel3CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.ChoiceModel3CreationTool_title,
				Messages.ChoiceModel3CreationTool_desc, Collections.singletonList(SplElementTypes.ChoiceModel_2001));
		entry.setId("createChoiceModel3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.ChoiceModel_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createClass4CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Class4CreationTool_title,
				Messages.Class4CreationTool_desc, Collections.singletonList(SplElementTypes.Class_3007));
		entry.setId("createClass4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Class_3007));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createClassDiagram5CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.ClassDiagram5CreationTool_title,
				Messages.ClassDiagram5CreationTool_desc, Collections.singletonList(SplElementTypes.ClassDiagram_2013));
		entry.setId("createClassDiagram5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.ClassDiagram_2013));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createDecMapping6CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.DecMapping6CreationTool_title,
				Messages.DecMapping6CreationTool_desc, Collections.singletonList(SplElementTypes.DecMapping_3022));
		entry.setId("createDecMapping6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.DecMapping_3022));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createDecisionMapping7CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.DecisionMapping7CreationTool_title,
				Messages.DecisionMapping7CreationTool_desc,
				Collections.singletonList(SplElementTypes.DecisionMapping_2006));
		entry.setId("createDecisionMapping7CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.DecisionMapping_2006));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createEntry8CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Entry8CreationTool_title,
				Messages.Entry8CreationTool_desc, Collections.singletonList(SplElementTypes.Entry_3016));
		entry.setId("createEntry8CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Entry_3016));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createFMconstraints9CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.FMconstraints9CreationTool_title,
				Messages.FMconstraints9CreationTool_desc,
				Collections.singletonList(SplElementTypes.FMconstraints_3009));
		entry.setId("createFMconstraints9CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.FMconstraints_3009));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createFeature10CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Feature10CreationTool_title,
				Messages.Feature10CreationTool_desc, Collections.singletonList(SplElementTypes.Feature_3004));
		entry.setId("createFeature10CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Feature_3004));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createFeatureMapping11CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.FeatureMapping11CreationTool_title,
				Messages.FeatureMapping11CreationTool_desc,
				Collections.singletonList(SplElementTypes.FeatureMapping_2005));
		entry.setId("createFeatureMapping11CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.FeatureMapping_2005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createFeatureModel12CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.FeatureModel12CreationTool_title,
				Messages.FeatureModel12CreationTool_desc, Collections.singletonList(SplElementTypes.FeatureModel_2011));
		entry.setId("createFeatureModel12CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.FeatureModel_2011));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createGuard13CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Guard13CreationTool_title,
				Messages.Guard13CreationTool_desc, Collections.singletonList(SplElementTypes.Guard_3014));
		entry.setId("createGuard13CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Guard_3014));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createInterfc14CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Interfc14CreationTool_title,
				Messages.Interfc14CreationTool_desc, Collections.singletonList(SplElementTypes.Interfc_3012));
		entry.setId("createInterfc14CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Interfc_3012));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createLabel15CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Label15CreationTool_title,
				Messages.Label15CreationTool_desc, Collections.singletonList(SplElementTypes.Label_3015));
		entry.setId("createLabel15CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Label_3015));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createMap16CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Map16CreationTool_title,
				Messages.Map16CreationTool_desc, Collections.singletonList(SplElementTypes.Map_3019));
		entry.setId("createMap16CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Map_3019));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createMapToEntity17CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.MapToEntity17CreationTool_title,
				Messages.MapToEntity17CreationTool_desc, Collections.singletonList(SplElementTypes.MapToEntity_3024));
		entry.setId("createMapToEntity17CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.MapToEntity_3024));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createMapdec18CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Mapdec18CreationTool_title,
				Messages.Mapdec18CreationTool_desc, Collections.singletonList(SplElementTypes.Mapdec_3023));
		entry.setId("createMapdec18CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Mapdec_3023));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createMapto19CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Mapto19CreationTool_title,
				Messages.Mapto19CreationTool_desc, Collections.singletonList(SplElementTypes.Mapto_3020));
		entry.setId("createMapto19CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Mapto_3020));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createProperty20CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Property20CreationTool_title,
				Messages.Property20CreationTool_desc, Collections.singletonList(SplElementTypes.Property_2008));
		entry.setId("createProperty20CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Property_2008));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createState21CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.State21CreationTool_title,
				Messages.State21CreationTool_desc, Collections.singletonList(SplElementTypes.State_3008));
		entry.setId("createState21CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.State_3008));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createStateChart22CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.StateChart22CreationTool_title,
				Messages.StateChart22CreationTool_desc, Collections.singletonList(SplElementTypes.StateChart_2014));
		entry.setId("createStateChart22CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.StateChart_2014));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createTransitionalBehavior23CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.TransitionalBehavior23CreationTool_title,
				Messages.TransitionalBehavior23CreationTool_desc,
				Collections.singletonList(SplElementTypes.TransitionalBehavior_3013));
		entry.setId("createTransitionalBehavior23CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.TransitionalBehavior_3013));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createTrigger24CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Trigger24CreationTool_title,
				Messages.Trigger24CreationTool_desc, Collections.singletonList(SplElementTypes.Trigger_3017));
		entry.setId("createTrigger24CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Trigger_3017));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createCdwellformedness25CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Cdwellformedness25CreationTool_title,
				Messages.Cdwellformedness25CreationTool_desc,
				Collections.singletonList(SplElementTypes.Cdwellformedness_3010));
		entry.setId("createCdwellformedness25CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Cdwellformedness_3010));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createFeatMapping26CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.FeatMapping26CreationTool_title,
				Messages.FeatMapping26CreationTool_desc, Collections.singletonList(SplElementTypes.FeatMapping_3021));
		entry.setId("createFeatMapping26CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.FeatMapping_3021));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createScwellformedness27CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Scwellformedness27CreationTool_title,
				Messages.Scwellformedness27CreationTool_desc,
				Collections.singletonList(SplElementTypes.Scwellformedness_3011));
		entry.setId("createScwellformedness27CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Scwellformedness_3011));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createVariabls28CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Variabls28CreationTool_title,
				Messages.Variabls28CreationTool_desc, Collections.singletonList(SplElementTypes.Variabls_2009));
		entry.setId("createVariabls28CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Variabls_2009));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createRelationship1CreationTool() {
		DefaultLinkToolEntry entry = new DefaultLinkToolEntry(Messages.Relationship1CreationTool_title,
				Messages.Relationship1CreationTool_desc, Collections.singletonList(SplElementTypes.Relationship_4005));
		entry.setId("createRelationship1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Relationship_4005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createTransition2CreationTool() {
		DefaultLinkToolEntry entry = new DefaultLinkToolEntry(Messages.Transition2CreationTool_title,
				Messages.Transition2CreationTool_desc, Collections.singletonList(SplElementTypes.Transition_4001));
		entry.setId("createTransition2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(SplElementTypes.getImageDescriptor(SplElementTypes.Transition_4001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

}
