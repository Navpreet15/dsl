/*
* 
*/
package spl.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;

import spl.CMconstraints;
import spl.Choice;
import spl.ChoiceModel;
import spl.Class;
import spl.ClassDiagram;
import spl.DecMapping;
import spl.DecisionMapping;
import spl.DesignChoices;
import spl.Entry;
import spl.FMconstraints;
import spl.Feature;
import spl.FeatureMapping;
import spl.FeatureModel;
import spl.Guard;
import spl.Interfc;
import spl.Label;
import spl.Map;
import spl.MapToEntity;
import spl.Mapdec;
import spl.Mapto;
import spl.Property;
import spl.Relationship;
import spl.SplPackage;
import spl.State;
import spl.StateChart;
import spl.Transition;
import spl.TransitionalBehavior;
import spl.Trigger;
import spl.diagram.edit.parts.CMconstraintsEditPart;
import spl.diagram.edit.parts.CdwellformednessEditPart;
import spl.diagram.edit.parts.ChoiceEditPart;
import spl.diagram.edit.parts.ChoiceModelChoiceModelChCompartmentEditPart;
import spl.diagram.edit.parts.ChoiceModelChoiceModelConstraintCompartmentEditPart;
import spl.diagram.edit.parts.ChoiceModelEditPart;
import spl.diagram.edit.parts.ClassDiagramClassDiagramClassCompartmentEditPart;
import spl.diagram.edit.parts.ClassDiagramClassDiagramWfCompartmentEditPart;
import spl.diagram.edit.parts.ClassDiagramEditPart;
import spl.diagram.edit.parts.ClassEditPart;
import spl.diagram.edit.parts.DecMappingDecMappingMapfromCompartmentEditPart;
import spl.diagram.edit.parts.DecMappingDecMappingMaptoEntityCompartmentEditPart;
import spl.diagram.edit.parts.DecMappingEditPart;
import spl.diagram.edit.parts.DecisionMappingDecisionMappingDecisionMapCompartmentEditPart;
import spl.diagram.edit.parts.DecisionMappingEditPart;
import spl.diagram.edit.parts.DesignChoicesEditPart;
import spl.diagram.edit.parts.EntryEditPart;
import spl.diagram.edit.parts.FMconstraintsEditPart;
import spl.diagram.edit.parts.FeatMappingEditPart;
import spl.diagram.edit.parts.FeatMappingFeatMappingMapfromCompartmentEditPart;
import spl.diagram.edit.parts.FeatMappingFeatMappingMaptoCompartmentEditPart;
import spl.diagram.edit.parts.FeatureEditPart;
import spl.diagram.edit.parts.FeatureMappingEditPart;
import spl.diagram.edit.parts.FeatureMappingFeatureMappingFmapCompartmentEditPart;
import spl.diagram.edit.parts.FeatureModelEditPart;
import spl.diagram.edit.parts.FeatureModelFeatureModelConstraintCompartmentEditPart;
import spl.diagram.edit.parts.FeatureModelFeatureModelFeatureCompartmentEditPart;
import spl.diagram.edit.parts.GuardEditPart;
import spl.diagram.edit.parts.InterfcEditPart;
import spl.diagram.edit.parts.LabelEditPart;
import spl.diagram.edit.parts.MapEditPart;
import spl.diagram.edit.parts.MapToEntityEditPart;
import spl.diagram.edit.parts.MapdecEditPart;
import spl.diagram.edit.parts.MaptoEditPart;
import spl.diagram.edit.parts.PropertyEditPart;
import spl.diagram.edit.parts.RelationshipEditPart;
import spl.diagram.edit.parts.ScwellformednessEditPart;
import spl.diagram.edit.parts.StateChartEditPart;
import spl.diagram.edit.parts.StateChartStateChartStateCompartmentEditPart;
import spl.diagram.edit.parts.StateChartStateChartWfCompartmentEditPart;
import spl.diagram.edit.parts.StateEditPart;
import spl.diagram.edit.parts.TransitionEditPart;
import spl.diagram.edit.parts.TransitionalBehaviorEditPart;
import spl.diagram.edit.parts.TriggerEditPart;
import spl.diagram.edit.parts.VariablsEditPart;
import spl.diagram.edit.parts.VariablsVariablsEntryCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsGrdCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsInterCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsLabelCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsTransCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsTriggerCompartmentEditPart;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class SplDiagramUpdater {

	/**
	* @generated
	*/
	public static boolean isShortcutOrphaned(View view) {
		return !view.isSetElement() || view.getElement() == null || view.getElement().eIsProxy();
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getSemanticChildren(View view) {
		switch (SplVisualIDRegistry.getVisualID(view)) {
		case DesignChoicesEditPart.VISUAL_ID:
			return getDesignChoices_1000SemanticChildren(view);
		case ChoiceModelChoiceModelChCompartmentEditPart.VISUAL_ID:
			return getChoiceModelChoiceModelChCompartment_7001SemanticChildren(view);
		case ChoiceModelChoiceModelConstraintCompartmentEditPart.VISUAL_ID:
			return getChoiceModelChoiceModelConstraintCompartment_7026SemanticChildren(view);
		case FeatureModelFeatureModelFeatureCompartmentEditPart.VISUAL_ID:
			return getFeatureModelFeatureModelFeatureCompartment_7018SemanticChildren(view);
		case FeatureModelFeatureModelConstraintCompartmentEditPart.VISUAL_ID:
			return getFeatureModelFeatureModelConstraintCompartment_7019SemanticChildren(view);
		case FeatureMappingFeatureMappingFmapCompartmentEditPart.VISUAL_ID:
			return getFeatureMappingFeatureMappingFmapCompartment_7029SemanticChildren(view);
		case FeatMappingFeatMappingMapfromCompartmentEditPart.VISUAL_ID:
			return getFeatMappingFeatMappingMapfromCompartment_7030SemanticChildren(view);
		case FeatMappingFeatMappingMaptoCompartmentEditPart.VISUAL_ID:
			return getFeatMappingFeatMappingMaptoCompartment_7031SemanticChildren(view);
		case DecisionMappingDecisionMappingDecisionMapCompartmentEditPart.VISUAL_ID:
			return getDecisionMappingDecisionMappingDecisionMapCompartment_7032SemanticChildren(view);
		case DecMappingDecMappingMapfromCompartmentEditPart.VISUAL_ID:
			return getDecMappingDecMappingMapfromCompartment_7033SemanticChildren(view);
		case DecMappingDecMappingMaptoEntityCompartmentEditPart.VISUAL_ID:
			return getDecMappingDecMappingMaptoEntityCompartment_7034SemanticChildren(view);
		case VariablsVariablsInterCompartmentEditPart.VISUAL_ID:
			return getVariablsVariablsInterCompartment_7012SemanticChildren(view);
		case VariablsVariablsTransCompartmentEditPart.VISUAL_ID:
			return getVariablsVariablsTransCompartment_7013SemanticChildren(view);
		case VariablsVariablsGrdCompartmentEditPart.VISUAL_ID:
			return getVariablsVariablsGrdCompartment_7014SemanticChildren(view);
		case VariablsVariablsLabelCompartmentEditPart.VISUAL_ID:
			return getVariablsVariablsLabelCompartment_7015SemanticChildren(view);
		case VariablsVariablsEntryCompartmentEditPart.VISUAL_ID:
			return getVariablsVariablsEntryCompartment_7016SemanticChildren(view);
		case VariablsVariablsTriggerCompartmentEditPart.VISUAL_ID:
			return getVariablsVariablsTriggerCompartment_7017SemanticChildren(view);
		case ClassDiagramClassDiagramClassCompartmentEditPart.VISUAL_ID:
			return getClassDiagramClassDiagramClassCompartment_7022SemanticChildren(view);
		case ClassDiagramClassDiagramWfCompartmentEditPart.VISUAL_ID:
			return getClassDiagramClassDiagramWfCompartment_7023SemanticChildren(view);
		case StateChartStateChartStateCompartmentEditPart.VISUAL_ID:
			return getStateChartStateChartStateCompartment_7024SemanticChildren(view);
		case StateChartStateChartWfCompartmentEditPart.VISUAL_ID:
			return getStateChartStateChartWfCompartment_7025SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getDesignChoices_1000SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		DesignChoices modelElement = (DesignChoices) view.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		{
			ChoiceModel childElement = modelElement.getCm();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == ChoiceModelEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
			}
		}
		{
			FeatureModel childElement = modelElement.getFm();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == FeatureModelEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
			}
		}
		{
			FeatureMapping childElement = modelElement.getFmap();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == FeatureMappingEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
			}
		}
		for (Iterator<?> it = modelElement.getDmap().iterator(); it.hasNext();) {
			DecisionMapping childElement = (DecisionMapping) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == DecisionMappingEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getProperty().iterator(); it.hasNext();) {
			Property childElement = (Property) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == PropertyEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getVar().iterator(); it.hasNext();) {
			spl.variabls childElement = (spl.variabls) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == VariablsEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		{
			ClassDiagram childElement = modelElement.getCd();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == ClassDiagramEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
			}
		}
		{
			StateChart childElement = modelElement.getSc();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == StateChartEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getChoiceModelChoiceModelChCompartment_7001SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		ChoiceModel modelElement = (ChoiceModel) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getCh().iterator(); it.hasNext();) {
			Choice childElement = (Choice) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == ChoiceEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getChoiceModelChoiceModelConstraintCompartment_7026SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		ChoiceModel modelElement = (ChoiceModel) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getConstraint().iterator(); it.hasNext();) {
			CMconstraints childElement = (CMconstraints) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == CMconstraintsEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getFeatureModelFeatureModelFeatureCompartment_7018SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		FeatureModel modelElement = (FeatureModel) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getFeature().iterator(); it.hasNext();) {
			Feature childElement = (Feature) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == FeatureEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getFeatureModelFeatureModelConstraintCompartment_7019SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		FeatureModel modelElement = (FeatureModel) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getConstraint().iterator(); it.hasNext();) {
			FMconstraints childElement = (FMconstraints) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == FMconstraintsEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getFeatureMappingFeatureMappingFmapCompartment_7029SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		FeatureMapping modelElement = (FeatureMapping) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getFmap().iterator(); it.hasNext();) {
			spl.featMapping childElement = (spl.featMapping) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == FeatMappingEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getFeatMappingFeatMappingMapfromCompartment_7030SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		spl.featMapping modelElement = (spl.featMapping) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getMapfrom().iterator(); it.hasNext();) {
			Map childElement = (Map) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == MapEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getFeatMappingFeatMappingMaptoCompartment_7031SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		spl.featMapping modelElement = (spl.featMapping) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getMapto().iterator(); it.hasNext();) {
			Mapto childElement = (Mapto) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == MaptoEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getDecisionMappingDecisionMappingDecisionMapCompartment_7032SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		DecisionMapping modelElement = (DecisionMapping) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getDecisionMap().iterator(); it.hasNext();) {
			DecMapping childElement = (DecMapping) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == DecMappingEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getDecMappingDecMappingMapfromCompartment_7033SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		DecMapping modelElement = (DecMapping) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getMapfrom().iterator(); it.hasNext();) {
			Mapdec childElement = (Mapdec) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == MapdecEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getDecMappingDecMappingMaptoEntityCompartment_7034SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		DecMapping modelElement = (DecMapping) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getMaptoEntity().iterator(); it.hasNext();) {
			MapToEntity childElement = (MapToEntity) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == MapToEntityEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getVariablsVariablsInterCompartment_7012SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		spl.variabls modelElement = (spl.variabls) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getInter().iterator(); it.hasNext();) {
			Interfc childElement = (Interfc) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == InterfcEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getVariablsVariablsTransCompartment_7013SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		spl.variabls modelElement = (spl.variabls) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getTrans().iterator(); it.hasNext();) {
			TransitionalBehavior childElement = (TransitionalBehavior) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == TransitionalBehaviorEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getVariablsVariablsGrdCompartment_7014SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		spl.variabls modelElement = (spl.variabls) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getGrd().iterator(); it.hasNext();) {
			Guard childElement = (Guard) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == GuardEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getVariablsVariablsLabelCompartment_7015SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		spl.variabls modelElement = (spl.variabls) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getLabel().iterator(); it.hasNext();) {
			Label childElement = (Label) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == LabelEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getVariablsVariablsEntryCompartment_7016SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		spl.variabls modelElement = (spl.variabls) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getEntry().iterator(); it.hasNext();) {
			Entry childElement = (Entry) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == EntryEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getVariablsVariablsTriggerCompartment_7017SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		spl.variabls modelElement = (spl.variabls) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getTrigger().iterator(); it.hasNext();) {
			Trigger childElement = (Trigger) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == TriggerEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getClassDiagramClassDiagramClassCompartment_7022SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		ClassDiagram modelElement = (ClassDiagram) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getClass_().iterator(); it.hasNext();) {
			Class childElement = (Class) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == ClassEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getClassDiagramClassDiagramWfCompartment_7023SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		ClassDiagram modelElement = (ClassDiagram) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getWf().iterator(); it.hasNext();) {
			spl.cdwellformedness childElement = (spl.cdwellformedness) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == CdwellformednessEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getStateChartStateChartStateCompartment_7024SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		StateChart modelElement = (StateChart) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getState().iterator(); it.hasNext();) {
			State childElement = (State) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == StateEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplNodeDescriptor> getStateChartStateChartWfCompartment_7025SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		StateChart modelElement = (StateChart) containerView.getElement();
		LinkedList<SplNodeDescriptor> result = new LinkedList<SplNodeDescriptor>();
		for (Iterator<?> it = modelElement.getWf().iterator(); it.hasNext();) {
			spl.scwellformedness childElement = (spl.scwellformedness) it.next();
			int visualID = SplVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == ScwellformednessEditPart.VISUAL_ID) {
				result.add(new SplNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<SplLinkDescriptor> getContainedLinks(View view) {
		switch (SplVisualIDRegistry.getVisualID(view)) {
		case DesignChoicesEditPart.VISUAL_ID:
			return getDesignChoices_1000ContainedLinks(view);
		case ChoiceModelEditPart.VISUAL_ID:
			return getChoiceModel_2001ContainedLinks(view);
		case FeatureModelEditPart.VISUAL_ID:
			return getFeatureModel_2011ContainedLinks(view);
		case FeatureMappingEditPart.VISUAL_ID:
			return getFeatureMapping_2005ContainedLinks(view);
		case DecisionMappingEditPart.VISUAL_ID:
			return getDecisionMapping_2006ContainedLinks(view);
		case PropertyEditPart.VISUAL_ID:
			return getProperty_2008ContainedLinks(view);
		case VariablsEditPart.VISUAL_ID:
			return getVariabls_2009ContainedLinks(view);
		case ClassDiagramEditPart.VISUAL_ID:
			return getClassDiagram_2013ContainedLinks(view);
		case StateChartEditPart.VISUAL_ID:
			return getStateChart_2014ContainedLinks(view);
		case ChoiceEditPart.VISUAL_ID:
			return getChoice_3001ContainedLinks(view);
		case CMconstraintsEditPart.VISUAL_ID:
			return getCMconstraints_3018ContainedLinks(view);
		case FeatureEditPart.VISUAL_ID:
			return getFeature_3004ContainedLinks(view);
		case FMconstraintsEditPart.VISUAL_ID:
			return getFMconstraints_3009ContainedLinks(view);
		case FeatMappingEditPart.VISUAL_ID:
			return getFeatMapping_3021ContainedLinks(view);
		case MapEditPart.VISUAL_ID:
			return getMap_3019ContainedLinks(view);
		case MaptoEditPart.VISUAL_ID:
			return getMapto_3020ContainedLinks(view);
		case DecMappingEditPart.VISUAL_ID:
			return getDecMapping_3022ContainedLinks(view);
		case MapdecEditPart.VISUAL_ID:
			return getMapdec_3023ContainedLinks(view);
		case MapToEntityEditPart.VISUAL_ID:
			return getMapToEntity_3024ContainedLinks(view);
		case InterfcEditPart.VISUAL_ID:
			return getInterfc_3012ContainedLinks(view);
		case TransitionalBehaviorEditPart.VISUAL_ID:
			return getTransitionalBehavior_3013ContainedLinks(view);
		case GuardEditPart.VISUAL_ID:
			return getGuard_3014ContainedLinks(view);
		case LabelEditPart.VISUAL_ID:
			return getLabel_3015ContainedLinks(view);
		case EntryEditPart.VISUAL_ID:
			return getEntry_3016ContainedLinks(view);
		case TriggerEditPart.VISUAL_ID:
			return getTrigger_3017ContainedLinks(view);
		case ClassEditPart.VISUAL_ID:
			return getClass_3007ContainedLinks(view);
		case CdwellformednessEditPart.VISUAL_ID:
			return getCdwellformedness_3010ContainedLinks(view);
		case StateEditPart.VISUAL_ID:
			return getState_3008ContainedLinks(view);
		case ScwellformednessEditPart.VISUAL_ID:
			return getScwellformedness_3011ContainedLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001ContainedLinks(view);
		case RelationshipEditPart.VISUAL_ID:
			return getRelationship_4005ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<SplLinkDescriptor> getIncomingLinks(View view) {
		switch (SplVisualIDRegistry.getVisualID(view)) {
		case ChoiceModelEditPart.VISUAL_ID:
			return getChoiceModel_2001IncomingLinks(view);
		case FeatureModelEditPart.VISUAL_ID:
			return getFeatureModel_2011IncomingLinks(view);
		case FeatureMappingEditPart.VISUAL_ID:
			return getFeatureMapping_2005IncomingLinks(view);
		case DecisionMappingEditPart.VISUAL_ID:
			return getDecisionMapping_2006IncomingLinks(view);
		case PropertyEditPart.VISUAL_ID:
			return getProperty_2008IncomingLinks(view);
		case VariablsEditPart.VISUAL_ID:
			return getVariabls_2009IncomingLinks(view);
		case ClassDiagramEditPart.VISUAL_ID:
			return getClassDiagram_2013IncomingLinks(view);
		case StateChartEditPart.VISUAL_ID:
			return getStateChart_2014IncomingLinks(view);
		case ChoiceEditPart.VISUAL_ID:
			return getChoice_3001IncomingLinks(view);
		case CMconstraintsEditPart.VISUAL_ID:
			return getCMconstraints_3018IncomingLinks(view);
		case FeatureEditPart.VISUAL_ID:
			return getFeature_3004IncomingLinks(view);
		case FMconstraintsEditPart.VISUAL_ID:
			return getFMconstraints_3009IncomingLinks(view);
		case FeatMappingEditPart.VISUAL_ID:
			return getFeatMapping_3021IncomingLinks(view);
		case MapEditPart.VISUAL_ID:
			return getMap_3019IncomingLinks(view);
		case MaptoEditPart.VISUAL_ID:
			return getMapto_3020IncomingLinks(view);
		case DecMappingEditPart.VISUAL_ID:
			return getDecMapping_3022IncomingLinks(view);
		case MapdecEditPart.VISUAL_ID:
			return getMapdec_3023IncomingLinks(view);
		case MapToEntityEditPart.VISUAL_ID:
			return getMapToEntity_3024IncomingLinks(view);
		case InterfcEditPart.VISUAL_ID:
			return getInterfc_3012IncomingLinks(view);
		case TransitionalBehaviorEditPart.VISUAL_ID:
			return getTransitionalBehavior_3013IncomingLinks(view);
		case GuardEditPart.VISUAL_ID:
			return getGuard_3014IncomingLinks(view);
		case LabelEditPart.VISUAL_ID:
			return getLabel_3015IncomingLinks(view);
		case EntryEditPart.VISUAL_ID:
			return getEntry_3016IncomingLinks(view);
		case TriggerEditPart.VISUAL_ID:
			return getTrigger_3017IncomingLinks(view);
		case ClassEditPart.VISUAL_ID:
			return getClass_3007IncomingLinks(view);
		case CdwellformednessEditPart.VISUAL_ID:
			return getCdwellformedness_3010IncomingLinks(view);
		case StateEditPart.VISUAL_ID:
			return getState_3008IncomingLinks(view);
		case ScwellformednessEditPart.VISUAL_ID:
			return getScwellformedness_3011IncomingLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001IncomingLinks(view);
		case RelationshipEditPart.VISUAL_ID:
			return getRelationship_4005IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<SplLinkDescriptor> getOutgoingLinks(View view) {
		switch (SplVisualIDRegistry.getVisualID(view)) {
		case ChoiceModelEditPart.VISUAL_ID:
			return getChoiceModel_2001OutgoingLinks(view);
		case FeatureModelEditPart.VISUAL_ID:
			return getFeatureModel_2011OutgoingLinks(view);
		case FeatureMappingEditPart.VISUAL_ID:
			return getFeatureMapping_2005OutgoingLinks(view);
		case DecisionMappingEditPart.VISUAL_ID:
			return getDecisionMapping_2006OutgoingLinks(view);
		case PropertyEditPart.VISUAL_ID:
			return getProperty_2008OutgoingLinks(view);
		case VariablsEditPart.VISUAL_ID:
			return getVariabls_2009OutgoingLinks(view);
		case ClassDiagramEditPart.VISUAL_ID:
			return getClassDiagram_2013OutgoingLinks(view);
		case StateChartEditPart.VISUAL_ID:
			return getStateChart_2014OutgoingLinks(view);
		case ChoiceEditPart.VISUAL_ID:
			return getChoice_3001OutgoingLinks(view);
		case CMconstraintsEditPart.VISUAL_ID:
			return getCMconstraints_3018OutgoingLinks(view);
		case FeatureEditPart.VISUAL_ID:
			return getFeature_3004OutgoingLinks(view);
		case FMconstraintsEditPart.VISUAL_ID:
			return getFMconstraints_3009OutgoingLinks(view);
		case FeatMappingEditPart.VISUAL_ID:
			return getFeatMapping_3021OutgoingLinks(view);
		case MapEditPart.VISUAL_ID:
			return getMap_3019OutgoingLinks(view);
		case MaptoEditPart.VISUAL_ID:
			return getMapto_3020OutgoingLinks(view);
		case DecMappingEditPart.VISUAL_ID:
			return getDecMapping_3022OutgoingLinks(view);
		case MapdecEditPart.VISUAL_ID:
			return getMapdec_3023OutgoingLinks(view);
		case MapToEntityEditPart.VISUAL_ID:
			return getMapToEntity_3024OutgoingLinks(view);
		case InterfcEditPart.VISUAL_ID:
			return getInterfc_3012OutgoingLinks(view);
		case TransitionalBehaviorEditPart.VISUAL_ID:
			return getTransitionalBehavior_3013OutgoingLinks(view);
		case GuardEditPart.VISUAL_ID:
			return getGuard_3014OutgoingLinks(view);
		case LabelEditPart.VISUAL_ID:
			return getLabel_3015OutgoingLinks(view);
		case EntryEditPart.VISUAL_ID:
			return getEntry_3016OutgoingLinks(view);
		case TriggerEditPart.VISUAL_ID:
			return getTrigger_3017OutgoingLinks(view);
		case ClassEditPart.VISUAL_ID:
			return getClass_3007OutgoingLinks(view);
		case CdwellformednessEditPart.VISUAL_ID:
			return getCdwellformedness_3010OutgoingLinks(view);
		case StateEditPart.VISUAL_ID:
			return getState_3008OutgoingLinks(view);
		case ScwellformednessEditPart.VISUAL_ID:
			return getScwellformedness_3011OutgoingLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001OutgoingLinks(view);
		case RelationshipEditPart.VISUAL_ID:
			return getRelationship_4005OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getDesignChoices_1000ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getChoiceModel_2001ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeatureModel_2011ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeatureMapping_2005ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getDecisionMapping_2006ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getProperty_2008ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getVariabls_2009ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getClassDiagram_2013ContainedLinks(View view) {
		ClassDiagram modelElement = (ClassDiagram) view.getElement();
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Relationship_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getStateChart_2014ContainedLinks(View view) {
		StateChart modelElement = (StateChart) view.getElement();
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Transition_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getChoice_3001ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getCMconstraints_3018ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeature_3004ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFMconstraints_3009ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeatMapping_3021ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMap_3019ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMapto_3020ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getDecMapping_3022ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMapdec_3023ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMapToEntity_3024ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getInterfc_3012ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getTransitionalBehavior_3013ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getGuard_3014ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getLabel_3015ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getEntry_3016ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getTrigger_3017ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getClass_3007ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getCdwellformedness_3010ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getState_3008ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getScwellformedness_3011ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getTransition_4001ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getRelationship_4005ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getChoiceModel_2001IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeatureModel_2011IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeatureMapping_2005IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getDecisionMapping_2006IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getProperty_2008IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getVariabls_2009IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getClassDiagram_2013IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getStateChart_2014IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getChoice_3001IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getCMconstraints_3018IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeature_3004IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFMconstraints_3009IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeatMapping_3021IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMap_3019IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMapto_3020IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getDecMapping_3022IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMapdec_3023IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMapToEntity_3024IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getInterfc_3012IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getTransitionalBehavior_3013IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getGuard_3014IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getLabel_3015IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getEntry_3016IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getTrigger_3017IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getClass_3007IncomingLinks(View view) {
		Class modelElement = (Class) view.getElement();
		java.util.Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Relationship_4005(modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getCdwellformedness_3010IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getState_3008IncomingLinks(View view) {
		State modelElement = (State) view.getElement();
		java.util.Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Transition_4001(modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getScwellformedness_3011IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getTransition_4001IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getRelationship_4005IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getChoiceModel_2001OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeatureModel_2011OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeatureMapping_2005OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getDecisionMapping_2006OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getProperty_2008OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getVariabls_2009OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getClassDiagram_2013OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getStateChart_2014OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getChoice_3001OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getCMconstraints_3018OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeature_3004OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFMconstraints_3009OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getFeatMapping_3021OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMap_3019OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMapto_3020OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getDecMapping_3022OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMapdec_3023OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getMapToEntity_3024OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getInterfc_3012OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getTransitionalBehavior_3013OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getGuard_3014OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getLabel_3015OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getEntry_3016OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getTrigger_3017OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getClass_3007OutgoingLinks(View view) {
		Class modelElement = (Class) view.getElement();
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Relationship_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getCdwellformedness_3010OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getState_3008OutgoingLinks(View view) {
		State modelElement = (State) view.getElement();
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Transition_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getScwellformedness_3011OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getTransition_4001OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<SplLinkDescriptor> getRelationship_4005OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	private static Collection<SplLinkDescriptor> getContainedTypeModelFacetLinks_Transition_4001(StateChart container) {
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		for (Iterator<?> links = container.getTransition().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Transition) {
				continue;
			}
			Transition link = (Transition) linkObject;
			if (TransitionEditPart.VISUAL_ID != SplVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			State dst = link.getTostate();
			State src = link.getFromstate();
			result.add(new SplLinkDescriptor(src, dst, link, SplElementTypes.Transition_4001,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	* @generated
	*/
	private static Collection<SplLinkDescriptor> getContainedTypeModelFacetLinks_Relationship_4005(
			ClassDiagram container) {
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		for (Iterator<?> links = container.getRelationship().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Relationship) {
				continue;
			}
			Relationship link = (Relationship) linkObject;
			if (RelationshipEditPart.VISUAL_ID != SplVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			Class dst = link.getTo();
			Class src = link.getFrom();
			result.add(new SplLinkDescriptor(src, dst, link, SplElementTypes.Relationship_4005,
					RelationshipEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<SplLinkDescriptor> getIncomingTypeModelFacetLinks_Transition_4001(State target,
			java.util.Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != SplPackage.eINSTANCE.getTransition_Tostate()
					|| false == setting.getEObject() instanceof Transition) {
				continue;
			}
			Transition link = (Transition) setting.getEObject();
			if (TransitionEditPart.VISUAL_ID != SplVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			State src = link.getFromstate();
			result.add(new SplLinkDescriptor(src, target, link, SplElementTypes.Transition_4001,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<SplLinkDescriptor> getIncomingTypeModelFacetLinks_Relationship_4005(Class target,
			java.util.Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != SplPackage.eINSTANCE.getRelationship_To()
					|| false == setting.getEObject() instanceof Relationship) {
				continue;
			}
			Relationship link = (Relationship) setting.getEObject();
			if (RelationshipEditPart.VISUAL_ID != SplVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			Class src = link.getFrom();
			result.add(new SplLinkDescriptor(src, target, link, SplElementTypes.Relationship_4005,
					RelationshipEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	* @generated
	*/
	private static Collection<SplLinkDescriptor> getOutgoingTypeModelFacetLinks_Transition_4001(State source) {
		StateChart container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element.eContainer()) {
			if (element instanceof StateChart) {
				container = (StateChart) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		for (Iterator<?> links = container.getTransition().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Transition) {
				continue;
			}
			Transition link = (Transition) linkObject;
			if (TransitionEditPart.VISUAL_ID != SplVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			State dst = link.getTostate();
			State src = link.getFromstate();
			if (src != source) {
				continue;
			}
			result.add(new SplLinkDescriptor(src, dst, link, SplElementTypes.Transition_4001,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	* @generated
	*/
	private static Collection<SplLinkDescriptor> getOutgoingTypeModelFacetLinks_Relationship_4005(Class source) {
		ClassDiagram container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element.eContainer()) {
			if (element instanceof ClassDiagram) {
				container = (ClassDiagram) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<SplLinkDescriptor> result = new LinkedList<SplLinkDescriptor>();
		for (Iterator<?> links = container.getRelationship().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Relationship) {
				continue;
			}
			Relationship link = (Relationship) linkObject;
			if (RelationshipEditPart.VISUAL_ID != SplVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			Class dst = link.getTo();
			Class src = link.getFrom();
			if (src != source) {
				continue;
			}
			result.add(new SplLinkDescriptor(src, dst, link, SplElementTypes.Relationship_4005,
					RelationshipEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	* @generated
	*/
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		* @generated
		*/
		@Override

		public List<SplNodeDescriptor> getSemanticChildren(View view) {
			return SplDiagramUpdater.getSemanticChildren(view);
		}

		/**
		* @generated
		*/
		@Override

		public List<SplLinkDescriptor> getContainedLinks(View view) {
			return SplDiagramUpdater.getContainedLinks(view);
		}

		/**
		* @generated
		*/
		@Override

		public List<SplLinkDescriptor> getIncomingLinks(View view) {
			return SplDiagramUpdater.getIncomingLinks(view);
		}

		/**
		* @generated
		*/
		@Override

		public List<SplLinkDescriptor> getOutgoingLinks(View view) {
			return SplDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
