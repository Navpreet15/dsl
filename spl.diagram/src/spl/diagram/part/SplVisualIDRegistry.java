/*
* 
*/
package spl.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;

import spl.DesignChoices;
import spl.SplPackage;
import spl.diagram.edit.parts.CMconstraintsEditPart;
import spl.diagram.edit.parts.CMconstraintsOprEditPart;
import spl.diagram.edit.parts.CdwellformednessEditPart;
import spl.diagram.edit.parts.CdwellformednessSourceTargetEditPart;
import spl.diagram.edit.parts.ChoiceEditPart;
import spl.diagram.edit.parts.ChoiceModelChoiceModelChCompartmentEditPart;
import spl.diagram.edit.parts.ChoiceModelChoiceModelConstraintCompartmentEditPart;
import spl.diagram.edit.parts.ChoiceModelEditPart;
import spl.diagram.edit.parts.ChoiceNameVarTypeEditPart;
import spl.diagram.edit.parts.ClassDiagramClassDiagramClassCompartmentEditPart;
import spl.diagram.edit.parts.ClassDiagramClassDiagramWfCompartmentEditPart;
import spl.diagram.edit.parts.ClassDiagramEditPart;
import spl.diagram.edit.parts.ClassEditPart;
import spl.diagram.edit.parts.ClassNameInterfcEditPart;
import spl.diagram.edit.parts.DecMappingDecMappingMapfromCompartmentEditPart;
import spl.diagram.edit.parts.DecMappingDecMappingMaptoEntityCompartmentEditPart;
import spl.diagram.edit.parts.DecMappingEditPart;
import spl.diagram.edit.parts.DecMappingOprLHSOprRHSEditPart;
import spl.diagram.edit.parts.DecisionMappingDecisionMappingDecisionMapCompartmentEditPart;
import spl.diagram.edit.parts.DecisionMappingEditPart;
import spl.diagram.edit.parts.DesignChoicesEditPart;
import spl.diagram.edit.parts.EntryEditPart;
import spl.diagram.edit.parts.EntryNameEditPart;
import spl.diagram.edit.parts.FMconstraintsEditPart;
import spl.diagram.edit.parts.FMconstraintsOprEditPart;
import spl.diagram.edit.parts.FeatMappingEditPart;
import spl.diagram.edit.parts.FeatMappingFeatMappingMapfromCompartmentEditPart;
import spl.diagram.edit.parts.FeatMappingFeatMappingMaptoCompartmentEditPart;
import spl.diagram.edit.parts.FeatMappingOprLHSOprRHSEditPart;
import spl.diagram.edit.parts.FeatureEditPart;
import spl.diagram.edit.parts.FeatureMappingEditPart;
import spl.diagram.edit.parts.FeatureMappingFeatureMappingFmapCompartmentEditPart;
import spl.diagram.edit.parts.FeatureModelEditPart;
import spl.diagram.edit.parts.FeatureModelFeatureModelConstraintCompartmentEditPart;
import spl.diagram.edit.parts.FeatureModelFeatureModelFeatureCompartmentEditPart;
import spl.diagram.edit.parts.FeatureNameVartypeEditPart;
import spl.diagram.edit.parts.GuardEditPart;
import spl.diagram.edit.parts.GuardNameEditPart;
import spl.diagram.edit.parts.InterfcEditPart;
import spl.diagram.edit.parts.InterfcNameEditPart;
import spl.diagram.edit.parts.LabelEditPart;
import spl.diagram.edit.parts.LabelNameEditPart;
import spl.diagram.edit.parts.MapEditPart;
import spl.diagram.edit.parts.MapPresenceEditPart;
import spl.diagram.edit.parts.MapToEntityEditPart;
import spl.diagram.edit.parts.MapToEntityPresenceEditPart;
import spl.diagram.edit.parts.MapdecEditPart;
import spl.diagram.edit.parts.MapdecPresenceEditPart;
import spl.diagram.edit.parts.MaptoEditPart;
import spl.diagram.edit.parts.MaptoPresenceEditPart;
import spl.diagram.edit.parts.PropertyEditPart;
import spl.diagram.edit.parts.PropertyNameTextEditPart;
import spl.diagram.edit.parts.RelationshipEditPart;
import spl.diagram.edit.parts.RelationshipNameLabInMulOutMulEditPart;
import spl.diagram.edit.parts.ScwellformednessEditPart;
import spl.diagram.edit.parts.ScwellformednessSourceTargetEditPart;
import spl.diagram.edit.parts.StateChartEditPart;
import spl.diagram.edit.parts.StateChartNameEditPart;
import spl.diagram.edit.parts.StateChartStateChartStateCompartmentEditPart;
import spl.diagram.edit.parts.StateChartStateChartWfCompartmentEditPart;
import spl.diagram.edit.parts.StateEditPart;
import spl.diagram.edit.parts.StateNameEntryEditPart;
import spl.diagram.edit.parts.TransitionEditPart;
import spl.diagram.edit.parts.TransitionNameTriggerGrdTransitioEditPart;
import spl.diagram.edit.parts.TransitionalBehaviorEditPart;
import spl.diagram.edit.parts.TransitionalBehaviorNameEditPart;
import spl.diagram.edit.parts.TriggerEditPart;
import spl.diagram.edit.parts.TriggerNameEditPart;
import spl.diagram.edit.parts.VariablsEditPart;
import spl.diagram.edit.parts.VariablsVariablsEntryCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsGrdCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsInterCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsLabelCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsTransCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsTriggerCompartmentEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class SplVisualIDRegistry {

	/**
	* @generated
	*/
	private static final String DEBUG_KEY = "spl.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	* @generated
	*/
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (DesignChoicesEditPart.MODEL_ID.equals(view.getType())) {
				return DesignChoicesEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return spl.diagram.part.SplVisualIDRegistry.getVisualID(view.getType());
	}

	/**
	* @generated
	*/
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	* @generated
	*/
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(Platform.getDebugOption(DEBUG_KEY))) {
				SplDiagramEditorPlugin.getInstance()
						.logError("Unable to parse view type as a visualID number: " + type);
			}
		}
		return -1;
	}

	/**
	* @generated
	*/
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	* @generated
	*/
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (SplPackage.eINSTANCE.getDesignChoices().isSuperTypeOf(domainElement.eClass())
				&& isDiagram((DesignChoices) domainElement)) {
			return DesignChoicesEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	* @generated
	*/
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = spl.diagram.part.SplVisualIDRegistry.getModelID(containerView);
		if (!DesignChoicesEditPart.MODEL_ID.equals(containerModelID) && !"spl".equals(containerModelID)) { //$NON-NLS-1$
			return -1;
		}
		int containerVisualID;
		if (DesignChoicesEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = spl.diagram.part.SplVisualIDRegistry.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = DesignChoicesEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case DesignChoicesEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getChoiceModel().isSuperTypeOf(domainElement.eClass())) {
				return ChoiceModelEditPart.VISUAL_ID;
			}
			if (SplPackage.eINSTANCE.getFeatureModel().isSuperTypeOf(domainElement.eClass())) {
				return FeatureModelEditPart.VISUAL_ID;
			}
			if (SplPackage.eINSTANCE.getFeatureMapping().isSuperTypeOf(domainElement.eClass())) {
				return FeatureMappingEditPart.VISUAL_ID;
			}
			if (SplPackage.eINSTANCE.getDecisionMapping().isSuperTypeOf(domainElement.eClass())) {
				return DecisionMappingEditPart.VISUAL_ID;
			}
			if (SplPackage.eINSTANCE.getProperty().isSuperTypeOf(domainElement.eClass())) {
				return PropertyEditPart.VISUAL_ID;
			}
			if (SplPackage.eINSTANCE.getvariabls().isSuperTypeOf(domainElement.eClass())) {
				return VariablsEditPart.VISUAL_ID;
			}
			if (SplPackage.eINSTANCE.getClassDiagram().isSuperTypeOf(domainElement.eClass())) {
				return ClassDiagramEditPart.VISUAL_ID;
			}
			if (SplPackage.eINSTANCE.getStateChart().isSuperTypeOf(domainElement.eClass())) {
				return StateChartEditPart.VISUAL_ID;
			}
			break;
		case ChoiceModelChoiceModelChCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getChoice().isSuperTypeOf(domainElement.eClass())) {
				return ChoiceEditPart.VISUAL_ID;
			}
			break;
		case ChoiceModelChoiceModelConstraintCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getCMconstraints().isSuperTypeOf(domainElement.eClass())) {
				return CMconstraintsEditPart.VISUAL_ID;
			}
			break;
		case FeatureModelFeatureModelFeatureCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getFeature().isSuperTypeOf(domainElement.eClass())) {
				return FeatureEditPart.VISUAL_ID;
			}
			break;
		case FeatureModelFeatureModelConstraintCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getFMconstraints().isSuperTypeOf(domainElement.eClass())) {
				return FMconstraintsEditPart.VISUAL_ID;
			}
			break;
		case FeatureMappingFeatureMappingFmapCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getfeatMapping().isSuperTypeOf(domainElement.eClass())) {
				return FeatMappingEditPart.VISUAL_ID;
			}
			break;
		case FeatMappingFeatMappingMapfromCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getMap().isSuperTypeOf(domainElement.eClass())) {
				return MapEditPart.VISUAL_ID;
			}
			break;
		case FeatMappingFeatMappingMaptoCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getMapto().isSuperTypeOf(domainElement.eClass())) {
				return MaptoEditPart.VISUAL_ID;
			}
			break;
		case DecisionMappingDecisionMappingDecisionMapCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getDecMapping().isSuperTypeOf(domainElement.eClass())) {
				return DecMappingEditPart.VISUAL_ID;
			}
			break;
		case DecMappingDecMappingMapfromCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getMapdec().isSuperTypeOf(domainElement.eClass())) {
				return MapdecEditPart.VISUAL_ID;
			}
			break;
		case DecMappingDecMappingMaptoEntityCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getMapToEntity().isSuperTypeOf(domainElement.eClass())) {
				return MapToEntityEditPart.VISUAL_ID;
			}
			break;
		case VariablsVariablsInterCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getInterfc().isSuperTypeOf(domainElement.eClass())) {
				return InterfcEditPart.VISUAL_ID;
			}
			break;
		case VariablsVariablsTransCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getTransitionalBehavior().isSuperTypeOf(domainElement.eClass())) {
				return TransitionalBehaviorEditPart.VISUAL_ID;
			}
			break;
		case VariablsVariablsGrdCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getGuard().isSuperTypeOf(domainElement.eClass())) {
				return GuardEditPart.VISUAL_ID;
			}
			break;
		case VariablsVariablsLabelCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getLabel().isSuperTypeOf(domainElement.eClass())) {
				return LabelEditPart.VISUAL_ID;
			}
			break;
		case VariablsVariablsEntryCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getEntry().isSuperTypeOf(domainElement.eClass())) {
				return EntryEditPart.VISUAL_ID;
			}
			break;
		case VariablsVariablsTriggerCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getTrigger().isSuperTypeOf(domainElement.eClass())) {
				return TriggerEditPart.VISUAL_ID;
			}
			break;
		case ClassDiagramClassDiagramClassCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getClass_().isSuperTypeOf(domainElement.eClass())) {
				return ClassEditPart.VISUAL_ID;
			}
			break;
		case ClassDiagramClassDiagramWfCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getcdwellformedness().isSuperTypeOf(domainElement.eClass())) {
				return CdwellformednessEditPart.VISUAL_ID;
			}
			break;
		case StateChartStateChartStateCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getState().isSuperTypeOf(domainElement.eClass())) {
				return StateEditPart.VISUAL_ID;
			}
			break;
		case StateChartStateChartWfCompartmentEditPart.VISUAL_ID:
			if (SplPackage.eINSTANCE.getscwellformedness().isSuperTypeOf(domainElement.eClass())) {
				return ScwellformednessEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	* @generated
	*/
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = spl.diagram.part.SplVisualIDRegistry.getModelID(containerView);
		if (!DesignChoicesEditPart.MODEL_ID.equals(containerModelID) && !"spl".equals(containerModelID)) { //$NON-NLS-1$
			return false;
		}
		int containerVisualID;
		if (DesignChoicesEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = spl.diagram.part.SplVisualIDRegistry.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = DesignChoicesEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case DesignChoicesEditPart.VISUAL_ID:
			if (ChoiceModelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (FeatureModelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (FeatureMappingEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (DecisionMappingEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (PropertyEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VariablsEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ClassDiagramEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (StateChartEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ChoiceModelEditPart.VISUAL_ID:
			if (ChoiceModelChoiceModelChCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ChoiceModelChoiceModelConstraintCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case FeatureModelEditPart.VISUAL_ID:
			if (FeatureModelFeatureModelFeatureCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (FeatureModelFeatureModelConstraintCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case FeatureMappingEditPart.VISUAL_ID:
			if (FeatureMappingFeatureMappingFmapCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DecisionMappingEditPart.VISUAL_ID:
			if (DecisionMappingDecisionMappingDecisionMapCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case PropertyEditPart.VISUAL_ID:
			if (PropertyNameTextEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariablsEditPart.VISUAL_ID:
			if (VariablsVariablsInterCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VariablsVariablsTransCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VariablsVariablsGrdCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VariablsVariablsLabelCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VariablsVariablsEntryCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VariablsVariablsTriggerCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ClassDiagramEditPart.VISUAL_ID:
			if (ClassDiagramClassDiagramClassCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ClassDiagramClassDiagramWfCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case StateChartEditPart.VISUAL_ID:
			if (StateChartNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (StateChartStateChartStateCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (StateChartStateChartWfCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ChoiceEditPart.VISUAL_ID:
			if (ChoiceNameVarTypeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case CMconstraintsEditPart.VISUAL_ID:
			if (CMconstraintsOprEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case FeatureEditPart.VISUAL_ID:
			if (FeatureNameVartypeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case FMconstraintsEditPart.VISUAL_ID:
			if (FMconstraintsOprEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case FeatMappingEditPart.VISUAL_ID:
			if (FeatMappingOprLHSOprRHSEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (FeatMappingFeatMappingMapfromCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (FeatMappingFeatMappingMaptoCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MapEditPart.VISUAL_ID:
			if (MapPresenceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MaptoEditPart.VISUAL_ID:
			if (MaptoPresenceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DecMappingEditPart.VISUAL_ID:
			if (DecMappingOprLHSOprRHSEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (DecMappingDecMappingMapfromCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (DecMappingDecMappingMaptoEntityCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MapdecEditPart.VISUAL_ID:
			if (MapdecPresenceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MapToEntityEditPart.VISUAL_ID:
			if (MapToEntityPresenceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case InterfcEditPart.VISUAL_ID:
			if (InterfcNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TransitionalBehaviorEditPart.VISUAL_ID:
			if (TransitionalBehaviorNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case GuardEditPart.VISUAL_ID:
			if (GuardNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LabelEditPart.VISUAL_ID:
			if (LabelNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case EntryEditPart.VISUAL_ID:
			if (EntryNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TriggerEditPart.VISUAL_ID:
			if (TriggerNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ClassEditPart.VISUAL_ID:
			if (ClassNameInterfcEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case CdwellformednessEditPart.VISUAL_ID:
			if (CdwellformednessSourceTargetEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case StateEditPart.VISUAL_ID:
			if (StateNameEntryEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ScwellformednessEditPart.VISUAL_ID:
			if (ScwellformednessSourceTargetEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ChoiceModelChoiceModelChCompartmentEditPart.VISUAL_ID:
			if (ChoiceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ChoiceModelChoiceModelConstraintCompartmentEditPart.VISUAL_ID:
			if (CMconstraintsEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case FeatureModelFeatureModelFeatureCompartmentEditPart.VISUAL_ID:
			if (FeatureEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case FeatureModelFeatureModelConstraintCompartmentEditPart.VISUAL_ID:
			if (FMconstraintsEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case FeatureMappingFeatureMappingFmapCompartmentEditPart.VISUAL_ID:
			if (FeatMappingEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case FeatMappingFeatMappingMapfromCompartmentEditPart.VISUAL_ID:
			if (MapEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case FeatMappingFeatMappingMaptoCompartmentEditPart.VISUAL_ID:
			if (MaptoEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DecisionMappingDecisionMappingDecisionMapCompartmentEditPart.VISUAL_ID:
			if (DecMappingEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DecMappingDecMappingMapfromCompartmentEditPart.VISUAL_ID:
			if (MapdecEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DecMappingDecMappingMaptoEntityCompartmentEditPart.VISUAL_ID:
			if (MapToEntityEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariablsVariablsInterCompartmentEditPart.VISUAL_ID:
			if (InterfcEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariablsVariablsTransCompartmentEditPart.VISUAL_ID:
			if (TransitionalBehaviorEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariablsVariablsGrdCompartmentEditPart.VISUAL_ID:
			if (GuardEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariablsVariablsLabelCompartmentEditPart.VISUAL_ID:
			if (LabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariablsVariablsEntryCompartmentEditPart.VISUAL_ID:
			if (EntryEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariablsVariablsTriggerCompartmentEditPart.VISUAL_ID:
			if (TriggerEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ClassDiagramClassDiagramClassCompartmentEditPart.VISUAL_ID:
			if (ClassEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ClassDiagramClassDiagramWfCompartmentEditPart.VISUAL_ID:
			if (CdwellformednessEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case StateChartStateChartStateCompartmentEditPart.VISUAL_ID:
			if (StateEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case StateChartStateChartWfCompartmentEditPart.VISUAL_ID:
			if (ScwellformednessEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TransitionEditPart.VISUAL_ID:
			if (TransitionNameTriggerGrdTransitioEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RelationshipEditPart.VISUAL_ID:
			if (RelationshipNameLabInMulOutMulEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	* @generated
	*/
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (SplPackage.eINSTANCE.getTransition().isSuperTypeOf(domainElement.eClass())) {
			return TransitionEditPart.VISUAL_ID;
		}
		if (SplPackage.eINSTANCE.getRelationship().isSuperTypeOf(domainElement.eClass())) {
			return RelationshipEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	* User can change implementation of this method to handle some specific
	* situations not covered by default logic.
	* 
	* @generated
	*/
	private static boolean isDiagram(DesignChoices element) {
		return true;
	}

	/**
	* @generated
	*/
	public static boolean checkNodeVisualID(View containerView, EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	* @generated
	*/
	public static boolean isCompartmentVisualID(int visualID) {
		switch (visualID) {
		case ChoiceModelChoiceModelChCompartmentEditPart.VISUAL_ID:
		case ChoiceModelChoiceModelConstraintCompartmentEditPart.VISUAL_ID:
		case FeatureModelFeatureModelFeatureCompartmentEditPart.VISUAL_ID:
		case FeatureModelFeatureModelConstraintCompartmentEditPart.VISUAL_ID:
		case FeatureMappingFeatureMappingFmapCompartmentEditPart.VISUAL_ID:
		case FeatMappingFeatMappingMapfromCompartmentEditPart.VISUAL_ID:
		case FeatMappingFeatMappingMaptoCompartmentEditPart.VISUAL_ID:
		case DecisionMappingDecisionMappingDecisionMapCompartmentEditPart.VISUAL_ID:
		case DecMappingDecMappingMapfromCompartmentEditPart.VISUAL_ID:
		case DecMappingDecMappingMaptoEntityCompartmentEditPart.VISUAL_ID:
		case VariablsVariablsInterCompartmentEditPart.VISUAL_ID:
		case VariablsVariablsTransCompartmentEditPart.VISUAL_ID:
		case VariablsVariablsGrdCompartmentEditPart.VISUAL_ID:
		case VariablsVariablsLabelCompartmentEditPart.VISUAL_ID:
		case VariablsVariablsEntryCompartmentEditPart.VISUAL_ID:
		case VariablsVariablsTriggerCompartmentEditPart.VISUAL_ID:
		case ClassDiagramClassDiagramClassCompartmentEditPart.VISUAL_ID:
		case ClassDiagramClassDiagramWfCompartmentEditPart.VISUAL_ID:
		case StateChartStateChartStateCompartmentEditPart.VISUAL_ID:
		case StateChartStateChartWfCompartmentEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	* @generated
	*/
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case DesignChoicesEditPart.VISUAL_ID:
			return false;
		case PropertyEditPart.VISUAL_ID:
		case ChoiceEditPart.VISUAL_ID:
		case FeatureEditPart.VISUAL_ID:
		case ClassEditPart.VISUAL_ID:
		case StateEditPart.VISUAL_ID:
		case FMconstraintsEditPart.VISUAL_ID:
		case CdwellformednessEditPart.VISUAL_ID:
		case ScwellformednessEditPart.VISUAL_ID:
		case InterfcEditPart.VISUAL_ID:
		case TransitionalBehaviorEditPart.VISUAL_ID:
		case GuardEditPart.VISUAL_ID:
		case LabelEditPart.VISUAL_ID:
		case EntryEditPart.VISUAL_ID:
		case TriggerEditPart.VISUAL_ID:
		case CMconstraintsEditPart.VISUAL_ID:
		case MapEditPart.VISUAL_ID:
		case MaptoEditPart.VISUAL_ID:
		case MapdecEditPart.VISUAL_ID:
		case MapToEntityEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	* @generated
	*/
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		* @generated
		*/
		@Override

		public int getVisualID(View view) {
			return spl.diagram.part.SplVisualIDRegistry.getVisualID(view);
		}

		/**
		* @generated
		*/
		@Override

		public String getModelID(View view) {
			return spl.diagram.part.SplVisualIDRegistry.getModelID(view);
		}

		/**
		* @generated
		*/
		@Override

		public int getNodeVisualID(View containerView, EObject domainElement) {
			return spl.diagram.part.SplVisualIDRegistry.getNodeVisualID(containerView, domainElement);
		}

		/**
		* @generated
		*/
		@Override

		public boolean checkNodeVisualID(View containerView, EObject domainElement, int candidate) {
			return spl.diagram.part.SplVisualIDRegistry.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		* @generated
		*/
		@Override

		public boolean isCompartmentVisualID(int visualID) {
			return spl.diagram.part.SplVisualIDRegistry.isCompartmentVisualID(visualID);
		}

		/**
		* @generated
		*/
		@Override

		public boolean isSemanticLeafVisualID(int visualID) {
			return spl.diagram.part.SplVisualIDRegistry.isSemanticLeafVisualID(visualID);
		}
	};

}
