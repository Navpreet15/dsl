/*
 * 
 */
package spl.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	* @generated
	*/
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private Messages() {
	}

	/**
	* @generated
	*/
	public static String SplCreationWizardTitle;

	/**
	* @generated
	*/
	public static String SplCreationWizard_DiagramModelFilePageTitle;

	/**
	* @generated
	*/
	public static String SplCreationWizard_DiagramModelFilePageDescription;

	/**
	* @generated
	*/
	public static String SplCreationWizard_DomainModelFilePageTitle;

	/**
	* @generated
	*/
	public static String SplCreationWizard_DomainModelFilePageDescription;

	/**
	* @generated
	*/
	public static String SplCreationWizardOpenEditorError;

	/**
	* @generated
	*/
	public static String SplCreationWizardCreationError;

	/**
	* @generated
	*/
	public static String SplCreationWizardPageExtensionError;

	/**
	* @generated
	*/
	public static String SplDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	* @generated
	*/
	public static String SplDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	* @generated
	*/
	public static String SplDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	* @generated
	*/
	public static String SplDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	* @generated
	*/
	public static String SplDocumentProvider_isModifiable;

	/**
	* @generated
	*/
	public static String SplDocumentProvider_handleElementContentChanged;

	/**
	* @generated
	*/
	public static String SplDocumentProvider_IncorrectInputError;

	/**
	* @generated
	*/
	public static String SplDocumentProvider_NoDiagramInResourceError;

	/**
	* @generated
	*/
	public static String SplDocumentProvider_DiagramLoadingError;

	/**
	* @generated
	*/
	public static String SplDocumentProvider_UnsynchronizedFileSaveError;

	/**
	* @generated
	*/
	public static String SplDocumentProvider_SaveDiagramTask;

	/**
	* @generated
	*/
	public static String SplDocumentProvider_SaveNextResourceTask;

	/**
	* @generated
	*/
	public static String SplDocumentProvider_SaveAsOperation;

	/**
	* @generated
	*/
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	* @generated
	*/
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	* @generated
	*/
	public static String InitDiagramFile_WizardTitle;

	/**
	* @generated
	*/
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	* @generated
	*/
	public static String SplNewDiagramFileWizard_CreationPageName;

	/**
	* @generated
	*/
	public static String SplNewDiagramFileWizard_CreationPageTitle;

	/**
	* @generated
	*/
	public static String SplNewDiagramFileWizard_CreationPageDescription;

	/**
	* @generated
	*/
	public static String SplNewDiagramFileWizard_RootSelectionPageName;

	/**
	* @generated
	*/
	public static String SplNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	* @generated
	*/
	public static String SplNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	* @generated
	*/
	public static String SplNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	* @generated
	*/
	public static String SplNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	* @generated
	*/
	public static String SplNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	* @generated
	*/
	public static String SplNewDiagramFileWizard_InitDiagramCommand;

	/**
	* @generated
	*/
	public static String SplNewDiagramFileWizard_IncorrectRootError;

	/**
	* @generated
	*/
	public static String SplDiagramEditor_SavingDeletedFile;

	/**
	* @generated
	*/
	public static String SplDiagramEditor_SaveAsErrorTitle;

	/**
	* @generated
	*/
	public static String SplDiagramEditor_SaveAsErrorMessage;

	/**
	* @generated
	*/
	public static String SplDiagramEditor_SaveErrorTitle;

	/**
	* @generated
	*/
	public static String SplDiagramEditor_SaveErrorMessage;

	/**
	* @generated
	*/
	public static String SplElementChooserDialog_SelectModelElementTitle;

	/**
	* @generated
	*/
	public static String ModelElementSelectionPageMessage;

	/**
	* @generated
	*/
	public static String ValidateActionMessage;

	/**
	* @generated
	*/
	public static String Objects1Group_title;

	/**
	* @generated
	*/
	public static String Connections2Group_title;

	/**
	* @generated
	*/
	public static String CMconstraints1CreationTool_title;

	/**
	* @generated
	*/
	public static String CMconstraints1CreationTool_desc;

	/**
	* @generated
	*/
	public static String Choice2CreationTool_title;

	/**
	* @generated
	*/
	public static String Choice2CreationTool_desc;

	/**
	* @generated
	*/
	public static String ChoiceModel3CreationTool_title;

	/**
	* @generated
	*/
	public static String ChoiceModel3CreationTool_desc;

	/**
	* @generated
	*/
	public static String Class4CreationTool_title;

	/**
	* @generated
	*/
	public static String Class4CreationTool_desc;

	/**
	* @generated
	*/
	public static String ClassDiagram5CreationTool_title;

	/**
	* @generated
	*/
	public static String ClassDiagram5CreationTool_desc;

	/**
	* @generated
	*/
	public static String DecMapping6CreationTool_title;

	/**
	* @generated
	*/
	public static String DecMapping6CreationTool_desc;

	/**
	* @generated
	*/
	public static String DecisionMapping7CreationTool_title;

	/**
	* @generated
	*/
	public static String DecisionMapping7CreationTool_desc;

	/**
	* @generated
	*/
	public static String Entry8CreationTool_title;

	/**
	* @generated
	*/
	public static String Entry8CreationTool_desc;

	/**
	* @generated
	*/
	public static String FMconstraints9CreationTool_title;

	/**
	* @generated
	*/
	public static String FMconstraints9CreationTool_desc;

	/**
	* @generated
	*/
	public static String Feature10CreationTool_title;

	/**
	* @generated
	*/
	public static String Feature10CreationTool_desc;

	/**
	* @generated
	*/
	public static String FeatureMapping11CreationTool_title;

	/**
	* @generated
	*/
	public static String FeatureMapping11CreationTool_desc;

	/**
	* @generated
	*/
	public static String FeatureModel12CreationTool_title;

	/**
	* @generated
	*/
	public static String FeatureModel12CreationTool_desc;

	/**
	* @generated
	*/
	public static String Guard13CreationTool_title;

	/**
	* @generated
	*/
	public static String Guard13CreationTool_desc;

	/**
	* @generated
	*/
	public static String Interfc14CreationTool_title;

	/**
	* @generated
	*/
	public static String Interfc14CreationTool_desc;

	/**
	* @generated
	*/
	public static String Label15CreationTool_title;

	/**
	* @generated
	*/
	public static String Label15CreationTool_desc;

	/**
	* @generated
	*/
	public static String Map16CreationTool_title;

	/**
	* @generated
	*/
	public static String Map16CreationTool_desc;

	/**
	* @generated
	*/
	public static String MapToEntity17CreationTool_title;

	/**
	* @generated
	*/
	public static String MapToEntity17CreationTool_desc;

	/**
	* @generated
	*/
	public static String Mapdec18CreationTool_title;

	/**
	* @generated
	*/
	public static String Mapdec18CreationTool_desc;

	/**
	* @generated
	*/
	public static String Mapto19CreationTool_title;

	/**
	* @generated
	*/
	public static String Mapto19CreationTool_desc;

	/**
	* @generated
	*/
	public static String Property20CreationTool_title;

	/**
	* @generated
	*/
	public static String Property20CreationTool_desc;

	/**
	* @generated
	*/
	public static String State21CreationTool_title;

	/**
	* @generated
	*/
	public static String State21CreationTool_desc;

	/**
	* @generated
	*/
	public static String StateChart22CreationTool_title;

	/**
	* @generated
	*/
	public static String StateChart22CreationTool_desc;

	/**
	* @generated
	*/
	public static String TransitionalBehavior23CreationTool_title;

	/**
	* @generated
	*/
	public static String TransitionalBehavior23CreationTool_desc;

	/**
	* @generated
	*/
	public static String Trigger24CreationTool_title;

	/**
	* @generated
	*/
	public static String Trigger24CreationTool_desc;

	/**
	* @generated
	*/
	public static String Cdwellformedness25CreationTool_title;

	/**
	* @generated
	*/
	public static String Cdwellformedness25CreationTool_desc;

	/**
	* @generated
	*/
	public static String FeatMapping26CreationTool_title;

	/**
	* @generated
	*/
	public static String FeatMapping26CreationTool_desc;

	/**
	* @generated
	*/
	public static String Scwellformedness27CreationTool_title;

	/**
	* @generated
	*/
	public static String Scwellformedness27CreationTool_desc;

	/**
	* @generated
	*/
	public static String Variabls28CreationTool_title;

	/**
	* @generated
	*/
	public static String Variabls28CreationTool_desc;

	/**
	* @generated
	*/
	public static String Relationship1CreationTool_title;

	/**
	* @generated
	*/
	public static String Relationship1CreationTool_desc;

	/**
	* @generated
	*/
	public static String Transition2CreationTool_title;

	/**
	* @generated
	*/
	public static String Transition2CreationTool_desc;

	/**
	* @generated
	*/
	public static String ChoiceModelChoiceModelChCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String ChoiceModelChoiceModelConstraintCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String FeatureModelFeatureModelFeatureCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String FeatureModelFeatureModelConstraintCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String FeatureMappingFeatureMappingFmapCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String FeatMappingFeatMappingMapfromCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String FeatMappingFeatMappingMaptoCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String DecisionMappingDecisionMappingDecisionMapCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String DecMappingDecMappingMapfromCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String DecMappingDecMappingMaptoEntityCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String VariablsVariablsInterCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String VariablsVariablsTransCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String VariablsVariablsGrdCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String VariablsVariablsLabelCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String VariablsVariablsEntryCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String VariablsVariablsTriggerCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String ClassDiagramClassDiagramClassCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String ClassDiagramClassDiagramWfCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String StateChartStateChartStateCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String StateChartStateChartWfCompartmentEditPart_title;

	/**
	* @generated
	*/
	public static String CommandName_OpenDiagram;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_DesignChoices_1000_links;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_Class_3007_incominglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_Class_3007_outgoinglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_State_3008_incominglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_State_3008_outgoinglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_Transition_4001_target;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_Transition_4001_source;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_Relationship_4005_target;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_Relationship_4005_source;

	/**
	* @generated
	*/
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	* @generated
	*/
	public static String MessageFormatParser_InvalidInputError;

	/**
	* @generated
	*/
	public static String SplModelingAssistantProviderTitle;

	/**
	* @generated
	*/
	public static String SplModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
