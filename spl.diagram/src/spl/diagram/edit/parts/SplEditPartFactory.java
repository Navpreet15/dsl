/*
 * 
 */
package spl.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;

import spl.diagram.part.SplVisualIDRegistry;

/**
 * @generated
 */
public class SplEditPartFactory implements EditPartFactory {

	/**
	* @generated
	*/
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (SplVisualIDRegistry.getVisualID(view)) {

			case DesignChoicesEditPart.VISUAL_ID:
				return new DesignChoicesEditPart(view);

			case ChoiceModelEditPart.VISUAL_ID:
				return new ChoiceModelEditPart(view);

			case FeatureModelEditPart.VISUAL_ID:
				return new FeatureModelEditPart(view);

			case FeatureMappingEditPart.VISUAL_ID:
				return new FeatureMappingEditPart(view);

			case DecisionMappingEditPart.VISUAL_ID:
				return new DecisionMappingEditPart(view);

			case PropertyEditPart.VISUAL_ID:
				return new PropertyEditPart(view);

			case PropertyNameTextEditPart.VISUAL_ID:
				return new PropertyNameTextEditPart(view);

			case VariablsEditPart.VISUAL_ID:
				return new VariablsEditPart(view);

			case ClassDiagramEditPart.VISUAL_ID:
				return new ClassDiagramEditPart(view);

			case StateChartEditPart.VISUAL_ID:
				return new StateChartEditPart(view);

			case StateChartNameEditPart.VISUAL_ID:
				return new StateChartNameEditPart(view);

			case ChoiceEditPart.VISUAL_ID:
				return new ChoiceEditPart(view);

			case ChoiceNameVarTypeEditPart.VISUAL_ID:
				return new ChoiceNameVarTypeEditPart(view);

			case CMconstraintsEditPart.VISUAL_ID:
				return new CMconstraintsEditPart(view);

			case CMconstraintsOprEditPart.VISUAL_ID:
				return new CMconstraintsOprEditPart(view);

			case FeatureEditPart.VISUAL_ID:
				return new FeatureEditPart(view);

			case FeatureNameVartypeEditPart.VISUAL_ID:
				return new FeatureNameVartypeEditPart(view);

			case FMconstraintsEditPart.VISUAL_ID:
				return new FMconstraintsEditPart(view);

			case FMconstraintsOprEditPart.VISUAL_ID:
				return new FMconstraintsOprEditPart(view);

			case FeatMappingEditPart.VISUAL_ID:
				return new FeatMappingEditPart(view);

			case FeatMappingOprLHSOprRHSEditPart.VISUAL_ID:
				return new FeatMappingOprLHSOprRHSEditPart(view);

			case MapEditPart.VISUAL_ID:
				return new MapEditPart(view);

			case MapPresenceEditPart.VISUAL_ID:
				return new MapPresenceEditPart(view);

			case MaptoEditPart.VISUAL_ID:
				return new MaptoEditPart(view);

			case MaptoPresenceEditPart.VISUAL_ID:
				return new MaptoPresenceEditPart(view);

			case DecMappingEditPart.VISUAL_ID:
				return new DecMappingEditPart(view);

			case DecMappingOprLHSOprRHSEditPart.VISUAL_ID:
				return new DecMappingOprLHSOprRHSEditPart(view);

			case MapdecEditPart.VISUAL_ID:
				return new MapdecEditPart(view);

			case MapdecPresenceEditPart.VISUAL_ID:
				return new MapdecPresenceEditPart(view);

			case MapToEntityEditPart.VISUAL_ID:
				return new MapToEntityEditPart(view);

			case MapToEntityPresenceEditPart.VISUAL_ID:
				return new MapToEntityPresenceEditPart(view);

			case InterfcEditPart.VISUAL_ID:
				return new InterfcEditPart(view);

			case InterfcNameEditPart.VISUAL_ID:
				return new InterfcNameEditPart(view);

			case TransitionalBehaviorEditPart.VISUAL_ID:
				return new TransitionalBehaviorEditPart(view);

			case TransitionalBehaviorNameEditPart.VISUAL_ID:
				return new TransitionalBehaviorNameEditPart(view);

			case GuardEditPart.VISUAL_ID:
				return new GuardEditPart(view);

			case GuardNameEditPart.VISUAL_ID:
				return new GuardNameEditPart(view);

			case LabelEditPart.VISUAL_ID:
				return new LabelEditPart(view);

			case LabelNameEditPart.VISUAL_ID:
				return new LabelNameEditPart(view);

			case EntryEditPart.VISUAL_ID:
				return new EntryEditPart(view);

			case EntryNameEditPart.VISUAL_ID:
				return new EntryNameEditPart(view);

			case TriggerEditPart.VISUAL_ID:
				return new TriggerEditPart(view);

			case TriggerNameEditPart.VISUAL_ID:
				return new TriggerNameEditPart(view);

			case ClassEditPart.VISUAL_ID:
				return new ClassEditPart(view);

			case ClassNameInterfcEditPart.VISUAL_ID:
				return new ClassNameInterfcEditPart(view);

			case CdwellformednessEditPart.VISUAL_ID:
				return new CdwellformednessEditPart(view);

			case CdwellformednessSourceTargetEditPart.VISUAL_ID:
				return new CdwellformednessSourceTargetEditPart(view);

			case StateEditPart.VISUAL_ID:
				return new StateEditPart(view);

			case StateNameEntryEditPart.VISUAL_ID:
				return new StateNameEntryEditPart(view);

			case ScwellformednessEditPart.VISUAL_ID:
				return new ScwellformednessEditPart(view);

			case ScwellformednessSourceTargetEditPart.VISUAL_ID:
				return new ScwellformednessSourceTargetEditPart(view);

			case ChoiceModelChoiceModelChCompartmentEditPart.VISUAL_ID:
				return new ChoiceModelChoiceModelChCompartmentEditPart(view);

			case ChoiceModelChoiceModelConstraintCompartmentEditPart.VISUAL_ID:
				return new ChoiceModelChoiceModelConstraintCompartmentEditPart(view);

			case FeatureModelFeatureModelFeatureCompartmentEditPart.VISUAL_ID:
				return new FeatureModelFeatureModelFeatureCompartmentEditPart(view);

			case FeatureModelFeatureModelConstraintCompartmentEditPart.VISUAL_ID:
				return new FeatureModelFeatureModelConstraintCompartmentEditPart(view);

			case FeatureMappingFeatureMappingFmapCompartmentEditPart.VISUAL_ID:
				return new FeatureMappingFeatureMappingFmapCompartmentEditPart(view);

			case FeatMappingFeatMappingMapfromCompartmentEditPart.VISUAL_ID:
				return new FeatMappingFeatMappingMapfromCompartmentEditPart(view);

			case FeatMappingFeatMappingMaptoCompartmentEditPart.VISUAL_ID:
				return new FeatMappingFeatMappingMaptoCompartmentEditPart(view);

			case DecisionMappingDecisionMappingDecisionMapCompartmentEditPart.VISUAL_ID:
				return new DecisionMappingDecisionMappingDecisionMapCompartmentEditPart(view);

			case DecMappingDecMappingMapfromCompartmentEditPart.VISUAL_ID:
				return new DecMappingDecMappingMapfromCompartmentEditPart(view);

			case DecMappingDecMappingMaptoEntityCompartmentEditPart.VISUAL_ID:
				return new DecMappingDecMappingMaptoEntityCompartmentEditPart(view);

			case VariablsVariablsInterCompartmentEditPart.VISUAL_ID:
				return new VariablsVariablsInterCompartmentEditPart(view);

			case VariablsVariablsTransCompartmentEditPart.VISUAL_ID:
				return new VariablsVariablsTransCompartmentEditPart(view);

			case VariablsVariablsGrdCompartmentEditPart.VISUAL_ID:
				return new VariablsVariablsGrdCompartmentEditPart(view);

			case VariablsVariablsLabelCompartmentEditPart.VISUAL_ID:
				return new VariablsVariablsLabelCompartmentEditPart(view);

			case VariablsVariablsEntryCompartmentEditPart.VISUAL_ID:
				return new VariablsVariablsEntryCompartmentEditPart(view);

			case VariablsVariablsTriggerCompartmentEditPart.VISUAL_ID:
				return new VariablsVariablsTriggerCompartmentEditPart(view);

			case ClassDiagramClassDiagramClassCompartmentEditPart.VISUAL_ID:
				return new ClassDiagramClassDiagramClassCompartmentEditPart(view);

			case ClassDiagramClassDiagramWfCompartmentEditPart.VISUAL_ID:
				return new ClassDiagramClassDiagramWfCompartmentEditPart(view);

			case StateChartStateChartStateCompartmentEditPart.VISUAL_ID:
				return new StateChartStateChartStateCompartmentEditPart(view);

			case StateChartStateChartWfCompartmentEditPart.VISUAL_ID:
				return new StateChartStateChartWfCompartmentEditPart(view);

			case TransitionEditPart.VISUAL_ID:
				return new TransitionEditPart(view);

			case TransitionNameTriggerGrdTransitioEditPart.VISUAL_ID:
				return new TransitionNameTriggerGrdTransitioEditPart(view);

			case RelationshipEditPart.VISUAL_ID:
				return new RelationshipEditPart(view);

			case RelationshipNameLabInMulOutMulEditPart.VISUAL_ID:
				return new RelationshipNameLabInMulOutMulEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	* @generated
	*/
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	* @generated
	*/
	public static CellEditorLocator getTextCellEditorLocator(ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE.getTextCellEditorLocator(source);
	}

}
