/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.ClassDiagramCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class DomainModelDomainModelCdCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public DomainModelDomainModelCdCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.DomainModel_3003);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.ClassDiagram_3005 == req.getElementType()) {
			return getGEFWrapper(new ClassDiagramCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
