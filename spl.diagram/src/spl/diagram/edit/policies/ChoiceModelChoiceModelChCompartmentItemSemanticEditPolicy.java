/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.ChoiceCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class ChoiceModelChoiceModelChCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public ChoiceModelChoiceModelChCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.ChoiceModel_2001);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Choice_3001 == req.getElementType()) {
			return getGEFWrapper(new ChoiceCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
