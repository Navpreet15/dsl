/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.TriggerCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class VariablsVariablsTriggerCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public VariablsVariablsTriggerCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.Variabls_2009);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Trigger_3017 == req.getElementType()) {
			return getGEFWrapper(new TriggerCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
