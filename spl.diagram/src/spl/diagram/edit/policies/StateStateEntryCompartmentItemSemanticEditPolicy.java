/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.EntryCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class StateStateEntryCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public StateStateEntryCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.State_3008);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Entry_3009 == req.getElementType()) {
			return getGEFWrapper(new EntryCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
