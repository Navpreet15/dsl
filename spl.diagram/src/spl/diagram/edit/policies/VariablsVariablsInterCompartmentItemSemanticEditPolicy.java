/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.InterfcCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class VariablsVariablsInterCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public VariablsVariablsInterCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.Variabls_2009);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Interfc_3012 == req.getElementType()) {
			return getGEFWrapper(new InterfcCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
