/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.FeatureCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class FeatureModelFeatureModelFeatureCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public FeatureModelFeatureModelFeatureCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.FeatureModel_2011);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Feature_3004 == req.getElementType()) {
			return getGEFWrapper(new FeatureCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
