/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.MapdecCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class DecMappingDecMappingMapfromCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public DecMappingDecMappingMapfromCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.DecMapping_3022);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Mapdec_3023 == req.getElementType()) {
			return getGEFWrapper(new MapdecCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
