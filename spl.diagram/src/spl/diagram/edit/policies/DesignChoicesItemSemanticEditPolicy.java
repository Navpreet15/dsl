/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

import spl.diagram.edit.commands.ChoiceModelCreateCommand;
import spl.diagram.edit.commands.ClassDiagramCreateCommand;
import spl.diagram.edit.commands.DecisionMappingCreateCommand;
import spl.diagram.edit.commands.FeatureMappingCreateCommand;
import spl.diagram.edit.commands.FeatureModelCreateCommand;
import spl.diagram.edit.commands.PropertyCreateCommand;
import spl.diagram.edit.commands.StateChartCreateCommand;
import spl.diagram.edit.commands.VariablsCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class DesignChoicesItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public DesignChoicesItemSemanticEditPolicy() {
		super(SplElementTypes.DesignChoices_1000);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.ChoiceModel_2001 == req.getElementType()) {
			return getGEFWrapper(new ChoiceModelCreateCommand(req));
		}
		if (SplElementTypes.FeatureModel_2011 == req.getElementType()) {
			return getGEFWrapper(new FeatureModelCreateCommand(req));
		}
		if (SplElementTypes.FeatureMapping_2005 == req.getElementType()) {
			return getGEFWrapper(new FeatureMappingCreateCommand(req));
		}
		if (SplElementTypes.DecisionMapping_2006 == req.getElementType()) {
			return getGEFWrapper(new DecisionMappingCreateCommand(req));
		}
		if (SplElementTypes.Property_2008 == req.getElementType()) {
			return getGEFWrapper(new PropertyCreateCommand(req));
		}
		if (SplElementTypes.Variabls_2009 == req.getElementType()) {
			return getGEFWrapper(new VariablsCreateCommand(req));
		}
		if (SplElementTypes.ClassDiagram_2013 == req.getElementType()) {
			return getGEFWrapper(new ClassDiagramCreateCommand(req));
		}
		if (SplElementTypes.StateChart_2014 == req.getElementType()) {
			return getGEFWrapper(new StateChartCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	* @generated
	*/
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost()).getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	* @generated
	*/
	private static class DuplicateAnythingCommand extends DuplicateEObjectsCommand {

		/**
		* @generated
		*/
		public DuplicateAnythingCommand(TransactionalEditingDomain editingDomain, DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req.getElementsToBeDuplicated(), req.getAllDuplicatedElementsMap());
		}

	}

}
