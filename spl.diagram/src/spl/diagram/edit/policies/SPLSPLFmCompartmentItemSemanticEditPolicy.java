/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.FeatureModelCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class SPLSPLFmCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public SPLSPLFmCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.SPL_2002);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.FeatureModel_3002 == req.getElementType()) {
			return getGEFWrapper(new FeatureModelCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
