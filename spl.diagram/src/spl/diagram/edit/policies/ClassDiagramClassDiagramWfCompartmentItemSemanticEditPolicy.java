/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.CdwellformednessCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class ClassDiagramClassDiagramWfCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public ClassDiagramClassDiagramWfCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.ClassDiagram_2013);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Cdwellformedness_3010 == req.getElementType()) {
			return getGEFWrapper(new CdwellformednessCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
