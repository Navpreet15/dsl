/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.FeatMappingCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class FeatureMappingFeatureMappingFmapCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public FeatureMappingFeatureMappingFmapCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.FeatureMapping_2005);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.FeatMapping_3021 == req.getElementType()) {
			return getGEFWrapper(new FeatMappingCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
