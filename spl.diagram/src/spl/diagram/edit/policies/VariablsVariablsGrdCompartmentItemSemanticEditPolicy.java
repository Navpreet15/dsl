/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.GuardCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class VariablsVariablsGrdCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public VariablsVariablsGrdCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.Variabls_2009);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Guard_3014 == req.getElementType()) {
			return getGEFWrapper(new GuardCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
