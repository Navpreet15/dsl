/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.FMconstraintsCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class FeatureModelFeatureModelConstraintCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public FeatureModelFeatureModelConstraintCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.FeatureModel_2011);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.FMconstraints_3009 == req.getElementType()) {
			return getGEFWrapper(new FMconstraintsCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
