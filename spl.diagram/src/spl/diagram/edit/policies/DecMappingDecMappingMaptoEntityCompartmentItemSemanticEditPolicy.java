/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.MapToEntityCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class DecMappingDecMappingMaptoEntityCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public DecMappingDecMappingMaptoEntityCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.DecMapping_3022);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.MapToEntity_3024 == req.getElementType()) {
			return getGEFWrapper(new MapToEntityCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
