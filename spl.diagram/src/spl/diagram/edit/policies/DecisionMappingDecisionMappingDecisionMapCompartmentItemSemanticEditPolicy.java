/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.DecMappingCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class DecisionMappingDecisionMappingDecisionMapCompartmentItemSemanticEditPolicy
		extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public DecisionMappingDecisionMappingDecisionMapCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.DecisionMapping_2006);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.DecMapping_3022 == req.getElementType()) {
			return getGEFWrapper(new DecMappingCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
