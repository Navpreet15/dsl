/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.EntryCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class VariablsVariablsEntryCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public VariablsVariablsEntryCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.Variabls_2009);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Entry_3016 == req.getElementType()) {
			return getGEFWrapper(new EntryCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
