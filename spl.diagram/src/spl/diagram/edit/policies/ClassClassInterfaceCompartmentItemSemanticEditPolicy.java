/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.InterfaceCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class ClassClassInterfaceCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public ClassClassInterfaceCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.Class_3007);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Interface_3010 == req.getElementType()) {
			return getGEFWrapper(new InterfaceCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
