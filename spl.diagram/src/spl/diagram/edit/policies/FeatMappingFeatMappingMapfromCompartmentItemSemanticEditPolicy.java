/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.MapCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class FeatMappingFeatMappingMapfromCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public FeatMappingFeatMappingMapfromCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.FeatMapping_3021);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Map_3019 == req.getElementType()) {
			return getGEFWrapper(new MapCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
