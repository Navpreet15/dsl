/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.StateChartCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class DomainModelDomainModelScCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public DomainModelDomainModelScCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.DomainModel_3003);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.StateChart_3006 == req.getElementType()) {
			return getGEFWrapper(new StateChartCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
