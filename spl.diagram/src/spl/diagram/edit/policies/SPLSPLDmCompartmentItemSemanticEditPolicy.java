/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.DomainModelCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class SPLSPLDmCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public SPLSPLDmCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.SPL_2002);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.DomainModel_3003 == req.getElementType()) {
			return getGEFWrapper(new DomainModelCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
