/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.ClassCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class ClassDiagramClassDiagramClassCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public ClassDiagramClassDiagramClassCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.ClassDiagram_2013);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Class_3007 == req.getElementType()) {
			return getGEFWrapper(new ClassCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
