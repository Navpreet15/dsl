/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.MaptoCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class FeatMappingFeatMappingMaptoCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public FeatMappingFeatMappingMaptoCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.FeatMapping_3021);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Mapto_3020 == req.getElementType()) {
			return getGEFWrapper(new MaptoCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
