/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.CMconstraintsCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class ChoiceModelChoiceModelConstraintCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public ChoiceModelChoiceModelConstraintCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.ChoiceModel_2001);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.CMconstraints_3018 == req.getElementType()) {
			return getGEFWrapper(new CMconstraintsCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
