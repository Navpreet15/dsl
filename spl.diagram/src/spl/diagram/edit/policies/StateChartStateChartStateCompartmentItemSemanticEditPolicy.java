/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.StateCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class StateChartStateChartStateCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public StateChartStateChartStateCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.StateChart_2014);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.State_3008 == req.getElementType()) {
			return getGEFWrapper(new StateCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
