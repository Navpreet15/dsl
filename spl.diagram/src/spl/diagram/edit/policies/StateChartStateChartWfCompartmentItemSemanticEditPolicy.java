/*
* 
*/
package spl.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import spl.diagram.edit.commands.ScwellformednessCreateCommand;
import spl.diagram.providers.SplElementTypes;

/**
 * @generated
 */
public class StateChartStateChartWfCompartmentItemSemanticEditPolicy extends SplBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public StateChartStateChartWfCompartmentItemSemanticEditPolicy() {
		super(SplElementTypes.StateChart_2014);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (SplElementTypes.Scwellformedness_3011 == req.getElementType()) {
			return getGEFWrapper(new ScwellformednessCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
