/*
 * 
 */
package spl.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import spl.Class;
import spl.ClassDiagram;
import spl.Relationship;
import spl.diagram.edit.policies.SplBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class RelationshipReorientCommand extends EditElementCommand {

	/**
	* @generated
	*/
	private final int reorientDirection;

	/**
	* @generated
	*/
	private final EObject oldEnd;

	/**
	* @generated
	*/
	private final EObject newEnd;

	/**
	* @generated
	*/
	public RelationshipReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	* @generated
	*/
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof Relationship) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	* @generated
	*/
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof Class && newEnd instanceof Class)) {
			return false;
		}
		Class target = getLink().getTo();
		if (!(getLink().eContainer() instanceof ClassDiagram)) {
			return false;
		}
		ClassDiagram container = (ClassDiagram) getLink().eContainer();
		return SplBaseItemSemanticEditPolicy.getLinkConstraints().canExistRelationship_4005(container, getLink(),
				getNewSource(), target);
	}

	/**
	* @generated
	*/
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof Class && newEnd instanceof Class)) {
			return false;
		}
		Class source = getLink().getFrom();
		if (!(getLink().eContainer() instanceof ClassDiagram)) {
			return false;
		}
		ClassDiagram container = (ClassDiagram) getLink().eContainer();
		return SplBaseItemSemanticEditPolicy.getLinkConstraints().canExistRelationship_4005(container, getLink(),
				source, getNewTarget());
	}

	/**
	* @generated
	*/
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException("Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	* @generated
	*/
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setFrom(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	* @generated
	*/
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTo(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	* @generated
	*/
	protected Relationship getLink() {
		return (Relationship) getElementToEdit();
	}

	/**
	* @generated
	*/
	protected Class getOldSource() {
		return (Class) oldEnd;
	}

	/**
	* @generated
	*/
	protected Class getNewSource() {
		return (Class) newEnd;
	}

	/**
	* @generated
	*/
	protected Class getOldTarget() {
		return (Class) oldEnd;
	}

	/**
	* @generated
	*/
	protected Class getNewTarget() {
		return (Class) newEnd;
	}
}
