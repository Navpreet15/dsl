/*
* 
*/
package spl.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

import spl.DesignChoices;
import spl.diagram.edit.parts.CMconstraintsEditPart;
import spl.diagram.edit.parts.CMconstraintsOprEditPart;
import spl.diagram.edit.parts.CdwellformednessEditPart;
import spl.diagram.edit.parts.CdwellformednessSourceTargetEditPart;
import spl.diagram.edit.parts.ChoiceEditPart;
import spl.diagram.edit.parts.ChoiceModelEditPart;
import spl.diagram.edit.parts.ChoiceNameVarTypeEditPart;
import spl.diagram.edit.parts.ClassDiagramEditPart;
import spl.diagram.edit.parts.ClassEditPart;
import spl.diagram.edit.parts.ClassNameInterfcEditPart;
import spl.diagram.edit.parts.DecMappingEditPart;
import spl.diagram.edit.parts.DecMappingOprLHSOprRHSEditPart;
import spl.diagram.edit.parts.DecisionMappingEditPart;
import spl.diagram.edit.parts.DesignChoicesEditPart;
import spl.diagram.edit.parts.EntryEditPart;
import spl.diagram.edit.parts.EntryNameEditPart;
import spl.diagram.edit.parts.FMconstraintsEditPart;
import spl.diagram.edit.parts.FMconstraintsOprEditPart;
import spl.diagram.edit.parts.FeatMappingEditPart;
import spl.diagram.edit.parts.FeatMappingOprLHSOprRHSEditPart;
import spl.diagram.edit.parts.FeatureEditPart;
import spl.diagram.edit.parts.FeatureMappingEditPart;
import spl.diagram.edit.parts.FeatureModelEditPart;
import spl.diagram.edit.parts.FeatureNameVartypeEditPart;
import spl.diagram.edit.parts.GuardEditPart;
import spl.diagram.edit.parts.GuardNameEditPart;
import spl.diagram.edit.parts.InterfcEditPart;
import spl.diagram.edit.parts.InterfcNameEditPart;
import spl.diagram.edit.parts.LabelEditPart;
import spl.diagram.edit.parts.LabelNameEditPart;
import spl.diagram.edit.parts.MapEditPart;
import spl.diagram.edit.parts.MapPresenceEditPart;
import spl.diagram.edit.parts.MapToEntityEditPart;
import spl.diagram.edit.parts.MapToEntityPresenceEditPart;
import spl.diagram.edit.parts.MapdecEditPart;
import spl.diagram.edit.parts.MapdecPresenceEditPart;
import spl.diagram.edit.parts.MaptoEditPart;
import spl.diagram.edit.parts.MaptoPresenceEditPart;
import spl.diagram.edit.parts.PropertyEditPart;
import spl.diagram.edit.parts.PropertyNameTextEditPart;
import spl.diagram.edit.parts.RelationshipEditPart;
import spl.diagram.edit.parts.RelationshipNameLabInMulOutMulEditPart;
import spl.diagram.edit.parts.ScwellformednessEditPart;
import spl.diagram.edit.parts.ScwellformednessSourceTargetEditPart;
import spl.diagram.edit.parts.StateChartEditPart;
import spl.diagram.edit.parts.StateChartNameEditPart;
import spl.diagram.edit.parts.StateEditPart;
import spl.diagram.edit.parts.StateNameEntryEditPart;
import spl.diagram.edit.parts.TransitionEditPart;
import spl.diagram.edit.parts.TransitionNameTriggerGrdTransitioEditPart;
import spl.diagram.edit.parts.TransitionalBehaviorEditPart;
import spl.diagram.edit.parts.TransitionalBehaviorNameEditPart;
import spl.diagram.edit.parts.TriggerEditPart;
import spl.diagram.edit.parts.TriggerNameEditPart;
import spl.diagram.edit.parts.VariablsEditPart;
import spl.diagram.part.SplDiagramEditorPlugin;
import spl.diagram.part.SplVisualIDRegistry;
import spl.diagram.providers.SplElementTypes;
import spl.diagram.providers.SplParserProvider;

/**
 * @generated
 */
public class SplNavigatorLabelProvider extends LabelProvider implements ICommonLabelProvider, ITreePathLabelProvider {

	/**
	* @generated
	*/
	static {
		SplDiagramEditorPlugin.getInstance().getImageRegistry().put("Navigator?UnknownElement", //$NON-NLS-1$
				ImageDescriptor.getMissingImageDescriptor());
		SplDiagramEditorPlugin.getInstance().getImageRegistry().put("Navigator?ImageNotFound", //$NON-NLS-1$
				ImageDescriptor.getMissingImageDescriptor());
	}

	/**
	* @generated
	*/
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof SplNavigatorItem && !isOwnView(((SplNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	* @generated
	*/
	public Image getImage(Object element) {
		if (element instanceof SplNavigatorGroup) {
			SplNavigatorGroup group = (SplNavigatorGroup) element;
			return SplDiagramEditorPlugin.getInstance().getBundledImage(group.getIcon());
		}

		if (element instanceof SplNavigatorItem) {
			SplNavigatorItem navigatorItem = (SplNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getImage(view);
			}
		}

		return super.getImage(element);
	}

	/**
	* @generated
	*/
	public Image getImage(View view) {
		switch (SplVisualIDRegistry.getVisualID(view)) {
		case DesignChoicesEditPart.VISUAL_ID:
			return getImage("Navigator?Diagram?http://www.example.org/spl?DesignChoices", //$NON-NLS-1$
					SplElementTypes.DesignChoices_1000);
		case ChoiceModelEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://www.example.org/spl?ChoiceModel", //$NON-NLS-1$
					SplElementTypes.ChoiceModel_2001);
		case FeatureMappingEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://www.example.org/spl?FeatureMapping", //$NON-NLS-1$
					SplElementTypes.FeatureMapping_2005);
		case DecisionMappingEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://www.example.org/spl?DecisionMapping", //$NON-NLS-1$
					SplElementTypes.DecisionMapping_2006);
		case PropertyEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://www.example.org/spl?Property", //$NON-NLS-1$
					SplElementTypes.Property_2008);
		case VariablsEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://www.example.org/spl?variabls", //$NON-NLS-1$
					SplElementTypes.Variabls_2009);
		case FeatureModelEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://www.example.org/spl?FeatureModel", //$NON-NLS-1$
					SplElementTypes.FeatureModel_2011);
		case ClassDiagramEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://www.example.org/spl?ClassDiagram", //$NON-NLS-1$
					SplElementTypes.ClassDiagram_2013);
		case StateChartEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://www.example.org/spl?StateChart", //$NON-NLS-1$
					SplElementTypes.StateChart_2014);
		case ChoiceEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?Choice", SplElementTypes.Choice_3001); //$NON-NLS-1$
		case FeatureEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?Feature", SplElementTypes.Feature_3004); //$NON-NLS-1$
		case ClassEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?Class", SplElementTypes.Class_3007); //$NON-NLS-1$
		case StateEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?State", SplElementTypes.State_3008); //$NON-NLS-1$
		case FMconstraintsEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?FMconstraints", //$NON-NLS-1$
					SplElementTypes.FMconstraints_3009);
		case CdwellformednessEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?cdwellformedness", //$NON-NLS-1$
					SplElementTypes.Cdwellformedness_3010);
		case ScwellformednessEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?scwellformedness", //$NON-NLS-1$
					SplElementTypes.Scwellformedness_3011);
		case InterfcEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?Interfc", SplElementTypes.Interfc_3012); //$NON-NLS-1$
		case TransitionalBehaviorEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?TransitionalBehavior", //$NON-NLS-1$
					SplElementTypes.TransitionalBehavior_3013);
		case GuardEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?Guard", SplElementTypes.Guard_3014); //$NON-NLS-1$
		case LabelEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?Label", SplElementTypes.Label_3015); //$NON-NLS-1$
		case EntryEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?Entry", SplElementTypes.Entry_3016); //$NON-NLS-1$
		case TriggerEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?Trigger", SplElementTypes.Trigger_3017); //$NON-NLS-1$
		case CMconstraintsEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?CMconstraints", //$NON-NLS-1$
					SplElementTypes.CMconstraints_3018);
		case MapEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?Map", SplElementTypes.Map_3019); //$NON-NLS-1$
		case MaptoEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?Mapto", SplElementTypes.Mapto_3020); //$NON-NLS-1$
		case FeatMappingEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?featMapping", SplElementTypes.FeatMapping_3021); //$NON-NLS-1$
		case DecMappingEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?DecMapping", SplElementTypes.DecMapping_3022); //$NON-NLS-1$
		case MapdecEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?Mapdec", SplElementTypes.Mapdec_3023); //$NON-NLS-1$
		case MapToEntityEditPart.VISUAL_ID:
			return getImage("Navigator?Node?http://www.example.org/spl?MapToEntity", SplElementTypes.MapToEntity_3024); //$NON-NLS-1$
		case TransitionEditPart.VISUAL_ID:
			return getImage("Navigator?Link?http://www.example.org/spl?Transition", SplElementTypes.Transition_4001); //$NON-NLS-1$
		case RelationshipEditPart.VISUAL_ID:
			return getImage("Navigator?Link?http://www.example.org/spl?Relationship", //$NON-NLS-1$
					SplElementTypes.Relationship_4005);
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = SplDiagramEditorPlugin.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null && SplElementTypes.isKnownElementType(elementType)) {
			image = SplElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	* @generated
	*/
	public String getText(Object element) {
		if (element instanceof SplNavigatorGroup) {
			SplNavigatorGroup group = (SplNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof SplNavigatorItem) {
			SplNavigatorItem navigatorItem = (SplNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getText(view);
			}
		}

		return super.getText(element);
	}

	/**
	* @generated
	*/
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (SplVisualIDRegistry.getVisualID(view)) {
		case DesignChoicesEditPart.VISUAL_ID:
			return getDesignChoices_1000Text(view);
		case ChoiceModelEditPart.VISUAL_ID:
			return getChoiceModel_2001Text(view);
		case FeatureMappingEditPart.VISUAL_ID:
			return getFeatureMapping_2005Text(view);
		case DecisionMappingEditPart.VISUAL_ID:
			return getDecisionMapping_2006Text(view);
		case PropertyEditPart.VISUAL_ID:
			return getProperty_2008Text(view);
		case VariablsEditPart.VISUAL_ID:
			return getVariabls_2009Text(view);
		case FeatureModelEditPart.VISUAL_ID:
			return getFeatureModel_2011Text(view);
		case ClassDiagramEditPart.VISUAL_ID:
			return getClassDiagram_2013Text(view);
		case StateChartEditPart.VISUAL_ID:
			return getStateChart_2014Text(view);
		case ChoiceEditPart.VISUAL_ID:
			return getChoice_3001Text(view);
		case FeatureEditPart.VISUAL_ID:
			return getFeature_3004Text(view);
		case ClassEditPart.VISUAL_ID:
			return getClass_3007Text(view);
		case StateEditPart.VISUAL_ID:
			return getState_3008Text(view);
		case FMconstraintsEditPart.VISUAL_ID:
			return getFMconstraints_3009Text(view);
		case CdwellformednessEditPart.VISUAL_ID:
			return getCdwellformedness_3010Text(view);
		case ScwellformednessEditPart.VISUAL_ID:
			return getScwellformedness_3011Text(view);
		case InterfcEditPart.VISUAL_ID:
			return getInterfc_3012Text(view);
		case TransitionalBehaviorEditPart.VISUAL_ID:
			return getTransitionalBehavior_3013Text(view);
		case GuardEditPart.VISUAL_ID:
			return getGuard_3014Text(view);
		case LabelEditPart.VISUAL_ID:
			return getLabel_3015Text(view);
		case EntryEditPart.VISUAL_ID:
			return getEntry_3016Text(view);
		case TriggerEditPart.VISUAL_ID:
			return getTrigger_3017Text(view);
		case CMconstraintsEditPart.VISUAL_ID:
			return getCMconstraints_3018Text(view);
		case MapEditPart.VISUAL_ID:
			return getMap_3019Text(view);
		case MaptoEditPart.VISUAL_ID:
			return getMapto_3020Text(view);
		case FeatMappingEditPart.VISUAL_ID:
			return getFeatMapping_3021Text(view);
		case DecMappingEditPart.VISUAL_ID:
			return getDecMapping_3022Text(view);
		case MapdecEditPart.VISUAL_ID:
			return getMapdec_3023Text(view);
		case MapToEntityEditPart.VISUAL_ID:
			return getMapToEntity_3024Text(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001Text(view);
		case RelationshipEditPart.VISUAL_ID:
			return getRelationship_4005Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	* @generated
	*/
	private String getDesignChoices_1000Text(View view) {
		DesignChoices domainModelElement = (DesignChoices) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			SplDiagramEditorPlugin.getInstance().logError("No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getChoiceModel_2001Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private String getFeatureMapping_2005Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private String getDecisionMapping_2006Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private String getProperty_2008Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Property_2008,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(PropertyNameTextEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5022); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getVariabls_2009Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private String getFeatureModel_2011Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private String getClassDiagram_2013Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private String getStateChart_2014Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.StateChart_2014,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(StateChartNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5031); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getChoice_3001Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Choice_3001,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(ChoiceNameVarTypeEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getFeature_3004Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Feature_3004,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(FeatureNameVartypeEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5006); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getClass_3007Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Class_3007,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(ClassNameInterfcEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getState_3008Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.State_3008,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(StateNameEntryEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5010); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getFMconstraints_3009Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.FMconstraints_3009,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(FMconstraintsOprEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5019); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getCdwellformedness_3010Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Cdwellformedness_3010,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(CdwellformednessSourceTargetEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5020); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getScwellformedness_3011Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Scwellformedness_3011,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(ScwellformednessSourceTargetEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5021); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getInterfc_3012Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Interfc_3012,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(InterfcNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5023); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getTransitionalBehavior_3013Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.TransitionalBehavior_3013,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(TransitionalBehaviorNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5024); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getGuard_3014Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Guard_3014,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(GuardNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5025); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getLabel_3015Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Label_3015,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(LabelNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5026); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getEntry_3016Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Entry_3016,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(EntryNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5027); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getTrigger_3017Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Trigger_3017,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(TriggerNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5028); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getCMconstraints_3018Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.CMconstraints_3018,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(CMconstraintsOprEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5032); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getMap_3019Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Map_3019,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(MapPresenceEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5034); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getMapto_3020Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Mapto_3020,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(MaptoPresenceEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5035); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getFeatMapping_3021Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.FeatMapping_3021,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(FeatMappingOprLHSOprRHSEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5036); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getDecMapping_3022Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.DecMapping_3022,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(DecMappingOprLHSOprRHSEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5039); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getMapdec_3023Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Mapdec_3023,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(MapdecPresenceEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5037); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getMapToEntity_3024Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.MapToEntity_3024,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(MapToEntityPresenceEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5038); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getTransition_4001Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Transition_4001,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(TransitionNameTriggerGrdTransitioEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 6009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getRelationship_4005Text(View view) {
		IParser parser = SplParserProvider.getParser(SplElementTypes.Relationship_4005,
				view.getElement() != null ? view.getElement() : view,
				SplVisualIDRegistry.getType(RelationshipNameLabInMulOutMulEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			SplDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 6010); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	* @generated
	*/
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	* @generated
	*/
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	* @generated
	*/
	public void restoreState(IMemento aMemento) {
	}

	/**
	* @generated
	*/
	public void saveState(IMemento aMemento) {
	}

	/**
	* @generated
	*/
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	* @generated
	*/
	private boolean isOwnView(View view) {
		return DesignChoicesEditPart.MODEL_ID.equals(SplVisualIDRegistry.getModelID(view));
	}

}
