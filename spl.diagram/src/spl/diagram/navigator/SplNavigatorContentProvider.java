/*
* 
*/
package spl.diagram.navigator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonContentProvider;

import spl.diagram.edit.parts.CMconstraintsEditPart;
import spl.diagram.edit.parts.CdwellformednessEditPart;
import spl.diagram.edit.parts.ChoiceEditPart;
import spl.diagram.edit.parts.ChoiceModelChoiceModelChCompartmentEditPart;
import spl.diagram.edit.parts.ChoiceModelChoiceModelConstraintCompartmentEditPart;
import spl.diagram.edit.parts.ChoiceModelEditPart;
import spl.diagram.edit.parts.ClassDiagramClassDiagramClassCompartmentEditPart;
import spl.diagram.edit.parts.ClassDiagramClassDiagramWfCompartmentEditPart;
import spl.diagram.edit.parts.ClassDiagramEditPart;
import spl.diagram.edit.parts.ClassEditPart;
import spl.diagram.edit.parts.DecMappingDecMappingMapfromCompartmentEditPart;
import spl.diagram.edit.parts.DecMappingDecMappingMaptoEntityCompartmentEditPart;
import spl.diagram.edit.parts.DecMappingEditPart;
import spl.diagram.edit.parts.DecisionMappingDecisionMappingDecisionMapCompartmentEditPart;
import spl.diagram.edit.parts.DecisionMappingEditPart;
import spl.diagram.edit.parts.DesignChoicesEditPart;
import spl.diagram.edit.parts.EntryEditPart;
import spl.diagram.edit.parts.FMconstraintsEditPart;
import spl.diagram.edit.parts.FeatMappingEditPart;
import spl.diagram.edit.parts.FeatMappingFeatMappingMapfromCompartmentEditPart;
import spl.diagram.edit.parts.FeatMappingFeatMappingMaptoCompartmentEditPart;
import spl.diagram.edit.parts.FeatureEditPart;
import spl.diagram.edit.parts.FeatureMappingEditPart;
import spl.diagram.edit.parts.FeatureMappingFeatureMappingFmapCompartmentEditPart;
import spl.diagram.edit.parts.FeatureModelEditPart;
import spl.diagram.edit.parts.FeatureModelFeatureModelConstraintCompartmentEditPart;
import spl.diagram.edit.parts.FeatureModelFeatureModelFeatureCompartmentEditPart;
import spl.diagram.edit.parts.GuardEditPart;
import spl.diagram.edit.parts.InterfcEditPart;
import spl.diagram.edit.parts.LabelEditPart;
import spl.diagram.edit.parts.MapEditPart;
import spl.diagram.edit.parts.MapToEntityEditPart;
import spl.diagram.edit.parts.MapdecEditPart;
import spl.diagram.edit.parts.MaptoEditPart;
import spl.diagram.edit.parts.PropertyEditPart;
import spl.diagram.edit.parts.RelationshipEditPart;
import spl.diagram.edit.parts.ScwellformednessEditPart;
import spl.diagram.edit.parts.StateChartEditPart;
import spl.diagram.edit.parts.StateChartStateChartStateCompartmentEditPart;
import spl.diagram.edit.parts.StateChartStateChartWfCompartmentEditPart;
import spl.diagram.edit.parts.StateEditPart;
import spl.diagram.edit.parts.TransitionEditPart;
import spl.diagram.edit.parts.TransitionalBehaviorEditPart;
import spl.diagram.edit.parts.TriggerEditPart;
import spl.diagram.edit.parts.VariablsEditPart;
import spl.diagram.edit.parts.VariablsVariablsEntryCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsGrdCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsInterCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsLabelCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsTransCompartmentEditPart;
import spl.diagram.edit.parts.VariablsVariablsTriggerCompartmentEditPart;
import spl.diagram.part.Messages;
import spl.diagram.part.SplVisualIDRegistry;

/**
 * @generated
 */
public class SplNavigatorContentProvider implements ICommonContentProvider {

	/**
	* @generated
	*/
	private static final Object[] EMPTY_ARRAY = new Object[0];

	/**
	* @generated
	*/
	private Viewer myViewer;

	/**
	* @generated
	*/
	private AdapterFactoryEditingDomain myEditingDomain;

	/**
	* @generated
	*/
	private WorkspaceSynchronizer myWorkspaceSynchronizer;

	/**
	* @generated
	*/
	private Runnable myViewerRefreshRunnable;

	/**
	* @generated
	*/
	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	public SplNavigatorContentProvider() {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE.createEditingDomain();
		myEditingDomain = (AdapterFactoryEditingDomain) editingDomain;
		myEditingDomain.setResourceToReadOnlyMap(new HashMap() {
			public Object get(Object key) {
				if (!containsKey(key)) {
					put(key, Boolean.TRUE);
				}
				return super.get(key);
			}
		});
		myViewerRefreshRunnable = new Runnable() {
			public void run() {
				if (myViewer != null) {
					myViewer.refresh();
				}
			}
		};
		myWorkspaceSynchronizer = new WorkspaceSynchronizer(editingDomain, new WorkspaceSynchronizer.Delegate() {
			public void dispose() {
			}

			public boolean handleResourceChanged(final Resource resource) {
				unloadAllResources();
				asyncRefresh();
				return true;
			}

			public boolean handleResourceDeleted(Resource resource) {
				unloadAllResources();
				asyncRefresh();
				return true;
			}

			public boolean handleResourceMoved(Resource resource, final URI newURI) {
				unloadAllResources();
				asyncRefresh();
				return true;
			}
		});
	}

	/**
	* @generated
	*/
	public void dispose() {
		myWorkspaceSynchronizer.dispose();
		myWorkspaceSynchronizer = null;
		myViewerRefreshRunnable = null;
		myViewer = null;
		unloadAllResources();
		((TransactionalEditingDomain) myEditingDomain).dispose();
		myEditingDomain = null;
	}

	/**
	* @generated
	*/
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		myViewer = viewer;
	}

	/**
	* @generated
	*/
	void unloadAllResources() {
		for (Resource nextResource : myEditingDomain.getResourceSet().getResources()) {
			nextResource.unload();
		}
	}

	/**
	* @generated
	*/
	void asyncRefresh() {
		if (myViewer != null && !myViewer.getControl().isDisposed()) {
			myViewer.getControl().getDisplay().asyncExec(myViewerRefreshRunnable);
		}
	}

	/**
	* @generated
	*/
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	/**
	* @generated
	*/
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	* @generated
	*/
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	* @generated
	*/
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof IFile) {
			IFile file = (IFile) parentElement;
			URI fileURI = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
			Resource resource = myEditingDomain.getResourceSet().getResource(fileURI, true);
			ArrayList<SplNavigatorItem> result = new ArrayList<SplNavigatorItem>();
			ArrayList<View> topViews = new ArrayList<View>(resource.getContents().size());
			for (EObject o : resource.getContents()) {
				if (o instanceof View) {
					topViews.add((View) o);
				}
			}
			result.addAll(
					createNavigatorItems(selectViewsByType(topViews, DesignChoicesEditPart.MODEL_ID), file, false));
			return result.toArray();
		}

		if (parentElement instanceof SplNavigatorGroup) {
			SplNavigatorGroup group = (SplNavigatorGroup) parentElement;
			return group.getChildren();
		}

		if (parentElement instanceof SplNavigatorItem) {
			SplNavigatorItem navigatorItem = (SplNavigatorItem) parentElement;
			if (navigatorItem.isLeaf() || !isOwnView(navigatorItem.getView())) {
				return EMPTY_ARRAY;
			}
			return getViewChildren(navigatorItem.getView(), parentElement);
		}

		/*
		* Due to plugin.xml restrictions this code will be called only for views representing
		* shortcuts to this diagram elements created on other diagrams. 
		*/
		if (parentElement instanceof IAdaptable) {
			View view = (View) ((IAdaptable) parentElement).getAdapter(View.class);
			if (view != null) {
				return getViewChildren(view, parentElement);
			}
		}

		return EMPTY_ARRAY;
	}

	/**
	* @generated
	*/
	private Object[] getViewChildren(View view, Object parentElement) {
		switch (SplVisualIDRegistry.getVisualID(view)) {

		case DesignChoicesEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			result.addAll(getForeignShortcuts((Diagram) view, parentElement));
			Diagram sv = (Diagram) view;
			SplNavigatorGroup links = new SplNavigatorGroup(Messages.NavigatorGroupName_DesignChoices_1000_links,
					"icons/linksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(ChoiceModelEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(FeatureModelEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(FeatureMappingEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(DecisionMappingEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(PropertyEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(VariablsEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(ClassDiagramEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(StateChartEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(TransitionEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(RelationshipEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			if (!links.isEmpty()) {
				result.add(links);
			}
			return result.toArray();
		}

		case ChoiceModelEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Node sv = (Node) view;
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(ChoiceModelChoiceModelChCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(ChoiceEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(ChoiceModelChoiceModelConstraintCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					SplVisualIDRegistry.getType(CMconstraintsEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			return result.toArray();
		}

		case FeatureMappingEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Node sv = (Node) view;
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(FeatureMappingFeatureMappingFmapCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					SplVisualIDRegistry.getType(FeatMappingEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			return result.toArray();
		}

		case DecisionMappingEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Node sv = (Node) view;
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv), SplVisualIDRegistry
					.getType(DecisionMappingDecisionMappingDecisionMapCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					SplVisualIDRegistry.getType(DecMappingEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			return result.toArray();
		}

		case VariablsEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Node sv = (Node) view;
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(VariablsVariablsInterCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(InterfcEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(VariablsVariablsTransCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					SplVisualIDRegistry.getType(TransitionalBehaviorEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(VariablsVariablsGrdCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(GuardEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(VariablsVariablsLabelCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(LabelEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(VariablsVariablsEntryCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(EntryEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(VariablsVariablsTriggerCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(TriggerEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			return result.toArray();
		}

		case FeatureModelEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Node sv = (Node) view;
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(FeatureModelFeatureModelFeatureCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(FeatureEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(FeatureModelFeatureModelConstraintCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					SplVisualIDRegistry.getType(FMconstraintsEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			return result.toArray();
		}

		case ClassDiagramEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Node sv = (Node) view;
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(ClassDiagramClassDiagramClassCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(ClassDiagramClassDiagramWfCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					SplVisualIDRegistry.getType(CdwellformednessEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			return result.toArray();
		}

		case StateChartEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Node sv = (Node) view;
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(StateChartStateChartStateCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(StateEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(StateChartStateChartWfCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					SplVisualIDRegistry.getType(ScwellformednessEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			return result.toArray();
		}

		case ClassEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Node sv = (Node) view;
			SplNavigatorGroup incominglinks = new SplNavigatorGroup(
					Messages.NavigatorGroupName_Class_3007_incominglinks, "icons/incomingLinksNavigatorGroup.gif", //$NON-NLS-1$
					parentElement);
			SplNavigatorGroup outgoinglinks = new SplNavigatorGroup(
					Messages.NavigatorGroupName_Class_3007_outgoinglinks, "icons/outgoingLinksNavigatorGroup.gif", //$NON-NLS-1$
					parentElement);
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(RelationshipEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews, incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(RelationshipEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews, outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case StateEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Node sv = (Node) view;
			SplNavigatorGroup incominglinks = new SplNavigatorGroup(
					Messages.NavigatorGroupName_State_3008_incominglinks, "icons/incomingLinksNavigatorGroup.gif", //$NON-NLS-1$
					parentElement);
			SplNavigatorGroup outgoinglinks = new SplNavigatorGroup(
					Messages.NavigatorGroupName_State_3008_outgoinglinks, "icons/outgoingLinksNavigatorGroup.gif", //$NON-NLS-1$
					parentElement);
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(TransitionEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews, incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(TransitionEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews, outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case FeatMappingEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Node sv = (Node) view;
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(FeatMappingFeatMappingMapfromCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(MapEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(FeatMappingFeatMappingMaptoCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(MaptoEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			return result.toArray();
		}

		case DecMappingEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Node sv = (Node) view;
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(DecMappingDecMappingMapfromCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews, SplVisualIDRegistry.getType(MapdecEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(DecMappingDecMappingMaptoEntityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					SplVisualIDRegistry.getType(MapToEntityEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement, false));
			return result.toArray();
		}

		case TransitionEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			SplNavigatorGroup target = new SplNavigatorGroup(Messages.NavigatorGroupName_Transition_4001_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			SplNavigatorGroup source = new SplNavigatorGroup(Messages.NavigatorGroupName_Transition_4001_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(StateEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target, true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(StateEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source, true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case RelationshipEditPart.VISUAL_ID: {
			LinkedList<SplAbstractNavigatorItem> result = new LinkedList<SplAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			SplNavigatorGroup target = new SplNavigatorGroup(Messages.NavigatorGroupName_Relationship_4005_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			SplNavigatorGroup source = new SplNavigatorGroup(Messages.NavigatorGroupName_Relationship_4005_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target, true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					SplVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source, true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}
		}
		return EMPTY_ARRAY;
	}

	/**
	* @generated
	*/
	private Collection<View> getLinksSourceByType(Collection<Edge> edges, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeSource = nextEdge.getSource();
			if (type.equals(nextEdgeSource.getType()) && isOwnView(nextEdgeSource)) {
				result.add(nextEdgeSource);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getLinksTargetByType(Collection<Edge> edges, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeTarget = nextEdge.getTarget();
			if (type.equals(nextEdgeTarget.getType()) && isOwnView(nextEdgeTarget)) {
				result.add(nextEdgeTarget);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getOutgoingLinksByType(Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getSourceEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getIncomingLinksByType(Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getTargetEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getChildrenByType(Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getChildren(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getDiagramLinksByType(Collection<Diagram> diagrams, String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (Diagram nextDiagram : diagrams) {
			result.addAll(selectViewsByType(nextDiagram.getEdges(), type));
		}
		return result;
	}

	// TODO refactor as static method
	/**
	 * @generated
	 */
	private Collection<View> selectViewsByType(Collection<View> views, String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (View nextView : views) {
			if (type.equals(nextView.getType()) && isOwnView(nextView)) {
				result.add(nextView);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return DesignChoicesEditPart.MODEL_ID.equals(SplVisualIDRegistry.getModelID(view));
	}

	/**
	 * @generated
	 */
	private Collection<SplNavigatorItem> createNavigatorItems(Collection<View> views, Object parent, boolean isLeafs) {
		ArrayList<SplNavigatorItem> result = new ArrayList<SplNavigatorItem>(views.size());
		for (View nextView : views) {
			result.add(new SplNavigatorItem(nextView, parent, isLeafs));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<SplNavigatorItem> getForeignShortcuts(Diagram diagram, Object parent) {
		LinkedList<View> result = new LinkedList<View>();
		for (Iterator<View> it = diagram.getChildren().iterator(); it.hasNext();) {
			View nextView = it.next();
			if (!isOwnView(nextView) && nextView.getEAnnotation("Shortcut") != null) { //$NON-NLS-1$
				result.add(nextView);
			}
		}
		return createNavigatorItems(result, parent, false);
	}

	/**
	* @generated
	*/
	public Object getParent(Object element) {
		if (element instanceof SplAbstractNavigatorItem) {
			SplAbstractNavigatorItem abstractNavigatorItem = (SplAbstractNavigatorItem) element;
			return abstractNavigatorItem.getParent();
		}
		return null;
	}

	/**
	* @generated
	*/
	public boolean hasChildren(Object element) {
		return element instanceof IFile || getChildren(element).length > 0;
	}

}
