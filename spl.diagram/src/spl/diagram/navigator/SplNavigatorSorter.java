/*
* 
*/
package spl.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

import spl.diagram.part.SplVisualIDRegistry;

/**
 * @generated
 */
public class SplNavigatorSorter extends ViewerSorter {

	/**
	* @generated
	*/
	private static final int GROUP_CATEGORY = 7036;

	/**
	* @generated
	*/
	private static final int SHORTCUTS_CATEGORY = 7035;

	/**
	* @generated
	*/
	public int category(Object element) {
		if (element instanceof SplNavigatorItem) {
			SplNavigatorItem item = (SplNavigatorItem) element;
			if (item.getView().getEAnnotation("Shortcut") != null) { //$NON-NLS-1$
				return SHORTCUTS_CATEGORY;
			}
			return SplVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
