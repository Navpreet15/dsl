package XMLFeatureModelParserSample;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xtext.example.mydsl.tyson.CMconstraints;
import org.xtext.example.mydsl.tyson.Choice;
import org.xtext.example.mydsl.tyson.ChoiceModel;
import org.xtext.example.mydsl.tyson.DesignChoices;
import org.xtext.example.mydsl.tyson.FMconstraints;
import org.xtext.example.mydsl.tyson.Feature;
import org.xtext.example.mydsl.tyson.Operator;
import org.xtext.example.mydsl.tyson.TysonFactory;

import fm.FeatureGroup;
import fm.FeatureModel;
import fm.FeatureModelStatistics;
import fm.FeatureTreeNode;
import fm.RootNode;
import fm.SolitaireFeature;
import fm.XMLFeatureModel;

public class XMLFeatureModelParserSampleCM {
	public static void main(String args[]) {
		new XMLFeatureModelParserSample().parse();
	} 
	
	public void parse() {
		
		try {

			String featureModelFile = "E:\\Studies\\SPLOT stuff\\splot\\splot\\models\\aircraft_fm.xml";
			
			/* Creates the Feature Model Object
			 * ********************************
			 * - Constant USE_VARIABLE_NAME_AS_ID indicates that if an ID has not been defined for a feature node
			 *   in the XML file the feature name should be used as the ID. 
			 * - Constant SET_ID_AUTOMATICALLY can be used to let the system create an unique ID for feature nodes 
			 *   without an ID specification
			 *   Note: if an ID is specified for a feature node in the XML file it will always prevail
			 */			
			FeatureModel featureModel = new XMLFeatureModel(featureModelFile, XMLFeatureModel.USE_VARIABLE_NAME_AS_ID);
			
			// Load the XML file and creates the feature model
			featureModel.loadModel();
			
			File file = new File("E:\\Studies\\SPLOT stuff\\splot\\splot\\Choicemodels\\aircraft_fm.ts");
			  
			//File xfile = new File("abc"+".xmi");	
			DocumentBuilderFactory dfactory=DocumentBuilderFactory.newInstance();
			
			try {
				DocumentBuilder Builder=dfactory.newDocumentBuilder();
				try {
					Document doc = Builder.parse(featureModelFile);
				} catch (SAXException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch2 block
				e.printStackTrace();
			}
			
			
			
			//creating instance of tyson factory
			TysonFactory tfactory = TysonFactory.eINSTANCE;
			// Create a resource set.
			  ResourceSet tresourceSet = new ResourceSetImpl();
			  
			  // Register the default resource factory -- only needed for stand-alone!
			  tresourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			  Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
			  
			  org.eclipse.emf.common.util.URI tfileURI = org.eclipse.emf.common.util.URI.createFileURI(file.getAbsolutePath());
			  // Create a resource for this file.
			  Resource tresource = tresourceSet.createResource(tfileURI);
			  
			  
			  // Create the root element
			  DesignChoices dc =tfactory.createDesignChoices();
			  tresource.getContents().add(dc);
			
			  
			  ChoiceModel feam = tfactory.createChoiceModel();
			 dc.setCm(feam);
			
			// A feature model object contains a feature tree and a set of contraints			
			// Let's traverse the feature tree first. We start at the root feature in depth first search.
			System.out.println("FEATURE TREE --------------------------------");
			

			traverseDFS(featureModel.getRoot(), 0, tfactory , dc, feam);
			
			tresource.save(null);
			
			
			// Now, let's traverse the extra constraints as a CNF formula
			System.out.println("EXTRA CONSTRAINTS ---------------------------");
			///traverseConstraints(featureModel, tfactory);

			// Now, let's print some statistics about the feature model
			FeatureModelStatistics stats = new FeatureModelStatistics(featureModel);
			stats.update();
			
			stats.dump();
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	
		
	public void traverseDFS(FeatureTreeNode node, int tab, TysonFactory tfactory, DesignChoices dc, ChoiceModel feam) {
		// Create Feature Model
		
		
		for( int j = 0 ; j < tab ; j++ ) {
			System.out.print("\t");
		}
		// Root Feature
		if ( node instanceof RootNode ) {
			System.out.print("Root");
		}
		// Solitaire Feature8
		else if ( node instanceof SolitaireFeature ) {
			// Create Feature
			Choice feat = tfactory.createChoice();
			feat.setName(node.getName());
			feam.getCh().add(feat);
			
			
			// Optional Feature
			if ( ((SolitaireFeature)node).isOptional())
				System.out.print("Optional");
			
			// Mandatory Feature
			else
			{
				System.out.print("Mandatory");
			
				CMconstraints cons = tfactory.createCMconstraints();
				cons.setOpr(Operator.MANDATORY);
				cons.getOperand().add(feat);
				feam.getConstraint().add(cons);
			}
			
			if ( node.getNextNode() instanceof FeatureGroup ) {
				
				CMconstraints cons = tfactory.createCMconstraints(); 
				for(int j=0; j<node.getNextNode().getChildCount();j++)
				{
					FeatureTreeNode childnode=(FeatureTreeNode) node.getNextNode().getChildAt(j);
					
						 Choice chl = tfactory.createChoice();
						 chl.setName(childnode.getName());
						 feat.getChild().add(chl);
						 cons.getOperand().add(chl);
					
				}
				int minCardinality = ((FeatureGroup)node.getNextNode()).getMin();
				int maxCardinality = ((FeatureGroup)node.getNextNode()).getMax();
				System.out.print("Feature Group[" + minCardinality + "," + maxCardinality + "]"); 
				if(minCardinality==1 && maxCardinality==-1){
					
					cons.setOpr(Operator.OR);
					feam.getConstraint().add(cons);
					
				}
				else if(minCardinality==1 && maxCardinality==1){
					cons.setOpr(Operator.XOR);
					feam.getConstraint().add(cons);
				}
	
			
				else if(minCardinality==1 && maxCardinality==node.getNextNode().getChildCount()){
					cons.setOpr(Operator.AND);
					feam.getConstraint().add(cons);

				}	
			}
		}
		// Feature Group
		
		// Grouped feature
		//else {
			//System.out.print("Grouped");
		//}
		//System.out.print( "(ID=" + node.getID() + ", NAME=" + node.getName() + ")\r\n");
		//if ( node instanceof SolitaireFeature ){
		//try {
			//writer.write(node.getName()+";");
		//} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		//}
		//}
		for( int i = 0 ; i < node.getChildCount() ; i++ ) {
			traverseDFS((FeatureTreeNode )node.getChildAt(i), tab+1, tfactory,  dc, feam);
		}
	}

}
