
// BASIC DEFINITIONS
abstract sig Feature{}

one sig  Withdraw ,  Deposit extends Feature{}


// FEATURE MODEL DEFINITION
abstract sig FeatureModel{
	feature: some Feature
}

//Domain Model Definition
abstract sig Interface{}


abstract sig Class{
interface: lone Interface,
}



//abstract sig Attribute{}
//one sig Illuminated, label extends Attribute{}
//Defining boolean datatype
abstract sig Bool{}
one sig True extends Bool{}
one sig False extends Bool{}

one sig HelpButton extends Class{
Illuminated: Bool,
label: String }


fact{HelpButton.Illuminated = True and HelpButton.label = "HELP"}

abstract sig Label{}

abstract sig Relationship{
from: one Class,
to: one Class,
label: one Label,
inMul:one Int,
outMul: one Int}




abstract sig ClassDiagram{
class : some Class,
relationship: some Relationship
}

//List all the actions
abstract sig Entry{}

abstract sig State{
entry : set Entry}
one sig  InsertCard ,  InsertAmount ,  DepositMoney ,  EnterPin ,  WithdrawMoney ,  PrintReceipt extends State{}



//List all the guards
abstract sig Trigger{}


abstract sig TransitionBehavior{}


abstract sig Guard{}


abstract sig Transition{
to: one State,
from: one State,
trigger: some Trigger,
tb : set TransitionBehavior,
guard : lone Guard
}

one sig  T1 ,  T2 ,  T3 ,  T4 ,  T5 ,  T6 extends Transition{}


abstract sig StateChart{
state: some State,
transition : some Transition}


//Entries Depending on th Design Choices

 fact {(T1.source = InsertCard) and (T1.target =EnterPin)
 )}
 fact {(T2.source = EnterPin) and (T2.target =InsertAmount)
 )}
 fact {(T3.source = InsertAmount) and (T3.target =WithdrawMoney)
 )}
 fact {(T4.source = InsertAmount) and (T4.target =DepositMoney)
 and (T4.guard= PaperRoll )}
 fact {(T5.source = DepositMoney) and (T5.target =PrintReceipt)
 and (T5.guard= Printer )}
 fact {(T6.source = WithdrawMoney) and (T6.target =PrintReceipt)
 )}

fact{T1.guard= none}
fact{T2.guard= none}
fact{T3.guard= none}
fact{T6.guard= none}
fact{T1.trigger= none}
fact{T2.trigger= none}
fact{T3.trigger= none}
fact{T4.trigger= none}
fact{T5.trigger= none}
fact{T6.trigger= none}
fact{T1.tb= none}
fact{T2.tb= none}
fact{T3.tb= none}
fact{T4.tb= none}
fact{T5.tb= none}
fact{T6.tb= none}





fact{all sc: StateChart | T1 in sc.transition}
fact{all sc: StateChart | T2 in sc.transition}




abstract sig DomainModel{
cd : one ClassDiagram,
sc: one StateChart
}



// Metamodel well-formedness constraints
fact {
	all d: ClassDiagram | all r: Relationship | r in d.relationship => 
	(r.from in d.class) and (r.to in d.class)
}


//Product Definition
abstract sig Product{
dm: one DomainModel,
config: one FeatureModel}

//Well formdness rules
fact{all f: FeatureModel | f in Product.config}
fact{all d: DomainModel | d in Product.dm}

//Product line definition
abstract sig SPL{
product : some Product
}

fact {all p: Product | p in SPL.product}


//Feature Mapping
fact {all p: Product | Withdraw in p.config.feature =>
((T3 in p.dm.sc.transition) )
else( (T3 not in p.dm.sc.transition) )}
fact {all p: Product | Deposit in p.config.feature =>
((T4 in p.dm.sc.transition) )
else( (T4 not in p.dm.sc.transition) )}


//SPLDCs
//Choices
abstract sig Choice{}
one sig  Multitask ,  Receipts extends Choice {}


//ChoiceModel definition
abstract sig ChoiceModel{
choice : set Choice}

//Design Choices Definition
abstract sig DesignChoices{
cm : one ChoiceModel,
spl : one SPL
}

fact {all c: ChoiceModel | c in DesignChoices.cm}
fact{all s: SPL | s in DesignChoices.spl}

//spldc definition
one sig SPLDC{
dc : some DesignChoices
}

fact {all d: DesignChoices | d in SPLDC.dc}

//Decision Mapping
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
((Receipts in dec.cm.choice)=> 
(T6 in p.dm.sc.transition)
else (T6 not in p.dm.sc.transition)}

fact DecMapMulti{ all dec: DesignChoices | all s: dec.spl | all p: s.product | 
Multitask in dec.cm.choice => 
((Deposit in p.config.feature)}



//symmetry breaking constraints
fact {all c1, c2 : ClassDiagram | c1.relationship = c2.relationship => c1 = c2}
fact {all s1, s2 : StateChart | (s1.state = s2.state) and (s1.transition = s2.transition)
=> s1=s2 }
fact {all f1, f2 : FeatureModel | f1.feature = f2.feature => f1= f2}
fact {all d1, d2 : DomainModel | (d1.cd = d2.cd) and (d1.sc = d2.sc) => d1=d2}
fact {all t1, t2 : Relationship | (t1.from = t2.from) and (t1.to = t2.to) =>
t1=t2}
fact {all p1, p2 : Product | (p1.config = p2.config) and (p1.dm=p2.dm)=>
p1 = p2}

fact{all c1, c2 : ChoiceModel | c1.choice = c2.choice =>
c1 = c2}
fact {all dc1, dc2 : DesignChoices | dc1.cm = dc2.cm =>
dc1=dc2}

pred model{}
run model for 10




// All o/p devices are controlled by elevator control
//For All For All

assert controlling { all pl: SPL | all p: pl.product | 
all r : p.dm.cd.relationship | ((r.label = Controls) and (r.to.interface= OutputDeviceInterface)) => r.from = ElevatorControl
  }

// There is no class related to itself
assert self { all pl: SPL | all p: pl.product | 
all r : p.dm.cd.relationship | (r.to != r.from)
  }

// there is no direct relation between input device and output device interface
assert direct { all pl: SPL | all p: pl.product | 
no r : p.dm.cd.relationship | (r.to.interface= InputDeviceInterface and  
r.from.interface = OutputDeviceInterface)
  }

// Gives product as counterexample
assert sch {all pl: SPL | all p: pl.product |  
all c : p.dm.cd.class | ElevatorScheduler in c
  }

//for all exist
assert schexist {all pl: SPL | some p: pl.product |  
ElevatorScheduler in p.dm.cd.class
  }

assert R18exist {all pl: SPL | some p: pl.product |  
R18 in p.dm.cd.relationship
  }

//exist for all
assert schexist1 {some pl: SPL | all p: pl.product |  
R15 in p.dm.cd.relationship
  }


//HELP_Button assurance cases
//Case 1: Help button always present in domain model
assert HelpbuttonNA {all pl: SPL | all p: pl.product |all d: p.dm | R19 in d.cd.relationship}
assert HelpbuttonNS{all pl:SPL | some p: pl.product |all d: p.dm| R19 in d.cd.relationship}
assert HelpbuttonPA{some pl:SPL | all p: pl.product |all d: p.dm | R19 in d.cd.relationship}
assert HelpbuttonPS{some pl:SPL | some p: pl.product |all d: p.dm | R19 in d.cd.relationship}


//Case 2: Helpbutton is always illuminated
assert HelpbuttonIlluminatedNA {all pl: SPL | all p: pl.product |all d: p.dm | HelpButton.Illuminated = True}

//Case 3: Help button labeled as HELP
assert HelpbuttonlabelNA {all pl: SPL | all p: pl.product |all d: p.dm | HelpButton.label = "HELP"}


// Two way communication provided in emergency
assert CommunicationChannelNA{all pl: SPL | all p: pl.product | all d: p.dm | 
CommunicationChannelActive in EmergencyMode.entry }

//Emergency mode state always present in the state chart

check controlling for 20
check self for 20
check direct for 20
check sch for 20
check schexist for 20
check R18exist for 20
check schexist1 for 20
check HelpbuttonNA for 10
check HelpbuttonNS for 10
check HelpbuttonPA for 10
check HelpbuttonPS for 10
check HelpbuttonIlluminatedNA for 10
check  HelpbuttonlabelNA for 10
check CommunicationChannelNA for 10

