
// BASIC DEFINITIONS
abstract sig Feature{}

one sig  EmergencyHandling ,  MultipleElevators ,  Timer ,  FloorLamp ,  ArrivalSense ,  FloorButton ,  ElevatorButton extends Feature{}
fact{all fm: FeatureModel | Timer in fm.feature}
fact{all fm: FeatureModel | FloorLamp in fm.feature}
fact{all fm: FeatureModel | FloorButton in fm.feature}
fact{all fm: FeatureModel | ElevatorButton in fm.feature}


// FEATURE MODEL DEFINITION
abstract sig FeatureModel{
	feature: some Feature
}

//Domain Model Definition
abstract sig Interface{}

one sig  StateDependentControl, InputDeviceInterface ,  OutputDeviceInterface ,  Alarm ,  Timers ,  Coordinator ,  Server ,  Entity extends Interface{}

abstract sig Class{
interface: lone Interface,
}

one sig  ElevatorControl ,  DoorInterface ,  ElevatorLampInterface ,  MotorInterface ,  ElevatorDeviceInterface ,  ArrivalSensorInterface ,  DoorTimer ,  EmergencyAlarm ,  DirectionLampInterface ,  FloorLampInterface ,  FloorButtonInterface ,  WeightSensorInterface ,  ElevatorStatusandPlan ,  ElevatorManager ,  ElevatorScheduler ,  ElevatorStatusandPlanServer ,  OverallElevatorStatusandPlan extends Class{}
fact{ElevatorControl.interface = StateDependentControl}
fact{DoorInterface.interface = OutputDeviceInterface}
fact{ElevatorLampInterface.interface = OutputDeviceInterface}
fact{MotorInterface.interface = OutputDeviceInterface}
fact{ElevatorDeviceInterface.interface = InputDeviceInterface}
fact{ArrivalSensorInterface.interface = InputDeviceInterface}
fact{DoorTimer.interface = Timers}
fact{EmergencyAlarm.interface = Alarm}
fact{DirectionLampInterface.interface = OutputDeviceInterface}
fact{FloorLampInterface.interface = OutputDeviceInterface}
fact{FloorButtonInterface.interface = InputDeviceInterface}
fact{WeightSensorInterface.interface = InputDeviceInterface}
fact{ElevatorStatusandPlan.interface = Entity}
fact{ElevatorManager.interface = Coordinator}
fact{ElevatorScheduler.interface = Coordinator}
fact{ElevatorStatusandPlanServer.interface = Server}
fact{OverallElevatorStatusandPlan.interface = Entity}



//abstract sig Attribute{}
//one sig Illuminated, label extends Attribute{}
//Defining boolean datatype
abstract sig Bool{}
one sig True extends Bool{}
one sig False extends Bool{}

one sig HelpButton extends Class{
Illuminated: Bool,
label: String }


fact{HelpButton.Illuminated = True and HelpButton.label = "HELP"}

abstract sig Label{}

abstract sig Relationship{
from: one Class,
to: one Class,
label: one Label,
inMul:one Int,
outMul: one Int}

one sig  R1 ,  R2 ,  R3 ,  R5 ,  R6 ,  R7 ,  R8 ,  R9 ,  R10 ,  R11 ,  R12 ,  R13 ,  R14 ,  R15 ,  R16 ,  R17 ,  R18 extends Relationship{}
 fact {(R1.source = ElevatorControl) and (R1.target =DoorInterface) and (R1.inMul= 1) and (R1.outMul = 1) and (R1.label= controls)}
 fact {(R2.source = ElevatorControl) and (R2.target =ElevatorLampInterface) and (R2.inMul= 1) and (R2.outMul = 1) and (R2.label= controls)}
 fact {(R3.source = ElevatorControl) and (R3.target =MotorInterface) and (R3.inMul= 1) and (R3.outMul = 1) and (R3.label= controls)}
 fact {(R5.source = ArrivalSensorInterface) and (R5.target =ElevatorControl) and (R5.inMul= 1) and (R5.outMul = 1) and (R5.label= notifies)}
 fact {(R6.source = DoorTimer) and (R6.target =ElevatorControl) and (R6.inMul= 1) and (R6.outMul = 1) and (R6.label= notifies)}
 fact {(R7.source = EmergencyAlarm) and (R7.target =ElevatorControl) and (R7.inMul= 1) and (R7.outMul = 1) and (R7.label= notifies)}
 fact {(R8.source = ElevatorControl) and (R8.target =DirectionLampInterface) and (R8.inMul= 1) and (R8.outMul = 1) and (R8.label= controls)}
 fact {(R9.source = FloorButtonInterface) and (R9.target =ElevatorScheduler) and (R9.inMul= 1) and (R9.outMul = 1) and (R9.label= requests)}
 fact {(R10.source = WeightSensorInterface) and (R10.target =ElevatorControl) and (R10.inMul= 1) and (R10.outMul = 1) and (R10.label= notifies)}
 fact {(R11.source = ElevatorControl) and (R11.target =FloorLampInterface) and (R11.inMul= 1) and (R11.outMul = 1) and (R11.label= controls)}
 fact {(R12.source = ElevatorControl) and (R12.target =ElevatorStatusandPlan) and (R12.inMul= 1) and (R12.outMul = 1) and (R12.label= updates)}
 fact {(R13.source = ElevatorManager) and (R13.target =ElevatorStatusandPlan) and (R13.inMul= 1) and (R13.outMul = 1) and (R13.label= checks)}
 fact {(R14.source = ElevatorManager) and (R14.target =ElevatorControl) and (R14.inMul= 1) and (R14.outMul = 1) and (R14.label= commands)}
 fact {(R15.source = ElevatorScheduler) and (R15.target =OverallElevatorStatusandPlan) and (R15.inMul= 1) and (R15.outMul = 1) and (R15.label= selects)}
 fact {(R16.source = ElevatorDeviceInterface) and (R16.target =ElevatorManager) and (R16.inMul= 1) and (R16.outMul = 1) and (R16.label= requests)}
 fact {(R17.source = ElevatorManager) and (R17.target =ElevatorStatusandPlanServer) and (R17.inMul= 1) and (R17.outMul = 1) and (R17.label= updates)}
 fact {(R18.source = ElevatorStatusandPlanServer) and (R18.target =OverallElevatorStatusandPlan) and (R18.inMul= 1) and (R18.outMul = 1) and (R18.label= updates)}

fact{all sc: ClassDiagram | R1 in sc.relationship}
fact{all sc: ClassDiagram | R3 in sc.relationship}
fact{all sc: ClassDiagram | R6 in sc.relationship}
fact{all sc: ClassDiagram | R9 in sc.relationship}
fact{all sc: ClassDiagram | R12 in sc.relationship}
fact{all sc: ClassDiagram | R13 in sc.relationship}
fact{all sc: ClassDiagram | R14 in sc.relationship}
fact{all sc: ClassDiagram | R15 in sc.relationship}
fact{all sc: ClassDiagram | R16 in sc.relationship}
fact{all sc: ClassDiagram | R17 in sc.relationship}
fact{all sc: ClassDiagram | R18 in sc.relationship}


one sig  controls ,  requests ,  notifies ,  updates ,  checks ,  commands ,  selects extends Label{}


abstract sig ClassDiagram{
class : some Class,
relationship: some Relationship
}

//List all the actions
abstract sig Entry{}

abstract sig State{
entry : set Entry}
one sig  ElevatorIdle ,  DoorClosingToMoveUp ,  DoorClosingToMoveDown ,  ElevatorStartingUp ,  ElevatorStartingDown ,  ElevatorMoving ,  ElevatorStopping ,  ElevatorDoorOpening ,  ElevatorAtFloor ,  CheckingNextDestination extends State{}
one sig UpdateIdleStatus extends Entry{} one sig CloseDoor extends Entry{} one sig Departed extends Entry{} 


//List all the guards
abstract sig Trigger{}
one sig Uprequest extends Trigger{}
one sig DownRequest extends Trigger{}
one sig DoorClosed extends Trigger{}
one sig ElevatorStarted extends Trigger{}
one sig ApproachingRequestedFloor extends Trigger{}
one sig ElevatorStopped extends Trigger{}
one sig DoorOpened extends Trigger{}
one sig AfterTimeout extends Trigger{}
one sig UpRequest extends Trigger{}
one sig NoRequest extends Trigger{}


abstract sig TransitionBehavior{}
one sig  Up ,  Down ,  OffUpDirectionLamp ,  OffDownDirectionLamp ,  Stop ,  OnDirectionLamp , OffElevatorLamp, Arrived ,  StartTimer ,  CheckNextDestination extends TransitionBehavior{}


abstract sig Guard{}



abstract sig Transition{
source: one State,
target: one State,
trigger: some Trigger,
tb : set TransitionBehavior,
guard : lone Guard
}

one sig  T1 ,  T2 ,  T3 ,  T4 ,  T5 ,  T6 ,  T7 ,  T8 ,  T9 ,  T10 ,  T11 ,  T12 ,  T13 ,  T14 ,  T15 ,  T16 ,  T17 extends Transition{}


abstract sig StateChart{
state: some State,
transition : some Transition}


//Entries Depending on th Design Choices

 fact {(T1.source = ElevatorIdle) and (T1.target =DoorClosingToMoveUp)
   and (T1.trigger = Uprequest)}
 fact {(T2.source = ElevatorIdle) and (T2.target =DoorClosingToMoveDown)
   and (T2.trigger = DownRequest)}
 fact {(T3.source = DoorClosingToMoveUp) and (T3.target =ElevatorStartingUp)
  and (T3.trigger = DoorClosed) and (T3.tb= Up+OffUpDirectionLamp)}
 fact {(T4.source = DoorClosingToMoveDown) and (T4.target =ElevatorStartingDown)
   and (T4.trigger = DoorClosed) and (T4.tb= Down+OffDownDirectionLamp)}
 fact {(T5.source = ElevatorStartingUp) and (T5.target =ElevatorMoving)
   and (T5.trigger = ElevatorStarted)}
 fact {(T6.source = ElevatorStartingDown) and (T6.target =ElevatorMoving)
   and (T6.trigger = ElevatorStarted)}
 fact {(T7.source = ElevatorMoving) and (T7.target =ElevatorStopping)
  and (T7.trigger = ApproachingRequestedFloor) and (T7.tb= Stop+OnDirectionLamp)}
 fact {(T8.source = ElevatorStopping) and (T8.target =ElevatorDoorOpening)
   and (T8.trigger = ElevatorStopped) and (T8.tb= OffElevatorLamp+Arrived)}
 fact {(T9.source = ElevatorDoorOpening) and (T9.target =ElevatorAtFloor)
   and (T9.trigger = DoorOpened) and (T9.tb= StartTimer)}
 fact {(T10.source = ElevatorAtFloor) and (T10.target =CheckingNextDestination)
   and (T10.trigger = AfterTimeout) and (T10.tb= CheckNextDestination)}
 fact {(T11.source = CheckingNextDestination) and (T11.target =DoorClosingToMoveUp)
   and (T11.trigger = UpRequest)}
 fact {(T12.source = CheckingNextDestination) and (T12.target =DoorClosingToMoveDown)
   and (T12.trigger = DownRequest)}
 fact {(T13.source = CheckingNextDestination) and (T13.target =ElevatorIdle)
   and (T13.trigger = NoRequest)}
 fact {(T14.source = DoorClosingToMoveUp) and (T14.target =ElevatorStartingUp)
   and (T14.trigger = DoorClosed) and (T14.tb= Up)}
 fact {(T15.source = DoorClosingToMoveDown) and (T15.target =ElevatorStartingDown)
   and (T15.trigger = DoorClosed) and (T15.tb= Down)}
 fact {(T16.source = ElevatorMoving) and (T16.target =ElevatorStopping)
   and (T16.trigger = ApproachingRequestedFloor) and (T16.tb= Stop)}
 fact {(T17.source = ElevatorStopping) and (T17.target =ElevatorDoorOpening)
   and (T17.trigger = ElevatorStopped) and (T17.tb= Arrived)}

fact{T1.guard= none}
fact{T2.guard= none}
fact{T3.guard= none}
fact{T4.guard= none}
fact{T5.guard= none}
fact{T6.guard= none}
fact{T7.guard= none}
fact{T8.guard= none}
fact{T9.guard= none}
fact{T10.guard= none}
fact{T11.guard= none}
fact{T12.guard= none}
fact{T13.guard= none}
fact{T14.guard= none}
fact{T15.guard= none}
fact{T16.guard= none}
fact{T17.guard= none}





fact{all sc: StateChart | T1 in sc.transition}
fact{all sc: StateChart | T2 in sc.transition}
fact{all sc: StateChart | T5 in sc.transition}
fact{all sc: StateChart | T6 in sc.transition}
fact{all sc: StateChart | T9 in sc.transition}
fact{all sc: StateChart | T10 in sc.transition}
fact{all sc: StateChart | T11 in sc.transition}
fact{all sc: StateChart | T12 in sc.transition}
fact{all sc: StateChart | T13 in sc.transition}




abstract sig DomainModel{
cd : one ClassDiagram,
sc: one StateChart
}



// Metamodel well-formedness constraints
fact {
	all d: ClassDiagram | all r: Relationship | r in d.relationship => 
	(r.from in d.class) and (r.to in d.class)
}


//Product Definition
abstract sig Product{
dm: one DomainModel,
config: one FeatureModel}

//Well formdness rules
fact{all f: FeatureModel | f in Product.config}
fact{all d: DomainModel | d in Product.dm}

//Product line definition
abstract sig SPL{
product : some Product
}

fact {all p: Product | p in SPL.product}


//Feature Mapping
fact {all p: Product | EmergencyHandling in p.config.feature =>
((R7 in p.dm.cd.relationship) )
else( (R7 not in p.dm.cd.relationship) )}
fact {all p: Product | ArrivalSense in p.config.feature =>
((R5 in p.dm.cd.relationship) )
else( (R5 not in p.dm.cd.relationship) )}
fact {all p: Product | FloorLamp in p.config.feature =>
((R11 in p.dm.cd.relationship) )
else( (R11 not in p.dm.cd.relationship) )}
fact {all p: Product | FloorLamp in p.config.feature =>
((R2 in p.dm.cd.relationship) )
else( (R2 not in p.dm.cd.relationship) )}


//SPLDCs
//Choices
abstract sig Choice{}
one sig  WeightSensor ,  IlluminatingLamp ,  DirectionIndication ,  EmergencySystem extends Choice {}


//ChoiceModel definition
abstract sig ChoiceModel{
choice : set Choice}

//Design Choices Definition
abstract sig DesignChoices{
cm : one ChoiceModel,
spl : one SPL
}

fact {all c: ChoiceModel | c in DesignChoices.cm}
fact{all s: SPL | s in DesignChoices.spl}

//spldc definition
one sig SPLDC{
dc : some DesignChoices
}

fact {all d: DesignChoices | d in SPLDC.dc}

//Decision Mapping
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
((WeightSensor in dec.cm.choice)=> 
(R10 in p.dm.cd.relationship)
else (R10 not in  p.dm.cd.relationship))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
((DirectionIndication in dec.cm.choice)=> 
(R8 in p.dm.cd.relationship)
else (R8 not in  p.dm.cd.relationship))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
((IlluminatingLamp in dec.cm.choice)=> 
(T3 in p.dm.sc.transition)
else (T3 not in p.dm.sc.transition))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
((IlluminatingLamp in dec.cm.choice)=> 
(T14 not in p.dm.sc.transition)
else (T14 in p.dm.sc.transition))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
(((IlluminatingLamp + DirectionIndication) in dec.cm.choice)=> 
(T7 in p.dm.sc.transition)
else (T7 not in p.dm.sc.transition))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
(((IlluminatingLamp + DirectionIndication) in dec.cm.choice)=> 
(T3 in p.dm.sc.transition)
else (T3 not in p.dm.sc.transition))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
(((IlluminatingLamp + DirectionIndication) in dec.cm.choice)=> 
(T4 in p.dm.sc.transition)
else (T4 not in p.dm.sc.transition))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
(((IlluminatingLamp + DirectionIndication) in dec.cm.choice)=> 
(T16 not in p.dm.sc.transition)
else (T16 in p.dm.sc.transition))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
(((IlluminatingLamp + DirectionIndication) in dec.cm.choice)=> 
(T14 not in p.dm.sc.transition)
else (T14 in p.dm.sc.transition))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
(((IlluminatingLamp+DirectionIndication) in dec.cm.choice)=> 
(T15 not in p.dm.sc.transition)
else (T15 in p.dm.sc.transition))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
((IlluminatingLamp in dec.cm.choice)=> 
(T8 in p.dm.sc.transition)
else (T8 not in p.dm.sc.transition))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
((IlluminatingLamp in dec.cm.choice)=> 
(T17 not in p.dm.sc.transition)
else (T17 in p.dm.sc.transition))}

fact DecMapMulti{ all dec: DesignChoices | all s: dec.spl | all p: s.product | 
EmergencySystem in dec.cm.choice => 
((EmergencyHandling in p.config.feature))}



//symmetry breaking constraints
fact {all c1, c2 : ClassDiagram | c1.relationship = c2.relationship => c1 = c2}
fact {all s1, s2 : StateChart | (s1.state = s2.state) and (s1.transition = s2.transition)
=> s1=s2 }
fact {all f1, f2 : FeatureModel | f1.feature = f2.feature => f1= f2}
fact {all d1, d2 : DomainModel | (d1.cd = d2.cd) and (d1.sc = d2.sc) => d1=d2}
fact {all t1, t2 : Relationship | (t1.from = t2.from) and (t1.to = t2.to) =>
t1=t2}
fact {all p1, p2 : Product | (p1.config = p2.config) and (p1.dm=p2.dm)=>
p1 = p2}

fact{all c1, c2 : ChoiceModel | c1.choice = c2.choice =>
c1 = c2}
fact {all dc1, dc2 : DesignChoices | dc1.cm = dc2.cm =>
dc1=dc2}

pred model{}
run model for 10




// All o/p devices are controlled by elevator control
//For All For All

assert controlling { all pl: SPL | all p: pl.product | 
all r : p.dm.cd.relationship | ((r.label = controls) and (r.to.interface= OutputDeviceInterface)) => r.from = ElevatorControl
  }

// There is no class related to itself
assert self { all pl: SPL | all p: pl.product | 
all r : p.dm.cd.relationship | (r.to != r.from)
  }

// there is no direct relation between input device and output device interface
assert direct { all pl: SPL | all p: pl.product | 
no r : p.dm.cd.relationship | (r.to.interface= InputDeviceInterface and  
r.from.interface = OutputDeviceInterface)
  }

// Gives product as counterexample
assert sch {all pl: SPL | all p: pl.product |  
all c : p.dm.cd.class | ElevatorScheduler in c
  }

//for all exist
assert schexist {all pl: SPL | some p: pl.product |  
ElevatorScheduler in p.dm.cd.class
  }

assert R18exist {all pl: SPL | some p: pl.product |  
R18 in p.dm.cd.relationship
  }

//exist for all
assert schexist1 {some pl: SPL | all p: pl.product |  
R15 in p.dm.cd.relationship
  }


//HELP_Button assurance cases
//Case 1: Help button always present in domain model
//assert HelpbuttonNA {all pl: SPL | all p: pl.product |all d: p.dm | R19 in d.cd.relationship}
//assert HelpbuttonNS{all pl:SPL | some p: pl.product |all d: p.dm| R19 in d.cd.relationship}
//assert HelpbuttonPA{some pl:SPL | all p: pl.product |all d: p.dm | R19 in d.cd.relationship}
//assert HelpbuttonPS{some pl:SPL | some p: pl.product |all d: p.dm | R19 in d.cd.relationship}


//Case 2: Helpbutton is always illuminated
assert HelpbuttonIlluminatedNA {all pl: SPL | all p: pl.product |all d: p.dm | HelpButton.Illuminated = True}

//Case 3: Help button labeled as HELP
assert HelpbuttonlabelNA {all pl: SPL | all p: pl.product |all d: p.dm | HelpButton.label = "HELP"}


// Two way communication provided in emergency
//assert CommunicationChannelNA{all pl: SPL | all p: pl.product | all d: p.dm | 
//CommunicationChannelActive in EmergencyMode.entry }

//Emergency mode state always present in the state chart
assert initialState {all pl: SPL | all p: pl.product| no s: p.dm.sc.state | 
(s in p.dm.sc.transition.source) and (s not in  p.dm.sc.transition.target)}


check controlling for 20
check self for 20
check direct for 20
check sch for 20
check schexist for 20
check R18exist for 20
check schexist1 for 20
check HelpbuttonIlluminatedNA for 10
check  HelpbuttonlabelNA for 10
check initialState for 10
//check CommunicationChannelNA for 10

