import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xtext.example.mydsl.tyson.Attribute;
import org.xtext.example.mydsl.tyson.Class;
import org.xtext.example.mydsl.tyson.ClassDiagram;
import org.xtext.example.mydsl.tyson.DesignChoices;
import org.xtext.example.mydsl.tyson.Relationship;
import org.xtext.example.mydsl.tyson.Type;
import org.xtext.example.mydsl.tyson.TysonFactory;

public class ec2ty {

	public static void main(String[] args) {
		
		for (int j = 0; j < args.length; j++) {
			String string = args[j];
		
		// Create instance of both factories (Ecore and Tyson)
		EcoreFactory efactory = EcoreFactory.eINSTANCE;
		
		// Create a resource set.
		  ResourceSet resourceSet = new ResourceSetImpl();

		  // Register the default resource factory -- only needed for stand-alone!
		  resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
		    Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());

		  // Get the URI of the model file.
		  File f =  new File(string);
		  URI fileURI = URI.createFileURI(f.getAbsolutePath());

		  // Create a resource for input model
		  Resource resource = resourceSet.getResource(fileURI, true);
		  EPackage packages =(EPackage) resource.getContents().get(0);
          //EObject root = resource.getContents().get(0);
          System.out.println(packages.getName());
        //  EClass C = root.eClass();
          
          
		
		
		// Create new file (output) using document builder factory
		//File xfile = new File("abc"+".xmi");	
		DocumentBuilderFactory dfactory=DocumentBuilderFactory.newInstance();
		
		try {
			DocumentBuilder Builder=dfactory.newDocumentBuilder();
			try {
				Document doc = Builder.parse(f);
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch2 block
			e.printStackTrace();
		}
		
		
		

		//creating instance of tyson factory
		TysonFactory tfactory = TysonFactory.eINSTANCE;
		// Create a resource set.
		  ResourceSet tresourceSet = new ResourceSetImpl();
		  // Register the default resource factory -- only needed for stand-alone!
		  tresourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
		  Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		  //org.eclipse.emf.common.util.URI tfileURI = org.eclipse.emf.common.util.URI.createFileURI(new File(string.substring(0, 28)+"classdiagrams"+string.substring(38, string.length()-6)+".ts").getAbsolutePath());
		  // Create a resource for this file.
		  
		  org.eclipse.emf.common.util.URI tfileURI = org.eclipse.emf.common.util.URI.createFileURI(new File("abc.ts").getAbsolutePath());
		  // Create a resource for this file.
		  
		  Resource tresource = tresourceSet.createResource(tfileURI);
		  
		  // Create the root element
		  DesignChoices dc =tfactory.createDesignChoices();
		  tresource.getContents().add(dc);

		
		
		// get all the classes in the adelfe model and create equal number of classes in tyson
		  getClasses(packages, efactory, tfactory, dc, tresource);
		  
		
		
		  try {
			tresource.save(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  System.out.println(tfileURI+ " generated");
		}
		
		
		// get all attributes in adelfe and create those in tyson
		//get name of attrib2ute and set same as name of attibute in tyson
		//get the type of attribute and set appropriate type in tyson
		
		
		
		// get all the references for a class
		// for all references, create a reference in tyson, 
		// get the name of current class and put that as relationshio.from in tyson
		//get other attributes of references likewise.
	}
	
	public static void getClasses(EPackage packages, EcoreFactory efactory, TysonFactory tfactory, DesignChoices dc, Resource tresource)
	{
	  //EClass clas = packages.getEClassifiers().get(0).eClass();
	  // Create Class Diagram
	  ClassDiagram cd = tfactory.createClassDiagram();
	  dc.setCd(cd);
	  
	  
	  
	  for(int i=0; i < packages.getEClassifiers().size() ; i++)
	  {   
		  // Create a  tyson class 
		  Class c = tfactory.createClass();
		// Set class name same as eclassifier name
		  EClassifier ec = packages.getEClassifiers().get(i);
		  String name = ec.getName();
		  c.setName(name);
		  
		  
		// Attributes
		  getClassAttributes(ec, tfactory, c);
		  
	/*	  for(int k=0 ; k < ((EClass) ec).getEAllReferences().size() ; k++)
		  {
			  EReference ref = ((EClass) ec).getEAllReferences().get(k);
			  String refname = ref.getName();
			  int lbound = ref.getLowerBound();
			  int ubound = ref.getUpperBound();
			  
			  // Create Tyson Relationship
			  Relationship rl = tfactory.createRelationship();
			  
			  // Set relationship parameters
			  rl.setName(name);
			  rl.setInMul(lbound);
			  rl.setOutMul(ubound);
			  rl.setFrom(c);
			  EClass cl = (EClass) ref.getEType();
			  Class cla= tfactory.createClass();
			  cla.setName(cl.getName());
			  
			  rl.setTo(cla);
			  
			  //Add Relationship to class diagram
			  cd.getRelationship().add(rl);
			 cd.getClass_().add(cla);
		  } */
		  
		  
		  // Get all the references of a class
		  //add class to the file
		  cd.getClass_().add(c);
	  }
	  
	  
	  List<EClass> classes = new ArrayList<EClass>();
		  // for each EClass
		  for(int i=0; i < packages.getEClassifiers().size() ; i++)
		  {		
			  EClassifier classifier = packages.getEClassifiers().get(i);
			  while (classifier != null) {
					if (classifier instanceof EClass) {
						EClass modelClass = (EClass) packages.getEClassifiers().get(i);
						classes.add(modelClass);
					}
					classifier = packages.getEClassifiers().get(i++);
			  }
		  }
		  
			for(int p=0; p<classes.size(); p++)
			{
				EClass ec = classes.get(p);
					  
			  for(int l=0 ; l < ec.getEReferences().size() ; l++)
			  {
				  EReference ref = ec.getEReferences().get(l);
				  Relationship r = tfactory.createRelationship();
				  r.setName(ref.getName());
				  r.setInMul(ref.getLowerBound());
				  r.setOutMul(ref.getUpperBound());
				  
				  for(int j=0; j< cd.getClass_().size(); j++)
				  {
					  Class c = cd.getClass_().get(j);
					  if(ec.getName() == c.getName())
					  {
						  r.setFrom(c);
					  }
					  
					  if(ref.getEType().getName() == c.getName())
					  {
						  System.out.println(ref.getEType().getName());
						  r.setTo(c);
					  }
					  
				  }
				  		cd.getRelationship().add(r);
			  }
			  
		  }	
			
		  
		  
		  
		  
		  
	  }
	  
	



  public static void getClassAttributes(EClassifier ec, TysonFactory tfactory, org.xtext.example.mydsl.tyson.Class c ){
	  // Get all the attributes of a clasc
	  
	  
	  
	  
	  
	  for(int j=0 ; j < ec.eClass().getEAllAttributes().size() ; j++)
	  {
	
		  EAttribute att = ec.eClass().getEAllAttributes().get(j);
		  // Fetch Attribute name
		  String aname = att.getName();
		  
		  // Create an attribute in tyson
		  Attribute attribute = tfactory.createAttribute();
		  attribute.setName(aname);
		  
		  // Get the type of attribute
		  String atype = att.getEAttributeType().getName();
		  if(atype.endsWith("String"))
		  {
			  attribute.setType(Type.STRING);
		  }
		  
		  else if(atype.endsWith("Int"))
		  {
			  attribute.setType(Type.INT);
		  }
		  
		  else if(atype.endsWith("Booleam"))
		  {
			  attribute.setType(Type.BOOLEAN);
		  }
		  
		  // Add attribute to the output file
		  c.getAttr().add(attribute);
	  }
	  
	  
  }
  
}

