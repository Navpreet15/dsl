package mergemodels;

import java.io.File;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.xtext.example.mydsl.tyson.Choice;
import org.xtext.example.mydsl.tyson.ChoiceModel;
import org.xtext.example.mydsl.tyson.Class;
import org.xtext.example.mydsl.tyson.ClassDiagram;
import org.xtext.example.mydsl.tyson.DecMapping;
import org.xtext.example.mydsl.tyson.DecisionMapping;
import org.xtext.example.mydsl.tyson.DesignChoices;
import org.xtext.example.mydsl.tyson.Feature;
import org.xtext.example.mydsl.tyson.FeatureMapping;
import org.xtext.example.mydsl.tyson.FeatureModel;
import org.xtext.example.mydsl.tyson.Map;
import org.xtext.example.mydsl.tyson.MapToEntity;
import org.xtext.example.mydsl.tyson.Mapdec;
import org.xtext.example.mydsl.tyson.Mapto;
import org.xtext.example.mydsl.tyson.Operator;
import org.xtext.example.mydsl.tyson.Presence;
import org.xtext.example.mydsl.tyson.Type;
import org.xtext.example.mydsl.tyson.TysonFactory;
import org.xtext.example.mydsl.tyson.featMapping;

import java.util.concurrent.ThreadLocalRandom;


public class mappings {

	public static void main(String[] args) {
		 ResourceSet tresourceSet = new ResourceSetImpl();
		  // Register the default resource factory -- only needed for stand-alone!
		  tresourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
		  Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		  org.eclipse.emf.common.util.URI tfileURI = org.eclipse.emf.common.util.URI.createFileURI(new File("model"+".ts").getAbsolutePath());
		  Resource tresource = tresourceSet.getResource(tfileURI, true);
		  
		  TysonFactory tfactory = TysonFactory.eINSTANCE;
		  DesignChoices dc = (DesignChoices) tresource.getContents().get(0);
		//Method to generate random mappings
		//1. Generate Feature Mappings
		  genfeaturemap(tresource, tfactory, dc);
		//2. Generate Decision Mappings
		  
		  gendecisionmap(tresource, tfactory, dc);

	}

	private static void genfeaturemap(Resource tresource, TysonFactory tfactory, DesignChoices dc) {
		// TODO Auto-generated method stub
		FeatureMapping fmap = tfactory.createFeatureMapping();
		// generate random integer 
		FeatureModel fm = dc.getFm();
		int randomNum = ThreadLocalRandom.current().nextInt(0, fm.getFeature().size() + 1);
		// select that indexed feature from the feature model
		Feature f = fm.getFeature().get(randomNum);
		
		// Add selected faeture to map
		/// Create instance of featmapping
		featMapping featmap = tfactory.createfeatMapping();
		fmap.getFmap().add(featmap);
		
		// create instance of map
		Map m = tfactory.createMap();
		
		// add random feature to Map
		m.setFtr(f);
		m.setPresence(Presence.INCLUDE);
		featmap.setOprLHS(Operator.AND);
		
		featmap.getMapfrom().add(m);
		
		// select random operator LHS
		
		
		// generate random number 
		int randomNum1 = ThreadLocalRandom.current().nextInt(0, dc.getCd().getClass_().size() + 1);
		Class c = dc.getCd().getClass_().get(randomNum1);
		
		Mapto mt = tfactory.createMapto();
		
		mt.setC(c);
		featmap.setOprRHS(Operator.AND);
		featmap.getMapto().add(mt);
		
	//  select that numbered class/ relationship from cd
		
	}

	private static void gendecisionmap(Resource tresource, TysonFactory tfactory, DesignChoices dc) {
		// TODO Auto-generated method stub
		DecisionMapping dmap = tfactory.createDecisionMapping();
		ChoiceModel cm = dc.getCm();
		int randomNum = ThreadLocalRandom.current().nextInt(0, cm.getCh().size() + 1);
		Choice c = cm.getCh().get(randomNum);
		
		
		
		DecMapping dm = tfactory.createDecMapping();
		dmap.getDecisionMap().add(dm);
		Mapdec mdec = tfactory.createMapdec();
		
		dm.getMapfrom().add(mdec);
		mdec.setChoice(c);
		mdec.setPresence(Presence.INCLUDE);
		dm.setOprLHS(Operator.AND);
		
		
		
		int randomNum1 = ThreadLocalRandom.current().nextInt(0, dc.getCd().getClass_().size() + 1);
		Class cl = dc.getCd().getClass_().get(randomNum1);
		MapToEntity mentity = tfactory.createMapToEntity();
		
		mentity.setC(cl);
		dm.setOprRHS(Operator.OR);

		
		
		
		/* Things to do:
		 * 1. Randomly selecting Operators
		 *2. Randomly selecting number of features in a feature mapping
		 *3. Randomly selecting number of classes in RHS in both feature mapping and decision mapping
		 *4. Randomly selecting number of choices in the Decision Mapping
		 *5. Randomly selecting number of feature mappings
		 *6. Randomly selecting number of decision mappings */
	}
}
