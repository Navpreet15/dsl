/**
 */
package spl.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import spl.SplFactory;
import spl.scwellformedness;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>scwellformedness</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class scwellformednessTest extends TestCase {

	/**
	 * The fixture for this scwellformedness test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected scwellformedness fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(scwellformednessTest.class);
	}

	/**
	 * Constructs a new scwellformedness test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public scwellformednessTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this scwellformedness test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(scwellformedness fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this scwellformedness test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected scwellformedness getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SplFactory.eINSTANCE.createscwellformedness());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //scwellformednessTest
