/**
 */
package spl.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import spl.SplFactory;
import spl.TransitionBehavior;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Transition Behavior</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TransitionBehaviorTest extends TestCase {

	/**
	 * The fixture for this Transition Behavior test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionBehavior fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TransitionBehaviorTest.class);
	}

	/**
	 * Constructs a new Transition Behavior test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionBehaviorTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Transition Behavior test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(TransitionBehavior fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Transition Behavior test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionBehavior getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SplFactory.eINSTANCE.createTransitionBehavior());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //TransitionBehaviorTest
