/**
 */
package spl.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import spl.ClassDiagram;
import spl.SplFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Class Diagram</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassDiagramTest extends TestCase {

	/**
	 * The fixture for this Class Diagram test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassDiagram fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ClassDiagramTest.class);
	}

	/**
	 * Constructs a new Class Diagram test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassDiagramTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Class Diagram test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ClassDiagram fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Class Diagram test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassDiagram getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SplFactory.eINSTANCE.createClassDiagram());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ClassDiagramTest
