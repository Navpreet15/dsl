/**
 */
package spl.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import spl.SplFactory;
import spl.featMapping;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>feat Mapping</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class featMappingTest extends TestCase {

	/**
	 * The fixture for this feat Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected featMapping fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(featMappingTest.class);
	}

	/**
	 * Constructs a new feat Mapping test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public featMappingTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this feat Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(featMapping fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this feat Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected featMapping getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SplFactory.eINSTANCE.createfeatMapping());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //featMappingTest
