/**
 */
package spl.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import spl.SplFactory;
import spl.cdwellformedness;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>cdwellformedness</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class cdwellformednessTest extends TestCase {

	/**
	 * The fixture for this cdwellformedness test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected cdwellformedness fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(cdwellformednessTest.class);
	}

	/**
	 * Constructs a new cdwellformedness test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public cdwellformednessTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this cdwellformedness test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(cdwellformedness fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this cdwellformedness test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected cdwellformedness getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SplFactory.eINSTANCE.createcdwellformedness());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //cdwellformednessTest
