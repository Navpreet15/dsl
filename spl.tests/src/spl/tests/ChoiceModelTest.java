/**
 */
package spl.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import spl.ChoiceModel;
import spl.SplFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Choice Model</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ChoiceModelTest extends TestCase {

	/**
	 * The fixture for this Choice Model test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChoiceModel fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ChoiceModelTest.class);
	}

	/**
	 * Constructs a new Choice Model test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoiceModelTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Choice Model test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ChoiceModel fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Choice Model test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChoiceModel getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SplFactory.eINSTANCE.createChoiceModel());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ChoiceModelTest
