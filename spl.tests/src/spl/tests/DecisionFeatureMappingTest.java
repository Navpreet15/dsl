/**
 */
package spl.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import spl.DecisionFeatureMapping;
import spl.SplFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Decision Feature Mapping</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DecisionFeatureMappingTest extends TestCase {

	/**
	 * The fixture for this Decision Feature Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecisionFeatureMapping fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DecisionFeatureMappingTest.class);
	}

	/**
	 * Constructs a new Decision Feature Mapping test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecisionFeatureMappingTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Decision Feature Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DecisionFeatureMapping fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Decision Feature Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecisionFeatureMapping getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SplFactory.eINSTANCE.createDecisionFeatureMapping());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DecisionFeatureMappingTest
