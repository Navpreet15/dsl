/**
 */
package spl.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import spl.DesignChoices;
import spl.SplFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Design Choices</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DesignChoicesTest extends TestCase {

	/**
	 * The fixture for this Design Choices test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DesignChoices fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DesignChoicesTest.class);
	}

	/**
	 * Constructs a new Design Choices test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignChoicesTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Design Choices test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DesignChoices fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Design Choices test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DesignChoices getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SplFactory.eINSTANCE.createDesignChoices());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DesignChoicesTest
