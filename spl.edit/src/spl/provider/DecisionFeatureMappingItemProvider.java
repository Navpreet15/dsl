/**
 */
package spl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import spl.DecisionFeatureMapping;
import spl.SplPackage;

/**
 * This is the item provider adapter for a {@link spl.DecisionFeatureMapping} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DecisionFeatureMappingItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecisionFeatureMappingItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFeaturenamePropertyDescriptor(object);
			addDecisionnamePropertyDescriptor(object);
			addClassnamePropertyDescriptor(object);
			addContainclassPropertyDescriptor(object);
			addRelationnamePropertyDescriptor(object);
			addContainrelationPropertyDescriptor(object);
			addStatenamePropertyDescriptor(object);
			addContainstatePropertyDescriptor(object);
			addTransitionnamePropertyDescriptor(object);
			addContaintransitionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Featurename feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFeaturenamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DecisionFeatureMapping_featurename_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DecisionFeatureMapping_featurename_feature", "_UI_DecisionFeatureMapping_type"),
				 SplPackage.Literals.DECISION_FEATURE_MAPPING__FEATURENAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Decisionname feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDecisionnamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DecisionFeatureMapping_decisionname_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DecisionFeatureMapping_decisionname_feature", "_UI_DecisionFeatureMapping_type"),
				 SplPackage.Literals.DECISION_FEATURE_MAPPING__DECISIONNAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Classname feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClassnamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DecisionFeatureMapping_classname_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DecisionFeatureMapping_classname_feature", "_UI_DecisionFeatureMapping_type"),
				 SplPackage.Literals.DECISION_FEATURE_MAPPING__CLASSNAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Containclass feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainclassPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DecisionFeatureMapping_Containclass_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DecisionFeatureMapping_Containclass_feature", "_UI_DecisionFeatureMapping_type"),
				 SplPackage.Literals.DECISION_FEATURE_MAPPING__CONTAINCLASS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Relationname feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRelationnamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DecisionFeatureMapping_relationname_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DecisionFeatureMapping_relationname_feature", "_UI_DecisionFeatureMapping_type"),
				 SplPackage.Literals.DECISION_FEATURE_MAPPING__RELATIONNAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Containrelation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainrelationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DecisionFeatureMapping_Containrelation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DecisionFeatureMapping_Containrelation_feature", "_UI_DecisionFeatureMapping_type"),
				 SplPackage.Literals.DECISION_FEATURE_MAPPING__CONTAINRELATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Statename feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStatenamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DecisionFeatureMapping_statename_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DecisionFeatureMapping_statename_feature", "_UI_DecisionFeatureMapping_type"),
				 SplPackage.Literals.DECISION_FEATURE_MAPPING__STATENAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Containstate feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainstatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DecisionFeatureMapping_Containstate_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DecisionFeatureMapping_Containstate_feature", "_UI_DecisionFeatureMapping_type"),
				 SplPackage.Literals.DECISION_FEATURE_MAPPING__CONTAINSTATE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Transitionname feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransitionnamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DecisionFeatureMapping_transitionname_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DecisionFeatureMapping_transitionname_feature", "_UI_DecisionFeatureMapping_type"),
				 SplPackage.Literals.DECISION_FEATURE_MAPPING__TRANSITIONNAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Containtransition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContaintransitionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DecisionFeatureMapping_Containtransition_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DecisionFeatureMapping_Containtransition_feature", "_UI_DecisionFeatureMapping_type"),
				 SplPackage.Literals.DECISION_FEATURE_MAPPING__CONTAINTRANSITION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns DecisionFeatureMapping.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DecisionFeatureMapping"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DecisionFeatureMapping)object).getFeaturename();
		return label == null || label.length() == 0 ?
			getString("_UI_DecisionFeatureMapping_type") :
			getString("_UI_DecisionFeatureMapping_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DecisionFeatureMapping.class)) {
			case SplPackage.DECISION_FEATURE_MAPPING__FEATURENAME:
			case SplPackage.DECISION_FEATURE_MAPPING__DECISIONNAME:
			case SplPackage.DECISION_FEATURE_MAPPING__CLASSNAME:
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINCLASS:
			case SplPackage.DECISION_FEATURE_MAPPING__RELATIONNAME:
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINRELATION:
			case SplPackage.DECISION_FEATURE_MAPPING__STATENAME:
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINSTATE:
			case SplPackage.DECISION_FEATURE_MAPPING__TRANSITIONNAME:
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINTRANSITION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return SplEditPlugin.INSTANCE;
	}

}
