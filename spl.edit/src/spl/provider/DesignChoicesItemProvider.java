/**
 */
package spl.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import spl.DesignChoices;
import spl.SplFactory;
import spl.SplPackage;

/**
 * This is the item provider adapter for a {@link spl.DesignChoices} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DesignChoicesItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignChoicesItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DesignChoices_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DesignChoices_name_feature", "_UI_DesignChoices_type"),
				 SplPackage.Literals.DESIGN_CHOICES__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SplPackage.Literals.DESIGN_CHOICES__CM);
			childrenFeatures.add(SplPackage.Literals.DESIGN_CHOICES__FM);
			childrenFeatures.add(SplPackage.Literals.DESIGN_CHOICES__FMAP);
			childrenFeatures.add(SplPackage.Literals.DESIGN_CHOICES__DMAP);
			childrenFeatures.add(SplPackage.Literals.DESIGN_CHOICES__PROPERTY);
			childrenFeatures.add(SplPackage.Literals.DESIGN_CHOICES__VAR);
			childrenFeatures.add(SplPackage.Literals.DESIGN_CHOICES__CD);
			childrenFeatures.add(SplPackage.Literals.DESIGN_CHOICES__SC);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns DesignChoices.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DesignChoices"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DesignChoices)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_DesignChoices_type") :
			getString("_UI_DesignChoices_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DesignChoices.class)) {
			case SplPackage.DESIGN_CHOICES__NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case SplPackage.DESIGN_CHOICES__CM:
			case SplPackage.DESIGN_CHOICES__FM:
			case SplPackage.DESIGN_CHOICES__FMAP:
			case SplPackage.DESIGN_CHOICES__DMAP:
			case SplPackage.DESIGN_CHOICES__PROPERTY:
			case SplPackage.DESIGN_CHOICES__VAR:
			case SplPackage.DESIGN_CHOICES__CD:
			case SplPackage.DESIGN_CHOICES__SC:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(SplPackage.Literals.DESIGN_CHOICES__CM,
				 SplFactory.eINSTANCE.createChoiceModel()));

		newChildDescriptors.add
			(createChildParameter
				(SplPackage.Literals.DESIGN_CHOICES__FM,
				 SplFactory.eINSTANCE.createFeatureModel()));

		newChildDescriptors.add
			(createChildParameter
				(SplPackage.Literals.DESIGN_CHOICES__FMAP,
				 SplFactory.eINSTANCE.createFeatureMapping()));

		newChildDescriptors.add
			(createChildParameter
				(SplPackage.Literals.DESIGN_CHOICES__DMAP,
				 SplFactory.eINSTANCE.createDecisionMapping()));

		newChildDescriptors.add
			(createChildParameter
				(SplPackage.Literals.DESIGN_CHOICES__PROPERTY,
				 SplFactory.eINSTANCE.createProperty()));

		newChildDescriptors.add
			(createChildParameter
				(SplPackage.Literals.DESIGN_CHOICES__VAR,
				 SplFactory.eINSTANCE.createvariabls()));

		newChildDescriptors.add
			(createChildParameter
				(SplPackage.Literals.DESIGN_CHOICES__CD,
				 SplFactory.eINSTANCE.createClassDiagram()));

		newChildDescriptors.add
			(createChildParameter
				(SplPackage.Literals.DESIGN_CHOICES__SC,
				 SplFactory.eINSTANCE.createStateChart()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return SplEditPlugin.INSTANCE;
	}

}
