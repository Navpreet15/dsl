Installation Instructions

There are some modeling compinents that need to be installed before you start working with this project. Following is the list of those components.
1. Eclipse Modelling Tools (https://www.eclipse.org/downloads/packages/eclipse-modeling-tools/neon3)
2. Xtext
3. Acceleo
4. Alloy (http://alloy.mit.edu/alloy/download.html)

To install Xtext and Acceleo, open eclipse, go to Help->Install Modeling Components, select both Xtext and Acceleo to install.


After you install everything Import the "org.xtext.example.mydsl.SPL" project. 

Then, right click on project to select Run-as-> Eclipse Application. This will launch a new instance of eclipse.

Then, import both "CodeGeneration" and "SPLmodel" projects into the new instance of eclipse.

Within "SPLmodel" folder, you can define a subfolder for your own SPLDC. 

In that sub-folder, create a new file with extension ".spl". You can define your own SPLDC in this file. 

After Saving this file go to CodeGeneration/src/CodeGeneration.main. In this folder, right click on generate.mtl, select Run-as->Run Configurations. 
Here you can specify your input model and your path to the target alloy file.

Run this configuration to generate the alloy code.

	To run the alloy code Run->External Tools->Command Prompt.

	Go to SPLmodel/command, to copy the content of command file and paste it on console. In place of "b.als" in the command, write the name of the generated file to run analyzer.

if you don't see commad prompt in External Tools, go to Run->External Tools->External Tools Configurations. In "Location" specify the path of command prompt. In working directory, 
give the path of SPLmodel folder. Go to "Common" and check "Allocate Console" checkbox.