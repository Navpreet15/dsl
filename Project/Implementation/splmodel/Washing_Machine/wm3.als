//============================================================================================================
// BASIC DEFINITIONS
abstract sig Feature{}
 one sig Wash extends Feature{}

 one sig Heat extends Feature{}

 one sig Delay extends Feature{}

 one sig Dry extends Feature{}



abstract sig Decision{}
 one sig Mutex extends Decision{}
 one sig IncrementalHeat extends Decision{}



// FEATURE MODEL DEFINITION
abstract sig FeatureModel{
	feature: some Feature
}

 fact { all f: FeatureModel | Wash in f.feature}



//============================================================================================================
// MODELLING DECISIONS THAT IMPACT FEATURES
// container for the mapping between Decision and Feature Model
sig FeatureDecision{
dec: some Decision,
featureModel: one FeatureModel
}

 fact { all f : FeatureDecision | Mutex in FeatureDecision.dec =>
( Dry not in f.featureModel.feature)	
or (Heat not in f.featureModel.feature)
}


// well formedness and symmetry breaking:
// featureDecision instances are unique
fact {
	all f1, f2 : FeatureDecision | 
	((f1.dec = f2.dec) and (f1.featureModel =f2.featureModel))=> f1=f2
}


//============================================================================================================
// MODELLING DECISIONS THAT IMPACT THE DOMAIN MODEL
// container of the mapping between Decisions and the Domain Model
sig DomainDecision{
	decision: some Decision,
	domainModel: one DomainModel
}

 fact { all d : DomainDecision | Mutex not in d.decision }



// SET OF ALL DECISIONS
// a container for both decisions that impact features and decisions that impact
// the domain model
sig DecisionModel{
	featureDecision: one FeatureDecision,
	domainDecision: one DomainDecision
}
//============================================================================================================
// DOMAIN MODEL DEFINITION
// container for model elements, i.e., the domain model
sig DomainModel{
	transition: some Transition,
	state: some State
}

// Metamodel
abstract sig Transition{
	source: one State,
	target: one State
}

// Metamodel well-formedness constraints
fact {
	all d: DomainModel |all t: Transition | t in d.transition => 
	(t.source in d.state) and (t.target in d.state)
}


abstract sig State{}
one sig Locking extends State{}
one sig Waiting extends State{}
one sig Washing extends State{}
one sig Drying extends State{}
one sig Unlocking extends State{}



//states only as target are left.

 one sig T1 extends Transition{} 

 one sig T2 extends Transition{} 

 one sig T3 extends Transition{} 

 one sig T4 extends Transition{} 

 one sig T5 extends Transition{} 

 one sig T6 extends Transition{} 

 one sig T7 extends Transition{} 


 fact { T1.source = Locking }
 fact { T2.source = Locking }
 fact { T3.source = Waiting }
 fact { T4.source = Washing }
 fact { T5.source = Washing }
 fact { T6.source = Drying }
 fact { T7.source = Waiting }

 fact { T1.target = Waiting }
 fact { T2.target = Washing }
 fact { T3.target = Washing }
 fact { T4.target = Drying }
 fact { T5.target = Unlocking }
 fact { T6.target = Unlocking }
 fact { T7.target = Waiting }



// Things we know for sure about all products
 fact { all d: DomainModel | T5 in d.transition}
 fact { all d: DomainModel | T2 in d.transition}



//============================================================================================================
// FEATURE MAPPING aka Correspondance
sig Correspondance{
	featureModel: one FeatureModel,
	domainModel: one DomainModel
}

// Constraints to derive DomainModel from FeatureModel
// Constraints from the feature model that impact the domain model

 fact { all c: Correspondance |( Dry in c.featureModel.feature) =>
(T4 in c.domainModel.transition)
else (T4 not in c.domainModel.transition) }
 fact { all c: Correspondance |( Dry in c.featureModel.feature) =>
(T6 in c.domainModel.transition)
else (T6 not in c.domainModel.transition) }
 fact { all c: Correspondance |( Delay in c.featureModel.feature) =>
(T1 in c.domainModel.transition)
else (T1 not in c.domainModel.transition) }
 fact { all c: Correspondance |( Delay in c.featureModel.feature) =>
(T3 in c.domainModel.transition)
else (T3 not in c.domainModel.transition) }
 fact { all c: Correspondance |( Heat in c.featureModel.feature) =>
(T1 in c.domainModel.transition)
else (T1 not in c.domainModel.transition) }
 fact { all c: Correspondance |( Heat in c.featureModel.feature) =>
(T3 in c.domainModel.transition)
else (T3 not in c.domainModel.transition) }


//============================================================================================================
// DECISION MAPPING 
// Constraints to derive DomainModel from Domain decisions

 fact { all d: DomainDecision | IncrementalHeat in d.decision => T7 in d.domainModel.transition 

 else T7 not in d.domainModel.transition}









//======
fact {all d: FeatureModel   | d in FeatureDecision.featureModel}
fact {all c: Correspondance | all d: FeatureDecision | c.featureModel in d.featureModel}
fact {all c: Correspondance | all d: FeatureDecision | d.featureModel in c.featureModel}

// symmetry breaking
fact {all f: FeatureDecision | f in DecisionModel.featureDecision}
fact {all d: DomainDecision | d in DecisionModel.domainDecision}
//============================================================================================================



//============================================================================================================
//SYMMETRY BREAKING CONSTRAINTS
// for the domain model
fact {all d1, d2 : DomainModel | d1.transition = d2.transition => d1=d2}
fact {all t1, t2 : Transition | (t1.source = t2.source) and (t1.target = t2.target) =>
t1=t2}

// for the spldc
fact {all f1, f2 : FeatureModel | f1.feature = f2.feature => f1= f2}
fact {all d1, d2 : DomainDecision | (d1.domainModel = d2.domainModel) => d1 = d2 }
fact {all f1, f2 : FeatureDecision | f1.featureModel = f2.featureModel => f1 = f2}
//============================================================================================================







//There exist any
fact {all d: DomainModel    | d in DomainDecision.domainModel}
fact {all c: Correspondance | all d: DomainDecision | c.domainModel in d.domainModel}
fact {all c: Correspondance | all d: DomainDecision | d.domainModel in c.domainModel}

pred d {all d: DomainModel | T4 not in d.transition}



run d for 20



