/**
 * generated by Xtext 2.10.0
 */
package org.xtext.example.mydsl.sPL.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.example.mydsl.sPL.Decision;
import org.xtext.example.mydsl.sPL.DomainDecision2;
import org.xtext.example.mydsl.sPL.Operator;
import org.xtext.example.mydsl.sPL.SPLPackage;
import org.xtext.example.mydsl.sPL.TransitionLabel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Domain Decision2</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.DomainDecision2Impl#getDdec2 <em>Ddec2</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.DomainDecision2Impl#getOp <em>Op</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.DomainDecision2Impl#getTl <em>Tl</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.DomainDecision2Impl#getLl6 <em>Ll6</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.DomainDecision2Impl#getTl2 <em>Tl2</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.DomainDecision2Impl#getLl7 <em>Ll7</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.DomainDecision2Impl#getLl8 <em>Ll8</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DomainDecision2Impl extends MinimalEObjectImpl.Container implements DomainDecision2
{
  /**
   * The cached value of the '{@link #getDdec2() <em>Ddec2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDdec2()
   * @generated
   * @ordered
   */
  protected EList<Decision> ddec2;

  /**
   * The cached value of the '{@link #getOp() <em>Op</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOp()
   * @generated
   * @ordered
   */
  protected EList<Operator> op;

  /**
   * The cached value of the '{@link #getTl() <em>Tl</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTl()
   * @generated
   * @ordered
   */
  protected EList<TransitionLabel> tl;

  /**
   * The cached value of the '{@link #getLl6() <em>Ll6</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLl6()
   * @generated
   * @ordered
   */
  protected EList<String> ll6;

  /**
   * The cached value of the '{@link #getTl2() <em>Tl2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTl2()
   * @generated
   * @ordered
   */
  protected EList<TransitionLabel> tl2;

  /**
   * The cached value of the '{@link #getLl7() <em>Ll7</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLl7()
   * @generated
   * @ordered
   */
  protected EList<String> ll7;

  /**
   * The cached value of the '{@link #getLl8() <em>Ll8</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLl8()
   * @generated
   * @ordered
   */
  protected EList<String> ll8;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DomainDecision2Impl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SPLPackage.Literals.DOMAIN_DECISION2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Decision> getDdec2()
  {
    if (ddec2 == null)
    {
      ddec2 = new EObjectContainmentEList<Decision>(Decision.class, this, SPLPackage.DOMAIN_DECISION2__DDEC2);
    }
    return ddec2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Operator> getOp()
  {
    if (op == null)
    {
      op = new EObjectContainmentEList<Operator>(Operator.class, this, SPLPackage.DOMAIN_DECISION2__OP);
    }
    return op;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TransitionLabel> getTl()
  {
    if (tl == null)
    {
      tl = new EObjectContainmentEList<TransitionLabel>(TransitionLabel.class, this, SPLPackage.DOMAIN_DECISION2__TL);
    }
    return tl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getLl6()
  {
    if (ll6 == null)
    {
      ll6 = new EDataTypeEList<String>(String.class, this, SPLPackage.DOMAIN_DECISION2__LL6);
    }
    return ll6;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TransitionLabel> getTl2()
  {
    if (tl2 == null)
    {
      tl2 = new EObjectContainmentEList<TransitionLabel>(TransitionLabel.class, this, SPLPackage.DOMAIN_DECISION2__TL2);
    }
    return tl2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getLl7()
  {
    if (ll7 == null)
    {
      ll7 = new EDataTypeEList<String>(String.class, this, SPLPackage.DOMAIN_DECISION2__LL7);
    }
    return ll7;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getLl8()
  {
    if (ll8 == null)
    {
      ll8 = new EDataTypeEList<String>(String.class, this, SPLPackage.DOMAIN_DECISION2__LL8);
    }
    return ll8;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SPLPackage.DOMAIN_DECISION2__DDEC2:
        return ((InternalEList<?>)getDdec2()).basicRemove(otherEnd, msgs);
      case SPLPackage.DOMAIN_DECISION2__OP:
        return ((InternalEList<?>)getOp()).basicRemove(otherEnd, msgs);
      case SPLPackage.DOMAIN_DECISION2__TL:
        return ((InternalEList<?>)getTl()).basicRemove(otherEnd, msgs);
      case SPLPackage.DOMAIN_DECISION2__TL2:
        return ((InternalEList<?>)getTl2()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SPLPackage.DOMAIN_DECISION2__DDEC2:
        return getDdec2();
      case SPLPackage.DOMAIN_DECISION2__OP:
        return getOp();
      case SPLPackage.DOMAIN_DECISION2__TL:
        return getTl();
      case SPLPackage.DOMAIN_DECISION2__LL6:
        return getLl6();
      case SPLPackage.DOMAIN_DECISION2__TL2:
        return getTl2();
      case SPLPackage.DOMAIN_DECISION2__LL7:
        return getLl7();
      case SPLPackage.DOMAIN_DECISION2__LL8:
        return getLl8();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SPLPackage.DOMAIN_DECISION2__DDEC2:
        getDdec2().clear();
        getDdec2().addAll((Collection<? extends Decision>)newValue);
        return;
      case SPLPackage.DOMAIN_DECISION2__OP:
        getOp().clear();
        getOp().addAll((Collection<? extends Operator>)newValue);
        return;
      case SPLPackage.DOMAIN_DECISION2__TL:
        getTl().clear();
        getTl().addAll((Collection<? extends TransitionLabel>)newValue);
        return;
      case SPLPackage.DOMAIN_DECISION2__LL6:
        getLl6().clear();
        getLl6().addAll((Collection<? extends String>)newValue);
        return;
      case SPLPackage.DOMAIN_DECISION2__TL2:
        getTl2().clear();
        getTl2().addAll((Collection<? extends TransitionLabel>)newValue);
        return;
      case SPLPackage.DOMAIN_DECISION2__LL7:
        getLl7().clear();
        getLl7().addAll((Collection<? extends String>)newValue);
        return;
      case SPLPackage.DOMAIN_DECISION2__LL8:
        getLl8().clear();
        getLl8().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SPLPackage.DOMAIN_DECISION2__DDEC2:
        getDdec2().clear();
        return;
      case SPLPackage.DOMAIN_DECISION2__OP:
        getOp().clear();
        return;
      case SPLPackage.DOMAIN_DECISION2__TL:
        getTl().clear();
        return;
      case SPLPackage.DOMAIN_DECISION2__LL6:
        getLl6().clear();
        return;
      case SPLPackage.DOMAIN_DECISION2__TL2:
        getTl2().clear();
        return;
      case SPLPackage.DOMAIN_DECISION2__LL7:
        getLl7().clear();
        return;
      case SPLPackage.DOMAIN_DECISION2__LL8:
        getLl8().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SPLPackage.DOMAIN_DECISION2__DDEC2:
        return ddec2 != null && !ddec2.isEmpty();
      case SPLPackage.DOMAIN_DECISION2__OP:
        return op != null && !op.isEmpty();
      case SPLPackage.DOMAIN_DECISION2__TL:
        return tl != null && !tl.isEmpty();
      case SPLPackage.DOMAIN_DECISION2__LL6:
        return ll6 != null && !ll6.isEmpty();
      case SPLPackage.DOMAIN_DECISION2__TL2:
        return tl2 != null && !tl2.isEmpty();
      case SPLPackage.DOMAIN_DECISION2__LL7:
        return ll7 != null && !ll7.isEmpty();
      case SPLPackage.DOMAIN_DECISION2__LL8:
        return ll8 != null && !ll8.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (ll6: ");
    result.append(ll6);
    result.append(", ll7: ");
    result.append(ll7);
    result.append(", ll8: ");
    result.append(ll8);
    result.append(')');
    return result.toString();
  }

} //DomainDecision2Impl
