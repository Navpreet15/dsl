/**
 * generated by Xtext 2.10.0
 */
package org.xtext.example.mydsl.sPL;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Correspondance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.sPL.Correspondance#getCfeat <em>Cfeat</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.Correspondance#getL3 <em>L3</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.Correspondance#getCtran <em>Ctran</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.Correspondance#getL4 <em>L4</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.Correspondance#getL5 <em>L5</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.mydsl.sPL.SPLPackage#getCorrespondance()
 * @model
 * @generated
 */
public interface Correspondance extends EObject
{
  /**
   * Returns the value of the '<em><b>Cfeat</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.sPL.FeatureName}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cfeat</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cfeat</em>' containment reference list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getCorrespondance_Cfeat()
   * @model containment="true"
   * @generated
   */
  EList<FeatureName> getCfeat();

  /**
   * Returns the value of the '<em><b>L3</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>L3</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>L3</em>' attribute list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getCorrespondance_L3()
   * @model unique="false"
   * @generated
   */
  EList<String> getL3();

  /**
   * Returns the value of the '<em><b>Ctran</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.sPL.TransitionLabel}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ctran</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ctran</em>' containment reference list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getCorrespondance_Ctran()
   * @model containment="true"
   * @generated
   */
  EList<TransitionLabel> getCtran();

  /**
   * Returns the value of the '<em><b>L4</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>L4</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>L4</em>' attribute list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getCorrespondance_L4()
   * @model unique="false"
   * @generated
   */
  EList<String> getL4();

  /**
   * Returns the value of the '<em><b>L5</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>L5</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>L5</em>' attribute list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getCorrespondance_L5()
   * @model unique="false"
   * @generated
   */
  EList<String> getL5();

} // Correspondance
