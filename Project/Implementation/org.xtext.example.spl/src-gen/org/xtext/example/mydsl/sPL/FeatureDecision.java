/**
 * generated by Xtext 2.10.0
 */
package org.xtext.example.mydsl.sPL;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Decision</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.sPL.FeatureDecision#getFdec <em>Fdec</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.FeatureDecision#getFn <em>Fn</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.FeatureDecision#getL <em>L</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.FeatureDecision#getOp <em>Op</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.FeatureDecision#getFn1 <em>Fn1</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.FeatureDecision#getLl <em>Ll</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.mydsl.sPL.SPLPackage#getFeatureDecision()
 * @model
 * @generated
 */
public interface FeatureDecision extends EObject
{
  /**
   * Returns the value of the '<em><b>Fdec</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.sPL.Decision}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fdec</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fdec</em>' containment reference list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getFeatureDecision_Fdec()
   * @model containment="true"
   * @generated
   */
  EList<Decision> getFdec();

  /**
   * Returns the value of the '<em><b>Fn</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.sPL.FeatureName}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fn</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fn</em>' containment reference list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getFeatureDecision_Fn()
   * @model containment="true"
   * @generated
   */
  EList<FeatureName> getFn();

  /**
   * Returns the value of the '<em><b>L</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>L</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>L</em>' attribute list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getFeatureDecision_L()
   * @model unique="false"
   * @generated
   */
  EList<String> getL();

  /**
   * Returns the value of the '<em><b>Op</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.sPL.Operator}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' containment reference list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getFeatureDecision_Op()
   * @model containment="true"
   * @generated
   */
  EList<Operator> getOp();

  /**
   * Returns the value of the '<em><b>Fn1</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.sPL.FeatureName}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fn1</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fn1</em>' containment reference list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getFeatureDecision_Fn1()
   * @model containment="true"
   * @generated
   */
  EList<FeatureName> getFn1();

  /**
   * Returns the value of the '<em><b>Ll</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ll</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ll</em>' attribute list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getFeatureDecision_Ll()
   * @model unique="false"
   * @generated
   */
  EList<String> getLl();

} // FeatureDecision
