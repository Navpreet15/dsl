/**
 * generated by Xtext 2.10.0
 */
package org.xtext.example.mydsl.sPL.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.example.mydsl.sPL.DomainFacts;
import org.xtext.example.mydsl.sPL.SPLPackage;
import org.xtext.example.mydsl.sPL.TransitionLabel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Domain Facts</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.DomainFactsImpl#getTL <em>TL</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.DomainFactsImpl#getL <em>L</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DomainFactsImpl extends MinimalEObjectImpl.Container implements DomainFacts
{
  /**
   * The cached value of the '{@link #getTL() <em>TL</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTL()
   * @generated
   * @ordered
   */
  protected EList<TransitionLabel> tL;

  /**
   * The cached value of the '{@link #getL() <em>L</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getL()
   * @generated
   * @ordered
   */
  protected EList<String> l;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DomainFactsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SPLPackage.Literals.DOMAIN_FACTS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TransitionLabel> getTL()
  {
    if (tL == null)
    {
      tL = new EObjectContainmentEList<TransitionLabel>(TransitionLabel.class, this, SPLPackage.DOMAIN_FACTS__TL);
    }
    return tL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getL()
  {
    if (l == null)
    {
      l = new EDataTypeEList<String>(String.class, this, SPLPackage.DOMAIN_FACTS__L);
    }
    return l;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SPLPackage.DOMAIN_FACTS__TL:
        return ((InternalEList<?>)getTL()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SPLPackage.DOMAIN_FACTS__TL:
        return getTL();
      case SPLPackage.DOMAIN_FACTS__L:
        return getL();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SPLPackage.DOMAIN_FACTS__TL:
        getTL().clear();
        getTL().addAll((Collection<? extends TransitionLabel>)newValue);
        return;
      case SPLPackage.DOMAIN_FACTS__L:
        getL().clear();
        getL().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SPLPackage.DOMAIN_FACTS__TL:
        getTL().clear();
        return;
      case SPLPackage.DOMAIN_FACTS__L:
        getL().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SPLPackage.DOMAIN_FACTS__TL:
        return tL != null && !tL.isEmpty();
      case SPLPackage.DOMAIN_FACTS__L:
        return l != null && !l.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (l: ");
    result.append(l);
    result.append(')');
    return result.toString();
  }

} //DomainFactsImpl
