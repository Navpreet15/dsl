/**
 * generated by Xtext 2.10.0
 */
package org.xtext.example.mydsl.sPL.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.example.mydsl.sPL.DecisionModel;
import org.xtext.example.mydsl.sPL.DomainModel;
import org.xtext.example.mydsl.sPL.FeatureModel;
import org.xtext.example.mydsl.sPL.Property;
import org.xtext.example.mydsl.sPL.SPL;
import org.xtext.example.mydsl.sPL.SPLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SPL</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.SPLImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.SPLImpl#getFm <em>Fm</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.SPLImpl#getDm <em>Dm</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.SPLImpl#getDc <em>Dc</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.SPLImpl#getPro <em>Pro</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SPLImpl extends MinimalEObjectImpl.Container implements SPL
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getFm() <em>Fm</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFm()
   * @generated
   * @ordered
   */
  protected EList<FeatureModel> fm;

  /**
   * The cached value of the '{@link #getDm() <em>Dm</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDm()
   * @generated
   * @ordered
   */
  protected EList<DomainModel> dm;

  /**
   * The cached value of the '{@link #getDc() <em>Dc</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDc()
   * @generated
   * @ordered
   */
  protected EList<DecisionModel> dc;

  /**
   * The cached value of the '{@link #getPro() <em>Pro</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPro()
   * @generated
   * @ordered
   */
  protected EList<Property> pro;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SPLImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SPLPackage.Literals.SPL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SPLPackage.SPL__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FeatureModel> getFm()
  {
    if (fm == null)
    {
      fm = new EObjectContainmentEList<FeatureModel>(FeatureModel.class, this, SPLPackage.SPL__FM);
    }
    return fm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<DomainModel> getDm()
  {
    if (dm == null)
    {
      dm = new EObjectContainmentEList<DomainModel>(DomainModel.class, this, SPLPackage.SPL__DM);
    }
    return dm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<DecisionModel> getDc()
  {
    if (dc == null)
    {
      dc = new EObjectContainmentEList<DecisionModel>(DecisionModel.class, this, SPLPackage.SPL__DC);
    }
    return dc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Property> getPro()
  {
    if (pro == null)
    {
      pro = new EObjectContainmentEList<Property>(Property.class, this, SPLPackage.SPL__PRO);
    }
    return pro;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SPLPackage.SPL__FM:
        return ((InternalEList<?>)getFm()).basicRemove(otherEnd, msgs);
      case SPLPackage.SPL__DM:
        return ((InternalEList<?>)getDm()).basicRemove(otherEnd, msgs);
      case SPLPackage.SPL__DC:
        return ((InternalEList<?>)getDc()).basicRemove(otherEnd, msgs);
      case SPLPackage.SPL__PRO:
        return ((InternalEList<?>)getPro()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SPLPackage.SPL__NAME:
        return getName();
      case SPLPackage.SPL__FM:
        return getFm();
      case SPLPackage.SPL__DM:
        return getDm();
      case SPLPackage.SPL__DC:
        return getDc();
      case SPLPackage.SPL__PRO:
        return getPro();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SPLPackage.SPL__NAME:
        setName((String)newValue);
        return;
      case SPLPackage.SPL__FM:
        getFm().clear();
        getFm().addAll((Collection<? extends FeatureModel>)newValue);
        return;
      case SPLPackage.SPL__DM:
        getDm().clear();
        getDm().addAll((Collection<? extends DomainModel>)newValue);
        return;
      case SPLPackage.SPL__DC:
        getDc().clear();
        getDc().addAll((Collection<? extends DecisionModel>)newValue);
        return;
      case SPLPackage.SPL__PRO:
        getPro().clear();
        getPro().addAll((Collection<? extends Property>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SPLPackage.SPL__NAME:
        setName(NAME_EDEFAULT);
        return;
      case SPLPackage.SPL__FM:
        getFm().clear();
        return;
      case SPLPackage.SPL__DM:
        getDm().clear();
        return;
      case SPLPackage.SPL__DC:
        getDc().clear();
        return;
      case SPLPackage.SPL__PRO:
        getPro().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SPLPackage.SPL__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case SPLPackage.SPL__FM:
        return fm != null && !fm.isEmpty();
      case SPLPackage.SPL__DM:
        return dm != null && !dm.isEmpty();
      case SPLPackage.SPL__DC:
        return dc != null && !dc.isEmpty();
      case SPLPackage.SPL__PRO:
        return pro != null && !pro.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //SPLImpl
