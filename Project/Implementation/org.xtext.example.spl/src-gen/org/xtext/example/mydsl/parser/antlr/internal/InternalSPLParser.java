package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.SPLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSPLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'FM'", "'{'", "','", "'}'", "'Feature'", "'DecM'", "'State_Chart'", "'State'", "'Transition'", "'to'", "'in'", "'not'", "'Feature_Model'", "'Domain_Model'", "'and'", "'or'", "'DomainDecision'", "'implies'", "'else'", "'DomainModel'", "'FeatureDecision'", "'FeatureModel'", "'Correspondence'", "'TransitionEffect'", "'Prop'", "'Decision_Model'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalSPLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSPLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSPLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSPL.g"; }



     	private SPLGrammarAccess grammarAccess;

        public InternalSPLParser(TokenStream input, SPLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "SPL";
       	}

       	@Override
       	protected SPLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleSPL"
    // InternalSPL.g:64:1: entryRuleSPL returns [EObject current=null] : iv_ruleSPL= ruleSPL EOF ;
    public final EObject entryRuleSPL() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSPL = null;


        try {
            // InternalSPL.g:64:44: (iv_ruleSPL= ruleSPL EOF )
            // InternalSPL.g:65:2: iv_ruleSPL= ruleSPL EOF
            {
             newCompositeNode(grammarAccess.getSPLRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSPL=ruleSPL();

            state._fsp--;

             current =iv_ruleSPL; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSPL"


    // $ANTLR start "ruleSPL"
    // InternalSPL.g:71:1: ruleSPL returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_fm_1_0= ruleFeatureModel ) ) ( (lv_dm_2_0= ruleDomainModel ) ) ( (lv_dc_3_0= ruleDecisionModel ) ) ( (lv_Pro_4_0= ruleProperty ) )* ) ;
    public final EObject ruleSPL() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_fm_1_0 = null;

        EObject lv_dm_2_0 = null;

        EObject lv_dc_3_0 = null;

        EObject lv_Pro_4_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:77:2: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_fm_1_0= ruleFeatureModel ) ) ( (lv_dm_2_0= ruleDomainModel ) ) ( (lv_dc_3_0= ruleDecisionModel ) ) ( (lv_Pro_4_0= ruleProperty ) )* ) )
            // InternalSPL.g:78:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_fm_1_0= ruleFeatureModel ) ) ( (lv_dm_2_0= ruleDomainModel ) ) ( (lv_dc_3_0= ruleDecisionModel ) ) ( (lv_Pro_4_0= ruleProperty ) )* )
            {
            // InternalSPL.g:78:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_fm_1_0= ruleFeatureModel ) ) ( (lv_dm_2_0= ruleDomainModel ) ) ( (lv_dc_3_0= ruleDecisionModel ) ) ( (lv_Pro_4_0= ruleProperty ) )* )
            // InternalSPL.g:79:3: ( (lv_name_0_0= RULE_ID ) ) ( (lv_fm_1_0= ruleFeatureModel ) ) ( (lv_dm_2_0= ruleDomainModel ) ) ( (lv_dc_3_0= ruleDecisionModel ) ) ( (lv_Pro_4_0= ruleProperty ) )*
            {
            // InternalSPL.g:79:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalSPL.g:80:4: (lv_name_0_0= RULE_ID )
            {
            // InternalSPL.g:80:4: (lv_name_0_0= RULE_ID )
            // InternalSPL.g:81:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getSPLAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSPLRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalSPL.g:97:3: ( (lv_fm_1_0= ruleFeatureModel ) )
            // InternalSPL.g:98:4: (lv_fm_1_0= ruleFeatureModel )
            {
            // InternalSPL.g:98:4: (lv_fm_1_0= ruleFeatureModel )
            // InternalSPL.g:99:5: lv_fm_1_0= ruleFeatureModel
            {

            					newCompositeNode(grammarAccess.getSPLAccess().getFmFeatureModelParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_fm_1_0=ruleFeatureModel();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSPLRule());
            					}
            					add(
            						current,
            						"fm",
            						lv_fm_1_0,
            						"org.xtext.example.mydsl.SPL.FeatureModel");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:116:3: ( (lv_dm_2_0= ruleDomainModel ) )
            // InternalSPL.g:117:4: (lv_dm_2_0= ruleDomainModel )
            {
            // InternalSPL.g:117:4: (lv_dm_2_0= ruleDomainModel )
            // InternalSPL.g:118:5: lv_dm_2_0= ruleDomainModel
            {

            					newCompositeNode(grammarAccess.getSPLAccess().getDmDomainModelParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_dm_2_0=ruleDomainModel();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSPLRule());
            					}
            					add(
            						current,
            						"dm",
            						lv_dm_2_0,
            						"org.xtext.example.mydsl.SPL.DomainModel");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:135:3: ( (lv_dc_3_0= ruleDecisionModel ) )
            // InternalSPL.g:136:4: (lv_dc_3_0= ruleDecisionModel )
            {
            // InternalSPL.g:136:4: (lv_dc_3_0= ruleDecisionModel )
            // InternalSPL.g:137:5: lv_dc_3_0= ruleDecisionModel
            {

            					newCompositeNode(grammarAccess.getSPLAccess().getDcDecisionModelParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_6);
            lv_dc_3_0=ruleDecisionModel();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSPLRule());
            					}
            					add(
            						current,
            						"dc",
            						lv_dc_3_0,
            						"org.xtext.example.mydsl.SPL.DecisionModel");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:154:3: ( (lv_Pro_4_0= ruleProperty ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==35) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSPL.g:155:4: (lv_Pro_4_0= ruleProperty )
            	    {
            	    // InternalSPL.g:155:4: (lv_Pro_4_0= ruleProperty )
            	    // InternalSPL.g:156:5: lv_Pro_4_0= ruleProperty
            	    {

            	    					newCompositeNode(grammarAccess.getSPLAccess().getProPropertyParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_Pro_4_0=ruleProperty();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getSPLRule());
            	    					}
            	    					add(
            	    						current,
            	    						"Pro",
            	    						lv_Pro_4_0,
            	    						"org.xtext.example.mydsl.SPL.Property");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSPL"


    // $ANTLR start "entryRuleFeatureModel"
    // InternalSPL.g:177:1: entryRuleFeatureModel returns [EObject current=null] : iv_ruleFeatureModel= ruleFeatureModel EOF ;
    public final EObject entryRuleFeatureModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureModel = null;


        try {
            // InternalSPL.g:177:53: (iv_ruleFeatureModel= ruleFeatureModel EOF )
            // InternalSPL.g:178:2: iv_ruleFeatureModel= ruleFeatureModel EOF
            {
             newCompositeNode(grammarAccess.getFeatureModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureModel=ruleFeatureModel();

            state._fsp--;

             current =iv_ruleFeatureModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureModel"


    // $ANTLR start "ruleFeatureModel"
    // InternalSPL.g:184:1: ruleFeatureModel returns [EObject current=null] : (otherlv_0= 'FM' otherlv_1= '{' ( (lv_features_2_0= ruleFeature ) )* (otherlv_3= ',' )? ( (lv_ffs_4_0= ruleFeatureFacts ) )* otherlv_5= '}' ) ;
    public final EObject ruleFeatureModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_features_2_0 = null;

        EObject lv_ffs_4_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:190:2: ( (otherlv_0= 'FM' otherlv_1= '{' ( (lv_features_2_0= ruleFeature ) )* (otherlv_3= ',' )? ( (lv_ffs_4_0= ruleFeatureFacts ) )* otherlv_5= '}' ) )
            // InternalSPL.g:191:2: (otherlv_0= 'FM' otherlv_1= '{' ( (lv_features_2_0= ruleFeature ) )* (otherlv_3= ',' )? ( (lv_ffs_4_0= ruleFeatureFacts ) )* otherlv_5= '}' )
            {
            // InternalSPL.g:191:2: (otherlv_0= 'FM' otherlv_1= '{' ( (lv_features_2_0= ruleFeature ) )* (otherlv_3= ',' )? ( (lv_ffs_4_0= ruleFeatureFacts ) )* otherlv_5= '}' )
            // InternalSPL.g:192:3: otherlv_0= 'FM' otherlv_1= '{' ( (lv_features_2_0= ruleFeature ) )* (otherlv_3= ',' )? ( (lv_ffs_4_0= ruleFeatureFacts ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getFeatureModelAccess().getFMKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_8); 

            			newLeafNode(otherlv_1, grammarAccess.getFeatureModelAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalSPL.g:200:3: ( (lv_features_2_0= ruleFeature ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==15) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalSPL.g:201:4: (lv_features_2_0= ruleFeature )
            	    {
            	    // InternalSPL.g:201:4: (lv_features_2_0= ruleFeature )
            	    // InternalSPL.g:202:5: lv_features_2_0= ruleFeature
            	    {

            	    					newCompositeNode(grammarAccess.getFeatureModelAccess().getFeaturesFeatureParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_features_2_0=ruleFeature();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFeatureModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"features",
            	    						lv_features_2_0,
            	    						"org.xtext.example.mydsl.SPL.Feature");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalSPL.g:219:3: (otherlv_3= ',' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalSPL.g:220:4: otherlv_3= ','
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_9); 

                    				newLeafNode(otherlv_3, grammarAccess.getFeatureModelAccess().getCommaKeyword_3());
                    			

                    }
                    break;

            }

            // InternalSPL.g:225:3: ( (lv_ffs_4_0= ruleFeatureFacts ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalSPL.g:226:4: (lv_ffs_4_0= ruleFeatureFacts )
            	    {
            	    // InternalSPL.g:226:4: (lv_ffs_4_0= ruleFeatureFacts )
            	    // InternalSPL.g:227:5: lv_ffs_4_0= ruleFeatureFacts
            	    {

            	    					newCompositeNode(grammarAccess.getFeatureModelAccess().getFfsFeatureFactsParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_9);
            	    lv_ffs_4_0=ruleFeatureFacts();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFeatureModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"ffs",
            	    						lv_ffs_4_0,
            	    						"org.xtext.example.mydsl.SPL.FeatureFacts");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_5=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getFeatureModelAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureModel"


    // $ANTLR start "entryRuleFeature"
    // InternalSPL.g:252:1: entryRuleFeature returns [EObject current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final EObject entryRuleFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeature = null;


        try {
            // InternalSPL.g:252:48: (iv_ruleFeature= ruleFeature EOF )
            // InternalSPL.g:253:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // InternalSPL.g:259:1: ruleFeature returns [EObject current=null] : (otherlv_0= 'Feature' ( ( (lv_fn_1_0= ruleFeatureName ) ) ( (lv_fn_2_0= ruleFeatureName ) )* )? ) ;
    public final EObject ruleFeature() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_fn_1_0 = null;

        EObject lv_fn_2_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:265:2: ( (otherlv_0= 'Feature' ( ( (lv_fn_1_0= ruleFeatureName ) ) ( (lv_fn_2_0= ruleFeatureName ) )* )? ) )
            // InternalSPL.g:266:2: (otherlv_0= 'Feature' ( ( (lv_fn_1_0= ruleFeatureName ) ) ( (lv_fn_2_0= ruleFeatureName ) )* )? )
            {
            // InternalSPL.g:266:2: (otherlv_0= 'Feature' ( ( (lv_fn_1_0= ruleFeatureName ) ) ( (lv_fn_2_0= ruleFeatureName ) )* )? )
            // InternalSPL.g:267:3: otherlv_0= 'Feature' ( ( (lv_fn_1_0= ruleFeatureName ) ) ( (lv_fn_2_0= ruleFeatureName ) )* )?
            {
            otherlv_0=(Token)match(input,15,FOLLOW_10); 

            			newLeafNode(otherlv_0, grammarAccess.getFeatureAccess().getFeatureKeyword_0());
            		
            // InternalSPL.g:271:3: ( ( (lv_fn_1_0= ruleFeatureName ) ) ( (lv_fn_2_0= ruleFeatureName ) )* )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_ID) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==EOF||LA6_1==RULE_ID||(LA6_1>=13 && LA6_1<=15)) ) {
                    alt6=1;
                }
            }
            switch (alt6) {
                case 1 :
                    // InternalSPL.g:272:4: ( (lv_fn_1_0= ruleFeatureName ) ) ( (lv_fn_2_0= ruleFeatureName ) )*
                    {
                    // InternalSPL.g:272:4: ( (lv_fn_1_0= ruleFeatureName ) )
                    // InternalSPL.g:273:5: (lv_fn_1_0= ruleFeatureName )
                    {
                    // InternalSPL.g:273:5: (lv_fn_1_0= ruleFeatureName )
                    // InternalSPL.g:274:6: lv_fn_1_0= ruleFeatureName
                    {

                    						newCompositeNode(grammarAccess.getFeatureAccess().getFnFeatureNameParserRuleCall_1_0_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_fn_1_0=ruleFeatureName();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFeatureRule());
                    						}
                    						add(
                    							current,
                    							"fn",
                    							lv_fn_1_0,
                    							"org.xtext.example.mydsl.SPL.FeatureName");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalSPL.g:291:4: ( (lv_fn_2_0= ruleFeatureName ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==RULE_ID) ) {
                            int LA5_2 = input.LA(2);

                            if ( (LA5_2==EOF||LA5_2==RULE_ID||(LA5_2>=13 && LA5_2<=15)) ) {
                                alt5=1;
                            }


                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalSPL.g:292:5: (lv_fn_2_0= ruleFeatureName )
                    	    {
                    	    // InternalSPL.g:292:5: (lv_fn_2_0= ruleFeatureName )
                    	    // InternalSPL.g:293:6: lv_fn_2_0= ruleFeatureName
                    	    {

                    	    						newCompositeNode(grammarAccess.getFeatureAccess().getFnFeatureNameParserRuleCall_1_1_0());
                    	    					
                    	    pushFollow(FOLLOW_10);
                    	    lv_fn_2_0=ruleFeatureName();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getFeatureRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"fn",
                    	    							lv_fn_2_0,
                    	    							"org.xtext.example.mydsl.SPL.FeatureName");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleFeatureName"
    // InternalSPL.g:315:1: entryRuleFeatureName returns [EObject current=null] : iv_ruleFeatureName= ruleFeatureName EOF ;
    public final EObject entryRuleFeatureName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureName = null;


        try {
            // InternalSPL.g:315:52: (iv_ruleFeatureName= ruleFeatureName EOF )
            // InternalSPL.g:316:2: iv_ruleFeatureName= ruleFeatureName EOF
            {
             newCompositeNode(grammarAccess.getFeatureNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureName=ruleFeatureName();

            state._fsp--;

             current =iv_ruleFeatureName; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureName"


    // $ANTLR start "ruleFeatureName"
    // InternalSPL.g:322:1: ruleFeatureName returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleFeatureName() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSPL.g:328:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalSPL.g:329:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalSPL.g:329:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalSPL.g:330:3: (lv_name_0_0= RULE_ID )
            {
            // InternalSPL.g:330:3: (lv_name_0_0= RULE_ID )
            // InternalSPL.g:331:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getFeatureNameAccess().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getFeatureNameRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureName"


    // $ANTLR start "entryRuleDecisionModel"
    // InternalSPL.g:350:1: entryRuleDecisionModel returns [EObject current=null] : iv_ruleDecisionModel= ruleDecisionModel EOF ;
    public final EObject entryRuleDecisionModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDecisionModel = null;


        try {
            // InternalSPL.g:350:54: (iv_ruleDecisionModel= ruleDecisionModel EOF )
            // InternalSPL.g:351:2: iv_ruleDecisionModel= ruleDecisionModel EOF
            {
             newCompositeNode(grammarAccess.getDecisionModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDecisionModel=ruleDecisionModel();

            state._fsp--;

             current =iv_ruleDecisionModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDecisionModel"


    // $ANTLR start "ruleDecisionModel"
    // InternalSPL.g:357:1: ruleDecisionModel returns [EObject current=null] : (otherlv_0= 'DecM' otherlv_1= '{' ( (lv_decisions_2_0= ruleDecision ) )* ( (lv_dmdec_3_0= ruleDomainDecision1 ) )* ( (lv_dmdec_4_0= ruleDomainDecision2 ) )* ( (lv_dmdec_5_0= ruleDomainDecision3 ) )* ( (lv_fdec_6_0= ruleFeatureDecision ) )* ( (lv_corres_7_0= ruleCorrespondance ) )* otherlv_8= '}' ) ;
    public final EObject ruleDecisionModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_8=null;
        EObject lv_decisions_2_0 = null;

        EObject lv_dmdec_3_0 = null;

        EObject lv_dmdec_4_0 = null;

        EObject lv_dmdec_5_0 = null;

        EObject lv_fdec_6_0 = null;

        EObject lv_corres_7_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:363:2: ( (otherlv_0= 'DecM' otherlv_1= '{' ( (lv_decisions_2_0= ruleDecision ) )* ( (lv_dmdec_3_0= ruleDomainDecision1 ) )* ( (lv_dmdec_4_0= ruleDomainDecision2 ) )* ( (lv_dmdec_5_0= ruleDomainDecision3 ) )* ( (lv_fdec_6_0= ruleFeatureDecision ) )* ( (lv_corres_7_0= ruleCorrespondance ) )* otherlv_8= '}' ) )
            // InternalSPL.g:364:2: (otherlv_0= 'DecM' otherlv_1= '{' ( (lv_decisions_2_0= ruleDecision ) )* ( (lv_dmdec_3_0= ruleDomainDecision1 ) )* ( (lv_dmdec_4_0= ruleDomainDecision2 ) )* ( (lv_dmdec_5_0= ruleDomainDecision3 ) )* ( (lv_fdec_6_0= ruleFeatureDecision ) )* ( (lv_corres_7_0= ruleCorrespondance ) )* otherlv_8= '}' )
            {
            // InternalSPL.g:364:2: (otherlv_0= 'DecM' otherlv_1= '{' ( (lv_decisions_2_0= ruleDecision ) )* ( (lv_dmdec_3_0= ruleDomainDecision1 ) )* ( (lv_dmdec_4_0= ruleDomainDecision2 ) )* ( (lv_dmdec_5_0= ruleDomainDecision3 ) )* ( (lv_fdec_6_0= ruleFeatureDecision ) )* ( (lv_corres_7_0= ruleCorrespondance ) )* otherlv_8= '}' )
            // InternalSPL.g:365:3: otherlv_0= 'DecM' otherlv_1= '{' ( (lv_decisions_2_0= ruleDecision ) )* ( (lv_dmdec_3_0= ruleDomainDecision1 ) )* ( (lv_dmdec_4_0= ruleDomainDecision2 ) )* ( (lv_dmdec_5_0= ruleDomainDecision3 ) )* ( (lv_fdec_6_0= ruleFeatureDecision ) )* ( (lv_corres_7_0= ruleCorrespondance ) )* otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,16,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getDecisionModelAccess().getDecMKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_11); 

            			newLeafNode(otherlv_1, grammarAccess.getDecisionModelAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalSPL.g:373:3: ( (lv_decisions_2_0= ruleDecision ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_ID) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalSPL.g:374:4: (lv_decisions_2_0= ruleDecision )
            	    {
            	    // InternalSPL.g:374:4: (lv_decisions_2_0= ruleDecision )
            	    // InternalSPL.g:375:5: lv_decisions_2_0= ruleDecision
            	    {

            	    					newCompositeNode(grammarAccess.getDecisionModelAccess().getDecisionsDecisionParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_11);
            	    lv_decisions_2_0=ruleDecision();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDecisionModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"decisions",
            	    						lv_decisions_2_0,
            	    						"org.xtext.example.mydsl.SPL.Decision");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            // InternalSPL.g:392:3: ( (lv_dmdec_3_0= ruleDomainDecision1 ) )*
            loop8:
            do {
                int alt8=2;
                alt8 = dfa8.predict(input);
                switch (alt8) {
            	case 1 :
            	    // InternalSPL.g:393:4: (lv_dmdec_3_0= ruleDomainDecision1 )
            	    {
            	    // InternalSPL.g:393:4: (lv_dmdec_3_0= ruleDomainDecision1 )
            	    // InternalSPL.g:394:5: lv_dmdec_3_0= ruleDomainDecision1
            	    {

            	    					newCompositeNode(grammarAccess.getDecisionModelAccess().getDmdecDomainDecision1ParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_dmdec_3_0=ruleDomainDecision1();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDecisionModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"dmdec",
            	    						lv_dmdec_3_0,
            	    						"org.xtext.example.mydsl.SPL.DomainDecision1");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            // InternalSPL.g:411:3: ( (lv_dmdec_4_0= ruleDomainDecision2 ) )*
            loop9:
            do {
                int alt9=2;
                alt9 = dfa9.predict(input);
                switch (alt9) {
            	case 1 :
            	    // InternalSPL.g:412:4: (lv_dmdec_4_0= ruleDomainDecision2 )
            	    {
            	    // InternalSPL.g:412:4: (lv_dmdec_4_0= ruleDomainDecision2 )
            	    // InternalSPL.g:413:5: lv_dmdec_4_0= ruleDomainDecision2
            	    {

            	    					newCompositeNode(grammarAccess.getDecisionModelAccess().getDmdecDomainDecision2ParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_dmdec_4_0=ruleDomainDecision2();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDecisionModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"dmdec",
            	    						lv_dmdec_4_0,
            	    						"org.xtext.example.mydsl.SPL.DomainDecision2");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            // InternalSPL.g:430:3: ( (lv_dmdec_5_0= ruleDomainDecision3 ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==27) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalSPL.g:431:4: (lv_dmdec_5_0= ruleDomainDecision3 )
            	    {
            	    // InternalSPL.g:431:4: (lv_dmdec_5_0= ruleDomainDecision3 )
            	    // InternalSPL.g:432:5: lv_dmdec_5_0= ruleDomainDecision3
            	    {

            	    					newCompositeNode(grammarAccess.getDecisionModelAccess().getDmdecDomainDecision3ParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_dmdec_5_0=ruleDomainDecision3();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDecisionModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"dmdec",
            	    						lv_dmdec_5_0,
            	    						"org.xtext.example.mydsl.SPL.DomainDecision3");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            // InternalSPL.g:449:3: ( (lv_fdec_6_0= ruleFeatureDecision ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==31) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalSPL.g:450:4: (lv_fdec_6_0= ruleFeatureDecision )
            	    {
            	    // InternalSPL.g:450:4: (lv_fdec_6_0= ruleFeatureDecision )
            	    // InternalSPL.g:451:5: lv_fdec_6_0= ruleFeatureDecision
            	    {

            	    					newCompositeNode(grammarAccess.getDecisionModelAccess().getFdecFeatureDecisionParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_13);
            	    lv_fdec_6_0=ruleFeatureDecision();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDecisionModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"fdec",
            	    						lv_fdec_6_0,
            	    						"org.xtext.example.mydsl.SPL.FeatureDecision");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // InternalSPL.g:468:3: ( (lv_corres_7_0= ruleCorrespondance ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==33) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalSPL.g:469:4: (lv_corres_7_0= ruleCorrespondance )
            	    {
            	    // InternalSPL.g:469:4: (lv_corres_7_0= ruleCorrespondance )
            	    // InternalSPL.g:470:5: lv_corres_7_0= ruleCorrespondance
            	    {

            	    					newCompositeNode(grammarAccess.getDecisionModelAccess().getCorresCorrespondanceParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_14);
            	    lv_corres_7_0=ruleCorrespondance();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDecisionModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"corres",
            	    						lv_corres_7_0,
            	    						"org.xtext.example.mydsl.SPL.Correspondance");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            otherlv_8=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getDecisionModelAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDecisionModel"


    // $ANTLR start "entryRuleDomainModel"
    // InternalSPL.g:495:1: entryRuleDomainModel returns [EObject current=null] : iv_ruleDomainModel= ruleDomainModel EOF ;
    public final EObject entryRuleDomainModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainModel = null;


        try {
            // InternalSPL.g:495:52: (iv_ruleDomainModel= ruleDomainModel EOF )
            // InternalSPL.g:496:2: iv_ruleDomainModel= ruleDomainModel EOF
            {
             newCompositeNode(grammarAccess.getDomainModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDomainModel=ruleDomainModel();

            state._fsp--;

             current =iv_ruleDomainModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainModel"


    // $ANTLR start "ruleDomainModel"
    // InternalSPL.g:502:1: ruleDomainModel returns [EObject current=null] : (otherlv_0= 'State_Chart' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_states_3_0= ruleState ) )* ( (lv_transitions_4_0= ruleTransition ) )* (otherlv_5= ',' )? ( (lv_Dfs_6_0= ruleDomainFacts ) )* ( (lv_tet_7_0= ruleTransitionEffect ) )* otherlv_8= '}' ) ;
    public final EObject ruleDomainModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_8=null;
        EObject lv_states_3_0 = null;

        EObject lv_transitions_4_0 = null;

        EObject lv_Dfs_6_0 = null;

        EObject lv_tet_7_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:508:2: ( (otherlv_0= 'State_Chart' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_states_3_0= ruleState ) )* ( (lv_transitions_4_0= ruleTransition ) )* (otherlv_5= ',' )? ( (lv_Dfs_6_0= ruleDomainFacts ) )* ( (lv_tet_7_0= ruleTransitionEffect ) )* otherlv_8= '}' ) )
            // InternalSPL.g:509:2: (otherlv_0= 'State_Chart' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_states_3_0= ruleState ) )* ( (lv_transitions_4_0= ruleTransition ) )* (otherlv_5= ',' )? ( (lv_Dfs_6_0= ruleDomainFacts ) )* ( (lv_tet_7_0= ruleTransitionEffect ) )* otherlv_8= '}' )
            {
            // InternalSPL.g:509:2: (otherlv_0= 'State_Chart' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_states_3_0= ruleState ) )* ( (lv_transitions_4_0= ruleTransition ) )* (otherlv_5= ',' )? ( (lv_Dfs_6_0= ruleDomainFacts ) )* ( (lv_tet_7_0= ruleTransitionEffect ) )* otherlv_8= '}' )
            // InternalSPL.g:510:3: otherlv_0= 'State_Chart' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_states_3_0= ruleState ) )* ( (lv_transitions_4_0= ruleTransition ) )* (otherlv_5= ',' )? ( (lv_Dfs_6_0= ruleDomainFacts ) )* ( (lv_tet_7_0= ruleTransitionEffect ) )* otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getDomainModelAccess().getState_ChartKeyword_0());
            		
            // InternalSPL.g:514:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalSPL.g:515:4: (lv_name_1_0= RULE_ID )
            {
            // InternalSPL.g:515:4: (lv_name_1_0= RULE_ID )
            // InternalSPL.g:516:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(lv_name_1_0, grammarAccess.getDomainModelAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDomainModelRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getDomainModelAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalSPL.g:536:3: ( (lv_states_3_0= ruleState ) )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==18) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalSPL.g:537:4: (lv_states_3_0= ruleState )
            	    {
            	    // InternalSPL.g:537:4: (lv_states_3_0= ruleState )
            	    // InternalSPL.g:538:5: lv_states_3_0= ruleState
            	    {

            	    					newCompositeNode(grammarAccess.getDomainModelAccess().getStatesStateParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_16);
            	    lv_states_3_0=ruleState();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDomainModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"states",
            	    						lv_states_3_0,
            	    						"org.xtext.example.mydsl.SPL.State");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            // InternalSPL.g:555:3: ( (lv_transitions_4_0= ruleTransition ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==19) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalSPL.g:556:4: (lv_transitions_4_0= ruleTransition )
            	    {
            	    // InternalSPL.g:556:4: (lv_transitions_4_0= ruleTransition )
            	    // InternalSPL.g:557:5: lv_transitions_4_0= ruleTransition
            	    {

            	    					newCompositeNode(grammarAccess.getDomainModelAccess().getTransitionsTransitionParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_17);
            	    lv_transitions_4_0=ruleTransition();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDomainModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"transitions",
            	    						lv_transitions_4_0,
            	    						"org.xtext.example.mydsl.SPL.Transition");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            // InternalSPL.g:574:3: (otherlv_5= ',' )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==13) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalSPL.g:575:4: otherlv_5= ','
                    {
                    otherlv_5=(Token)match(input,13,FOLLOW_18); 

                    				newLeafNode(otherlv_5, grammarAccess.getDomainModelAccess().getCommaKeyword_5());
                    			

                    }
                    break;

            }

            // InternalSPL.g:580:3: ( (lv_Dfs_6_0= ruleDomainFacts ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_ID) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalSPL.g:581:4: (lv_Dfs_6_0= ruleDomainFacts )
            	    {
            	    // InternalSPL.g:581:4: (lv_Dfs_6_0= ruleDomainFacts )
            	    // InternalSPL.g:582:5: lv_Dfs_6_0= ruleDomainFacts
            	    {

            	    					newCompositeNode(grammarAccess.getDomainModelAccess().getDfsDomainFactsParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_18);
            	    lv_Dfs_6_0=ruleDomainFacts();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDomainModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"Dfs",
            	    						lv_Dfs_6_0,
            	    						"org.xtext.example.mydsl.SPL.DomainFacts");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            // InternalSPL.g:599:3: ( (lv_tet_7_0= ruleTransitionEffect ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==34) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalSPL.g:600:4: (lv_tet_7_0= ruleTransitionEffect )
            	    {
            	    // InternalSPL.g:600:4: (lv_tet_7_0= ruleTransitionEffect )
            	    // InternalSPL.g:601:5: lv_tet_7_0= ruleTransitionEffect
            	    {

            	    					newCompositeNode(grammarAccess.getDomainModelAccess().getTetTransitionEffectParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_19);
            	    lv_tet_7_0=ruleTransitionEffect();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDomainModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"tet",
            	    						lv_tet_7_0,
            	    						"org.xtext.example.mydsl.SPL.TransitionEffect");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            otherlv_8=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getDomainModelAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainModel"


    // $ANTLR start "entryRuleTransitionLabel"
    // InternalSPL.g:626:1: entryRuleTransitionLabel returns [EObject current=null] : iv_ruleTransitionLabel= ruleTransitionLabel EOF ;
    public final EObject entryRuleTransitionLabel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransitionLabel = null;


        try {
            // InternalSPL.g:626:56: (iv_ruleTransitionLabel= ruleTransitionLabel EOF )
            // InternalSPL.g:627:2: iv_ruleTransitionLabel= ruleTransitionLabel EOF
            {
             newCompositeNode(grammarAccess.getTransitionLabelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransitionLabel=ruleTransitionLabel();

            state._fsp--;

             current =iv_ruleTransitionLabel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransitionLabel"


    // $ANTLR start "ruleTransitionLabel"
    // InternalSPL.g:633:1: ruleTransitionLabel returns [EObject current=null] : ( (lv_label_0_0= RULE_ID ) ) ;
    public final EObject ruleTransitionLabel() throws RecognitionException {
        EObject current = null;

        Token lv_label_0_0=null;


        	enterRule();

        try {
            // InternalSPL.g:639:2: ( ( (lv_label_0_0= RULE_ID ) ) )
            // InternalSPL.g:640:2: ( (lv_label_0_0= RULE_ID ) )
            {
            // InternalSPL.g:640:2: ( (lv_label_0_0= RULE_ID ) )
            // InternalSPL.g:641:3: (lv_label_0_0= RULE_ID )
            {
            // InternalSPL.g:641:3: (lv_label_0_0= RULE_ID )
            // InternalSPL.g:642:4: lv_label_0_0= RULE_ID
            {
            lv_label_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_label_0_0, grammarAccess.getTransitionLabelAccess().getLabelIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getTransitionLabelRule());
            				}
            				setWithLastConsumed(
            					current,
            					"label",
            					lv_label_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransitionLabel"


    // $ANTLR start "entryRuleState"
    // InternalSPL.g:661:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalSPL.g:661:46: (iv_ruleState= ruleState EOF )
            // InternalSPL.g:662:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalSPL.g:668:1: ruleState returns [EObject current=null] : (otherlv_0= 'State' ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalSPL.g:674:2: ( (otherlv_0= 'State' ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalSPL.g:675:2: (otherlv_0= 'State' ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalSPL.g:675:2: (otherlv_0= 'State' ( (lv_name_1_0= RULE_ID ) ) )
            // InternalSPL.g:676:3: otherlv_0= 'State' ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getStateAccess().getStateKeyword_0());
            		
            // InternalSPL.g:680:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalSPL.g:681:4: (lv_name_1_0= RULE_ID )
            {
            // InternalSPL.g:681:4: (lv_name_1_0= RULE_ID )
            // InternalSPL.g:682:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStateRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalSPL.g:702:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalSPL.g:702:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalSPL.g:703:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalSPL.g:709:1: ruleTransition returns [EObject current=null] : (otherlv_0= 'Transition' ( (lv_tl_1_0= ruleTransitionLabel ) ) ( (lv_source_2_0= ruleState ) )* otherlv_3= 'to' ( (lv_target_4_0= ruleState ) )* ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        EObject lv_tl_1_0 = null;

        EObject lv_source_2_0 = null;

        EObject lv_target_4_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:715:2: ( (otherlv_0= 'Transition' ( (lv_tl_1_0= ruleTransitionLabel ) ) ( (lv_source_2_0= ruleState ) )* otherlv_3= 'to' ( (lv_target_4_0= ruleState ) )* ) )
            // InternalSPL.g:716:2: (otherlv_0= 'Transition' ( (lv_tl_1_0= ruleTransitionLabel ) ) ( (lv_source_2_0= ruleState ) )* otherlv_3= 'to' ( (lv_target_4_0= ruleState ) )* )
            {
            // InternalSPL.g:716:2: (otherlv_0= 'Transition' ( (lv_tl_1_0= ruleTransitionLabel ) ) ( (lv_source_2_0= ruleState ) )* otherlv_3= 'to' ( (lv_target_4_0= ruleState ) )* )
            // InternalSPL.g:717:3: otherlv_0= 'Transition' ( (lv_tl_1_0= ruleTransitionLabel ) ) ( (lv_source_2_0= ruleState ) )* otherlv_3= 'to' ( (lv_target_4_0= ruleState ) )*
            {
            otherlv_0=(Token)match(input,19,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getTransitionKeyword_0());
            		
            // InternalSPL.g:721:3: ( (lv_tl_1_0= ruleTransitionLabel ) )
            // InternalSPL.g:722:4: (lv_tl_1_0= ruleTransitionLabel )
            {
            // InternalSPL.g:722:4: (lv_tl_1_0= ruleTransitionLabel )
            // InternalSPL.g:723:5: lv_tl_1_0= ruleTransitionLabel
            {

            					newCompositeNode(grammarAccess.getTransitionAccess().getTlTransitionLabelParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_20);
            lv_tl_1_0=ruleTransitionLabel();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTransitionRule());
            					}
            					add(
            						current,
            						"tl",
            						lv_tl_1_0,
            						"org.xtext.example.mydsl.SPL.TransitionLabel");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:740:3: ( (lv_source_2_0= ruleState ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==18) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalSPL.g:741:4: (lv_source_2_0= ruleState )
            	    {
            	    // InternalSPL.g:741:4: (lv_source_2_0= ruleState )
            	    // InternalSPL.g:742:5: lv_source_2_0= ruleState
            	    {

            	    					newCompositeNode(grammarAccess.getTransitionAccess().getSourceStateParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_20);
            	    lv_source_2_0=ruleState();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTransitionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"source",
            	    						lv_source_2_0,
            	    						"org.xtext.example.mydsl.SPL.State");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            otherlv_3=(Token)match(input,20,FOLLOW_21); 

            			newLeafNode(otherlv_3, grammarAccess.getTransitionAccess().getToKeyword_3());
            		
            // InternalSPL.g:763:3: ( (lv_target_4_0= ruleState ) )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==18) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalSPL.g:764:4: (lv_target_4_0= ruleState )
            	    {
            	    // InternalSPL.g:764:4: (lv_target_4_0= ruleState )
            	    // InternalSPL.g:765:5: lv_target_4_0= ruleState
            	    {

            	    					newCompositeNode(grammarAccess.getTransitionAccess().getTargetStateParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_21);
            	    lv_target_4_0=ruleState();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTransitionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"target",
            	    						lv_target_4_0,
            	    						"org.xtext.example.mydsl.SPL.State");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "entryRuleDecision"
    // InternalSPL.g:786:1: entryRuleDecision returns [EObject current=null] : iv_ruleDecision= ruleDecision EOF ;
    public final EObject entryRuleDecision() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDecision = null;


        try {
            // InternalSPL.g:786:49: (iv_ruleDecision= ruleDecision EOF )
            // InternalSPL.g:787:2: iv_ruleDecision= ruleDecision EOF
            {
             newCompositeNode(grammarAccess.getDecisionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDecision=ruleDecision();

            state._fsp--;

             current =iv_ruleDecision; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDecision"


    // $ANTLR start "ruleDecision"
    // InternalSPL.g:793:1: ruleDecision returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleDecision() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSPL.g:799:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalSPL.g:800:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalSPL.g:800:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalSPL.g:801:3: (lv_name_0_0= RULE_ID )
            {
            // InternalSPL.g:801:3: (lv_name_0_0= RULE_ID )
            // InternalSPL.g:802:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getDecisionAccess().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getDecisionRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDecision"


    // $ANTLR start "entryRuleLogic"
    // InternalSPL.g:821:1: entryRuleLogic returns [String current=null] : iv_ruleLogic= ruleLogic EOF ;
    public final String entryRuleLogic() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleLogic = null;


        try {
            // InternalSPL.g:821:45: (iv_ruleLogic= ruleLogic EOF )
            // InternalSPL.g:822:2: iv_ruleLogic= ruleLogic EOF
            {
             newCompositeNode(grammarAccess.getLogicRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLogic=ruleLogic();

            state._fsp--;

             current =iv_ruleLogic.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogic"


    // $ANTLR start "ruleLogic"
    // InternalSPL.g:828:1: ruleLogic returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'in' | (kw= 'not' kw= 'in' ) ) ;
    public final AntlrDatatypeRuleToken ruleLogic() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSPL.g:834:2: ( (kw= 'in' | (kw= 'not' kw= 'in' ) ) )
            // InternalSPL.g:835:2: (kw= 'in' | (kw= 'not' kw= 'in' ) )
            {
            // InternalSPL.g:835:2: (kw= 'in' | (kw= 'not' kw= 'in' ) )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==21) ) {
                alt20=1;
            }
            else if ( (LA20_0==22) ) {
                alt20=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // InternalSPL.g:836:3: kw= 'in'
                    {
                    kw=(Token)match(input,21,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getLogicAccess().getInKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSPL.g:842:3: (kw= 'not' kw= 'in' )
                    {
                    // InternalSPL.g:842:3: (kw= 'not' kw= 'in' )
                    // InternalSPL.g:843:4: kw= 'not' kw= 'in'
                    {
                    kw=(Token)match(input,22,FOLLOW_22); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getLogicAccess().getNotKeyword_1_0());
                    			
                    kw=(Token)match(input,21,FOLLOW_2); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getLogicAccess().getInKeyword_1_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogic"


    // $ANTLR start "entryRuleFeatureFacts"
    // InternalSPL.g:858:1: entryRuleFeatureFacts returns [EObject current=null] : iv_ruleFeatureFacts= ruleFeatureFacts EOF ;
    public final EObject entryRuleFeatureFacts() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureFacts = null;


        try {
            // InternalSPL.g:858:53: (iv_ruleFeatureFacts= ruleFeatureFacts EOF )
            // InternalSPL.g:859:2: iv_ruleFeatureFacts= ruleFeatureFacts EOF
            {
             newCompositeNode(grammarAccess.getFeatureFactsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureFacts=ruleFeatureFacts();

            state._fsp--;

             current =iv_ruleFeatureFacts; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureFacts"


    // $ANTLR start "ruleFeatureFacts"
    // InternalSPL.g:865:1: ruleFeatureFacts returns [EObject current=null] : ( ( (lv_fn_0_0= ruleFeatureName ) ) ( (lv_lg_1_0= ruleLogic ) ) otherlv_2= 'Feature_Model' ) ;
    public final EObject ruleFeatureFacts() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_fn_0_0 = null;

        AntlrDatatypeRuleToken lv_lg_1_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:871:2: ( ( ( (lv_fn_0_0= ruleFeatureName ) ) ( (lv_lg_1_0= ruleLogic ) ) otherlv_2= 'Feature_Model' ) )
            // InternalSPL.g:872:2: ( ( (lv_fn_0_0= ruleFeatureName ) ) ( (lv_lg_1_0= ruleLogic ) ) otherlv_2= 'Feature_Model' )
            {
            // InternalSPL.g:872:2: ( ( (lv_fn_0_0= ruleFeatureName ) ) ( (lv_lg_1_0= ruleLogic ) ) otherlv_2= 'Feature_Model' )
            // InternalSPL.g:873:3: ( (lv_fn_0_0= ruleFeatureName ) ) ( (lv_lg_1_0= ruleLogic ) ) otherlv_2= 'Feature_Model'
            {
            // InternalSPL.g:873:3: ( (lv_fn_0_0= ruleFeatureName ) )
            // InternalSPL.g:874:4: (lv_fn_0_0= ruleFeatureName )
            {
            // InternalSPL.g:874:4: (lv_fn_0_0= ruleFeatureName )
            // InternalSPL.g:875:5: lv_fn_0_0= ruleFeatureName
            {

            					newCompositeNode(grammarAccess.getFeatureFactsAccess().getFnFeatureNameParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_23);
            lv_fn_0_0=ruleFeatureName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFeatureFactsRule());
            					}
            					add(
            						current,
            						"fn",
            						lv_fn_0_0,
            						"org.xtext.example.mydsl.SPL.FeatureName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:892:3: ( (lv_lg_1_0= ruleLogic ) )
            // InternalSPL.g:893:4: (lv_lg_1_0= ruleLogic )
            {
            // InternalSPL.g:893:4: (lv_lg_1_0= ruleLogic )
            // InternalSPL.g:894:5: lv_lg_1_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getFeatureFactsAccess().getLgLogicParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_24);
            lv_lg_1_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFeatureFactsRule());
            					}
            					add(
            						current,
            						"lg",
            						lv_lg_1_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getFeatureFactsAccess().getFeature_ModelKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureFacts"


    // $ANTLR start "entryRuleDomainFacts"
    // InternalSPL.g:919:1: entryRuleDomainFacts returns [EObject current=null] : iv_ruleDomainFacts= ruleDomainFacts EOF ;
    public final EObject entryRuleDomainFacts() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainFacts = null;


        try {
            // InternalSPL.g:919:52: (iv_ruleDomainFacts= ruleDomainFacts EOF )
            // InternalSPL.g:920:2: iv_ruleDomainFacts= ruleDomainFacts EOF
            {
             newCompositeNode(grammarAccess.getDomainFactsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDomainFacts=ruleDomainFacts();

            state._fsp--;

             current =iv_ruleDomainFacts; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainFacts"


    // $ANTLR start "ruleDomainFacts"
    // InternalSPL.g:926:1: ruleDomainFacts returns [EObject current=null] : ( ( (lv_tL_0_0= ruleTransitionLabel ) ) ( (lv_l_1_0= ruleLogic ) ) otherlv_2= 'Domain_Model' ) ;
    public final EObject ruleDomainFacts() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_tL_0_0 = null;

        AntlrDatatypeRuleToken lv_l_1_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:932:2: ( ( ( (lv_tL_0_0= ruleTransitionLabel ) ) ( (lv_l_1_0= ruleLogic ) ) otherlv_2= 'Domain_Model' ) )
            // InternalSPL.g:933:2: ( ( (lv_tL_0_0= ruleTransitionLabel ) ) ( (lv_l_1_0= ruleLogic ) ) otherlv_2= 'Domain_Model' )
            {
            // InternalSPL.g:933:2: ( ( (lv_tL_0_0= ruleTransitionLabel ) ) ( (lv_l_1_0= ruleLogic ) ) otherlv_2= 'Domain_Model' )
            // InternalSPL.g:934:3: ( (lv_tL_0_0= ruleTransitionLabel ) ) ( (lv_l_1_0= ruleLogic ) ) otherlv_2= 'Domain_Model'
            {
            // InternalSPL.g:934:3: ( (lv_tL_0_0= ruleTransitionLabel ) )
            // InternalSPL.g:935:4: (lv_tL_0_0= ruleTransitionLabel )
            {
            // InternalSPL.g:935:4: (lv_tL_0_0= ruleTransitionLabel )
            // InternalSPL.g:936:5: lv_tL_0_0= ruleTransitionLabel
            {

            					newCompositeNode(grammarAccess.getDomainFactsAccess().getTLTransitionLabelParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_23);
            lv_tL_0_0=ruleTransitionLabel();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDomainFactsRule());
            					}
            					add(
            						current,
            						"tL",
            						lv_tL_0_0,
            						"org.xtext.example.mydsl.SPL.TransitionLabel");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:953:3: ( (lv_l_1_0= ruleLogic ) )
            // InternalSPL.g:954:4: (lv_l_1_0= ruleLogic )
            {
            // InternalSPL.g:954:4: (lv_l_1_0= ruleLogic )
            // InternalSPL.g:955:5: lv_l_1_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getDomainFactsAccess().getLLogicParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_25);
            lv_l_1_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDomainFactsRule());
            					}
            					add(
            						current,
            						"l",
            						lv_l_1_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getDomainFactsAccess().getDomain_ModelKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainFacts"


    // $ANTLR start "entryRuleOperator"
    // InternalSPL.g:980:1: entryRuleOperator returns [EObject current=null] : iv_ruleOperator= ruleOperator EOF ;
    public final EObject entryRuleOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperator = null;


        try {
            // InternalSPL.g:980:49: (iv_ruleOperator= ruleOperator EOF )
            // InternalSPL.g:981:2: iv_ruleOperator= ruleOperator EOF
            {
             newCompositeNode(grammarAccess.getOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOperator=ruleOperator();

            state._fsp--;

             current =iv_ruleOperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperator"


    // $ANTLR start "ruleOperator"
    // InternalSPL.g:987:1: ruleOperator returns [EObject current=null] : ( ( (lv_name_0_1= 'and' | lv_name_0_2= 'or' | lv_name_0_3= 'not' ) ) ) ;
    public final EObject ruleOperator() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_1=null;
        Token lv_name_0_2=null;
        Token lv_name_0_3=null;


        	enterRule();

        try {
            // InternalSPL.g:993:2: ( ( ( (lv_name_0_1= 'and' | lv_name_0_2= 'or' | lv_name_0_3= 'not' ) ) ) )
            // InternalSPL.g:994:2: ( ( (lv_name_0_1= 'and' | lv_name_0_2= 'or' | lv_name_0_3= 'not' ) ) )
            {
            // InternalSPL.g:994:2: ( ( (lv_name_0_1= 'and' | lv_name_0_2= 'or' | lv_name_0_3= 'not' ) ) )
            // InternalSPL.g:995:3: ( (lv_name_0_1= 'and' | lv_name_0_2= 'or' | lv_name_0_3= 'not' ) )
            {
            // InternalSPL.g:995:3: ( (lv_name_0_1= 'and' | lv_name_0_2= 'or' | lv_name_0_3= 'not' ) )
            // InternalSPL.g:996:4: (lv_name_0_1= 'and' | lv_name_0_2= 'or' | lv_name_0_3= 'not' )
            {
            // InternalSPL.g:996:4: (lv_name_0_1= 'and' | lv_name_0_2= 'or' | lv_name_0_3= 'not' )
            int alt21=3;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt21=1;
                }
                break;
            case 26:
                {
                alt21=2;
                }
                break;
            case 22:
                {
                alt21=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }

            switch (alt21) {
                case 1 :
                    // InternalSPL.g:997:5: lv_name_0_1= 'and'
                    {
                    lv_name_0_1=(Token)match(input,25,FOLLOW_2); 

                    					newLeafNode(lv_name_0_1, grammarAccess.getOperatorAccess().getNameAndKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getOperatorRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_1, null);
                    				

                    }
                    break;
                case 2 :
                    // InternalSPL.g:1008:5: lv_name_0_2= 'or'
                    {
                    lv_name_0_2=(Token)match(input,26,FOLLOW_2); 

                    					newLeafNode(lv_name_0_2, grammarAccess.getOperatorAccess().getNameOrKeyword_0_1());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getOperatorRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_2, null);
                    				

                    }
                    break;
                case 3 :
                    // InternalSPL.g:1019:5: lv_name_0_3= 'not'
                    {
                    lv_name_0_3=(Token)match(input,22,FOLLOW_2); 

                    					newLeafNode(lv_name_0_3, grammarAccess.getOperatorAccess().getNameNotKeyword_0_2());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getOperatorRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_3, null);
                    				

                    }
                    break;

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperator"


    // $ANTLR start "entryRuleDomainDecision1"
    // InternalSPL.g:1035:1: entryRuleDomainDecision1 returns [EObject current=null] : iv_ruleDomainDecision1= ruleDomainDecision1 EOF ;
    public final EObject entryRuleDomainDecision1() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainDecision1 = null;


        try {
            // InternalSPL.g:1035:56: (iv_ruleDomainDecision1= ruleDomainDecision1 EOF )
            // InternalSPL.g:1036:2: iv_ruleDomainDecision1= ruleDomainDecision1 EOF
            {
             newCompositeNode(grammarAccess.getDomainDecision1Rule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDomainDecision1=ruleDomainDecision1();

            state._fsp--;

             current =iv_ruleDomainDecision1; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainDecision1"


    // $ANTLR start "ruleDomainDecision1"
    // InternalSPL.g:1042:1: ruleDomainDecision1 returns [EObject current=null] : (otherlv_0= 'DomainDecision' ( (lv_ddec_1_0= ruleDecision ) )* otherlv_2= 'implies' ( (lv_ddom_3_0= ruleTransitionLabel ) )* ( (lv_ll_4_0= ruleLogic ) ) otherlv_5= 'Domain_Model' ( ( (lv_opr_6_0= ruleOperator ) ) ( (lv_ddom1_7_0= ruleTransitionLabel ) )* ( (lv_llg_8_0= ruleLogic ) ) otherlv_9= 'Domain_Model' )* otherlv_10= 'else' ( (lv_ll1_11_0= ruleLogic ) ) otherlv_12= 'Domain_Model' ) ;
    public final EObject ruleDomainDecision1() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        EObject lv_ddec_1_0 = null;

        EObject lv_ddom_3_0 = null;

        AntlrDatatypeRuleToken lv_ll_4_0 = null;

        EObject lv_opr_6_0 = null;

        EObject lv_ddom1_7_0 = null;

        AntlrDatatypeRuleToken lv_llg_8_0 = null;

        AntlrDatatypeRuleToken lv_ll1_11_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:1048:2: ( (otherlv_0= 'DomainDecision' ( (lv_ddec_1_0= ruleDecision ) )* otherlv_2= 'implies' ( (lv_ddom_3_0= ruleTransitionLabel ) )* ( (lv_ll_4_0= ruleLogic ) ) otherlv_5= 'Domain_Model' ( ( (lv_opr_6_0= ruleOperator ) ) ( (lv_ddom1_7_0= ruleTransitionLabel ) )* ( (lv_llg_8_0= ruleLogic ) ) otherlv_9= 'Domain_Model' )* otherlv_10= 'else' ( (lv_ll1_11_0= ruleLogic ) ) otherlv_12= 'Domain_Model' ) )
            // InternalSPL.g:1049:2: (otherlv_0= 'DomainDecision' ( (lv_ddec_1_0= ruleDecision ) )* otherlv_2= 'implies' ( (lv_ddom_3_0= ruleTransitionLabel ) )* ( (lv_ll_4_0= ruleLogic ) ) otherlv_5= 'Domain_Model' ( ( (lv_opr_6_0= ruleOperator ) ) ( (lv_ddom1_7_0= ruleTransitionLabel ) )* ( (lv_llg_8_0= ruleLogic ) ) otherlv_9= 'Domain_Model' )* otherlv_10= 'else' ( (lv_ll1_11_0= ruleLogic ) ) otherlv_12= 'Domain_Model' )
            {
            // InternalSPL.g:1049:2: (otherlv_0= 'DomainDecision' ( (lv_ddec_1_0= ruleDecision ) )* otherlv_2= 'implies' ( (lv_ddom_3_0= ruleTransitionLabel ) )* ( (lv_ll_4_0= ruleLogic ) ) otherlv_5= 'Domain_Model' ( ( (lv_opr_6_0= ruleOperator ) ) ( (lv_ddom1_7_0= ruleTransitionLabel ) )* ( (lv_llg_8_0= ruleLogic ) ) otherlv_9= 'Domain_Model' )* otherlv_10= 'else' ( (lv_ll1_11_0= ruleLogic ) ) otherlv_12= 'Domain_Model' )
            // InternalSPL.g:1050:3: otherlv_0= 'DomainDecision' ( (lv_ddec_1_0= ruleDecision ) )* otherlv_2= 'implies' ( (lv_ddom_3_0= ruleTransitionLabel ) )* ( (lv_ll_4_0= ruleLogic ) ) otherlv_5= 'Domain_Model' ( ( (lv_opr_6_0= ruleOperator ) ) ( (lv_ddom1_7_0= ruleTransitionLabel ) )* ( (lv_llg_8_0= ruleLogic ) ) otherlv_9= 'Domain_Model' )* otherlv_10= 'else' ( (lv_ll1_11_0= ruleLogic ) ) otherlv_12= 'Domain_Model'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_26); 

            			newLeafNode(otherlv_0, grammarAccess.getDomainDecision1Access().getDomainDecisionKeyword_0());
            		
            // InternalSPL.g:1054:3: ( (lv_ddec_1_0= ruleDecision ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==RULE_ID) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalSPL.g:1055:4: (lv_ddec_1_0= ruleDecision )
            	    {
            	    // InternalSPL.g:1055:4: (lv_ddec_1_0= ruleDecision )
            	    // InternalSPL.g:1056:5: lv_ddec_1_0= ruleDecision
            	    {

            	    					newCompositeNode(grammarAccess.getDomainDecision1Access().getDdecDecisionParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_26);
            	    lv_ddec_1_0=ruleDecision();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDomainDecision1Rule());
            	    					}
            	    					add(
            	    						current,
            	    						"ddec",
            	    						lv_ddec_1_0,
            	    						"org.xtext.example.mydsl.SPL.Decision");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

            otherlv_2=(Token)match(input,28,FOLLOW_27); 

            			newLeafNode(otherlv_2, grammarAccess.getDomainDecision1Access().getImpliesKeyword_2());
            		
            // InternalSPL.g:1077:3: ( (lv_ddom_3_0= ruleTransitionLabel ) )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==RULE_ID) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalSPL.g:1078:4: (lv_ddom_3_0= ruleTransitionLabel )
            	    {
            	    // InternalSPL.g:1078:4: (lv_ddom_3_0= ruleTransitionLabel )
            	    // InternalSPL.g:1079:5: lv_ddom_3_0= ruleTransitionLabel
            	    {

            	    					newCompositeNode(grammarAccess.getDomainDecision1Access().getDdomTransitionLabelParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_27);
            	    lv_ddom_3_0=ruleTransitionLabel();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDomainDecision1Rule());
            	    					}
            	    					add(
            	    						current,
            	    						"ddom",
            	    						lv_ddom_3_0,
            	    						"org.xtext.example.mydsl.SPL.TransitionLabel");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

            // InternalSPL.g:1096:3: ( (lv_ll_4_0= ruleLogic ) )
            // InternalSPL.g:1097:4: (lv_ll_4_0= ruleLogic )
            {
            // InternalSPL.g:1097:4: (lv_ll_4_0= ruleLogic )
            // InternalSPL.g:1098:5: lv_ll_4_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getDomainDecision1Access().getLlLogicParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_25);
            lv_ll_4_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDomainDecision1Rule());
            					}
            					add(
            						current,
            						"ll",
            						lv_ll_4_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,24,FOLLOW_28); 

            			newLeafNode(otherlv_5, grammarAccess.getDomainDecision1Access().getDomain_ModelKeyword_5());
            		
            // InternalSPL.g:1119:3: ( ( (lv_opr_6_0= ruleOperator ) ) ( (lv_ddom1_7_0= ruleTransitionLabel ) )* ( (lv_llg_8_0= ruleLogic ) ) otherlv_9= 'Domain_Model' )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==22||(LA25_0>=25 && LA25_0<=26)) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalSPL.g:1120:4: ( (lv_opr_6_0= ruleOperator ) ) ( (lv_ddom1_7_0= ruleTransitionLabel ) )* ( (lv_llg_8_0= ruleLogic ) ) otherlv_9= 'Domain_Model'
            	    {
            	    // InternalSPL.g:1120:4: ( (lv_opr_6_0= ruleOperator ) )
            	    // InternalSPL.g:1121:5: (lv_opr_6_0= ruleOperator )
            	    {
            	    // InternalSPL.g:1121:5: (lv_opr_6_0= ruleOperator )
            	    // InternalSPL.g:1122:6: lv_opr_6_0= ruleOperator
            	    {

            	    						newCompositeNode(grammarAccess.getDomainDecision1Access().getOprOperatorParserRuleCall_6_0_0());
            	    					
            	    pushFollow(FOLLOW_27);
            	    lv_opr_6_0=ruleOperator();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getDomainDecision1Rule());
            	    						}
            	    						add(
            	    							current,
            	    							"opr",
            	    							lv_opr_6_0,
            	    							"org.xtext.example.mydsl.SPL.Operator");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalSPL.g:1139:4: ( (lv_ddom1_7_0= ruleTransitionLabel ) )*
            	    loop24:
            	    do {
            	        int alt24=2;
            	        int LA24_0 = input.LA(1);

            	        if ( (LA24_0==RULE_ID) ) {
            	            alt24=1;
            	        }


            	        switch (alt24) {
            	    	case 1 :
            	    	    // InternalSPL.g:1140:5: (lv_ddom1_7_0= ruleTransitionLabel )
            	    	    {
            	    	    // InternalSPL.g:1140:5: (lv_ddom1_7_0= ruleTransitionLabel )
            	    	    // InternalSPL.g:1141:6: lv_ddom1_7_0= ruleTransitionLabel
            	    	    {

            	    	    						newCompositeNode(grammarAccess.getDomainDecision1Access().getDdom1TransitionLabelParserRuleCall_6_1_0());
            	    	    					
            	    	    pushFollow(FOLLOW_27);
            	    	    lv_ddom1_7_0=ruleTransitionLabel();

            	    	    state._fsp--;


            	    	    						if (current==null) {
            	    	    							current = createModelElementForParent(grammarAccess.getDomainDecision1Rule());
            	    	    						}
            	    	    						add(
            	    	    							current,
            	    	    							"ddom1",
            	    	    							lv_ddom1_7_0,
            	    	    							"org.xtext.example.mydsl.SPL.TransitionLabel");
            	    	    						afterParserOrEnumRuleCall();
            	    	    					

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop24;
            	        }
            	    } while (true);

            	    // InternalSPL.g:1158:4: ( (lv_llg_8_0= ruleLogic ) )
            	    // InternalSPL.g:1159:5: (lv_llg_8_0= ruleLogic )
            	    {
            	    // InternalSPL.g:1159:5: (lv_llg_8_0= ruleLogic )
            	    // InternalSPL.g:1160:6: lv_llg_8_0= ruleLogic
            	    {

            	    						newCompositeNode(grammarAccess.getDomainDecision1Access().getLlgLogicParserRuleCall_6_2_0());
            	    					
            	    pushFollow(FOLLOW_25);
            	    lv_llg_8_0=ruleLogic();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getDomainDecision1Rule());
            	    						}
            	    						add(
            	    							current,
            	    							"llg",
            	    							lv_llg_8_0,
            	    							"org.xtext.example.mydsl.SPL.Logic");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_9=(Token)match(input,24,FOLLOW_28); 

            	    				newLeafNode(otherlv_9, grammarAccess.getDomainDecision1Access().getDomain_ModelKeyword_6_3());
            	    			

            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            otherlv_10=(Token)match(input,29,FOLLOW_23); 

            			newLeafNode(otherlv_10, grammarAccess.getDomainDecision1Access().getElseKeyword_7());
            		
            // InternalSPL.g:1186:3: ( (lv_ll1_11_0= ruleLogic ) )
            // InternalSPL.g:1187:4: (lv_ll1_11_0= ruleLogic )
            {
            // InternalSPL.g:1187:4: (lv_ll1_11_0= ruleLogic )
            // InternalSPL.g:1188:5: lv_ll1_11_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getDomainDecision1Access().getLl1LogicParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_25);
            lv_ll1_11_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDomainDecision1Rule());
            					}
            					add(
            						current,
            						"ll1",
            						lv_ll1_11_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_12=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_12, grammarAccess.getDomainDecision1Access().getDomain_ModelKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainDecision1"


    // $ANTLR start "entryRuleDomainDecision3"
    // InternalSPL.g:1213:1: entryRuleDomainDecision3 returns [EObject current=null] : iv_ruleDomainDecision3= ruleDomainDecision3 EOF ;
    public final EObject entryRuleDomainDecision3() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainDecision3 = null;


        try {
            // InternalSPL.g:1213:56: (iv_ruleDomainDecision3= ruleDomainDecision3 EOF )
            // InternalSPL.g:1214:2: iv_ruleDomainDecision3= ruleDomainDecision3 EOF
            {
             newCompositeNode(grammarAccess.getDomainDecision3Rule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDomainDecision3=ruleDomainDecision3();

            state._fsp--;

             current =iv_ruleDomainDecision3; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainDecision3"


    // $ANTLR start "ruleDomainDecision3"
    // InternalSPL.g:1220:1: ruleDomainDecision3 returns [EObject current=null] : (otherlv_0= 'DomainDecision' ( (lv_ddec_1_0= ruleDecision ) )* otherlv_2= 'implies' ( (lv_ddom_3_0= ruleTransitionLabel ) )* ( (lv_l1_4_0= ruleLogic ) ) otherlv_5= 'DomainModel' ) ;
    public final EObject ruleDomainDecision3() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_ddec_1_0 = null;

        EObject lv_ddom_3_0 = null;

        AntlrDatatypeRuleToken lv_l1_4_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:1226:2: ( (otherlv_0= 'DomainDecision' ( (lv_ddec_1_0= ruleDecision ) )* otherlv_2= 'implies' ( (lv_ddom_3_0= ruleTransitionLabel ) )* ( (lv_l1_4_0= ruleLogic ) ) otherlv_5= 'DomainModel' ) )
            // InternalSPL.g:1227:2: (otherlv_0= 'DomainDecision' ( (lv_ddec_1_0= ruleDecision ) )* otherlv_2= 'implies' ( (lv_ddom_3_0= ruleTransitionLabel ) )* ( (lv_l1_4_0= ruleLogic ) ) otherlv_5= 'DomainModel' )
            {
            // InternalSPL.g:1227:2: (otherlv_0= 'DomainDecision' ( (lv_ddec_1_0= ruleDecision ) )* otherlv_2= 'implies' ( (lv_ddom_3_0= ruleTransitionLabel ) )* ( (lv_l1_4_0= ruleLogic ) ) otherlv_5= 'DomainModel' )
            // InternalSPL.g:1228:3: otherlv_0= 'DomainDecision' ( (lv_ddec_1_0= ruleDecision ) )* otherlv_2= 'implies' ( (lv_ddom_3_0= ruleTransitionLabel ) )* ( (lv_l1_4_0= ruleLogic ) ) otherlv_5= 'DomainModel'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_26); 

            			newLeafNode(otherlv_0, grammarAccess.getDomainDecision3Access().getDomainDecisionKeyword_0());
            		
            // InternalSPL.g:1232:3: ( (lv_ddec_1_0= ruleDecision ) )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==RULE_ID) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalSPL.g:1233:4: (lv_ddec_1_0= ruleDecision )
            	    {
            	    // InternalSPL.g:1233:4: (lv_ddec_1_0= ruleDecision )
            	    // InternalSPL.g:1234:5: lv_ddec_1_0= ruleDecision
            	    {

            	    					newCompositeNode(grammarAccess.getDomainDecision3Access().getDdecDecisionParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_26);
            	    lv_ddec_1_0=ruleDecision();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDomainDecision3Rule());
            	    					}
            	    					add(
            	    						current,
            	    						"ddec",
            	    						lv_ddec_1_0,
            	    						"org.xtext.example.mydsl.SPL.Decision");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

            otherlv_2=(Token)match(input,28,FOLLOW_27); 

            			newLeafNode(otherlv_2, grammarAccess.getDomainDecision3Access().getImpliesKeyword_2());
            		
            // InternalSPL.g:1255:3: ( (lv_ddom_3_0= ruleTransitionLabel ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==RULE_ID) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalSPL.g:1256:4: (lv_ddom_3_0= ruleTransitionLabel )
            	    {
            	    // InternalSPL.g:1256:4: (lv_ddom_3_0= ruleTransitionLabel )
            	    // InternalSPL.g:1257:5: lv_ddom_3_0= ruleTransitionLabel
            	    {

            	    					newCompositeNode(grammarAccess.getDomainDecision3Access().getDdomTransitionLabelParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_27);
            	    lv_ddom_3_0=ruleTransitionLabel();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDomainDecision3Rule());
            	    					}
            	    					add(
            	    						current,
            	    						"ddom",
            	    						lv_ddom_3_0,
            	    						"org.xtext.example.mydsl.SPL.TransitionLabel");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

            // InternalSPL.g:1274:3: ( (lv_l1_4_0= ruleLogic ) )
            // InternalSPL.g:1275:4: (lv_l1_4_0= ruleLogic )
            {
            // InternalSPL.g:1275:4: (lv_l1_4_0= ruleLogic )
            // InternalSPL.g:1276:5: lv_l1_4_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getDomainDecision3Access().getL1LogicParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_29);
            lv_l1_4_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDomainDecision3Rule());
            					}
            					add(
            						current,
            						"l1",
            						lv_l1_4_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,30,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getDomainDecision3Access().getDomainModelKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainDecision3"


    // $ANTLR start "entryRuleDomainDecision2"
    // InternalSPL.g:1301:1: entryRuleDomainDecision2 returns [EObject current=null] : iv_ruleDomainDecision2= ruleDomainDecision2 EOF ;
    public final EObject entryRuleDomainDecision2() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainDecision2 = null;


        try {
            // InternalSPL.g:1301:56: (iv_ruleDomainDecision2= ruleDomainDecision2 EOF )
            // InternalSPL.g:1302:2: iv_ruleDomainDecision2= ruleDomainDecision2 EOF
            {
             newCompositeNode(grammarAccess.getDomainDecision2Rule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDomainDecision2=ruleDomainDecision2();

            state._fsp--;

             current =iv_ruleDomainDecision2; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainDecision2"


    // $ANTLR start "ruleDomainDecision2"
    // InternalSPL.g:1308:1: ruleDomainDecision2 returns [EObject current=null] : (otherlv_0= 'DomainDecision' ( (lv_ddec2_1_0= ruleDecision ) )* ( (lv_op_2_0= ruleOperator ) ) ( (lv_tl_3_0= ruleTransitionLabel ) ) ( (lv_ll6_4_0= ruleLogic ) ) otherlv_5= 'Domain_Model' otherlv_6= 'implies' ( (lv_tl2_7_0= ruleTransitionLabel ) ) ( (lv_ll7_8_0= ruleLogic ) ) otherlv_9= 'DomainModel' otherlv_10= 'else' ( (lv_ll8_11_0= ruleLogic ) ) otherlv_12= 'Domain_Model' ) ;
    public final EObject ruleDomainDecision2() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        EObject lv_ddec2_1_0 = null;

        EObject lv_op_2_0 = null;

        EObject lv_tl_3_0 = null;

        AntlrDatatypeRuleToken lv_ll6_4_0 = null;

        EObject lv_tl2_7_0 = null;

        AntlrDatatypeRuleToken lv_ll7_8_0 = null;

        AntlrDatatypeRuleToken lv_ll8_11_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:1314:2: ( (otherlv_0= 'DomainDecision' ( (lv_ddec2_1_0= ruleDecision ) )* ( (lv_op_2_0= ruleOperator ) ) ( (lv_tl_3_0= ruleTransitionLabel ) ) ( (lv_ll6_4_0= ruleLogic ) ) otherlv_5= 'Domain_Model' otherlv_6= 'implies' ( (lv_tl2_7_0= ruleTransitionLabel ) ) ( (lv_ll7_8_0= ruleLogic ) ) otherlv_9= 'DomainModel' otherlv_10= 'else' ( (lv_ll8_11_0= ruleLogic ) ) otherlv_12= 'Domain_Model' ) )
            // InternalSPL.g:1315:2: (otherlv_0= 'DomainDecision' ( (lv_ddec2_1_0= ruleDecision ) )* ( (lv_op_2_0= ruleOperator ) ) ( (lv_tl_3_0= ruleTransitionLabel ) ) ( (lv_ll6_4_0= ruleLogic ) ) otherlv_5= 'Domain_Model' otherlv_6= 'implies' ( (lv_tl2_7_0= ruleTransitionLabel ) ) ( (lv_ll7_8_0= ruleLogic ) ) otherlv_9= 'DomainModel' otherlv_10= 'else' ( (lv_ll8_11_0= ruleLogic ) ) otherlv_12= 'Domain_Model' )
            {
            // InternalSPL.g:1315:2: (otherlv_0= 'DomainDecision' ( (lv_ddec2_1_0= ruleDecision ) )* ( (lv_op_2_0= ruleOperator ) ) ( (lv_tl_3_0= ruleTransitionLabel ) ) ( (lv_ll6_4_0= ruleLogic ) ) otherlv_5= 'Domain_Model' otherlv_6= 'implies' ( (lv_tl2_7_0= ruleTransitionLabel ) ) ( (lv_ll7_8_0= ruleLogic ) ) otherlv_9= 'DomainModel' otherlv_10= 'else' ( (lv_ll8_11_0= ruleLogic ) ) otherlv_12= 'Domain_Model' )
            // InternalSPL.g:1316:3: otherlv_0= 'DomainDecision' ( (lv_ddec2_1_0= ruleDecision ) )* ( (lv_op_2_0= ruleOperator ) ) ( (lv_tl_3_0= ruleTransitionLabel ) ) ( (lv_ll6_4_0= ruleLogic ) ) otherlv_5= 'Domain_Model' otherlv_6= 'implies' ( (lv_tl2_7_0= ruleTransitionLabel ) ) ( (lv_ll7_8_0= ruleLogic ) ) otherlv_9= 'DomainModel' otherlv_10= 'else' ( (lv_ll8_11_0= ruleLogic ) ) otherlv_12= 'Domain_Model'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_30); 

            			newLeafNode(otherlv_0, grammarAccess.getDomainDecision2Access().getDomainDecisionKeyword_0());
            		
            // InternalSPL.g:1320:3: ( (lv_ddec2_1_0= ruleDecision ) )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==RULE_ID) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalSPL.g:1321:4: (lv_ddec2_1_0= ruleDecision )
            	    {
            	    // InternalSPL.g:1321:4: (lv_ddec2_1_0= ruleDecision )
            	    // InternalSPL.g:1322:5: lv_ddec2_1_0= ruleDecision
            	    {

            	    					newCompositeNode(grammarAccess.getDomainDecision2Access().getDdec2DecisionParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_30);
            	    lv_ddec2_1_0=ruleDecision();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDomainDecision2Rule());
            	    					}
            	    					add(
            	    						current,
            	    						"ddec2",
            	    						lv_ddec2_1_0,
            	    						"org.xtext.example.mydsl.SPL.Decision");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

            // InternalSPL.g:1339:3: ( (lv_op_2_0= ruleOperator ) )
            // InternalSPL.g:1340:4: (lv_op_2_0= ruleOperator )
            {
            // InternalSPL.g:1340:4: (lv_op_2_0= ruleOperator )
            // InternalSPL.g:1341:5: lv_op_2_0= ruleOperator
            {

            					newCompositeNode(grammarAccess.getDomainDecision2Access().getOpOperatorParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_15);
            lv_op_2_0=ruleOperator();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDomainDecision2Rule());
            					}
            					add(
            						current,
            						"op",
            						lv_op_2_0,
            						"org.xtext.example.mydsl.SPL.Operator");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:1358:3: ( (lv_tl_3_0= ruleTransitionLabel ) )
            // InternalSPL.g:1359:4: (lv_tl_3_0= ruleTransitionLabel )
            {
            // InternalSPL.g:1359:4: (lv_tl_3_0= ruleTransitionLabel )
            // InternalSPL.g:1360:5: lv_tl_3_0= ruleTransitionLabel
            {

            					newCompositeNode(grammarAccess.getDomainDecision2Access().getTlTransitionLabelParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_23);
            lv_tl_3_0=ruleTransitionLabel();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDomainDecision2Rule());
            					}
            					add(
            						current,
            						"tl",
            						lv_tl_3_0,
            						"org.xtext.example.mydsl.SPL.TransitionLabel");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:1377:3: ( (lv_ll6_4_0= ruleLogic ) )
            // InternalSPL.g:1378:4: (lv_ll6_4_0= ruleLogic )
            {
            // InternalSPL.g:1378:4: (lv_ll6_4_0= ruleLogic )
            // InternalSPL.g:1379:5: lv_ll6_4_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getDomainDecision2Access().getLl6LogicParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_25);
            lv_ll6_4_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDomainDecision2Rule());
            					}
            					add(
            						current,
            						"ll6",
            						lv_ll6_4_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,24,FOLLOW_31); 

            			newLeafNode(otherlv_5, grammarAccess.getDomainDecision2Access().getDomain_ModelKeyword_5());
            		
            otherlv_6=(Token)match(input,28,FOLLOW_15); 

            			newLeafNode(otherlv_6, grammarAccess.getDomainDecision2Access().getImpliesKeyword_6());
            		
            // InternalSPL.g:1404:3: ( (lv_tl2_7_0= ruleTransitionLabel ) )
            // InternalSPL.g:1405:4: (lv_tl2_7_0= ruleTransitionLabel )
            {
            // InternalSPL.g:1405:4: (lv_tl2_7_0= ruleTransitionLabel )
            // InternalSPL.g:1406:5: lv_tl2_7_0= ruleTransitionLabel
            {

            					newCompositeNode(grammarAccess.getDomainDecision2Access().getTl2TransitionLabelParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_23);
            lv_tl2_7_0=ruleTransitionLabel();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDomainDecision2Rule());
            					}
            					add(
            						current,
            						"tl2",
            						lv_tl2_7_0,
            						"org.xtext.example.mydsl.SPL.TransitionLabel");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:1423:3: ( (lv_ll7_8_0= ruleLogic ) )
            // InternalSPL.g:1424:4: (lv_ll7_8_0= ruleLogic )
            {
            // InternalSPL.g:1424:4: (lv_ll7_8_0= ruleLogic )
            // InternalSPL.g:1425:5: lv_ll7_8_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getDomainDecision2Access().getLl7LogicParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_29);
            lv_ll7_8_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDomainDecision2Rule());
            					}
            					add(
            						current,
            						"ll7",
            						lv_ll7_8_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_9=(Token)match(input,30,FOLLOW_32); 

            			newLeafNode(otherlv_9, grammarAccess.getDomainDecision2Access().getDomainModelKeyword_9());
            		
            otherlv_10=(Token)match(input,29,FOLLOW_23); 

            			newLeafNode(otherlv_10, grammarAccess.getDomainDecision2Access().getElseKeyword_10());
            		
            // InternalSPL.g:1450:3: ( (lv_ll8_11_0= ruleLogic ) )
            // InternalSPL.g:1451:4: (lv_ll8_11_0= ruleLogic )
            {
            // InternalSPL.g:1451:4: (lv_ll8_11_0= ruleLogic )
            // InternalSPL.g:1452:5: lv_ll8_11_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getDomainDecision2Access().getLl8LogicParserRuleCall_11_0());
            				
            pushFollow(FOLLOW_25);
            lv_ll8_11_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDomainDecision2Rule());
            					}
            					add(
            						current,
            						"ll8",
            						lv_ll8_11_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_12=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_12, grammarAccess.getDomainDecision2Access().getDomain_ModelKeyword_12());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainDecision2"


    // $ANTLR start "entryRuleFeatureDecision"
    // InternalSPL.g:1477:1: entryRuleFeatureDecision returns [EObject current=null] : iv_ruleFeatureDecision= ruleFeatureDecision EOF ;
    public final EObject entryRuleFeatureDecision() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureDecision = null;


        try {
            // InternalSPL.g:1477:56: (iv_ruleFeatureDecision= ruleFeatureDecision EOF )
            // InternalSPL.g:1478:2: iv_ruleFeatureDecision= ruleFeatureDecision EOF
            {
             newCompositeNode(grammarAccess.getFeatureDecisionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureDecision=ruleFeatureDecision();

            state._fsp--;

             current =iv_ruleFeatureDecision; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureDecision"


    // $ANTLR start "ruleFeatureDecision"
    // InternalSPL.g:1484:1: ruleFeatureDecision returns [EObject current=null] : (otherlv_0= 'FeatureDecision' ( (lv_fdec_1_0= ruleDecision ) ) otherlv_2= 'implies' ( (lv_fn_3_0= ruleFeatureName ) ) ( (lv_l_4_0= ruleLogic ) ) otherlv_5= 'Feature_Model' ( ( (lv_op_6_0= ruleOperator ) ) ( (lv_fn1_7_0= ruleFeatureName ) ) ( (lv_ll_8_0= ruleLogic ) ) otherlv_9= 'FeatureModel' )* ) ;
    public final EObject ruleFeatureDecision() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_9=null;
        EObject lv_fdec_1_0 = null;

        EObject lv_fn_3_0 = null;

        AntlrDatatypeRuleToken lv_l_4_0 = null;

        EObject lv_op_6_0 = null;

        EObject lv_fn1_7_0 = null;

        AntlrDatatypeRuleToken lv_ll_8_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:1490:2: ( (otherlv_0= 'FeatureDecision' ( (lv_fdec_1_0= ruleDecision ) ) otherlv_2= 'implies' ( (lv_fn_3_0= ruleFeatureName ) ) ( (lv_l_4_0= ruleLogic ) ) otherlv_5= 'Feature_Model' ( ( (lv_op_6_0= ruleOperator ) ) ( (lv_fn1_7_0= ruleFeatureName ) ) ( (lv_ll_8_0= ruleLogic ) ) otherlv_9= 'FeatureModel' )* ) )
            // InternalSPL.g:1491:2: (otherlv_0= 'FeatureDecision' ( (lv_fdec_1_0= ruleDecision ) ) otherlv_2= 'implies' ( (lv_fn_3_0= ruleFeatureName ) ) ( (lv_l_4_0= ruleLogic ) ) otherlv_5= 'Feature_Model' ( ( (lv_op_6_0= ruleOperator ) ) ( (lv_fn1_7_0= ruleFeatureName ) ) ( (lv_ll_8_0= ruleLogic ) ) otherlv_9= 'FeatureModel' )* )
            {
            // InternalSPL.g:1491:2: (otherlv_0= 'FeatureDecision' ( (lv_fdec_1_0= ruleDecision ) ) otherlv_2= 'implies' ( (lv_fn_3_0= ruleFeatureName ) ) ( (lv_l_4_0= ruleLogic ) ) otherlv_5= 'Feature_Model' ( ( (lv_op_6_0= ruleOperator ) ) ( (lv_fn1_7_0= ruleFeatureName ) ) ( (lv_ll_8_0= ruleLogic ) ) otherlv_9= 'FeatureModel' )* )
            // InternalSPL.g:1492:3: otherlv_0= 'FeatureDecision' ( (lv_fdec_1_0= ruleDecision ) ) otherlv_2= 'implies' ( (lv_fn_3_0= ruleFeatureName ) ) ( (lv_l_4_0= ruleLogic ) ) otherlv_5= 'Feature_Model' ( ( (lv_op_6_0= ruleOperator ) ) ( (lv_fn1_7_0= ruleFeatureName ) ) ( (lv_ll_8_0= ruleLogic ) ) otherlv_9= 'FeatureModel' )*
            {
            otherlv_0=(Token)match(input,31,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getFeatureDecisionAccess().getFeatureDecisionKeyword_0());
            		
            // InternalSPL.g:1496:3: ( (lv_fdec_1_0= ruleDecision ) )
            // InternalSPL.g:1497:4: (lv_fdec_1_0= ruleDecision )
            {
            // InternalSPL.g:1497:4: (lv_fdec_1_0= ruleDecision )
            // InternalSPL.g:1498:5: lv_fdec_1_0= ruleDecision
            {

            					newCompositeNode(grammarAccess.getFeatureDecisionAccess().getFdecDecisionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_31);
            lv_fdec_1_0=ruleDecision();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFeatureDecisionRule());
            					}
            					add(
            						current,
            						"fdec",
            						lv_fdec_1_0,
            						"org.xtext.example.mydsl.SPL.Decision");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,28,FOLLOW_15); 

            			newLeafNode(otherlv_2, grammarAccess.getFeatureDecisionAccess().getImpliesKeyword_2());
            		
            // InternalSPL.g:1519:3: ( (lv_fn_3_0= ruleFeatureName ) )
            // InternalSPL.g:1520:4: (lv_fn_3_0= ruleFeatureName )
            {
            // InternalSPL.g:1520:4: (lv_fn_3_0= ruleFeatureName )
            // InternalSPL.g:1521:5: lv_fn_3_0= ruleFeatureName
            {

            					newCompositeNode(grammarAccess.getFeatureDecisionAccess().getFnFeatureNameParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_23);
            lv_fn_3_0=ruleFeatureName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFeatureDecisionRule());
            					}
            					add(
            						current,
            						"fn",
            						lv_fn_3_0,
            						"org.xtext.example.mydsl.SPL.FeatureName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:1538:3: ( (lv_l_4_0= ruleLogic ) )
            // InternalSPL.g:1539:4: (lv_l_4_0= ruleLogic )
            {
            // InternalSPL.g:1539:4: (lv_l_4_0= ruleLogic )
            // InternalSPL.g:1540:5: lv_l_4_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getFeatureDecisionAccess().getLLogicParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_24);
            lv_l_4_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFeatureDecisionRule());
            					}
            					add(
            						current,
            						"l",
            						lv_l_4_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,23,FOLLOW_33); 

            			newLeafNode(otherlv_5, grammarAccess.getFeatureDecisionAccess().getFeature_ModelKeyword_5());
            		
            // InternalSPL.g:1561:3: ( ( (lv_op_6_0= ruleOperator ) ) ( (lv_fn1_7_0= ruleFeatureName ) ) ( (lv_ll_8_0= ruleLogic ) ) otherlv_9= 'FeatureModel' )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==22||(LA29_0>=25 && LA29_0<=26)) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalSPL.g:1562:4: ( (lv_op_6_0= ruleOperator ) ) ( (lv_fn1_7_0= ruleFeatureName ) ) ( (lv_ll_8_0= ruleLogic ) ) otherlv_9= 'FeatureModel'
            	    {
            	    // InternalSPL.g:1562:4: ( (lv_op_6_0= ruleOperator ) )
            	    // InternalSPL.g:1563:5: (lv_op_6_0= ruleOperator )
            	    {
            	    // InternalSPL.g:1563:5: (lv_op_6_0= ruleOperator )
            	    // InternalSPL.g:1564:6: lv_op_6_0= ruleOperator
            	    {

            	    						newCompositeNode(grammarAccess.getFeatureDecisionAccess().getOpOperatorParserRuleCall_6_0_0());
            	    					
            	    pushFollow(FOLLOW_15);
            	    lv_op_6_0=ruleOperator();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getFeatureDecisionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"op",
            	    							lv_op_6_0,
            	    							"org.xtext.example.mydsl.SPL.Operator");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalSPL.g:1581:4: ( (lv_fn1_7_0= ruleFeatureName ) )
            	    // InternalSPL.g:1582:5: (lv_fn1_7_0= ruleFeatureName )
            	    {
            	    // InternalSPL.g:1582:5: (lv_fn1_7_0= ruleFeatureName )
            	    // InternalSPL.g:1583:6: lv_fn1_7_0= ruleFeatureName
            	    {

            	    						newCompositeNode(grammarAccess.getFeatureDecisionAccess().getFn1FeatureNameParserRuleCall_6_1_0());
            	    					
            	    pushFollow(FOLLOW_23);
            	    lv_fn1_7_0=ruleFeatureName();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getFeatureDecisionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"fn1",
            	    							lv_fn1_7_0,
            	    							"org.xtext.example.mydsl.SPL.FeatureName");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalSPL.g:1600:4: ( (lv_ll_8_0= ruleLogic ) )
            	    // InternalSPL.g:1601:5: (lv_ll_8_0= ruleLogic )
            	    {
            	    // InternalSPL.g:1601:5: (lv_ll_8_0= ruleLogic )
            	    // InternalSPL.g:1602:6: lv_ll_8_0= ruleLogic
            	    {

            	    						newCompositeNode(grammarAccess.getFeatureDecisionAccess().getLlLogicParserRuleCall_6_2_0());
            	    					
            	    pushFollow(FOLLOW_34);
            	    lv_ll_8_0=ruleLogic();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getFeatureDecisionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"ll",
            	    							lv_ll_8_0,
            	    							"org.xtext.example.mydsl.SPL.Logic");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_9=(Token)match(input,32,FOLLOW_33); 

            	    				newLeafNode(otherlv_9, grammarAccess.getFeatureDecisionAccess().getFeatureModelKeyword_6_3());
            	    			

            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureDecision"


    // $ANTLR start "entryRuleCorrespondance"
    // InternalSPL.g:1628:1: entryRuleCorrespondance returns [EObject current=null] : iv_ruleCorrespondance= ruleCorrespondance EOF ;
    public final EObject entryRuleCorrespondance() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCorrespondance = null;


        try {
            // InternalSPL.g:1628:55: (iv_ruleCorrespondance= ruleCorrespondance EOF )
            // InternalSPL.g:1629:2: iv_ruleCorrespondance= ruleCorrespondance EOF
            {
             newCompositeNode(grammarAccess.getCorrespondanceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCorrespondance=ruleCorrespondance();

            state._fsp--;

             current =iv_ruleCorrespondance; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCorrespondance"


    // $ANTLR start "ruleCorrespondance"
    // InternalSPL.g:1635:1: ruleCorrespondance returns [EObject current=null] : (otherlv_0= 'Correspondence' ( (lv_cfeat_1_0= ruleFeatureName ) )* ( (lv_l3_2_0= ruleLogic ) ) otherlv_3= 'Feature_Model' otherlv_4= 'implies' ( (lv_ctran_5_0= ruleTransitionLabel ) )* ( (lv_l4_6_0= ruleLogic ) ) otherlv_7= 'Domain_Model' otherlv_8= 'else' ( (lv_l5_9_0= ruleLogic ) ) otherlv_10= 'Domain_Model' ) ;
    public final EObject ruleCorrespondance() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_cfeat_1_0 = null;

        AntlrDatatypeRuleToken lv_l3_2_0 = null;

        EObject lv_ctran_5_0 = null;

        AntlrDatatypeRuleToken lv_l4_6_0 = null;

        AntlrDatatypeRuleToken lv_l5_9_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:1641:2: ( (otherlv_0= 'Correspondence' ( (lv_cfeat_1_0= ruleFeatureName ) )* ( (lv_l3_2_0= ruleLogic ) ) otherlv_3= 'Feature_Model' otherlv_4= 'implies' ( (lv_ctran_5_0= ruleTransitionLabel ) )* ( (lv_l4_6_0= ruleLogic ) ) otherlv_7= 'Domain_Model' otherlv_8= 'else' ( (lv_l5_9_0= ruleLogic ) ) otherlv_10= 'Domain_Model' ) )
            // InternalSPL.g:1642:2: (otherlv_0= 'Correspondence' ( (lv_cfeat_1_0= ruleFeatureName ) )* ( (lv_l3_2_0= ruleLogic ) ) otherlv_3= 'Feature_Model' otherlv_4= 'implies' ( (lv_ctran_5_0= ruleTransitionLabel ) )* ( (lv_l4_6_0= ruleLogic ) ) otherlv_7= 'Domain_Model' otherlv_8= 'else' ( (lv_l5_9_0= ruleLogic ) ) otherlv_10= 'Domain_Model' )
            {
            // InternalSPL.g:1642:2: (otherlv_0= 'Correspondence' ( (lv_cfeat_1_0= ruleFeatureName ) )* ( (lv_l3_2_0= ruleLogic ) ) otherlv_3= 'Feature_Model' otherlv_4= 'implies' ( (lv_ctran_5_0= ruleTransitionLabel ) )* ( (lv_l4_6_0= ruleLogic ) ) otherlv_7= 'Domain_Model' otherlv_8= 'else' ( (lv_l5_9_0= ruleLogic ) ) otherlv_10= 'Domain_Model' )
            // InternalSPL.g:1643:3: otherlv_0= 'Correspondence' ( (lv_cfeat_1_0= ruleFeatureName ) )* ( (lv_l3_2_0= ruleLogic ) ) otherlv_3= 'Feature_Model' otherlv_4= 'implies' ( (lv_ctran_5_0= ruleTransitionLabel ) )* ( (lv_l4_6_0= ruleLogic ) ) otherlv_7= 'Domain_Model' otherlv_8= 'else' ( (lv_l5_9_0= ruleLogic ) ) otherlv_10= 'Domain_Model'
            {
            otherlv_0=(Token)match(input,33,FOLLOW_27); 

            			newLeafNode(otherlv_0, grammarAccess.getCorrespondanceAccess().getCorrespondenceKeyword_0());
            		
            // InternalSPL.g:1647:3: ( (lv_cfeat_1_0= ruleFeatureName ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==RULE_ID) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalSPL.g:1648:4: (lv_cfeat_1_0= ruleFeatureName )
            	    {
            	    // InternalSPL.g:1648:4: (lv_cfeat_1_0= ruleFeatureName )
            	    // InternalSPL.g:1649:5: lv_cfeat_1_0= ruleFeatureName
            	    {

            	    					newCompositeNode(grammarAccess.getCorrespondanceAccess().getCfeatFeatureNameParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_27);
            	    lv_cfeat_1_0=ruleFeatureName();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getCorrespondanceRule());
            	    					}
            	    					add(
            	    						current,
            	    						"cfeat",
            	    						lv_cfeat_1_0,
            	    						"org.xtext.example.mydsl.SPL.FeatureName");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

            // InternalSPL.g:1666:3: ( (lv_l3_2_0= ruleLogic ) )
            // InternalSPL.g:1667:4: (lv_l3_2_0= ruleLogic )
            {
            // InternalSPL.g:1667:4: (lv_l3_2_0= ruleLogic )
            // InternalSPL.g:1668:5: lv_l3_2_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getCorrespondanceAccess().getL3LogicParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_24);
            lv_l3_2_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCorrespondanceRule());
            					}
            					add(
            						current,
            						"l3",
            						lv_l3_2_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,23,FOLLOW_31); 

            			newLeafNode(otherlv_3, grammarAccess.getCorrespondanceAccess().getFeature_ModelKeyword_3());
            		
            otherlv_4=(Token)match(input,28,FOLLOW_27); 

            			newLeafNode(otherlv_4, grammarAccess.getCorrespondanceAccess().getImpliesKeyword_4());
            		
            // InternalSPL.g:1693:3: ( (lv_ctran_5_0= ruleTransitionLabel ) )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==RULE_ID) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalSPL.g:1694:4: (lv_ctran_5_0= ruleTransitionLabel )
            	    {
            	    // InternalSPL.g:1694:4: (lv_ctran_5_0= ruleTransitionLabel )
            	    // InternalSPL.g:1695:5: lv_ctran_5_0= ruleTransitionLabel
            	    {

            	    					newCompositeNode(grammarAccess.getCorrespondanceAccess().getCtranTransitionLabelParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_27);
            	    lv_ctran_5_0=ruleTransitionLabel();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getCorrespondanceRule());
            	    					}
            	    					add(
            	    						current,
            	    						"ctran",
            	    						lv_ctran_5_0,
            	    						"org.xtext.example.mydsl.SPL.TransitionLabel");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

            // InternalSPL.g:1712:3: ( (lv_l4_6_0= ruleLogic ) )
            // InternalSPL.g:1713:4: (lv_l4_6_0= ruleLogic )
            {
            // InternalSPL.g:1713:4: (lv_l4_6_0= ruleLogic )
            // InternalSPL.g:1714:5: lv_l4_6_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getCorrespondanceAccess().getL4LogicParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_25);
            lv_l4_6_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCorrespondanceRule());
            					}
            					add(
            						current,
            						"l4",
            						lv_l4_6_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_7=(Token)match(input,24,FOLLOW_32); 

            			newLeafNode(otherlv_7, grammarAccess.getCorrespondanceAccess().getDomain_ModelKeyword_7());
            		
            otherlv_8=(Token)match(input,29,FOLLOW_23); 

            			newLeafNode(otherlv_8, grammarAccess.getCorrespondanceAccess().getElseKeyword_8());
            		
            // InternalSPL.g:1739:3: ( (lv_l5_9_0= ruleLogic ) )
            // InternalSPL.g:1740:4: (lv_l5_9_0= ruleLogic )
            {
            // InternalSPL.g:1740:4: (lv_l5_9_0= ruleLogic )
            // InternalSPL.g:1741:5: lv_l5_9_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getCorrespondanceAccess().getL5LogicParserRuleCall_9_0());
            				
            pushFollow(FOLLOW_25);
            lv_l5_9_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCorrespondanceRule());
            					}
            					add(
            						current,
            						"l5",
            						lv_l5_9_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_10=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getCorrespondanceAccess().getDomain_ModelKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCorrespondance"


    // $ANTLR start "entryRuleTransitionEffect"
    // InternalSPL.g:1766:1: entryRuleTransitionEffect returns [EObject current=null] : iv_ruleTransitionEffect= ruleTransitionEffect EOF ;
    public final EObject entryRuleTransitionEffect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransitionEffect = null;


        try {
            // InternalSPL.g:1766:57: (iv_ruleTransitionEffect= ruleTransitionEffect EOF )
            // InternalSPL.g:1767:2: iv_ruleTransitionEffect= ruleTransitionEffect EOF
            {
             newCompositeNode(grammarAccess.getTransitionEffectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransitionEffect=ruleTransitionEffect();

            state._fsp--;

             current =iv_ruleTransitionEffect; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransitionEffect"


    // $ANTLR start "ruleTransitionEffect"
    // InternalSPL.g:1773:1: ruleTransitionEffect returns [EObject current=null] : (otherlv_0= 'TransitionEffect' ( (lv_tl3_1_0= ruleTransitionLabel ) ) ( (lv_ll9_2_0= ruleLogic ) ) otherlv_3= 'Domain_Model' otherlv_4= 'implies' ( (lv_tl4_5_0= ruleTransitionLabel ) ) ( (lv_ll10_6_0= ruleLogic ) ) otherlv_7= 'Domain_Model' ) ;
    public final EObject ruleTransitionEffect() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_7=null;
        EObject lv_tl3_1_0 = null;

        AntlrDatatypeRuleToken lv_ll9_2_0 = null;

        EObject lv_tl4_5_0 = null;

        AntlrDatatypeRuleToken lv_ll10_6_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:1779:2: ( (otherlv_0= 'TransitionEffect' ( (lv_tl3_1_0= ruleTransitionLabel ) ) ( (lv_ll9_2_0= ruleLogic ) ) otherlv_3= 'Domain_Model' otherlv_4= 'implies' ( (lv_tl4_5_0= ruleTransitionLabel ) ) ( (lv_ll10_6_0= ruleLogic ) ) otherlv_7= 'Domain_Model' ) )
            // InternalSPL.g:1780:2: (otherlv_0= 'TransitionEffect' ( (lv_tl3_1_0= ruleTransitionLabel ) ) ( (lv_ll9_2_0= ruleLogic ) ) otherlv_3= 'Domain_Model' otherlv_4= 'implies' ( (lv_tl4_5_0= ruleTransitionLabel ) ) ( (lv_ll10_6_0= ruleLogic ) ) otherlv_7= 'Domain_Model' )
            {
            // InternalSPL.g:1780:2: (otherlv_0= 'TransitionEffect' ( (lv_tl3_1_0= ruleTransitionLabel ) ) ( (lv_ll9_2_0= ruleLogic ) ) otherlv_3= 'Domain_Model' otherlv_4= 'implies' ( (lv_tl4_5_0= ruleTransitionLabel ) ) ( (lv_ll10_6_0= ruleLogic ) ) otherlv_7= 'Domain_Model' )
            // InternalSPL.g:1781:3: otherlv_0= 'TransitionEffect' ( (lv_tl3_1_0= ruleTransitionLabel ) ) ( (lv_ll9_2_0= ruleLogic ) ) otherlv_3= 'Domain_Model' otherlv_4= 'implies' ( (lv_tl4_5_0= ruleTransitionLabel ) ) ( (lv_ll10_6_0= ruleLogic ) ) otherlv_7= 'Domain_Model'
            {
            otherlv_0=(Token)match(input,34,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getTransitionEffectAccess().getTransitionEffectKeyword_0());
            		
            // InternalSPL.g:1785:3: ( (lv_tl3_1_0= ruleTransitionLabel ) )
            // InternalSPL.g:1786:4: (lv_tl3_1_0= ruleTransitionLabel )
            {
            // InternalSPL.g:1786:4: (lv_tl3_1_0= ruleTransitionLabel )
            // InternalSPL.g:1787:5: lv_tl3_1_0= ruleTransitionLabel
            {

            					newCompositeNode(grammarAccess.getTransitionEffectAccess().getTl3TransitionLabelParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_23);
            lv_tl3_1_0=ruleTransitionLabel();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTransitionEffectRule());
            					}
            					add(
            						current,
            						"tl3",
            						lv_tl3_1_0,
            						"org.xtext.example.mydsl.SPL.TransitionLabel");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:1804:3: ( (lv_ll9_2_0= ruleLogic ) )
            // InternalSPL.g:1805:4: (lv_ll9_2_0= ruleLogic )
            {
            // InternalSPL.g:1805:4: (lv_ll9_2_0= ruleLogic )
            // InternalSPL.g:1806:5: lv_ll9_2_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getTransitionEffectAccess().getLl9LogicParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_25);
            lv_ll9_2_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTransitionEffectRule());
            					}
            					add(
            						current,
            						"ll9",
            						lv_ll9_2_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,24,FOLLOW_31); 

            			newLeafNode(otherlv_3, grammarAccess.getTransitionEffectAccess().getDomain_ModelKeyword_3());
            		
            otherlv_4=(Token)match(input,28,FOLLOW_15); 

            			newLeafNode(otherlv_4, grammarAccess.getTransitionEffectAccess().getImpliesKeyword_4());
            		
            // InternalSPL.g:1831:3: ( (lv_tl4_5_0= ruleTransitionLabel ) )
            // InternalSPL.g:1832:4: (lv_tl4_5_0= ruleTransitionLabel )
            {
            // InternalSPL.g:1832:4: (lv_tl4_5_0= ruleTransitionLabel )
            // InternalSPL.g:1833:5: lv_tl4_5_0= ruleTransitionLabel
            {

            					newCompositeNode(grammarAccess.getTransitionEffectAccess().getTl4TransitionLabelParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_23);
            lv_tl4_5_0=ruleTransitionLabel();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTransitionEffectRule());
            					}
            					add(
            						current,
            						"tl4",
            						lv_tl4_5_0,
            						"org.xtext.example.mydsl.SPL.TransitionLabel");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSPL.g:1850:3: ( (lv_ll10_6_0= ruleLogic ) )
            // InternalSPL.g:1851:4: (lv_ll10_6_0= ruleLogic )
            {
            // InternalSPL.g:1851:4: (lv_ll10_6_0= ruleLogic )
            // InternalSPL.g:1852:5: lv_ll10_6_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getTransitionEffectAccess().getLl10LogicParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_25);
            lv_ll10_6_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTransitionEffectRule());
            					}
            					add(
            						current,
            						"ll10",
            						lv_ll10_6_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_7=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getTransitionEffectAccess().getDomain_ModelKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransitionEffect"


    // $ANTLR start "entryRuleQuantifier1"
    // InternalSPL.g:1877:1: entryRuleQuantifier1 returns [EObject current=null] : iv_ruleQuantifier1= ruleQuantifier1 EOF ;
    public final EObject entryRuleQuantifier1() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuantifier1 = null;


        try {
            // InternalSPL.g:1877:52: (iv_ruleQuantifier1= ruleQuantifier1 EOF )
            // InternalSPL.g:1878:2: iv_ruleQuantifier1= ruleQuantifier1 EOF
            {
             newCompositeNode(grammarAccess.getQuantifier1Rule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuantifier1=ruleQuantifier1();

            state._fsp--;

             current =iv_ruleQuantifier1; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuantifier1"


    // $ANTLR start "ruleQuantifier1"
    // InternalSPL.g:1884:1: ruleQuantifier1 returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleQuantifier1() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSPL.g:1890:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalSPL.g:1891:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalSPL.g:1891:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalSPL.g:1892:3: (lv_name_0_0= RULE_ID )
            {
            // InternalSPL.g:1892:3: (lv_name_0_0= RULE_ID )
            // InternalSPL.g:1893:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getQuantifier1Access().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getQuantifier1Rule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuantifier1"


    // $ANTLR start "entryRuleQuantifier2"
    // InternalSPL.g:1912:1: entryRuleQuantifier2 returns [EObject current=null] : iv_ruleQuantifier2= ruleQuantifier2 EOF ;
    public final EObject entryRuleQuantifier2() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuantifier2 = null;


        try {
            // InternalSPL.g:1912:52: (iv_ruleQuantifier2= ruleQuantifier2 EOF )
            // InternalSPL.g:1913:2: iv_ruleQuantifier2= ruleQuantifier2 EOF
            {
             newCompositeNode(grammarAccess.getQuantifier2Rule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuantifier2=ruleQuantifier2();

            state._fsp--;

             current =iv_ruleQuantifier2; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuantifier2"


    // $ANTLR start "ruleQuantifier2"
    // InternalSPL.g:1919:1: ruleQuantifier2 returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleQuantifier2() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalSPL.g:1925:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalSPL.g:1926:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalSPL.g:1926:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalSPL.g:1927:3: (lv_name_0_0= RULE_ID )
            {
            // InternalSPL.g:1927:3: (lv_name_0_0= RULE_ID )
            // InternalSPL.g:1928:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getQuantifier2Access().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getQuantifier2Rule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuantifier2"


    // $ANTLR start "entryRuleProperty"
    // InternalSPL.g:1947:1: entryRuleProperty returns [EObject current=null] : iv_ruleProperty= ruleProperty EOF ;
    public final EObject entryRuleProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProperty = null;


        try {
            // InternalSPL.g:1947:49: (iv_ruleProperty= ruleProperty EOF )
            // InternalSPL.g:1948:2: iv_ruleProperty= ruleProperty EOF
            {
             newCompositeNode(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProperty=ruleProperty();

            state._fsp--;

             current =iv_ruleProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // InternalSPL.g:1954:1: ruleProperty returns [EObject current=null] : (otherlv_0= 'Prop' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_QF1_3_0= ruleQuantifier1 ) ) otherlv_4= 'Decision_Model' ( (lv_QF2_5_0= ruleQuantifier2 ) ) otherlv_6= 'Feature_Model' ( (otherlv_7= 'Transition' ( (lv_dtransitions_8_0= ruleTransitionLabel ) ) ) | ( (lv_dstates_9_0= ruleState ) ) ) ( (lv_ll_10_0= ruleLogic ) ) otherlv_11= 'Domain_Model' otherlv_12= '}' ) ;
    public final EObject ruleProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        EObject lv_QF1_3_0 = null;

        EObject lv_QF2_5_0 = null;

        EObject lv_dtransitions_8_0 = null;

        EObject lv_dstates_9_0 = null;

        AntlrDatatypeRuleToken lv_ll_10_0 = null;



        	enterRule();

        try {
            // InternalSPL.g:1960:2: ( (otherlv_0= 'Prop' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_QF1_3_0= ruleQuantifier1 ) ) otherlv_4= 'Decision_Model' ( (lv_QF2_5_0= ruleQuantifier2 ) ) otherlv_6= 'Feature_Model' ( (otherlv_7= 'Transition' ( (lv_dtransitions_8_0= ruleTransitionLabel ) ) ) | ( (lv_dstates_9_0= ruleState ) ) ) ( (lv_ll_10_0= ruleLogic ) ) otherlv_11= 'Domain_Model' otherlv_12= '}' ) )
            // InternalSPL.g:1961:2: (otherlv_0= 'Prop' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_QF1_3_0= ruleQuantifier1 ) ) otherlv_4= 'Decision_Model' ( (lv_QF2_5_0= ruleQuantifier2 ) ) otherlv_6= 'Feature_Model' ( (otherlv_7= 'Transition' ( (lv_dtransitions_8_0= ruleTransitionLabel ) ) ) | ( (lv_dstates_9_0= ruleState ) ) ) ( (lv_ll_10_0= ruleLogic ) ) otherlv_11= 'Domain_Model' otherlv_12= '}' )
            {
            // InternalSPL.g:1961:2: (otherlv_0= 'Prop' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_QF1_3_0= ruleQuantifier1 ) ) otherlv_4= 'Decision_Model' ( (lv_QF2_5_0= ruleQuantifier2 ) ) otherlv_6= 'Feature_Model' ( (otherlv_7= 'Transition' ( (lv_dtransitions_8_0= ruleTransitionLabel ) ) ) | ( (lv_dstates_9_0= ruleState ) ) ) ( (lv_ll_10_0= ruleLogic ) ) otherlv_11= 'Domain_Model' otherlv_12= '}' )
            // InternalSPL.g:1962:3: otherlv_0= 'Prop' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_QF1_3_0= ruleQuantifier1 ) ) otherlv_4= 'Decision_Model' ( (lv_QF2_5_0= ruleQuantifier2 ) ) otherlv_6= 'Feature_Model' ( (otherlv_7= 'Transition' ( (lv_dtransitions_8_0= ruleTransitionLabel ) ) ) | ( (lv_dstates_9_0= ruleState ) ) ) ( (lv_ll_10_0= ruleLogic ) ) otherlv_11= 'Domain_Model' otherlv_12= '}'
            {
            otherlv_0=(Token)match(input,35,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getPropertyAccess().getPropKeyword_0());
            		
            // InternalSPL.g:1966:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalSPL.g:1967:4: (lv_name_1_0= RULE_ID )
            {
            // InternalSPL.g:1967:4: (lv_name_1_0= RULE_ID )
            // InternalSPL.g:1968:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(lv_name_1_0, grammarAccess.getPropertyAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPropertyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_15); 

            			newLeafNode(otherlv_2, grammarAccess.getPropertyAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalSPL.g:1988:3: ( (lv_QF1_3_0= ruleQuantifier1 ) )
            // InternalSPL.g:1989:4: (lv_QF1_3_0= ruleQuantifier1 )
            {
            // InternalSPL.g:1989:4: (lv_QF1_3_0= ruleQuantifier1 )
            // InternalSPL.g:1990:5: lv_QF1_3_0= ruleQuantifier1
            {

            					newCompositeNode(grammarAccess.getPropertyAccess().getQF1Quantifier1ParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_35);
            lv_QF1_3_0=ruleQuantifier1();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPropertyRule());
            					}
            					add(
            						current,
            						"QF1",
            						lv_QF1_3_0,
            						"org.xtext.example.mydsl.SPL.Quantifier1");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,36,FOLLOW_15); 

            			newLeafNode(otherlv_4, grammarAccess.getPropertyAccess().getDecision_ModelKeyword_4());
            		
            // InternalSPL.g:2011:3: ( (lv_QF2_5_0= ruleQuantifier2 ) )
            // InternalSPL.g:2012:4: (lv_QF2_5_0= ruleQuantifier2 )
            {
            // InternalSPL.g:2012:4: (lv_QF2_5_0= ruleQuantifier2 )
            // InternalSPL.g:2013:5: lv_QF2_5_0= ruleQuantifier2
            {

            					newCompositeNode(grammarAccess.getPropertyAccess().getQF2Quantifier2ParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_24);
            lv_QF2_5_0=ruleQuantifier2();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPropertyRule());
            					}
            					add(
            						current,
            						"QF2",
            						lv_QF2_5_0,
            						"org.xtext.example.mydsl.SPL.Quantifier2");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,23,FOLLOW_36); 

            			newLeafNode(otherlv_6, grammarAccess.getPropertyAccess().getFeature_ModelKeyword_6());
            		
            // InternalSPL.g:2034:3: ( (otherlv_7= 'Transition' ( (lv_dtransitions_8_0= ruleTransitionLabel ) ) ) | ( (lv_dstates_9_0= ruleState ) ) )
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==19) ) {
                alt32=1;
            }
            else if ( (LA32_0==18) ) {
                alt32=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }
            switch (alt32) {
                case 1 :
                    // InternalSPL.g:2035:4: (otherlv_7= 'Transition' ( (lv_dtransitions_8_0= ruleTransitionLabel ) ) )
                    {
                    // InternalSPL.g:2035:4: (otherlv_7= 'Transition' ( (lv_dtransitions_8_0= ruleTransitionLabel ) ) )
                    // InternalSPL.g:2036:5: otherlv_7= 'Transition' ( (lv_dtransitions_8_0= ruleTransitionLabel ) )
                    {
                    otherlv_7=(Token)match(input,19,FOLLOW_15); 

                    					newLeafNode(otherlv_7, grammarAccess.getPropertyAccess().getTransitionKeyword_7_0_0());
                    				
                    // InternalSPL.g:2040:5: ( (lv_dtransitions_8_0= ruleTransitionLabel ) )
                    // InternalSPL.g:2041:6: (lv_dtransitions_8_0= ruleTransitionLabel )
                    {
                    // InternalSPL.g:2041:6: (lv_dtransitions_8_0= ruleTransitionLabel )
                    // InternalSPL.g:2042:7: lv_dtransitions_8_0= ruleTransitionLabel
                    {

                    							newCompositeNode(grammarAccess.getPropertyAccess().getDtransitionsTransitionLabelParserRuleCall_7_0_1_0());
                    						
                    pushFollow(FOLLOW_23);
                    lv_dtransitions_8_0=ruleTransitionLabel();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getPropertyRule());
                    							}
                    							add(
                    								current,
                    								"dtransitions",
                    								lv_dtransitions_8_0,
                    								"org.xtext.example.mydsl.SPL.TransitionLabel");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSPL.g:2061:4: ( (lv_dstates_9_0= ruleState ) )
                    {
                    // InternalSPL.g:2061:4: ( (lv_dstates_9_0= ruleState ) )
                    // InternalSPL.g:2062:5: (lv_dstates_9_0= ruleState )
                    {
                    // InternalSPL.g:2062:5: (lv_dstates_9_0= ruleState )
                    // InternalSPL.g:2063:6: lv_dstates_9_0= ruleState
                    {

                    						newCompositeNode(grammarAccess.getPropertyAccess().getDstatesStateParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_23);
                    lv_dstates_9_0=ruleState();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyRule());
                    						}
                    						add(
                    							current,
                    							"dstates",
                    							lv_dstates_9_0,
                    							"org.xtext.example.mydsl.SPL.State");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSPL.g:2081:3: ( (lv_ll_10_0= ruleLogic ) )
            // InternalSPL.g:2082:4: (lv_ll_10_0= ruleLogic )
            {
            // InternalSPL.g:2082:4: (lv_ll_10_0= ruleLogic )
            // InternalSPL.g:2083:5: lv_ll_10_0= ruleLogic
            {

            					newCompositeNode(grammarAccess.getPropertyAccess().getLlLogicParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_25);
            lv_ll_10_0=ruleLogic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPropertyRule());
            					}
            					add(
            						current,
            						"ll",
            						lv_ll_10_0,
            						"org.xtext.example.mydsl.SPL.Logic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_11=(Token)match(input,24,FOLLOW_37); 

            			newLeafNode(otherlv_11, grammarAccess.getPropertyAccess().getDomain_ModelKeyword_9());
            		
            otherlv_12=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_12, grammarAccess.getPropertyAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProperty"

    // Delegated rules


    protected DFA8 dfa8 = new DFA8(this);
    protected DFA9 dfa9 = new DFA9(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\1\16\1\4\1\uffff\3\4\1\30\1\25\1\uffff\1\30";
    static final String dfa_3s = "\1\41\1\34\1\uffff\1\34\2\26\1\36\1\25\1\uffff\1\36";
    static final String dfa_4s = "\2\uffff\1\2\5\uffff\1\1\1\uffff";
    static final String dfa_5s = "\12\uffff}>";
    static final String[] dfa_6s = {
            "\1\2\14\uffff\1\1\3\uffff\1\2\1\uffff\1\2",
            "\1\3\21\uffff\1\2\2\uffff\2\2\1\uffff\1\4",
            "",
            "\1\3\21\uffff\1\2\2\uffff\2\2\1\uffff\1\4",
            "\1\5\20\uffff\1\6\1\7",
            "\1\5\20\uffff\1\6\1\7",
            "\1\10\5\uffff\1\2",
            "\1\11",
            "",
            "\1\10\5\uffff\1\2"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "()* loopback of 392:3: ( (lv_dmdec_3_0= ruleDomainDecision1 ) )*";
        }
    }
    static final String dfa_7s = "\5\uffff";
    static final String dfa_8s = "\1\16\1\4\1\uffff\1\4\1\uffff";
    static final String dfa_9s = "\1\41\1\34\1\uffff\1\34\1\uffff";
    static final String dfa_10s = "\2\uffff\1\2\1\uffff\1\1";
    static final String dfa_11s = "\5\uffff}>";
    static final String[] dfa_12s = {
            "\1\2\14\uffff\1\1\3\uffff\1\2\1\uffff\1\2",
            "\1\3\21\uffff\1\4\2\uffff\2\4\1\uffff\1\2",
            "",
            "\1\3\21\uffff\1\4\2\uffff\2\4\1\uffff\1\2",
            ""
    };

    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final char[] dfa_8 = DFA.unpackEncodedStringToUnsignedChars(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final short[] dfa_10 = DFA.unpackEncodedString(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[][] dfa_12 = unpackEncodedStringArray(dfa_12s);

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = dfa_7;
            this.eof = dfa_7;
            this.min = dfa_8;
            this.max = dfa_9;
            this.accept = dfa_10;
            this.special = dfa_11;
            this.transition = dfa_12;
        }
        public String getDescription() {
            return "()* loopback of 411:3: ( (lv_dmdec_4_0= ruleDomainDecision2 ) )*";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000000E010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000288004010L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000288004000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000280004000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000200004000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x00000004000C6010L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000400086010L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000400004010L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000400004000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000140000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000600000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000010000010L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000600010L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000026400000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000006400010L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000006400002L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000000004000L});

}