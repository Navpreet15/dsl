/**
 * generated by Xtext 2.10.0
 */
package org.xtext.example.mydsl.sPL.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.example.mydsl.sPL.Feature;
import org.xtext.example.mydsl.sPL.FeatureFacts;
import org.xtext.example.mydsl.sPL.FeatureModel;
import org.xtext.example.mydsl.sPL.SPLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.FeatureModelImpl#getFeatures <em>Features</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.impl.FeatureModelImpl#getFfs <em>Ffs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureModelImpl extends MinimalEObjectImpl.Container implements FeatureModel
{
  /**
   * The cached value of the '{@link #getFeatures() <em>Features</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFeatures()
   * @generated
   * @ordered
   */
  protected EList<Feature> features;

  /**
   * The cached value of the '{@link #getFfs() <em>Ffs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFfs()
   * @generated
   * @ordered
   */
  protected EList<FeatureFacts> ffs;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FeatureModelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SPLPackage.Literals.FEATURE_MODEL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Feature> getFeatures()
  {
    if (features == null)
    {
      features = new EObjectContainmentEList<Feature>(Feature.class, this, SPLPackage.FEATURE_MODEL__FEATURES);
    }
    return features;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FeatureFacts> getFfs()
  {
    if (ffs == null)
    {
      ffs = new EObjectContainmentEList<FeatureFacts>(FeatureFacts.class, this, SPLPackage.FEATURE_MODEL__FFS);
    }
    return ffs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SPLPackage.FEATURE_MODEL__FEATURES:
        return ((InternalEList<?>)getFeatures()).basicRemove(otherEnd, msgs);
      case SPLPackage.FEATURE_MODEL__FFS:
        return ((InternalEList<?>)getFfs()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SPLPackage.FEATURE_MODEL__FEATURES:
        return getFeatures();
      case SPLPackage.FEATURE_MODEL__FFS:
        return getFfs();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SPLPackage.FEATURE_MODEL__FEATURES:
        getFeatures().clear();
        getFeatures().addAll((Collection<? extends Feature>)newValue);
        return;
      case SPLPackage.FEATURE_MODEL__FFS:
        getFfs().clear();
        getFfs().addAll((Collection<? extends FeatureFacts>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SPLPackage.FEATURE_MODEL__FEATURES:
        getFeatures().clear();
        return;
      case SPLPackage.FEATURE_MODEL__FFS:
        getFfs().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SPLPackage.FEATURE_MODEL__FEATURES:
        return features != null && !features.isEmpty();
      case SPLPackage.FEATURE_MODEL__FFS:
        return ffs != null && !ffs.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //FeatureModelImpl
