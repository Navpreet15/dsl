/**
 * generated by Xtext 2.10.0
 */
package org.xtext.example.mydsl.sPL;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SPL</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.sPL.SPL#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.SPL#getFm <em>Fm</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.SPL#getDm <em>Dm</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.SPL#getDc <em>Dc</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.sPL.SPL#getPro <em>Pro</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.mydsl.sPL.SPLPackage#getSPL()
 * @model
 * @generated
 */
public interface SPL extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getSPL_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.sPL.SPL#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Fm</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.sPL.FeatureModel}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fm</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fm</em>' containment reference list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getSPL_Fm()
   * @model containment="true"
   * @generated
   */
  EList<FeatureModel> getFm();

  /**
   * Returns the value of the '<em><b>Dm</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.sPL.DomainModel}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dm</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dm</em>' containment reference list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getSPL_Dm()
   * @model containment="true"
   * @generated
   */
  EList<DomainModel> getDm();

  /**
   * Returns the value of the '<em><b>Dc</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.sPL.DecisionModel}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dc</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dc</em>' containment reference list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getSPL_Dc()
   * @model containment="true"
   * @generated
   */
  EList<DecisionModel> getDc();

  /**
   * Returns the value of the '<em><b>Pro</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.sPL.Property}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pro</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pro</em>' containment reference list.
   * @see org.xtext.example.mydsl.sPL.SPLPackage#getSPL_Pro()
   * @model containment="true"
   * @generated
   */
  EList<Property> getPro();

} // SPL
