/**
 */
package spl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapdec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.Mapdec#getPresence <em>Presence</em>}</li>
 *   <li>{@link spl.Mapdec#getChoice <em>Choice</em>}</li>
 *   <li>{@link spl.Mapdec#getFeature <em>Feature</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getMapdec()
 * @model annotation="gmf.node label='presence'"
 * @generated
 */
public interface Mapdec extends EObject {
	/**
	 * Returns the value of the '<em><b>Presence</b></em>' attribute.
	 * The literals are from the enumeration {@link spl.Presence}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Presence</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Presence</em>' attribute.
	 * @see spl.Presence
	 * @see #setPresence(Presence)
	 * @see spl.SplPackage#getMapdec_Presence()
	 * @model
	 * @generated
	 */
	Presence getPresence();

	/**
	 * Sets the value of the '{@link spl.Mapdec#getPresence <em>Presence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Presence</em>' attribute.
	 * @see spl.Presence
	 * @see #getPresence()
	 * @generated
	 */
	void setPresence(Presence value);

	/**
	 * Returns the value of the '<em><b>Choice</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choice</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choice</em>' reference.
	 * @see #setChoice(Choice)
	 * @see spl.SplPackage#getMapdec_Choice()
	 * @model
	 * @generated
	 */
	Choice getChoice();

	/**
	 * Sets the value of the '{@link spl.Mapdec#getChoice <em>Choice</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Choice</em>' reference.
	 * @see #getChoice()
	 * @generated
	 */
	void setChoice(Choice value);

	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #setFeature(Feature)
	 * @see spl.SplPackage#getMapdec_Feature()
	 * @model
	 * @generated
	 */
	Feature getFeature();

	/**
	 * Sets the value of the '{@link spl.Mapdec#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(Feature value);

} // Mapdec
