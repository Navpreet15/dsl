/**
 */
package spl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import spl.Entry;
import spl.Guard;
import spl.Interfc;
import spl.Label;
import spl.SplPackage;
import spl.TransitionalBehavior;
import spl.Trigger;
import spl.variabls;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>variabls</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.variablsImpl#getInter <em>Inter</em>}</li>
 *   <li>{@link spl.impl.variablsImpl#getTrans <em>Trans</em>}</li>
 *   <li>{@link spl.impl.variablsImpl#getGrd <em>Grd</em>}</li>
 *   <li>{@link spl.impl.variablsImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link spl.impl.variablsImpl#getEntry <em>Entry</em>}</li>
 *   <li>{@link spl.impl.variablsImpl#getTrigger <em>Trigger</em>}</li>
 * </ul>
 *
 * @generated
 */
public class variablsImpl extends EObjectImpl implements variabls {
	/**
	 * The cached value of the '{@link #getInter() <em>Inter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInter()
	 * @generated
	 * @ordered
	 */
	protected EList<Interfc> inter;

	/**
	 * The cached value of the '{@link #getTrans() <em>Trans</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrans()
	 * @generated
	 * @ordered
	 */
	protected EList<TransitionalBehavior> trans;

	/**
	 * The cached value of the '{@link #getGrd() <em>Grd</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGrd()
	 * @generated
	 * @ordered
	 */
	protected EList<Guard> grd;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected EList<Label> label;

	/**
	 * The cached value of the '{@link #getEntry() <em>Entry</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntry()
	 * @generated
	 * @ordered
	 */
	protected EList<Entry> entry;

	/**
	 * The cached value of the '{@link #getTrigger() <em>Trigger</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrigger()
	 * @generated
	 * @ordered
	 */
	protected EList<Trigger> trigger;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected variablsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.VARIABLS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Interfc> getInter() {
		if (inter == null) {
			inter = new EObjectContainmentEList<Interfc>(Interfc.class, this, SplPackage.VARIABLS__INTER);
		}
		return inter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransitionalBehavior> getTrans() {
		if (trans == null) {
			trans = new EObjectContainmentEList<TransitionalBehavior>(TransitionalBehavior.class, this, SplPackage.VARIABLS__TRANS);
		}
		return trans;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Guard> getGrd() {
		if (grd == null) {
			grd = new EObjectContainmentEList<Guard>(Guard.class, this, SplPackage.VARIABLS__GRD);
		}
		return grd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Label> getLabel() {
		if (label == null) {
			label = new EObjectContainmentEList<Label>(Label.class, this, SplPackage.VARIABLS__LABEL);
		}
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Entry> getEntry() {
		if (entry == null) {
			entry = new EObjectContainmentEList<Entry>(Entry.class, this, SplPackage.VARIABLS__ENTRY);
		}
		return entry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Trigger> getTrigger() {
		if (trigger == null) {
			trigger = new EObjectContainmentEList<Trigger>(Trigger.class, this, SplPackage.VARIABLS__TRIGGER);
		}
		return trigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SplPackage.VARIABLS__INTER:
				return ((InternalEList<?>)getInter()).basicRemove(otherEnd, msgs);
			case SplPackage.VARIABLS__TRANS:
				return ((InternalEList<?>)getTrans()).basicRemove(otherEnd, msgs);
			case SplPackage.VARIABLS__GRD:
				return ((InternalEList<?>)getGrd()).basicRemove(otherEnd, msgs);
			case SplPackage.VARIABLS__LABEL:
				return ((InternalEList<?>)getLabel()).basicRemove(otherEnd, msgs);
			case SplPackage.VARIABLS__ENTRY:
				return ((InternalEList<?>)getEntry()).basicRemove(otherEnd, msgs);
			case SplPackage.VARIABLS__TRIGGER:
				return ((InternalEList<?>)getTrigger()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.VARIABLS__INTER:
				return getInter();
			case SplPackage.VARIABLS__TRANS:
				return getTrans();
			case SplPackage.VARIABLS__GRD:
				return getGrd();
			case SplPackage.VARIABLS__LABEL:
				return getLabel();
			case SplPackage.VARIABLS__ENTRY:
				return getEntry();
			case SplPackage.VARIABLS__TRIGGER:
				return getTrigger();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.VARIABLS__INTER:
				getInter().clear();
				getInter().addAll((Collection<? extends Interfc>)newValue);
				return;
			case SplPackage.VARIABLS__TRANS:
				getTrans().clear();
				getTrans().addAll((Collection<? extends TransitionalBehavior>)newValue);
				return;
			case SplPackage.VARIABLS__GRD:
				getGrd().clear();
				getGrd().addAll((Collection<? extends Guard>)newValue);
				return;
			case SplPackage.VARIABLS__LABEL:
				getLabel().clear();
				getLabel().addAll((Collection<? extends Label>)newValue);
				return;
			case SplPackage.VARIABLS__ENTRY:
				getEntry().clear();
				getEntry().addAll((Collection<? extends Entry>)newValue);
				return;
			case SplPackage.VARIABLS__TRIGGER:
				getTrigger().clear();
				getTrigger().addAll((Collection<? extends Trigger>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.VARIABLS__INTER:
				getInter().clear();
				return;
			case SplPackage.VARIABLS__TRANS:
				getTrans().clear();
				return;
			case SplPackage.VARIABLS__GRD:
				getGrd().clear();
				return;
			case SplPackage.VARIABLS__LABEL:
				getLabel().clear();
				return;
			case SplPackage.VARIABLS__ENTRY:
				getEntry().clear();
				return;
			case SplPackage.VARIABLS__TRIGGER:
				getTrigger().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.VARIABLS__INTER:
				return inter != null && !inter.isEmpty();
			case SplPackage.VARIABLS__TRANS:
				return trans != null && !trans.isEmpty();
			case SplPackage.VARIABLS__GRD:
				return grd != null && !grd.isEmpty();
			case SplPackage.VARIABLS__LABEL:
				return label != null && !label.isEmpty();
			case SplPackage.VARIABLS__ENTRY:
				return entry != null && !entry.isEmpty();
			case SplPackage.VARIABLS__TRIGGER:
				return trigger != null && !trigger.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //variablsImpl
