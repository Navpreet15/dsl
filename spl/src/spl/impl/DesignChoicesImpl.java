/**
 */
package spl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import spl.ChoiceModel;
import spl.ClassDiagram;
import spl.DecisionFeatureMapping;
import spl.DecisionMapping;
import spl.DecisiontoFeature;
import spl.DesignChoices;
import spl.DomainModel;
import spl.FeatureMapping;
import spl.FeatureModel;
import spl.Property;
import spl.SPL;
import spl.SplPackage;
import spl.StateChart;
import spl.featMapping;
import spl.variabls;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Design Choices</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.DesignChoicesImpl#getName <em>Name</em>}</li>
 *   <li>{@link spl.impl.DesignChoicesImpl#getCm <em>Cm</em>}</li>
 *   <li>{@link spl.impl.DesignChoicesImpl#getFm <em>Fm</em>}</li>
 *   <li>{@link spl.impl.DesignChoicesImpl#getFmap <em>Fmap</em>}</li>
 *   <li>{@link spl.impl.DesignChoicesImpl#getDmap <em>Dmap</em>}</li>
 *   <li>{@link spl.impl.DesignChoicesImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link spl.impl.DesignChoicesImpl#getVar <em>Var</em>}</li>
 *   <li>{@link spl.impl.DesignChoicesImpl#getCd <em>Cd</em>}</li>
 *   <li>{@link spl.impl.DesignChoicesImpl#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DesignChoicesImpl extends EObjectImpl implements DesignChoices {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCm() <em>Cm</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCm()
	 * @generated
	 * @ordered
	 */
	protected ChoiceModel cm;

	/**
	 * The cached value of the '{@link #getFm() <em>Fm</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFm()
	 * @generated
	 * @ordered
	 */
	protected FeatureModel fm;

	/**
	 * The cached value of the '{@link #getFmap() <em>Fmap</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFmap()
	 * @generated
	 * @ordered
	 */
	protected FeatureMapping fmap;

	/**
	 * The cached value of the '{@link #getDmap() <em>Dmap</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDmap()
	 * @generated
	 * @ordered
	 */
	protected EList<DecisionMapping> dmap;

	/**
	 * The cached value of the '{@link #getProperty() <em>Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<Property> property;

	/**
	 * The cached value of the '{@link #getVar() <em>Var</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVar()
	 * @generated
	 * @ordered
	 */
	protected EList<variabls> var;

	/**
	 * The cached value of the '{@link #getCd() <em>Cd</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCd()
	 * @generated
	 * @ordered
	 */
	protected ClassDiagram cd;

	/**
	 * The cached value of the '{@link #getSc() <em>Sc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSc()
	 * @generated
	 * @ordered
	 */
	protected StateChart sc;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DesignChoicesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.DESIGN_CHOICES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DESIGN_CHOICES__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoiceModel getCm() {
		return cm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCm(ChoiceModel newCm, NotificationChain msgs) {
		ChoiceModel oldCm = cm;
		cm = newCm;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SplPackage.DESIGN_CHOICES__CM, oldCm, newCm);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCm(ChoiceModel newCm) {
		if (newCm != cm) {
			NotificationChain msgs = null;
			if (cm != null)
				msgs = ((InternalEObject)cm).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SplPackage.DESIGN_CHOICES__CM, null, msgs);
			if (newCm != null)
				msgs = ((InternalEObject)newCm).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SplPackage.DESIGN_CHOICES__CM, null, msgs);
			msgs = basicSetCm(newCm, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DESIGN_CHOICES__CM, newCm, newCm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModel getFm() {
		return fm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFm(FeatureModel newFm, NotificationChain msgs) {
		FeatureModel oldFm = fm;
		fm = newFm;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SplPackage.DESIGN_CHOICES__FM, oldFm, newFm);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFm(FeatureModel newFm) {
		if (newFm != fm) {
			NotificationChain msgs = null;
			if (fm != null)
				msgs = ((InternalEObject)fm).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SplPackage.DESIGN_CHOICES__FM, null, msgs);
			if (newFm != null)
				msgs = ((InternalEObject)newFm).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SplPackage.DESIGN_CHOICES__FM, null, msgs);
			msgs = basicSetFm(newFm, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DESIGN_CHOICES__FM, newFm, newFm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMapping getFmap() {
		return fmap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFmap(FeatureMapping newFmap, NotificationChain msgs) {
		FeatureMapping oldFmap = fmap;
		fmap = newFmap;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SplPackage.DESIGN_CHOICES__FMAP, oldFmap, newFmap);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFmap(FeatureMapping newFmap) {
		if (newFmap != fmap) {
			NotificationChain msgs = null;
			if (fmap != null)
				msgs = ((InternalEObject)fmap).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SplPackage.DESIGN_CHOICES__FMAP, null, msgs);
			if (newFmap != null)
				msgs = ((InternalEObject)newFmap).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SplPackage.DESIGN_CHOICES__FMAP, null, msgs);
			msgs = basicSetFmap(newFmap, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DESIGN_CHOICES__FMAP, newFmap, newFmap));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DecisionMapping> getDmap() {
		if (dmap == null) {
			dmap = new EObjectContainmentEList<DecisionMapping>(DecisionMapping.class, this, SplPackage.DESIGN_CHOICES__DMAP);
		}
		return dmap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Property> getProperty() {
		if (property == null) {
			property = new EObjectContainmentEList<Property>(Property.class, this, SplPackage.DESIGN_CHOICES__PROPERTY);
		}
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<variabls> getVar() {
		if (var == null) {
			var = new EObjectContainmentEList<variabls>(variabls.class, this, SplPackage.DESIGN_CHOICES__VAR);
		}
		return var;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassDiagram getCd() {
		return cd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCd(ClassDiagram newCd, NotificationChain msgs) {
		ClassDiagram oldCd = cd;
		cd = newCd;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SplPackage.DESIGN_CHOICES__CD, oldCd, newCd);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCd(ClassDiagram newCd) {
		if (newCd != cd) {
			NotificationChain msgs = null;
			if (cd != null)
				msgs = ((InternalEObject)cd).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SplPackage.DESIGN_CHOICES__CD, null, msgs);
			if (newCd != null)
				msgs = ((InternalEObject)newCd).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SplPackage.DESIGN_CHOICES__CD, null, msgs);
			msgs = basicSetCd(newCd, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DESIGN_CHOICES__CD, newCd, newCd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateChart getSc() {
		return sc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSc(StateChart newSc, NotificationChain msgs) {
		StateChart oldSc = sc;
		sc = newSc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SplPackage.DESIGN_CHOICES__SC, oldSc, newSc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSc(StateChart newSc) {
		if (newSc != sc) {
			NotificationChain msgs = null;
			if (sc != null)
				msgs = ((InternalEObject)sc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SplPackage.DESIGN_CHOICES__SC, null, msgs);
			if (newSc != null)
				msgs = ((InternalEObject)newSc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SplPackage.DESIGN_CHOICES__SC, null, msgs);
			msgs = basicSetSc(newSc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DESIGN_CHOICES__SC, newSc, newSc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SplPackage.DESIGN_CHOICES__CM:
				return basicSetCm(null, msgs);
			case SplPackage.DESIGN_CHOICES__FM:
				return basicSetFm(null, msgs);
			case SplPackage.DESIGN_CHOICES__FMAP:
				return basicSetFmap(null, msgs);
			case SplPackage.DESIGN_CHOICES__DMAP:
				return ((InternalEList<?>)getDmap()).basicRemove(otherEnd, msgs);
			case SplPackage.DESIGN_CHOICES__PROPERTY:
				return ((InternalEList<?>)getProperty()).basicRemove(otherEnd, msgs);
			case SplPackage.DESIGN_CHOICES__VAR:
				return ((InternalEList<?>)getVar()).basicRemove(otherEnd, msgs);
			case SplPackage.DESIGN_CHOICES__CD:
				return basicSetCd(null, msgs);
			case SplPackage.DESIGN_CHOICES__SC:
				return basicSetSc(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.DESIGN_CHOICES__NAME:
				return getName();
			case SplPackage.DESIGN_CHOICES__CM:
				return getCm();
			case SplPackage.DESIGN_CHOICES__FM:
				return getFm();
			case SplPackage.DESIGN_CHOICES__FMAP:
				return getFmap();
			case SplPackage.DESIGN_CHOICES__DMAP:
				return getDmap();
			case SplPackage.DESIGN_CHOICES__PROPERTY:
				return getProperty();
			case SplPackage.DESIGN_CHOICES__VAR:
				return getVar();
			case SplPackage.DESIGN_CHOICES__CD:
				return getCd();
			case SplPackage.DESIGN_CHOICES__SC:
				return getSc();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.DESIGN_CHOICES__NAME:
				setName((String)newValue);
				return;
			case SplPackage.DESIGN_CHOICES__CM:
				setCm((ChoiceModel)newValue);
				return;
			case SplPackage.DESIGN_CHOICES__FM:
				setFm((FeatureModel)newValue);
				return;
			case SplPackage.DESIGN_CHOICES__FMAP:
				setFmap((FeatureMapping)newValue);
				return;
			case SplPackage.DESIGN_CHOICES__DMAP:
				getDmap().clear();
				getDmap().addAll((Collection<? extends DecisionMapping>)newValue);
				return;
			case SplPackage.DESIGN_CHOICES__PROPERTY:
				getProperty().clear();
				getProperty().addAll((Collection<? extends Property>)newValue);
				return;
			case SplPackage.DESIGN_CHOICES__VAR:
				getVar().clear();
				getVar().addAll((Collection<? extends variabls>)newValue);
				return;
			case SplPackage.DESIGN_CHOICES__CD:
				setCd((ClassDiagram)newValue);
				return;
			case SplPackage.DESIGN_CHOICES__SC:
				setSc((StateChart)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.DESIGN_CHOICES__NAME:
				setName(NAME_EDEFAULT);
				return;
			case SplPackage.DESIGN_CHOICES__CM:
				setCm((ChoiceModel)null);
				return;
			case SplPackage.DESIGN_CHOICES__FM:
				setFm((FeatureModel)null);
				return;
			case SplPackage.DESIGN_CHOICES__FMAP:
				setFmap((FeatureMapping)null);
				return;
			case SplPackage.DESIGN_CHOICES__DMAP:
				getDmap().clear();
				return;
			case SplPackage.DESIGN_CHOICES__PROPERTY:
				getProperty().clear();
				return;
			case SplPackage.DESIGN_CHOICES__VAR:
				getVar().clear();
				return;
			case SplPackage.DESIGN_CHOICES__CD:
				setCd((ClassDiagram)null);
				return;
			case SplPackage.DESIGN_CHOICES__SC:
				setSc((StateChart)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.DESIGN_CHOICES__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case SplPackage.DESIGN_CHOICES__CM:
				return cm != null;
			case SplPackage.DESIGN_CHOICES__FM:
				return fm != null;
			case SplPackage.DESIGN_CHOICES__FMAP:
				return fmap != null;
			case SplPackage.DESIGN_CHOICES__DMAP:
				return dmap != null && !dmap.isEmpty();
			case SplPackage.DESIGN_CHOICES__PROPERTY:
				return property != null && !property.isEmpty();
			case SplPackage.DESIGN_CHOICES__VAR:
				return var != null && !var.isEmpty();
			case SplPackage.DESIGN_CHOICES__CD:
				return cd != null;
			case SplPackage.DESIGN_CHOICES__SC:
				return sc != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DesignChoicesImpl
