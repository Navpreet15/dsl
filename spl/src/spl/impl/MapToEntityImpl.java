/**
 */
package spl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import spl.Feature;
import spl.MapToEntity;
import spl.Presence;
import spl.Relationship;
import spl.SplPackage;
import spl.State;
import spl.Transition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Map To Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.MapToEntityImpl#getPresence <em>Presence</em>}</li>
 *   <li>{@link spl.impl.MapToEntityImpl#getFtr <em>Ftr</em>}</li>
 *   <li>{@link spl.impl.MapToEntityImpl#getC <em>C</em>}</li>
 *   <li>{@link spl.impl.MapToEntityImpl#getRelation <em>Relation</em>}</li>
 *   <li>{@link spl.impl.MapToEntityImpl#getState <em>State</em>}</li>
 *   <li>{@link spl.impl.MapToEntityImpl#getTransition <em>Transition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MapToEntityImpl extends EObjectImpl implements MapToEntity {
	/**
	 * The default value of the '{@link #getPresence() <em>Presence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPresence()
	 * @generated
	 * @ordered
	 */
	protected static final Presence PRESENCE_EDEFAULT = Presence.INCLUDE;

	/**
	 * The cached value of the '{@link #getPresence() <em>Presence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPresence()
	 * @generated
	 * @ordered
	 */
	protected Presence presence = PRESENCE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFtr() <em>Ftr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFtr()
	 * @generated
	 * @ordered
	 */
	protected Feature ftr;

	/**
	 * The cached value of the '{@link #getC() <em>C</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getC()
	 * @generated
	 * @ordered
	 */
	protected spl.Class c;

	/**
	 * The cached value of the '{@link #getRelation() <em>Relation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelation()
	 * @generated
	 * @ordered
	 */
	protected Relationship relation;

	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected State state;

	/**
	 * The cached value of the '{@link #getTransition() <em>Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransition()
	 * @generated
	 * @ordered
	 */
	protected Transition transition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MapToEntityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.MAP_TO_ENTITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Presence getPresence() {
		return presence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPresence(Presence newPresence) {
		Presence oldPresence = presence;
		presence = newPresence == null ? PRESENCE_EDEFAULT : newPresence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.MAP_TO_ENTITY__PRESENCE, oldPresence, presence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature getFtr() {
		if (ftr != null && ftr.eIsProxy()) {
			InternalEObject oldFtr = (InternalEObject)ftr;
			ftr = (Feature)eResolveProxy(oldFtr);
			if (ftr != oldFtr) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SplPackage.MAP_TO_ENTITY__FTR, oldFtr, ftr));
			}
		}
		return ftr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature basicGetFtr() {
		return ftr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFtr(Feature newFtr) {
		Feature oldFtr = ftr;
		ftr = newFtr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.MAP_TO_ENTITY__FTR, oldFtr, ftr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public spl.Class getC() {
		if (c != null && c.eIsProxy()) {
			InternalEObject oldC = (InternalEObject)c;
			c = (spl.Class)eResolveProxy(oldC);
			if (c != oldC) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SplPackage.MAP_TO_ENTITY__C, oldC, c));
			}
		}
		return c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public spl.Class basicGetC() {
		return c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setC(spl.Class newC) {
		spl.Class oldC = c;
		c = newC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.MAP_TO_ENTITY__C, oldC, c));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Relationship getRelation() {
		if (relation != null && relation.eIsProxy()) {
			InternalEObject oldRelation = (InternalEObject)relation;
			relation = (Relationship)eResolveProxy(oldRelation);
			if (relation != oldRelation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SplPackage.MAP_TO_ENTITY__RELATION, oldRelation, relation));
			}
		}
		return relation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Relationship basicGetRelation() {
		return relation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelation(Relationship newRelation) {
		Relationship oldRelation = relation;
		relation = newRelation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.MAP_TO_ENTITY__RELATION, oldRelation, relation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getState() {
		if (state != null && state.eIsProxy()) {
			InternalEObject oldState = (InternalEObject)state;
			state = (State)eResolveProxy(oldState);
			if (state != oldState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SplPackage.MAP_TO_ENTITY__STATE, oldState, state));
			}
		}
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState(State newState) {
		State oldState = state;
		state = newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.MAP_TO_ENTITY__STATE, oldState, state));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition getTransition() {
		if (transition != null && transition.eIsProxy()) {
			InternalEObject oldTransition = (InternalEObject)transition;
			transition = (Transition)eResolveProxy(oldTransition);
			if (transition != oldTransition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SplPackage.MAP_TO_ENTITY__TRANSITION, oldTransition, transition));
			}
		}
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition basicGetTransition() {
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransition(Transition newTransition) {
		Transition oldTransition = transition;
		transition = newTransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.MAP_TO_ENTITY__TRANSITION, oldTransition, transition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.MAP_TO_ENTITY__PRESENCE:
				return getPresence();
			case SplPackage.MAP_TO_ENTITY__FTR:
				if (resolve) return getFtr();
				return basicGetFtr();
			case SplPackage.MAP_TO_ENTITY__C:
				if (resolve) return getC();
				return basicGetC();
			case SplPackage.MAP_TO_ENTITY__RELATION:
				if (resolve) return getRelation();
				return basicGetRelation();
			case SplPackage.MAP_TO_ENTITY__STATE:
				if (resolve) return getState();
				return basicGetState();
			case SplPackage.MAP_TO_ENTITY__TRANSITION:
				if (resolve) return getTransition();
				return basicGetTransition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.MAP_TO_ENTITY__PRESENCE:
				setPresence((Presence)newValue);
				return;
			case SplPackage.MAP_TO_ENTITY__FTR:
				setFtr((Feature)newValue);
				return;
			case SplPackage.MAP_TO_ENTITY__C:
				setC((spl.Class)newValue);
				return;
			case SplPackage.MAP_TO_ENTITY__RELATION:
				setRelation((Relationship)newValue);
				return;
			case SplPackage.MAP_TO_ENTITY__STATE:
				setState((State)newValue);
				return;
			case SplPackage.MAP_TO_ENTITY__TRANSITION:
				setTransition((Transition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.MAP_TO_ENTITY__PRESENCE:
				setPresence(PRESENCE_EDEFAULT);
				return;
			case SplPackage.MAP_TO_ENTITY__FTR:
				setFtr((Feature)null);
				return;
			case SplPackage.MAP_TO_ENTITY__C:
				setC((spl.Class)null);
				return;
			case SplPackage.MAP_TO_ENTITY__RELATION:
				setRelation((Relationship)null);
				return;
			case SplPackage.MAP_TO_ENTITY__STATE:
				setState((State)null);
				return;
			case SplPackage.MAP_TO_ENTITY__TRANSITION:
				setTransition((Transition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.MAP_TO_ENTITY__PRESENCE:
				return presence != PRESENCE_EDEFAULT;
			case SplPackage.MAP_TO_ENTITY__FTR:
				return ftr != null;
			case SplPackage.MAP_TO_ENTITY__C:
				return c != null;
			case SplPackage.MAP_TO_ENTITY__RELATION:
				return relation != null;
			case SplPackage.MAP_TO_ENTITY__STATE:
				return state != null;
			case SplPackage.MAP_TO_ENTITY__TRANSITION:
				return transition != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (presence: ");
		result.append(presence);
		result.append(')');
		return result.toString();
	}

} //MapToEntityImpl
