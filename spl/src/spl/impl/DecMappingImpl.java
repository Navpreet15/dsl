/**
 */
package spl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import spl.DecMapping;
import spl.MapToEntity;
import spl.Mapdec;
import spl.Operator;
import spl.SplPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dec Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.DecMappingImpl#getOprLHS <em>Opr LHS</em>}</li>
 *   <li>{@link spl.impl.DecMappingImpl#getMapfrom <em>Mapfrom</em>}</li>
 *   <li>{@link spl.impl.DecMappingImpl#getOprRHS <em>Opr RHS</em>}</li>
 *   <li>{@link spl.impl.DecMappingImpl#getMaptoEntity <em>Mapto Entity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DecMappingImpl extends EObjectImpl implements DecMapping {
	/**
	 * The default value of the '{@link #getOprLHS() <em>Opr LHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOprLHS()
	 * @generated
	 * @ordered
	 */
	protected static final Operator OPR_LHS_EDEFAULT = Operator.AND;

	/**
	 * The cached value of the '{@link #getOprLHS() <em>Opr LHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOprLHS()
	 * @generated
	 * @ordered
	 */
	protected Operator oprLHS = OPR_LHS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMapfrom() <em>Mapfrom</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMapfrom()
	 * @generated
	 * @ordered
	 */
	protected EList<Mapdec> mapfrom;

	/**
	 * The default value of the '{@link #getOprRHS() <em>Opr RHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOprRHS()
	 * @generated
	 * @ordered
	 */
	protected static final Operator OPR_RHS_EDEFAULT = Operator.AND;

	/**
	 * The cached value of the '{@link #getOprRHS() <em>Opr RHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOprRHS()
	 * @generated
	 * @ordered
	 */
	protected Operator oprRHS = OPR_RHS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMaptoEntity() <em>Mapto Entity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaptoEntity()
	 * @generated
	 * @ordered
	 */
	protected EList<MapToEntity> maptoEntity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.DEC_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operator getOprLHS() {
		return oprLHS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOprLHS(Operator newOprLHS) {
		Operator oldOprLHS = oprLHS;
		oprLHS = newOprLHS == null ? OPR_LHS_EDEFAULT : newOprLHS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DEC_MAPPING__OPR_LHS, oldOprLHS, oprLHS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Mapdec> getMapfrom() {
		if (mapfrom == null) {
			mapfrom = new EObjectContainmentEList<Mapdec>(Mapdec.class, this, SplPackage.DEC_MAPPING__MAPFROM);
		}
		return mapfrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operator getOprRHS() {
		return oprRHS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOprRHS(Operator newOprRHS) {
		Operator oldOprRHS = oprRHS;
		oprRHS = newOprRHS == null ? OPR_RHS_EDEFAULT : newOprRHS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DEC_MAPPING__OPR_RHS, oldOprRHS, oprRHS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MapToEntity> getMaptoEntity() {
		if (maptoEntity == null) {
			maptoEntity = new EObjectContainmentEList<MapToEntity>(MapToEntity.class, this, SplPackage.DEC_MAPPING__MAPTO_ENTITY);
		}
		return maptoEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SplPackage.DEC_MAPPING__MAPFROM:
				return ((InternalEList<?>)getMapfrom()).basicRemove(otherEnd, msgs);
			case SplPackage.DEC_MAPPING__MAPTO_ENTITY:
				return ((InternalEList<?>)getMaptoEntity()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.DEC_MAPPING__OPR_LHS:
				return getOprLHS();
			case SplPackage.DEC_MAPPING__MAPFROM:
				return getMapfrom();
			case SplPackage.DEC_MAPPING__OPR_RHS:
				return getOprRHS();
			case SplPackage.DEC_MAPPING__MAPTO_ENTITY:
				return getMaptoEntity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.DEC_MAPPING__OPR_LHS:
				setOprLHS((Operator)newValue);
				return;
			case SplPackage.DEC_MAPPING__MAPFROM:
				getMapfrom().clear();
				getMapfrom().addAll((Collection<? extends Mapdec>)newValue);
				return;
			case SplPackage.DEC_MAPPING__OPR_RHS:
				setOprRHS((Operator)newValue);
				return;
			case SplPackage.DEC_MAPPING__MAPTO_ENTITY:
				getMaptoEntity().clear();
				getMaptoEntity().addAll((Collection<? extends MapToEntity>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.DEC_MAPPING__OPR_LHS:
				setOprLHS(OPR_LHS_EDEFAULT);
				return;
			case SplPackage.DEC_MAPPING__MAPFROM:
				getMapfrom().clear();
				return;
			case SplPackage.DEC_MAPPING__OPR_RHS:
				setOprRHS(OPR_RHS_EDEFAULT);
				return;
			case SplPackage.DEC_MAPPING__MAPTO_ENTITY:
				getMaptoEntity().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.DEC_MAPPING__OPR_LHS:
				return oprLHS != OPR_LHS_EDEFAULT;
			case SplPackage.DEC_MAPPING__MAPFROM:
				return mapfrom != null && !mapfrom.isEmpty();
			case SplPackage.DEC_MAPPING__OPR_RHS:
				return oprRHS != OPR_RHS_EDEFAULT;
			case SplPackage.DEC_MAPPING__MAPTO_ENTITY:
				return maptoEntity != null && !maptoEntity.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (oprLHS: ");
		result.append(oprLHS);
		result.append(", oprRHS: ");
		result.append(oprRHS);
		result.append(')');
		return result.toString();
	}

} //DecMappingImpl
