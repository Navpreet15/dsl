/**
 */
package spl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import spl.ClassDiagram;
import spl.DomainModel;
import spl.SplPackage;
import spl.StateChart;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Domain Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.DomainModelImpl#getName <em>Name</em>}</li>
 *   <li>{@link spl.impl.DomainModelImpl#getCd <em>Cd</em>}</li>
 *   <li>{@link spl.impl.DomainModelImpl#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DomainModelImpl extends EObjectImpl implements DomainModel {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCd() <em>Cd</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCd()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassDiagram> cd;

	/**
	 * The cached value of the '{@link #getSc() <em>Sc</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSc()
	 * @generated
	 * @ordered
	 */
	protected EList<StateChart> sc;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DomainModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.DOMAIN_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DOMAIN_MODEL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassDiagram> getCd() {
		if (cd == null) {
			cd = new EObjectContainmentEList<ClassDiagram>(ClassDiagram.class, this, SplPackage.DOMAIN_MODEL__CD);
		}
		return cd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StateChart> getSc() {
		if (sc == null) {
			sc = new EObjectContainmentEList<StateChart>(StateChart.class, this, SplPackage.DOMAIN_MODEL__SC);
		}
		return sc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SplPackage.DOMAIN_MODEL__CD:
				return ((InternalEList<?>)getCd()).basicRemove(otherEnd, msgs);
			case SplPackage.DOMAIN_MODEL__SC:
				return ((InternalEList<?>)getSc()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.DOMAIN_MODEL__NAME:
				return getName();
			case SplPackage.DOMAIN_MODEL__CD:
				return getCd();
			case SplPackage.DOMAIN_MODEL__SC:
				return getSc();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.DOMAIN_MODEL__NAME:
				setName((String)newValue);
				return;
			case SplPackage.DOMAIN_MODEL__CD:
				getCd().clear();
				getCd().addAll((Collection<? extends ClassDiagram>)newValue);
				return;
			case SplPackage.DOMAIN_MODEL__SC:
				getSc().clear();
				getSc().addAll((Collection<? extends StateChart>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.DOMAIN_MODEL__NAME:
				setName(NAME_EDEFAULT);
				return;
			case SplPackage.DOMAIN_MODEL__CD:
				getCd().clear();
				return;
			case SplPackage.DOMAIN_MODEL__SC:
				getSc().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.DOMAIN_MODEL__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case SplPackage.DOMAIN_MODEL__CD:
				return cd != null && !cd.isEmpty();
			case SplPackage.DOMAIN_MODEL__SC:
				return sc != null && !sc.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DomainModelImpl
