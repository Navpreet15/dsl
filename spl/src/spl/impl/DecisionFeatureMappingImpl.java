/**
 */
package spl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import spl.DecisionFeatureMapping;
import spl.SplPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decision Feature Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.DecisionFeatureMappingImpl#getFeaturename <em>Featurename</em>}</li>
 *   <li>{@link spl.impl.DecisionFeatureMappingImpl#getDecisionname <em>Decisionname</em>}</li>
 *   <li>{@link spl.impl.DecisionFeatureMappingImpl#getClassname <em>Classname</em>}</li>
 *   <li>{@link spl.impl.DecisionFeatureMappingImpl#getContainclass <em>Containclass</em>}</li>
 *   <li>{@link spl.impl.DecisionFeatureMappingImpl#getRelationname <em>Relationname</em>}</li>
 *   <li>{@link spl.impl.DecisionFeatureMappingImpl#getContainrelation <em>Containrelation</em>}</li>
 *   <li>{@link spl.impl.DecisionFeatureMappingImpl#getStatename <em>Statename</em>}</li>
 *   <li>{@link spl.impl.DecisionFeatureMappingImpl#getContainstate <em>Containstate</em>}</li>
 *   <li>{@link spl.impl.DecisionFeatureMappingImpl#getTransitionname <em>Transitionname</em>}</li>
 *   <li>{@link spl.impl.DecisionFeatureMappingImpl#getContaintransition <em>Containtransition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DecisionFeatureMappingImpl extends EObjectImpl implements DecisionFeatureMapping {
	/**
	 * The default value of the '{@link #getFeaturename() <em>Featurename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeaturename()
	 * @generated
	 * @ordered
	 */
	protected static final String FEATURENAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFeaturename() <em>Featurename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeaturename()
	 * @generated
	 * @ordered
	 */
	protected String featurename = FEATURENAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDecisionname() <em>Decisionname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDecisionname()
	 * @generated
	 * @ordered
	 */
	protected static final String DECISIONNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDecisionname() <em>Decisionname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDecisionname()
	 * @generated
	 * @ordered
	 */
	protected String decisionname = DECISIONNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getClassname() <em>Classname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassname()
	 * @generated
	 * @ordered
	 */
	protected static final String CLASSNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClassname() <em>Classname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassname()
	 * @generated
	 * @ordered
	 */
	protected String classname = CLASSNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContainclass() <em>Containclass</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainclass()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTAINCLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContainclass() <em>Containclass</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainclass()
	 * @generated
	 * @ordered
	 */
	protected String containclass = CONTAINCLASS_EDEFAULT;

	/**
	 * The default value of the '{@link #getRelationname() <em>Relationname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationname()
	 * @generated
	 * @ordered
	 */
	protected static final String RELATIONNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRelationname() <em>Relationname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationname()
	 * @generated
	 * @ordered
	 */
	protected String relationname = RELATIONNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContainrelation() <em>Containrelation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainrelation()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTAINRELATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContainrelation() <em>Containrelation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainrelation()
	 * @generated
	 * @ordered
	 */
	protected String containrelation = CONTAINRELATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getStatename() <em>Statename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatename()
	 * @generated
	 * @ordered
	 */
	protected static final String STATENAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStatename() <em>Statename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatename()
	 * @generated
	 * @ordered
	 */
	protected String statename = STATENAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContainstate() <em>Containstate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainstate()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTAINSTATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContainstate() <em>Containstate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainstate()
	 * @generated
	 * @ordered
	 */
	protected String containstate = CONTAINSTATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTransitionname() <em>Transitionname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitionname()
	 * @generated
	 * @ordered
	 */
	protected static final String TRANSITIONNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTransitionname() <em>Transitionname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitionname()
	 * @generated
	 * @ordered
	 */
	protected String transitionname = TRANSITIONNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContaintransition() <em>Containtransition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContaintransition()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTAINTRANSITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContaintransition() <em>Containtransition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContaintransition()
	 * @generated
	 * @ordered
	 */
	protected String containtransition = CONTAINTRANSITION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecisionFeatureMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.DECISION_FEATURE_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFeaturename() {
		return featurename;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeaturename(String newFeaturename) {
		String oldFeaturename = featurename;
		featurename = newFeaturename;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISION_FEATURE_MAPPING__FEATURENAME, oldFeaturename, featurename));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDecisionname() {
		return decisionname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDecisionname(String newDecisionname) {
		String oldDecisionname = decisionname;
		decisionname = newDecisionname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISION_FEATURE_MAPPING__DECISIONNAME, oldDecisionname, decisionname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getClassname() {
		return classname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassname(String newClassname) {
		String oldClassname = classname;
		classname = newClassname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISION_FEATURE_MAPPING__CLASSNAME, oldClassname, classname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContainclass() {
		return containclass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainclass(String newContainclass) {
		String oldContainclass = containclass;
		containclass = newContainclass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISION_FEATURE_MAPPING__CONTAINCLASS, oldContainclass, containclass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRelationname() {
		return relationname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelationname(String newRelationname) {
		String oldRelationname = relationname;
		relationname = newRelationname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISION_FEATURE_MAPPING__RELATIONNAME, oldRelationname, relationname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContainrelation() {
		return containrelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainrelation(String newContainrelation) {
		String oldContainrelation = containrelation;
		containrelation = newContainrelation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISION_FEATURE_MAPPING__CONTAINRELATION, oldContainrelation, containrelation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStatename() {
		return statename;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatename(String newStatename) {
		String oldStatename = statename;
		statename = newStatename;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISION_FEATURE_MAPPING__STATENAME, oldStatename, statename));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContainstate() {
		return containstate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainstate(String newContainstate) {
		String oldContainstate = containstate;
		containstate = newContainstate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISION_FEATURE_MAPPING__CONTAINSTATE, oldContainstate, containstate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTransitionname() {
		return transitionname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransitionname(String newTransitionname) {
		String oldTransitionname = transitionname;
		transitionname = newTransitionname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISION_FEATURE_MAPPING__TRANSITIONNAME, oldTransitionname, transitionname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContaintransition() {
		return containtransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContaintransition(String newContaintransition) {
		String oldContaintransition = containtransition;
		containtransition = newContaintransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISION_FEATURE_MAPPING__CONTAINTRANSITION, oldContaintransition, containtransition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.DECISION_FEATURE_MAPPING__FEATURENAME:
				return getFeaturename();
			case SplPackage.DECISION_FEATURE_MAPPING__DECISIONNAME:
				return getDecisionname();
			case SplPackage.DECISION_FEATURE_MAPPING__CLASSNAME:
				return getClassname();
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINCLASS:
				return getContainclass();
			case SplPackage.DECISION_FEATURE_MAPPING__RELATIONNAME:
				return getRelationname();
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINRELATION:
				return getContainrelation();
			case SplPackage.DECISION_FEATURE_MAPPING__STATENAME:
				return getStatename();
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINSTATE:
				return getContainstate();
			case SplPackage.DECISION_FEATURE_MAPPING__TRANSITIONNAME:
				return getTransitionname();
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINTRANSITION:
				return getContaintransition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.DECISION_FEATURE_MAPPING__FEATURENAME:
				setFeaturename((String)newValue);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__DECISIONNAME:
				setDecisionname((String)newValue);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__CLASSNAME:
				setClassname((String)newValue);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINCLASS:
				setContainclass((String)newValue);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__RELATIONNAME:
				setRelationname((String)newValue);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINRELATION:
				setContainrelation((String)newValue);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__STATENAME:
				setStatename((String)newValue);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINSTATE:
				setContainstate((String)newValue);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__TRANSITIONNAME:
				setTransitionname((String)newValue);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINTRANSITION:
				setContaintransition((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.DECISION_FEATURE_MAPPING__FEATURENAME:
				setFeaturename(FEATURENAME_EDEFAULT);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__DECISIONNAME:
				setDecisionname(DECISIONNAME_EDEFAULT);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__CLASSNAME:
				setClassname(CLASSNAME_EDEFAULT);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINCLASS:
				setContainclass(CONTAINCLASS_EDEFAULT);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__RELATIONNAME:
				setRelationname(RELATIONNAME_EDEFAULT);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINRELATION:
				setContainrelation(CONTAINRELATION_EDEFAULT);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__STATENAME:
				setStatename(STATENAME_EDEFAULT);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINSTATE:
				setContainstate(CONTAINSTATE_EDEFAULT);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__TRANSITIONNAME:
				setTransitionname(TRANSITIONNAME_EDEFAULT);
				return;
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINTRANSITION:
				setContaintransition(CONTAINTRANSITION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.DECISION_FEATURE_MAPPING__FEATURENAME:
				return FEATURENAME_EDEFAULT == null ? featurename != null : !FEATURENAME_EDEFAULT.equals(featurename);
			case SplPackage.DECISION_FEATURE_MAPPING__DECISIONNAME:
				return DECISIONNAME_EDEFAULT == null ? decisionname != null : !DECISIONNAME_EDEFAULT.equals(decisionname);
			case SplPackage.DECISION_FEATURE_MAPPING__CLASSNAME:
				return CLASSNAME_EDEFAULT == null ? classname != null : !CLASSNAME_EDEFAULT.equals(classname);
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINCLASS:
				return CONTAINCLASS_EDEFAULT == null ? containclass != null : !CONTAINCLASS_EDEFAULT.equals(containclass);
			case SplPackage.DECISION_FEATURE_MAPPING__RELATIONNAME:
				return RELATIONNAME_EDEFAULT == null ? relationname != null : !RELATIONNAME_EDEFAULT.equals(relationname);
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINRELATION:
				return CONTAINRELATION_EDEFAULT == null ? containrelation != null : !CONTAINRELATION_EDEFAULT.equals(containrelation);
			case SplPackage.DECISION_FEATURE_MAPPING__STATENAME:
				return STATENAME_EDEFAULT == null ? statename != null : !STATENAME_EDEFAULT.equals(statename);
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINSTATE:
				return CONTAINSTATE_EDEFAULT == null ? containstate != null : !CONTAINSTATE_EDEFAULT.equals(containstate);
			case SplPackage.DECISION_FEATURE_MAPPING__TRANSITIONNAME:
				return TRANSITIONNAME_EDEFAULT == null ? transitionname != null : !TRANSITIONNAME_EDEFAULT.equals(transitionname);
			case SplPackage.DECISION_FEATURE_MAPPING__CONTAINTRANSITION:
				return CONTAINTRANSITION_EDEFAULT == null ? containtransition != null : !CONTAINTRANSITION_EDEFAULT.equals(containtransition);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (featurename: ");
		result.append(featurename);
		result.append(", decisionname: ");
		result.append(decisionname);
		result.append(", classname: ");
		result.append(classname);
		result.append(", Containclass: ");
		result.append(containclass);
		result.append(", relationname: ");
		result.append(relationname);
		result.append(", Containrelation: ");
		result.append(containrelation);
		result.append(", statename: ");
		result.append(statename);
		result.append(", Containstate: ");
		result.append(containstate);
		result.append(", transitionname: ");
		result.append(transitionname);
		result.append(", Containtransition: ");
		result.append(containtransition);
		result.append(')');
		return result.toString();
	}

} //DecisionFeatureMappingImpl
