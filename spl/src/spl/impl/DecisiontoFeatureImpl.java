/**
 */
package spl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import spl.DecisiontoFeature;
import spl.SplPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decisionto Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.DecisiontoFeatureImpl#getDecisionname <em>Decisionname</em>}</li>
 *   <li>{@link spl.impl.DecisiontoFeatureImpl#getFeaturename <em>Featurename</em>}</li>
 *   <li>{@link spl.impl.DecisiontoFeatureImpl#getContainfeature <em>Containfeature</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DecisiontoFeatureImpl extends EObjectImpl implements DecisiontoFeature {
	/**
	 * The default value of the '{@link #getDecisionname() <em>Decisionname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDecisionname()
	 * @generated
	 * @ordered
	 */
	protected static final String DECISIONNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDecisionname() <em>Decisionname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDecisionname()
	 * @generated
	 * @ordered
	 */
	protected String decisionname = DECISIONNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getFeaturename() <em>Featurename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeaturename()
	 * @generated
	 * @ordered
	 */
	protected static final String FEATURENAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFeaturename() <em>Featurename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeaturename()
	 * @generated
	 * @ordered
	 */
	protected String featurename = FEATURENAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContainfeature() <em>Containfeature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainfeature()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTAINFEATURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContainfeature() <em>Containfeature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainfeature()
	 * @generated
	 * @ordered
	 */
	protected String containfeature = CONTAINFEATURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecisiontoFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.DECISIONTO_FEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDecisionname() {
		return decisionname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDecisionname(String newDecisionname) {
		String oldDecisionname = decisionname;
		decisionname = newDecisionname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISIONTO_FEATURE__DECISIONNAME, oldDecisionname, decisionname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFeaturename() {
		return featurename;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeaturename(String newFeaturename) {
		String oldFeaturename = featurename;
		featurename = newFeaturename;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISIONTO_FEATURE__FEATURENAME, oldFeaturename, featurename));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContainfeature() {
		return containfeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainfeature(String newContainfeature) {
		String oldContainfeature = containfeature;
		containfeature = newContainfeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.DECISIONTO_FEATURE__CONTAINFEATURE, oldContainfeature, containfeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.DECISIONTO_FEATURE__DECISIONNAME:
				return getDecisionname();
			case SplPackage.DECISIONTO_FEATURE__FEATURENAME:
				return getFeaturename();
			case SplPackage.DECISIONTO_FEATURE__CONTAINFEATURE:
				return getContainfeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.DECISIONTO_FEATURE__DECISIONNAME:
				setDecisionname((String)newValue);
				return;
			case SplPackage.DECISIONTO_FEATURE__FEATURENAME:
				setFeaturename((String)newValue);
				return;
			case SplPackage.DECISIONTO_FEATURE__CONTAINFEATURE:
				setContainfeature((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.DECISIONTO_FEATURE__DECISIONNAME:
				setDecisionname(DECISIONNAME_EDEFAULT);
				return;
			case SplPackage.DECISIONTO_FEATURE__FEATURENAME:
				setFeaturename(FEATURENAME_EDEFAULT);
				return;
			case SplPackage.DECISIONTO_FEATURE__CONTAINFEATURE:
				setContainfeature(CONTAINFEATURE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.DECISIONTO_FEATURE__DECISIONNAME:
				return DECISIONNAME_EDEFAULT == null ? decisionname != null : !DECISIONNAME_EDEFAULT.equals(decisionname);
			case SplPackage.DECISIONTO_FEATURE__FEATURENAME:
				return FEATURENAME_EDEFAULT == null ? featurename != null : !FEATURENAME_EDEFAULT.equals(featurename);
			case SplPackage.DECISIONTO_FEATURE__CONTAINFEATURE:
				return CONTAINFEATURE_EDEFAULT == null ? containfeature != null : !CONTAINFEATURE_EDEFAULT.equals(containfeature);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (decisionname: ");
		result.append(decisionname);
		result.append(", featurename: ");
		result.append(featurename);
		result.append(", containfeature: ");
		result.append(containfeature);
		result.append(')');
		return result.toString();
	}

} //DecisiontoFeatureImpl
