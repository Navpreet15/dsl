/**
 */
package spl.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import spl.CMconstraints;
import spl.Choice;
import spl.ChoiceModel;
import spl.ClassDiagram;
import spl.DecMapping;
import spl.DecisionFeatureMapping;
import spl.DecisionMapping;
import spl.DecisiontoFeature;
import spl.DesignChoices;
import spl.DomainModel;
import spl.Entry;
import spl.FMconstraints;
import spl.Feature;
import spl.FeatureMapping;
import spl.FeatureModel;
import spl.Guard;
import spl.Interfc;
import spl.Label;
import spl.Map;
import spl.MapToEntity;
import spl.Mapdec;
import spl.Mapto;
import spl.Operator;
import spl.Presence;
import spl.Property;
import spl.Relationship;
import spl.SPL;
import spl.SplFactory;
import spl.SplPackage;
import spl.State;
import spl.StateChart;
import spl.Transition;
import spl.TransitionalBehavior;
import spl.Trigger;
import spl.VarType;
import spl.cdwellformedness;
import spl.featMapping;
import spl.scwellformedness;
import spl.variabls;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SplFactoryImpl extends EFactoryImpl implements SplFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SplFactory init() {
		try {
			SplFactory theSplFactory = (SplFactory)EPackage.Registry.INSTANCE.getEFactory(SplPackage.eNS_URI);
			if (theSplFactory != null) {
				return theSplFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SplFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SplFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SplPackage.DESIGN_CHOICES: return createDesignChoices();
			case SplPackage.VARIABLS: return createvariabls();
			case SplPackage.LABEL: return createLabel();
			case SplPackage.TRIGGER: return createTrigger();
			case SplPackage.ENTRY: return createEntry();
			case SplPackage.INTERFC: return createInterfc();
			case SplPackage.TRANSITIONAL_BEHAVIOR: return createTransitionalBehavior();
			case SplPackage.GUARD: return createGuard();
			case SplPackage.CHOICE_MODEL: return createChoiceModel();
			case SplPackage.CHOICE: return createChoice();
			case SplPackage.FEATURE_MODEL: return createFeatureModel();
			case SplPackage.FEATURE: return createFeature();
			case SplPackage.SCWELLFORMEDNESS: return createscwellformedness();
			case SplPackage.CDWELLFORMEDNESS: return createcdwellformedness();
			case SplPackage.FMCONSTRAINTS: return createFMconstraints();
			case SplPackage.CMCONSTRAINTS: return createCMconstraints();
			case SplPackage.STATE_CHART: return createStateChart();
			case SplPackage.CLASS_DIAGRAM: return createClassDiagram();
			case SplPackage.STATE: return createState();
			case SplPackage.TRANSITION: return createTransition();
			case SplPackage.CLASS: return createClass();
			case SplPackage.RELATIONSHIP: return createRelationship();
			case SplPackage.FEATURE_MAPPING: return createFeatureMapping();
			case SplPackage.MAP: return createMap();
			case SplPackage.MAPTO: return createMapto();
			case SplPackage.FEAT_MAPPING: return createfeatMapping();
			case SplPackage.MAPDEC: return createMapdec();
			case SplPackage.MAP_TO_ENTITY: return createMapToEntity();
			case SplPackage.DEC_MAPPING: return createDecMapping();
			case SplPackage.DECISION_MAPPING: return createDecisionMapping();
			case SplPackage.PROPERTY: return createProperty();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case SplPackage.VAR_TYPE:
				return createVarTypeFromString(eDataType, initialValue);
			case SplPackage.OPERATOR:
				return createOperatorFromString(eDataType, initialValue);
			case SplPackage.PRESENCE:
				return createPresenceFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case SplPackage.VAR_TYPE:
				return convertVarTypeToString(eDataType, instanceValue);
			case SplPackage.OPERATOR:
				return convertOperatorToString(eDataType, instanceValue);
			case SplPackage.PRESENCE:
				return convertPresenceToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignChoices createDesignChoices() {
		DesignChoicesImpl designChoices = new DesignChoicesImpl();
		return designChoices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public variabls createvariabls() {
		variablsImpl variabls = new variablsImpl();
		return variabls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Label createLabel() {
		LabelImpl label = new LabelImpl();
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Trigger createTrigger() {
		TriggerImpl trigger = new TriggerImpl();
		return trigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Entry createEntry() {
		EntryImpl entry = new EntryImpl();
		return entry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interfc createInterfc() {
		InterfcImpl interfc = new InterfcImpl();
		return interfc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionalBehavior createTransitionalBehavior() {
		TransitionalBehaviorImpl transitionalBehavior = new TransitionalBehaviorImpl();
		return transitionalBehavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Guard createGuard() {
		GuardImpl guard = new GuardImpl();
		return guard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoiceModel createChoiceModel() {
		ChoiceModelImpl choiceModel = new ChoiceModelImpl();
		return choiceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Choice createChoice() {
		ChoiceImpl choice = new ChoiceImpl();
		return choice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModel createFeatureModel() {
		FeatureModelImpl featureModel = new FeatureModelImpl();
		return featureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature createFeature() {
		FeatureImpl feature = new FeatureImpl();
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public scwellformedness createscwellformedness() {
		scwellformednessImpl scwellformedness = new scwellformednessImpl();
		return scwellformedness;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public cdwellformedness createcdwellformedness() {
		cdwellformednessImpl cdwellformedness = new cdwellformednessImpl();
		return cdwellformedness;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FMconstraints createFMconstraints() {
		FMconstraintsImpl fMconstraints = new FMconstraintsImpl();
		return fMconstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CMconstraints createCMconstraints() {
		CMconstraintsImpl cMconstraints = new CMconstraintsImpl();
		return cMconstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateChart createStateChart() {
		StateChartImpl stateChart = new StateChartImpl();
		return stateChart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassDiagram createClassDiagram() {
		ClassDiagramImpl classDiagram = new ClassDiagramImpl();
		return classDiagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State createState() {
		StateImpl state = new StateImpl();
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition createTransition() {
		TransitionImpl transition = new TransitionImpl();
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public spl.Class createClass() {
		ClassImpl class_ = new ClassImpl();
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Relationship createRelationship() {
		RelationshipImpl relationship = new RelationshipImpl();
		return relationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMapping createFeatureMapping() {
		FeatureMappingImpl featureMapping = new FeatureMappingImpl();
		return featureMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map createMap() {
		MapImpl map = new MapImpl();
		return map;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mapto createMapto() {
		MaptoImpl mapto = new MaptoImpl();
		return mapto;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public featMapping createfeatMapping() {
		featMappingImpl featMapping = new featMappingImpl();
		return featMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mapdec createMapdec() {
		MapdecImpl mapdec = new MapdecImpl();
		return mapdec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MapToEntity createMapToEntity() {
		MapToEntityImpl mapToEntity = new MapToEntityImpl();
		return mapToEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecMapping createDecMapping() {
		DecMappingImpl decMapping = new DecMappingImpl();
		return decMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecisionMapping createDecisionMapping() {
		DecisionMappingImpl decisionMapping = new DecisionMappingImpl();
		return decisionMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property createProperty() {
		PropertyImpl property = new PropertyImpl();
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VarType createVarTypeFromString(EDataType eDataType, String initialValue) {
		VarType result = VarType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVarTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operator createOperatorFromString(EDataType eDataType, String initialValue) {
		Operator result = Operator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Presence createPresenceFromString(EDataType eDataType, String initialValue) {
		Presence result = Presence.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPresenceToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SplPackage getSplPackage() {
		return (SplPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SplPackage getPackage() {
		return SplPackage.eINSTANCE;
	}

} //SplFactoryImpl
