/**
 */
package spl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import spl.SplPackage;
import spl.State;
import spl.Transition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.TransitionImpl#getName <em>Name</em>}</li>
 *   <li>{@link spl.impl.TransitionImpl#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link spl.impl.TransitionImpl#getGrd <em>Grd</em>}</li>
 *   <li>{@link spl.impl.TransitionImpl#getTransitionbehavior <em>Transitionbehavior</em>}</li>
 *   <li>{@link spl.impl.TransitionImpl#getAlwaysPresent <em>Always Present</em>}</li>
 *   <li>{@link spl.impl.TransitionImpl#getTostate <em>Tostate</em>}</li>
 *   <li>{@link spl.impl.TransitionImpl#getFromstate <em>Fromstate</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransitionImpl extends EObjectImpl implements Transition {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getTrigger() <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrigger()
	 * @generated
	 * @ordered
	 */
	protected static final String TRIGGER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTrigger() <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrigger()
	 * @generated
	 * @ordered
	 */
	protected String trigger = TRIGGER_EDEFAULT;

	/**
	 * The default value of the '{@link #getGrd() <em>Grd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGrd()
	 * @generated
	 * @ordered
	 */
	protected static final String GRD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGrd() <em>Grd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGrd()
	 * @generated
	 * @ordered
	 */
	protected String grd = GRD_EDEFAULT;

	/**
	 * The default value of the '{@link #getTransitionbehavior() <em>Transitionbehavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitionbehavior()
	 * @generated
	 * @ordered
	 */
	protected static final String TRANSITIONBEHAVIOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTransitionbehavior() <em>Transitionbehavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitionbehavior()
	 * @generated
	 * @ordered
	 */
	protected String transitionbehavior = TRANSITIONBEHAVIOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getAlwaysPresent() <em>Always Present</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlwaysPresent()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ALWAYS_PRESENT_EDEFAULT = Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getAlwaysPresent() <em>Always Present</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlwaysPresent()
	 * @generated
	 * @ordered
	 */
	protected Boolean alwaysPresent = ALWAYS_PRESENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTostate() <em>Tostate</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTostate()
	 * @generated
	 * @ordered
	 */
	protected State tostate;

	/**
	 * The cached value of the '{@link #getFromstate() <em>Fromstate</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromstate()
	 * @generated
	 * @ordered
	 */
	protected State fromstate;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.TRANSITION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTrigger() {
		return trigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTrigger(String newTrigger) {
		String oldTrigger = trigger;
		trigger = newTrigger;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.TRANSITION__TRIGGER, oldTrigger, trigger));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGrd() {
		return grd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGrd(String newGrd) {
		String oldGrd = grd;
		grd = newGrd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.TRANSITION__GRD, oldGrd, grd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTransitionbehavior() {
		return transitionbehavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransitionbehavior(String newTransitionbehavior) {
		String oldTransitionbehavior = transitionbehavior;
		transitionbehavior = newTransitionbehavior;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.TRANSITION__TRANSITIONBEHAVIOR, oldTransitionbehavior, transitionbehavior));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAlwaysPresent() {
		return alwaysPresent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlwaysPresent(Boolean newAlwaysPresent) {
		Boolean oldAlwaysPresent = alwaysPresent;
		alwaysPresent = newAlwaysPresent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.TRANSITION__ALWAYS_PRESENT, oldAlwaysPresent, alwaysPresent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getTostate() {
		if (tostate != null && tostate.eIsProxy()) {
			InternalEObject oldTostate = (InternalEObject)tostate;
			tostate = (State)eResolveProxy(oldTostate);
			if (tostate != oldTostate) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SplPackage.TRANSITION__TOSTATE, oldTostate, tostate));
			}
		}
		return tostate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetTostate() {
		return tostate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTostate(State newTostate) {
		State oldTostate = tostate;
		tostate = newTostate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.TRANSITION__TOSTATE, oldTostate, tostate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getFromstate() {
		if (fromstate != null && fromstate.eIsProxy()) {
			InternalEObject oldFromstate = (InternalEObject)fromstate;
			fromstate = (State)eResolveProxy(oldFromstate);
			if (fromstate != oldFromstate) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SplPackage.TRANSITION__FROMSTATE, oldFromstate, fromstate));
			}
		}
		return fromstate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetFromstate() {
		return fromstate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromstate(State newFromstate) {
		State oldFromstate = fromstate;
		fromstate = newFromstate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.TRANSITION__FROMSTATE, oldFromstate, fromstate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.TRANSITION__NAME:
				return getName();
			case SplPackage.TRANSITION__TRIGGER:
				return getTrigger();
			case SplPackage.TRANSITION__GRD:
				return getGrd();
			case SplPackage.TRANSITION__TRANSITIONBEHAVIOR:
				return getTransitionbehavior();
			case SplPackage.TRANSITION__ALWAYS_PRESENT:
				return getAlwaysPresent();
			case SplPackage.TRANSITION__TOSTATE:
				if (resolve) return getTostate();
				return basicGetTostate();
			case SplPackage.TRANSITION__FROMSTATE:
				if (resolve) return getFromstate();
				return basicGetFromstate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.TRANSITION__NAME:
				setName((String)newValue);
				return;
			case SplPackage.TRANSITION__TRIGGER:
				setTrigger((String)newValue);
				return;
			case SplPackage.TRANSITION__GRD:
				setGrd((String)newValue);
				return;
			case SplPackage.TRANSITION__TRANSITIONBEHAVIOR:
				setTransitionbehavior((String)newValue);
				return;
			case SplPackage.TRANSITION__ALWAYS_PRESENT:
				setAlwaysPresent((Boolean)newValue);
				return;
			case SplPackage.TRANSITION__TOSTATE:
				setTostate((State)newValue);
				return;
			case SplPackage.TRANSITION__FROMSTATE:
				setFromstate((State)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.TRANSITION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case SplPackage.TRANSITION__TRIGGER:
				setTrigger(TRIGGER_EDEFAULT);
				return;
			case SplPackage.TRANSITION__GRD:
				setGrd(GRD_EDEFAULT);
				return;
			case SplPackage.TRANSITION__TRANSITIONBEHAVIOR:
				setTransitionbehavior(TRANSITIONBEHAVIOR_EDEFAULT);
				return;
			case SplPackage.TRANSITION__ALWAYS_PRESENT:
				setAlwaysPresent(ALWAYS_PRESENT_EDEFAULT);
				return;
			case SplPackage.TRANSITION__TOSTATE:
				setTostate((State)null);
				return;
			case SplPackage.TRANSITION__FROMSTATE:
				setFromstate((State)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.TRANSITION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case SplPackage.TRANSITION__TRIGGER:
				return TRIGGER_EDEFAULT == null ? trigger != null : !TRIGGER_EDEFAULT.equals(trigger);
			case SplPackage.TRANSITION__GRD:
				return GRD_EDEFAULT == null ? grd != null : !GRD_EDEFAULT.equals(grd);
			case SplPackage.TRANSITION__TRANSITIONBEHAVIOR:
				return TRANSITIONBEHAVIOR_EDEFAULT == null ? transitionbehavior != null : !TRANSITIONBEHAVIOR_EDEFAULT.equals(transitionbehavior);
			case SplPackage.TRANSITION__ALWAYS_PRESENT:
				return ALWAYS_PRESENT_EDEFAULT == null ? alwaysPresent != null : !ALWAYS_PRESENT_EDEFAULT.equals(alwaysPresent);
			case SplPackage.TRANSITION__TOSTATE:
				return tostate != null;
			case SplPackage.TRANSITION__FROMSTATE:
				return fromstate != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", trigger: ");
		result.append(trigger);
		result.append(", grd: ");
		result.append(grd);
		result.append(", transitionbehavior: ");
		result.append(transitionbehavior);
		result.append(", AlwaysPresent: ");
		result.append(alwaysPresent);
		result.append(')');
		return result.toString();
	}

} //TransitionImpl
