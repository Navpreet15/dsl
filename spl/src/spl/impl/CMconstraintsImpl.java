/**
 */
package spl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import spl.CMconstraints;
import spl.Choice;
import spl.Operator;
import spl.SplPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CMconstraints</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.CMconstraintsImpl#getOpr <em>Opr</em>}</li>
 *   <li>{@link spl.impl.CMconstraintsImpl#getOperand <em>Operand</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CMconstraintsImpl extends EObjectImpl implements CMconstraints {
	/**
	 * The default value of the '{@link #getOpr() <em>Opr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpr()
	 * @generated
	 * @ordered
	 */
	protected static final Operator OPR_EDEFAULT = Operator.AND;

	/**
	 * The cached value of the '{@link #getOpr() <em>Opr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpr()
	 * @generated
	 * @ordered
	 */
	protected Operator opr = OPR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOperand() <em>Operand</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperand()
	 * @generated
	 * @ordered
	 */
	protected EList<Choice> operand;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CMconstraintsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.CMCONSTRAINTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operator getOpr() {
		return opr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpr(Operator newOpr) {
		Operator oldOpr = opr;
		opr = newOpr == null ? OPR_EDEFAULT : newOpr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.CMCONSTRAINTS__OPR, oldOpr, opr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Choice> getOperand() {
		if (operand == null) {
			operand = new EObjectResolvingEList<Choice>(Choice.class, this, SplPackage.CMCONSTRAINTS__OPERAND);
		}
		return operand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.CMCONSTRAINTS__OPR:
				return getOpr();
			case SplPackage.CMCONSTRAINTS__OPERAND:
				return getOperand();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.CMCONSTRAINTS__OPR:
				setOpr((Operator)newValue);
				return;
			case SplPackage.CMCONSTRAINTS__OPERAND:
				getOperand().clear();
				getOperand().addAll((Collection<? extends Choice>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.CMCONSTRAINTS__OPR:
				setOpr(OPR_EDEFAULT);
				return;
			case SplPackage.CMCONSTRAINTS__OPERAND:
				getOperand().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.CMCONSTRAINTS__OPR:
				return opr != OPR_EDEFAULT;
			case SplPackage.CMCONSTRAINTS__OPERAND:
				return operand != null && !operand.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (opr: ");
		result.append(opr);
		result.append(')');
		return result.toString();
	}

} //CMconstraintsImpl
