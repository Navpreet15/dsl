/**
 */
package spl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import spl.Relationship;
import spl.SplPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Relationship</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.RelationshipImpl#getAlwaysPresent <em>Always Present</em>}</li>
 *   <li>{@link spl.impl.RelationshipImpl#getName <em>Name</em>}</li>
 *   <li>{@link spl.impl.RelationshipImpl#getLab <em>Lab</em>}</li>
 *   <li>{@link spl.impl.RelationshipImpl#getInMul <em>In Mul</em>}</li>
 *   <li>{@link spl.impl.RelationshipImpl#getOutMul <em>Out Mul</em>}</li>
 *   <li>{@link spl.impl.RelationshipImpl#getFrom <em>From</em>}</li>
 *   <li>{@link spl.impl.RelationshipImpl#getTo <em>To</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RelationshipImpl extends EObjectImpl implements Relationship {
	/**
	 * The default value of the '{@link #getAlwaysPresent() <em>Always Present</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlwaysPresent()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ALWAYS_PRESENT_EDEFAULT = Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getAlwaysPresent() <em>Always Present</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlwaysPresent()
	 * @generated
	 * @ordered
	 */
	protected Boolean alwaysPresent = ALWAYS_PRESENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLab() <em>Lab</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLab()
	 * @generated
	 * @ordered
	 */
	protected static final String LAB_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLab() <em>Lab</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLab()
	 * @generated
	 * @ordered
	 */
	protected String lab = LAB_EDEFAULT;

	/**
	 * The default value of the '{@link #getInMul() <em>In Mul</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInMul()
	 * @generated
	 * @ordered
	 */
	protected static final int IN_MUL_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getInMul() <em>In Mul</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInMul()
	 * @generated
	 * @ordered
	 */
	protected int inMul = IN_MUL_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutMul() <em>Out Mul</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutMul()
	 * @generated
	 * @ordered
	 */
	protected static final int OUT_MUL_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getOutMul() <em>Out Mul</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutMul()
	 * @generated
	 * @ordered
	 */
	protected int outMul = OUT_MUL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFrom() <em>From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrom()
	 * @generated
	 * @ordered
	 */
	protected spl.Class from;

	/**
	 * The cached value of the '{@link #getTo() <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTo()
	 * @generated
	 * @ordered
	 */
	protected spl.Class to;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RelationshipImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.RELATIONSHIP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAlwaysPresent() {
		return alwaysPresent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlwaysPresent(Boolean newAlwaysPresent) {
		Boolean oldAlwaysPresent = alwaysPresent;
		alwaysPresent = newAlwaysPresent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.RELATIONSHIP__ALWAYS_PRESENT, oldAlwaysPresent, alwaysPresent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.RELATIONSHIP__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLab() {
		return lab;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLab(String newLab) {
		String oldLab = lab;
		lab = newLab;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.RELATIONSHIP__LAB, oldLab, lab));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInMul() {
		return inMul;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInMul(int newInMul) {
		int oldInMul = inMul;
		inMul = newInMul;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.RELATIONSHIP__IN_MUL, oldInMul, inMul));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOutMul() {
		return outMul;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutMul(int newOutMul) {
		int oldOutMul = outMul;
		outMul = newOutMul;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.RELATIONSHIP__OUT_MUL, oldOutMul, outMul));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public spl.Class getFrom() {
		if (from != null && from.eIsProxy()) {
			InternalEObject oldFrom = (InternalEObject)from;
			from = (spl.Class)eResolveProxy(oldFrom);
			if (from != oldFrom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SplPackage.RELATIONSHIP__FROM, oldFrom, from));
			}
		}
		return from;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public spl.Class basicGetFrom() {
		return from;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrom(spl.Class newFrom) {
		spl.Class oldFrom = from;
		from = newFrom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.RELATIONSHIP__FROM, oldFrom, from));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public spl.Class getTo() {
		if (to != null && to.eIsProxy()) {
			InternalEObject oldTo = (InternalEObject)to;
			to = (spl.Class)eResolveProxy(oldTo);
			if (to != oldTo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SplPackage.RELATIONSHIP__TO, oldTo, to));
			}
		}
		return to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public spl.Class basicGetTo() {
		return to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTo(spl.Class newTo) {
		spl.Class oldTo = to;
		to = newTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.RELATIONSHIP__TO, oldTo, to));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.RELATIONSHIP__ALWAYS_PRESENT:
				return getAlwaysPresent();
			case SplPackage.RELATIONSHIP__NAME:
				return getName();
			case SplPackage.RELATIONSHIP__LAB:
				return getLab();
			case SplPackage.RELATIONSHIP__IN_MUL:
				return getInMul();
			case SplPackage.RELATIONSHIP__OUT_MUL:
				return getOutMul();
			case SplPackage.RELATIONSHIP__FROM:
				if (resolve) return getFrom();
				return basicGetFrom();
			case SplPackage.RELATIONSHIP__TO:
				if (resolve) return getTo();
				return basicGetTo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.RELATIONSHIP__ALWAYS_PRESENT:
				setAlwaysPresent((Boolean)newValue);
				return;
			case SplPackage.RELATIONSHIP__NAME:
				setName((String)newValue);
				return;
			case SplPackage.RELATIONSHIP__LAB:
				setLab((String)newValue);
				return;
			case SplPackage.RELATIONSHIP__IN_MUL:
				setInMul((Integer)newValue);
				return;
			case SplPackage.RELATIONSHIP__OUT_MUL:
				setOutMul((Integer)newValue);
				return;
			case SplPackage.RELATIONSHIP__FROM:
				setFrom((spl.Class)newValue);
				return;
			case SplPackage.RELATIONSHIP__TO:
				setTo((spl.Class)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.RELATIONSHIP__ALWAYS_PRESENT:
				setAlwaysPresent(ALWAYS_PRESENT_EDEFAULT);
				return;
			case SplPackage.RELATIONSHIP__NAME:
				setName(NAME_EDEFAULT);
				return;
			case SplPackage.RELATIONSHIP__LAB:
				setLab(LAB_EDEFAULT);
				return;
			case SplPackage.RELATIONSHIP__IN_MUL:
				setInMul(IN_MUL_EDEFAULT);
				return;
			case SplPackage.RELATIONSHIP__OUT_MUL:
				setOutMul(OUT_MUL_EDEFAULT);
				return;
			case SplPackage.RELATIONSHIP__FROM:
				setFrom((spl.Class)null);
				return;
			case SplPackage.RELATIONSHIP__TO:
				setTo((spl.Class)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.RELATIONSHIP__ALWAYS_PRESENT:
				return ALWAYS_PRESENT_EDEFAULT == null ? alwaysPresent != null : !ALWAYS_PRESENT_EDEFAULT.equals(alwaysPresent);
			case SplPackage.RELATIONSHIP__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case SplPackage.RELATIONSHIP__LAB:
				return LAB_EDEFAULT == null ? lab != null : !LAB_EDEFAULT.equals(lab);
			case SplPackage.RELATIONSHIP__IN_MUL:
				return inMul != IN_MUL_EDEFAULT;
			case SplPackage.RELATIONSHIP__OUT_MUL:
				return outMul != OUT_MUL_EDEFAULT;
			case SplPackage.RELATIONSHIP__FROM:
				return from != null;
			case SplPackage.RELATIONSHIP__TO:
				return to != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (AlwaysPresent: ");
		result.append(alwaysPresent);
		result.append(", name: ");
		result.append(name);
		result.append(", lab: ");
		result.append(lab);
		result.append(", inMul: ");
		result.append(inMul);
		result.append(", outMul: ");
		result.append(outMul);
		result.append(')');
		return result.toString();
	}

} //RelationshipImpl
