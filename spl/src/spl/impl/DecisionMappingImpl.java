/**
 */
package spl.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import spl.DecMapping;
import spl.DecisionMapping;
import spl.SplPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decision Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.DecisionMappingImpl#getDecisionMap <em>Decision Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DecisionMappingImpl extends EObjectImpl implements DecisionMapping {
	/**
	 * The cached value of the '{@link #getDecisionMap() <em>Decision Map</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDecisionMap()
	 * @generated
	 * @ordered
	 */
	protected EList<DecMapping> decisionMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecisionMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.DECISION_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DecMapping> getDecisionMap() {
		if (decisionMap == null) {
			decisionMap = new EObjectContainmentEList<DecMapping>(DecMapping.class, this, SplPackage.DECISION_MAPPING__DECISION_MAP);
		}
		return decisionMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SplPackage.DECISION_MAPPING__DECISION_MAP:
				return ((InternalEList<?>)getDecisionMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.DECISION_MAPPING__DECISION_MAP:
				return getDecisionMap();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.DECISION_MAPPING__DECISION_MAP:
				getDecisionMap().clear();
				getDecisionMap().addAll((Collection<? extends DecMapping>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.DECISION_MAPPING__DECISION_MAP:
				getDecisionMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.DECISION_MAPPING__DECISION_MAP:
				return decisionMap != null && !decisionMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DecisionMappingImpl
