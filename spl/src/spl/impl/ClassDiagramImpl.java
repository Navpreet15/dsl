/**
 */
package spl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import spl.ClassDiagram;
import spl.Relationship;
import spl.SplPackage;
import spl.cdwellformedness;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Diagram</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.ClassDiagramImpl#getRelationship <em>Relationship</em>}</li>
 *   <li>{@link spl.impl.ClassDiagramImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link spl.impl.ClassDiagramImpl#getWf <em>Wf</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClassDiagramImpl extends EObjectImpl implements ClassDiagram {
	/**
	 * The cached value of the '{@link #getRelationship() <em>Relationship</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationship()
	 * @generated
	 * @ordered
	 */
	protected EList<Relationship> relationship;

	/**
	 * The cached value of the '{@link #getClass_() <em>Class</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClass_()
	 * @generated
	 * @ordered
	 */
	protected EList<spl.Class> class_;

	/**
	 * The cached value of the '{@link #getWf() <em>Wf</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWf()
	 * @generated
	 * @ordered
	 */
	protected EList<cdwellformedness> wf;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassDiagramImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.CLASS_DIAGRAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Relationship> getRelationship() {
		if (relationship == null) {
			relationship = new EObjectContainmentEList<Relationship>(Relationship.class, this, SplPackage.CLASS_DIAGRAM__RELATIONSHIP);
		}
		return relationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<spl.Class> getClass_() {
		if (class_ == null) {
			class_ = new EObjectContainmentEList<spl.Class>(spl.Class.class, this, SplPackage.CLASS_DIAGRAM__CLASS);
		}
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<cdwellformedness> getWf() {
		if (wf == null) {
			wf = new EObjectContainmentEList<cdwellformedness>(cdwellformedness.class, this, SplPackage.CLASS_DIAGRAM__WF);
		}
		return wf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SplPackage.CLASS_DIAGRAM__RELATIONSHIP:
				return ((InternalEList<?>)getRelationship()).basicRemove(otherEnd, msgs);
			case SplPackage.CLASS_DIAGRAM__CLASS:
				return ((InternalEList<?>)getClass_()).basicRemove(otherEnd, msgs);
			case SplPackage.CLASS_DIAGRAM__WF:
				return ((InternalEList<?>)getWf()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.CLASS_DIAGRAM__RELATIONSHIP:
				return getRelationship();
			case SplPackage.CLASS_DIAGRAM__CLASS:
				return getClass_();
			case SplPackage.CLASS_DIAGRAM__WF:
				return getWf();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.CLASS_DIAGRAM__RELATIONSHIP:
				getRelationship().clear();
				getRelationship().addAll((Collection<? extends Relationship>)newValue);
				return;
			case SplPackage.CLASS_DIAGRAM__CLASS:
				getClass_().clear();
				getClass_().addAll((Collection<? extends spl.Class>)newValue);
				return;
			case SplPackage.CLASS_DIAGRAM__WF:
				getWf().clear();
				getWf().addAll((Collection<? extends cdwellformedness>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.CLASS_DIAGRAM__RELATIONSHIP:
				getRelationship().clear();
				return;
			case SplPackage.CLASS_DIAGRAM__CLASS:
				getClass_().clear();
				return;
			case SplPackage.CLASS_DIAGRAM__WF:
				getWf().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.CLASS_DIAGRAM__RELATIONSHIP:
				return relationship != null && !relationship.isEmpty();
			case SplPackage.CLASS_DIAGRAM__CLASS:
				return class_ != null && !class_.isEmpty();
			case SplPackage.CLASS_DIAGRAM__WF:
				return wf != null && !wf.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ClassDiagramImpl
