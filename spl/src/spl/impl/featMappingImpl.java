/**
 */
package spl.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import spl.Map;
import spl.Mapto;
import spl.Operator;
import spl.SplPackage;
import spl.featMapping;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>feat Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.featMappingImpl#getOprLHS <em>Opr LHS</em>}</li>
 *   <li>{@link spl.impl.featMappingImpl#getMapfrom <em>Mapfrom</em>}</li>
 *   <li>{@link spl.impl.featMappingImpl#getOprRHS <em>Opr RHS</em>}</li>
 *   <li>{@link spl.impl.featMappingImpl#getMapto <em>Mapto</em>}</li>
 * </ul>
 *
 * @generated
 */
public class featMappingImpl extends EObjectImpl implements featMapping {
	/**
	 * The default value of the '{@link #getOprLHS() <em>Opr LHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOprLHS()
	 * @generated
	 * @ordered
	 */
	protected static final Operator OPR_LHS_EDEFAULT = Operator.AND;

	/**
	 * The cached value of the '{@link #getOprLHS() <em>Opr LHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOprLHS()
	 * @generated
	 * @ordered
	 */
	protected Operator oprLHS = OPR_LHS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMapfrom() <em>Mapfrom</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMapfrom()
	 * @generated
	 * @ordered
	 */
	protected EList<Map> mapfrom;

	/**
	 * The default value of the '{@link #getOprRHS() <em>Opr RHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOprRHS()
	 * @generated
	 * @ordered
	 */
	protected static final Operator OPR_RHS_EDEFAULT = Operator.AND;

	/**
	 * The cached value of the '{@link #getOprRHS() <em>Opr RHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOprRHS()
	 * @generated
	 * @ordered
	 */
	protected Operator oprRHS = OPR_RHS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMapto() <em>Mapto</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMapto()
	 * @generated
	 * @ordered
	 */
	protected EList<Mapto> mapto;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected featMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.FEAT_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operator getOprLHS() {
		return oprLHS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOprLHS(Operator newOprLHS) {
		Operator oldOprLHS = oprLHS;
		oprLHS = newOprLHS == null ? OPR_LHS_EDEFAULT : newOprLHS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.FEAT_MAPPING__OPR_LHS, oldOprLHS, oprLHS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Map> getMapfrom() {
		if (mapfrom == null) {
			mapfrom = new EObjectContainmentEList<Map>(Map.class, this, SplPackage.FEAT_MAPPING__MAPFROM);
		}
		return mapfrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operator getOprRHS() {
		return oprRHS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOprRHS(Operator newOprRHS) {
		Operator oldOprRHS = oprRHS;
		oprRHS = newOprRHS == null ? OPR_RHS_EDEFAULT : newOprRHS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.FEAT_MAPPING__OPR_RHS, oldOprRHS, oprRHS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Mapto> getMapto() {
		if (mapto == null) {
			mapto = new EObjectContainmentEList<Mapto>(Mapto.class, this, SplPackage.FEAT_MAPPING__MAPTO);
		}
		return mapto;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SplPackage.FEAT_MAPPING__MAPFROM:
				return ((InternalEList<?>)getMapfrom()).basicRemove(otherEnd, msgs);
			case SplPackage.FEAT_MAPPING__MAPTO:
				return ((InternalEList<?>)getMapto()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.FEAT_MAPPING__OPR_LHS:
				return getOprLHS();
			case SplPackage.FEAT_MAPPING__MAPFROM:
				return getMapfrom();
			case SplPackage.FEAT_MAPPING__OPR_RHS:
				return getOprRHS();
			case SplPackage.FEAT_MAPPING__MAPTO:
				return getMapto();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.FEAT_MAPPING__OPR_LHS:
				setOprLHS((Operator)newValue);
				return;
			case SplPackage.FEAT_MAPPING__MAPFROM:
				getMapfrom().clear();
				getMapfrom().addAll((Collection<? extends Map>)newValue);
				return;
			case SplPackage.FEAT_MAPPING__OPR_RHS:
				setOprRHS((Operator)newValue);
				return;
			case SplPackage.FEAT_MAPPING__MAPTO:
				getMapto().clear();
				getMapto().addAll((Collection<? extends Mapto>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.FEAT_MAPPING__OPR_LHS:
				setOprLHS(OPR_LHS_EDEFAULT);
				return;
			case SplPackage.FEAT_MAPPING__MAPFROM:
				getMapfrom().clear();
				return;
			case SplPackage.FEAT_MAPPING__OPR_RHS:
				setOprRHS(OPR_RHS_EDEFAULT);
				return;
			case SplPackage.FEAT_MAPPING__MAPTO:
				getMapto().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.FEAT_MAPPING__OPR_LHS:
				return oprLHS != OPR_LHS_EDEFAULT;
			case SplPackage.FEAT_MAPPING__MAPFROM:
				return mapfrom != null && !mapfrom.isEmpty();
			case SplPackage.FEAT_MAPPING__OPR_RHS:
				return oprRHS != OPR_RHS_EDEFAULT;
			case SplPackage.FEAT_MAPPING__MAPTO:
				return mapto != null && !mapto.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (oprLHS: ");
		result.append(oprLHS);
		result.append(", oprRHS: ");
		result.append(oprRHS);
		result.append(')');
		return result.toString();
	}

} //featMappingImpl
