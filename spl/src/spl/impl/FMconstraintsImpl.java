/**
 */
package spl.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import spl.FMconstraints;
import spl.Feature;
import spl.Operator;
import spl.SplPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FMconstraints</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link spl.impl.FMconstraintsImpl#getOpr <em>Opr</em>}</li>
 *   <li>{@link spl.impl.FMconstraintsImpl#getOperand <em>Operand</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FMconstraintsImpl extends EObjectImpl implements FMconstraints {
	/**
	 * The default value of the '{@link #getOpr() <em>Opr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpr()
	 * @generated
	 * @ordered
	 */
	protected static final Operator OPR_EDEFAULT = Operator.AND;

	/**
	 * The cached value of the '{@link #getOpr() <em>Opr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpr()
	 * @generated
	 * @ordered
	 */
	protected Operator opr = OPR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOperand() <em>Operand</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperand()
	 * @generated
	 * @ordered
	 */
	protected EList<Feature> operand;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FMconstraintsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SplPackage.Literals.FMCONSTRAINTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operator getOpr() {
		return opr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpr(Operator newOpr) {
		Operator oldOpr = opr;
		opr = newOpr == null ? OPR_EDEFAULT : newOpr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SplPackage.FMCONSTRAINTS__OPR, oldOpr, opr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Feature> getOperand() {
		if (operand == null) {
			operand = new EObjectResolvingEList<Feature>(Feature.class, this, SplPackage.FMCONSTRAINTS__OPERAND);
		}
		return operand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SplPackage.FMCONSTRAINTS__OPR:
				return getOpr();
			case SplPackage.FMCONSTRAINTS__OPERAND:
				return getOperand();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SplPackage.FMCONSTRAINTS__OPR:
				setOpr((Operator)newValue);
				return;
			case SplPackage.FMCONSTRAINTS__OPERAND:
				getOperand().clear();
				getOperand().addAll((Collection<? extends Feature>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SplPackage.FMCONSTRAINTS__OPR:
				setOpr(OPR_EDEFAULT);
				return;
			case SplPackage.FMCONSTRAINTS__OPERAND:
				getOperand().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SplPackage.FMCONSTRAINTS__OPR:
				return opr != OPR_EDEFAULT;
			case SplPackage.FMCONSTRAINTS__OPERAND:
				return operand != null && !operand.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (opr: ");
		result.append(opr);
		result.append(')');
		return result.toString();
	}

} //FMconstraintsImpl
