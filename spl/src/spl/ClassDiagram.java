/**
 */
package spl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Diagram</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.ClassDiagram#getRelationship <em>Relationship</em>}</li>
 *   <li>{@link spl.ClassDiagram#getClass_ <em>Class</em>}</li>
 *   <li>{@link spl.ClassDiagram#getWf <em>Wf</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getClassDiagram()
 * @model annotation="gmf.node label.placement='none' label.text='DomainModel'"
 * @generated
 */
public interface ClassDiagram extends EObject {
	/**
	 * Returns the value of the '<em><b>Relationship</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Relationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationship</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationship</em>' containment reference list.
	 * @see spl.SplPackage#getClassDiagram_Relationship()
	 * @model containment="true"
	 * @generated
	 */
	EList<Relationship> getRelationship();

	/**
	 * Returns the value of the '<em><b>Class</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' containment reference list.
	 * @see spl.SplPackage#getClassDiagram_Class()
	 * @model containment="true"
	 * @generated
	 */
	EList<spl.Class> getClass_();

	/**
	 * Returns the value of the '<em><b>Wf</b></em>' containment reference list.
	 * The list contents are of type {@link spl.cdwellformedness}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wf</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wf</em>' containment reference list.
	 * @see spl.SplPackage#getClassDiagram_Wf()
	 * @model containment="true"
	 * @generated
	 */
	EList<cdwellformedness> getWf();

} // ClassDiagram
