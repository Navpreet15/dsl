/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Mapping State Chart</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.FeatureMappingStateChart#getName <em>Name</em>}</li>
 *   <li>{@link spl.FeatureMappingStateChart#getContainment <em>Containment</em>}</li>
 *   <li>{@link spl.FeatureMappingStateChart#getFeatMap1 <em>Feat Map1</em>}</li>
 *   <li>{@link spl.FeatureMappingStateChart#getFeatmaptrans <em>Featmaptrans</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getFeatureMappingStateChart()
 * @model annotation="gmf.node label='name'"
 * @generated
 */
public interface FeatureMappingStateChart extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see spl.SplPackage#getFeatureMappingStateChart_Name()
	 * @model id="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link spl.FeatureMappingStateChart#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containment</em>' attribute.
	 * @see #setContainment(String)
	 * @see spl.SplPackage#getFeatureMappingStateChart_Containment()
	 * @model
	 * @generated
	 */
	String getContainment();

	/**
	 * Sets the value of the '{@link spl.FeatureMappingStateChart#getContainment <em>Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containment</em>' attribute.
	 * @see #getContainment()
	 * @generated
	 */
	void setContainment(String value);

	/**
	 * Returns the value of the '<em><b>Feat Map1</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Feature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feat Map1</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feat Map1</em>' containment reference list.
	 * @see spl.SplPackage#getFeatureMappingStateChart_FeatMap1()
	 * @model containment="true"
	 * @generated
	 */
	EList<Feature> getFeatMap1();

	/**
	 * Returns the value of the '<em><b>Featmaptrans</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Featmaptrans</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Featmaptrans</em>' containment reference list.
	 * @see spl.SplPackage#getFeatureMappingStateChart_Featmaptrans()
	 * @model containment="true"
	 * @generated
	 */
	EList<Transition> getFeatmaptrans();

} // FeatureMappingStateChart
