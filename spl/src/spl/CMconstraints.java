/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CMconstraints</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.CMconstraints#getOpr <em>Opr</em>}</li>
 *   <li>{@link spl.CMconstraints#getOperand <em>Operand</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getCMconstraints()
 * @model annotation="gmf.node label='opr'"
 * @generated
 */
public interface CMconstraints extends EObject {
	/**
	 * Returns the value of the '<em><b>Opr</b></em>' attribute.
	 * The literals are from the enumeration {@link spl.Operator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opr</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opr</em>' attribute.
	 * @see spl.Operator
	 * @see #setOpr(Operator)
	 * @see spl.SplPackage#getCMconstraints_Opr()
	 * @model
	 * @generated
	 */
	Operator getOpr();

	/**
	 * Sets the value of the '{@link spl.CMconstraints#getOpr <em>Opr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opr</em>' attribute.
	 * @see spl.Operator
	 * @see #getOpr()
	 * @generated
	 */
	void setOpr(Operator value);

	/**
	 * Returns the value of the '<em><b>Operand</b></em>' reference list.
	 * The list contents are of type {@link spl.Choice}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operand</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operand</em>' reference list.
	 * @see spl.SplPackage#getCMconstraints_Operand()
	 * @model
	 * @generated
	 */
	EList<Choice> getOperand();

} // CMconstraints
