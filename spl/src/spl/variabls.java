/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>variabls</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.variabls#getInter <em>Inter</em>}</li>
 *   <li>{@link spl.variabls#getTrans <em>Trans</em>}</li>
 *   <li>{@link spl.variabls#getGrd <em>Grd</em>}</li>
 *   <li>{@link spl.variabls#getLabel <em>Label</em>}</li>
 *   <li>{@link spl.variabls#getEntry <em>Entry</em>}</li>
 *   <li>{@link spl.variabls#getTrigger <em>Trigger</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getvariabls()
 * @model annotation="gmf.node label.placement='none' label.text='Variables' layout='list'"
 * @generated
 */
public interface variabls extends EObject {
	/**
	 * Returns the value of the '<em><b>Inter</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Interfc}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inter</em>' containment reference list.
	 * @see spl.SplPackage#getvariabls_Inter()
	 * @model containment="true"
	 *        annotation="gmf.compartment layout='list' label.placement='none' label.text='Interfaces'"
	 * @generated
	 */
	EList<Interfc> getInter();

	/**
	 * Returns the value of the '<em><b>Trans</b></em>' containment reference list.
	 * The list contents are of type {@link spl.TransitionalBehavior}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trans</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trans</em>' containment reference list.
	 * @see spl.SplPackage#getvariabls_Trans()
	 * @model containment="true"
	 *        annotation="gmf.compartment layout='list'"
	 * @generated
	 */
	EList<TransitionalBehavior> getTrans();

	/**
	 * Returns the value of the '<em><b>Grd</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Guard}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grd</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grd</em>' containment reference list.
	 * @see spl.SplPackage#getvariabls_Grd()
	 * @model containment="true"
	 *        annotation="gmf.compartment layout='list'"
	 * @generated
	 */
	EList<Guard> getGrd();

	/**
	 * Returns the value of the '<em><b>Label</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Label}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' containment reference list.
	 * @see spl.SplPackage#getvariabls_Label()
	 * @model containment="true"
	 *        annotation="gmf.compartment layout='list'"
	 * @generated
	 */
	EList<Label> getLabel();

	/**
	 * Returns the value of the '<em><b>Entry</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' containment reference list.
	 * @see spl.SplPackage#getvariabls_Entry()
	 * @model containment="true"
	 *        annotation="gmf.compartment layout='list'"
	 * @generated
	 */
	EList<Entry> getEntry();

	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Trigger}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigger</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' containment reference list.
	 * @see spl.SplPackage#getvariabls_Trigger()
	 * @model containment="true"
	 *        annotation="gmf.compartment layout='list'"
	 * @generated
	 */
	EList<Trigger> getTrigger();

} // variabls
