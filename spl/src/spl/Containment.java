/**
 */
package spl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Containment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see spl.SplPackage#getContainment()
 * @model
 * @generated
 */
public interface Containment extends EObject {
} // Containment
