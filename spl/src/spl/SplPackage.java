/**
 */
package spl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see spl.SplFactory
 * @model kind="package"
 * @generated
 */
public interface SplPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "spl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/spl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "spl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SplPackage eINSTANCE = spl.impl.SplPackageImpl.init();

	/**
	 * The meta object id for the '{@link spl.impl.DesignChoicesImpl <em>Design Choices</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.DesignChoicesImpl
	 * @see spl.impl.SplPackageImpl#getDesignChoices()
	 * @generated
	 */
	int DESIGN_CHOICES = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_CHOICES__NAME = 0;

	/**
	 * The feature id for the '<em><b>Cm</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_CHOICES__CM = 1;

	/**
	 * The feature id for the '<em><b>Fm</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_CHOICES__FM = 2;

	/**
	 * The feature id for the '<em><b>Fmap</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_CHOICES__FMAP = 3;

	/**
	 * The feature id for the '<em><b>Dmap</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_CHOICES__DMAP = 4;

	/**
	 * The feature id for the '<em><b>Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_CHOICES__PROPERTY = 5;

	/**
	 * The feature id for the '<em><b>Var</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_CHOICES__VAR = 6;

	/**
	 * The feature id for the '<em><b>Cd</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_CHOICES__CD = 7;

	/**
	 * The feature id for the '<em><b>Sc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_CHOICES__SC = 8;

	/**
	 * The number of structural features of the '<em>Design Choices</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_CHOICES_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link spl.impl.variablsImpl <em>variabls</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.variablsImpl
	 * @see spl.impl.SplPackageImpl#getvariabls()
	 * @generated
	 */
	int VARIABLS = 1;

	/**
	 * The feature id for the '<em><b>Inter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLS__INTER = 0;

	/**
	 * The feature id for the '<em><b>Trans</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLS__TRANS = 1;

	/**
	 * The feature id for the '<em><b>Grd</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLS__GRD = 2;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLS__LABEL = 3;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLS__ENTRY = 4;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLS__TRIGGER = 5;

	/**
	 * The number of structural features of the '<em>variabls</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLS_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link spl.impl.LabelImpl <em>Label</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.LabelImpl
	 * @see spl.impl.SplPackageImpl#getLabel()
	 * @generated
	 */
	int LABEL = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__NAME = 0;

	/**
	 * The number of structural features of the '<em>Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link spl.impl.TriggerImpl <em>Trigger</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.TriggerImpl
	 * @see spl.impl.SplPackageImpl#getTrigger()
	 * @generated
	 */
	int TRIGGER = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER__NAME = 0;

	/**
	 * The number of structural features of the '<em>Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link spl.impl.EntryImpl <em>Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.EntryImpl
	 * @see spl.impl.SplPackageImpl#getEntry()
	 * @generated
	 */
	int ENTRY = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTRY__NAME = 0;

	/**
	 * The number of structural features of the '<em>Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTRY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link spl.impl.InterfcImpl <em>Interfc</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.InterfcImpl
	 * @see spl.impl.SplPackageImpl#getInterfc()
	 * @generated
	 */
	int INTERFC = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFC__NAME = 0;

	/**
	 * The number of structural features of the '<em>Interfc</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFC_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link spl.impl.TransitionalBehaviorImpl <em>Transitional Behavior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.TransitionalBehaviorImpl
	 * @see spl.impl.SplPackageImpl#getTransitionalBehavior()
	 * @generated
	 */
	int TRANSITIONAL_BEHAVIOR = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITIONAL_BEHAVIOR__NAME = 0;

	/**
	 * The number of structural features of the '<em>Transitional Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITIONAL_BEHAVIOR_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link spl.impl.GuardImpl <em>Guard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.GuardImpl
	 * @see spl.impl.SplPackageImpl#getGuard()
	 * @generated
	 */
	int GUARD = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__NAME = 0;

	/**
	 * The number of structural features of the '<em>Guard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link spl.impl.ChoiceModelImpl <em>Choice Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.ChoiceModelImpl
	 * @see spl.impl.SplPackageImpl#getChoiceModel()
	 * @generated
	 */
	int CHOICE_MODEL = 8;

	/**
	 * The feature id for the '<em><b>Ch</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_MODEL__CH = 0;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_MODEL__CONSTRAINT = 1;

	/**
	 * The number of structural features of the '<em>Choice Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_MODEL_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link spl.impl.ChoiceImpl <em>Choice</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.ChoiceImpl
	 * @see spl.impl.SplPackageImpl#getChoice()
	 * @generated
	 */
	int CHOICE = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Var Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE__VAR_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link spl.impl.FeatureModelImpl <em>Feature Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.FeatureModelImpl
	 * @see spl.impl.SplPackageImpl#getFeatureModel()
	 * @generated
	 */
	int FEATURE_MODEL = 10;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__FEATURE = 0;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__CONSTRAINT = 1;

	/**
	 * The number of structural features of the '<em>Feature Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link spl.impl.FeatureImpl <em>Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.FeatureImpl
	 * @see spl.impl.SplPackageImpl#getFeature()
	 * @generated
	 */
	int FEATURE = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Vartype</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__VARTYPE = 1;

	/**
	 * The number of structural features of the '<em>Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link spl.impl.scwellformednessImpl <em>scwellformedness</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.scwellformednessImpl
	 * @see spl.impl.SplPackageImpl#getscwellformedness()
	 * @generated
	 */
	int SCWELLFORMEDNESS = 12;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCWELLFORMEDNESS__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCWELLFORMEDNESS__TARGET = 1;

	/**
	 * The number of structural features of the '<em>scwellformedness</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCWELLFORMEDNESS_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link spl.impl.cdwellformednessImpl <em>cdwellformedness</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.cdwellformednessImpl
	 * @see spl.impl.SplPackageImpl#getcdwellformedness()
	 * @generated
	 */
	int CDWELLFORMEDNESS = 13;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDWELLFORMEDNESS__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDWELLFORMEDNESS__TARGET = 1;

	/**
	 * The number of structural features of the '<em>cdwellformedness</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDWELLFORMEDNESS_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link spl.impl.FMconstraintsImpl <em>FMconstraints</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.FMconstraintsImpl
	 * @see spl.impl.SplPackageImpl#getFMconstraints()
	 * @generated
	 */
	int FMCONSTRAINTS = 14;

	/**
	 * The feature id for the '<em><b>Opr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMCONSTRAINTS__OPR = 0;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMCONSTRAINTS__OPERAND = 1;

	/**
	 * The number of structural features of the '<em>FMconstraints</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMCONSTRAINTS_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link spl.impl.CMconstraintsImpl <em>CMconstraints</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.CMconstraintsImpl
	 * @see spl.impl.SplPackageImpl#getCMconstraints()
	 * @generated
	 */
	int CMCONSTRAINTS = 15;

	/**
	 * The feature id for the '<em><b>Opr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CMCONSTRAINTS__OPR = 0;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CMCONSTRAINTS__OPERAND = 1;

	/**
	 * The number of structural features of the '<em>CMconstraints</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CMCONSTRAINTS_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link spl.impl.ClassDiagramImpl <em>Class Diagram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.ClassDiagramImpl
	 * @see spl.impl.SplPackageImpl#getClassDiagram()
	 * @generated
	 */
	int CLASS_DIAGRAM = 17;

	/**
	 * The meta object id for the '{@link spl.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.StateImpl
	 * @see spl.impl.SplPackageImpl#getState()
	 * @generated
	 */
	int STATE = 18;

	/**
	 * The meta object id for the '{@link spl.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.TransitionImpl
	 * @see spl.impl.SplPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 19;

	/**
	 * The meta object id for the '{@link spl.impl.ClassImpl <em>Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.ClassImpl
	 * @see spl.impl.SplPackageImpl#getClass_()
	 * @generated
	 */
	int CLASS = 20;

	/**
	 * The meta object id for the '{@link spl.impl.RelationshipImpl <em>Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.RelationshipImpl
	 * @see spl.impl.SplPackageImpl#getRelationship()
	 * @generated
	 */
	int RELATIONSHIP = 21;

	/**
	 * The meta object id for the '{@link spl.impl.StateChartImpl <em>State Chart</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.StateChartImpl
	 * @see spl.impl.SplPackageImpl#getStateChart()
	 * @generated
	 */
	int STATE_CHART = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHART__NAME = 0;

	/**
	 * The feature id for the '<em><b>State</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHART__STATE = 1;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHART__TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Wf</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHART__WF = 3;

	/**
	 * The number of structural features of the '<em>State Chart</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHART_FEATURE_COUNT = 4;

	/**
	 * The feature id for the '<em><b>Relationship</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_DIAGRAM__RELATIONSHIP = 0;

	/**
	 * The feature id for the '<em><b>Class</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_DIAGRAM__CLASS = 1;

	/**
	 * The feature id for the '<em><b>Wf</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_DIAGRAM__WF = 2;

	/**
	 * The number of structural features of the '<em>Class Diagram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_DIAGRAM_FEATURE_COUNT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ENTRY = 1;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TRIGGER = 1;

	/**
	 * The feature id for the '<em><b>Grd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__GRD = 2;

	/**
	 * The feature id for the '<em><b>Transitionbehavior</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TRANSITIONBEHAVIOR = 3;

	/**
	 * The feature id for the '<em><b>Always Present</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ALWAYS_PRESENT = 4;

	/**
	 * The feature id for the '<em><b>Tostate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TOSTATE = 5;

	/**
	 * The feature id for the '<em><b>Fromstate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__FROMSTATE = 6;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__NAME = 0;

	/**
	 * The feature id for the '<em><b>Interfc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__INTERFC = 1;

	/**
	 * The number of structural features of the '<em>Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Always Present</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__ALWAYS_PRESENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__NAME = 1;

	/**
	 * The feature id for the '<em><b>Lab</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__LAB = 2;

	/**
	 * The feature id for the '<em><b>In Mul</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__IN_MUL = 3;

	/**
	 * The feature id for the '<em><b>Out Mul</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__OUT_MUL = 4;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__FROM = 5;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__TO = 6;

	/**
	 * The number of structural features of the '<em>Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link spl.impl.FeatureMappingImpl <em>Feature Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.FeatureMappingImpl
	 * @see spl.impl.SplPackageImpl#getFeatureMapping()
	 * @generated
	 */
	int FEATURE_MAPPING = 22;

	/**
	 * The feature id for the '<em><b>Fmap</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MAPPING__FMAP = 0;

	/**
	 * The number of structural features of the '<em>Feature Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MAPPING_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link spl.impl.MapImpl <em>Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.MapImpl
	 * @see spl.impl.SplPackageImpl#getMap()
	 * @generated
	 */
	int MAP = 23;

	/**
	 * The feature id for the '<em><b>Ftr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP__FTR = 0;

	/**
	 * The feature id for the '<em><b>Presence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP__PRESENCE = 1;

	/**
	 * The number of structural features of the '<em>Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link spl.impl.MaptoImpl <em>Mapto</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.MaptoImpl
	 * @see spl.impl.SplPackageImpl#getMapto()
	 * @generated
	 */
	int MAPTO = 24;

	/**
	 * The feature id for the '<em><b>Presence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPTO__PRESENCE = 0;

	/**
	 * The feature id for the '<em><b>C</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPTO__C = 1;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPTO__RELATION = 2;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPTO__STATE = 3;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPTO__TRANSITION = 4;

	/**
	 * The number of structural features of the '<em>Mapto</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPTO_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link spl.impl.featMappingImpl <em>feat Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.featMappingImpl
	 * @see spl.impl.SplPackageImpl#getfeatMapping()
	 * @generated
	 */
	int FEAT_MAPPING = 25;

	/**
	 * The feature id for the '<em><b>Opr LHS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_MAPPING__OPR_LHS = 0;

	/**
	 * The feature id for the '<em><b>Mapfrom</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_MAPPING__MAPFROM = 1;

	/**
	 * The feature id for the '<em><b>Opr RHS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_MAPPING__OPR_RHS = 2;

	/**
	 * The feature id for the '<em><b>Mapto</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_MAPPING__MAPTO = 3;

	/**
	 * The number of structural features of the '<em>feat Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_MAPPING_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link spl.impl.MapdecImpl <em>Mapdec</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.MapdecImpl
	 * @see spl.impl.SplPackageImpl#getMapdec()
	 * @generated
	 */
	int MAPDEC = 26;

	/**
	 * The feature id for the '<em><b>Presence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPDEC__PRESENCE = 0;

	/**
	 * The feature id for the '<em><b>Choice</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPDEC__CHOICE = 1;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPDEC__FEATURE = 2;

	/**
	 * The number of structural features of the '<em>Mapdec</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPDEC_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link spl.impl.MapToEntityImpl <em>Map To Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.MapToEntityImpl
	 * @see spl.impl.SplPackageImpl#getMapToEntity()
	 * @generated
	 */
	int MAP_TO_ENTITY = 27;

	/**
	 * The feature id for the '<em><b>Presence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_TO_ENTITY__PRESENCE = 0;

	/**
	 * The feature id for the '<em><b>Ftr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_TO_ENTITY__FTR = 1;

	/**
	 * The feature id for the '<em><b>C</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_TO_ENTITY__C = 2;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_TO_ENTITY__RELATION = 3;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_TO_ENTITY__STATE = 4;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_TO_ENTITY__TRANSITION = 5;

	/**
	 * The number of structural features of the '<em>Map To Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_TO_ENTITY_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link spl.impl.DecMappingImpl <em>Dec Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.DecMappingImpl
	 * @see spl.impl.SplPackageImpl#getDecMapping()
	 * @generated
	 */
	int DEC_MAPPING = 28;

	/**
	 * The feature id for the '<em><b>Opr LHS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_MAPPING__OPR_LHS = 0;

	/**
	 * The feature id for the '<em><b>Mapfrom</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_MAPPING__MAPFROM = 1;

	/**
	 * The feature id for the '<em><b>Opr RHS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_MAPPING__OPR_RHS = 2;

	/**
	 * The feature id for the '<em><b>Mapto Entity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_MAPPING__MAPTO_ENTITY = 3;

	/**
	 * The number of structural features of the '<em>Dec Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEC_MAPPING_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link spl.impl.DecisionMappingImpl <em>Decision Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.DecisionMappingImpl
	 * @see spl.impl.SplPackageImpl#getDecisionMapping()
	 * @generated
	 */
	int DECISION_MAPPING = 29;

	/**
	 * The feature id for the '<em><b>Decision Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_MAPPING__DECISION_MAP = 0;

	/**
	 * The number of structural features of the '<em>Decision Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_MAPPING_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link spl.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.impl.PropertyImpl
	 * @see spl.impl.SplPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 30;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__TEXT = 1;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link spl.VarType <em>Var Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.VarType
	 * @see spl.impl.SplPackageImpl#getVarType()
	 * @generated
	 */
	int VAR_TYPE = 31;

	/**
	 * The meta object id for the '{@link spl.Operator <em>Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.Operator
	 * @see spl.impl.SplPackageImpl#getOperator()
	 * @generated
	 */
	int OPERATOR = 32;

	/**
	 * The meta object id for the '{@link spl.Presence <em>Presence</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see spl.Presence
	 * @see spl.impl.SplPackageImpl#getPresence()
	 * @generated
	 */
	int PRESENCE = 33;

	/**
	 * Returns the meta object for class '{@link spl.DesignChoices <em>Design Choices</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Design Choices</em>'.
	 * @see spl.DesignChoices
	 * @generated
	 */
	EClass getDesignChoices();

	/**
	 * Returns the meta object for the attribute '{@link spl.DesignChoices#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.DesignChoices#getName()
	 * @see #getDesignChoices()
	 * @generated
	 */
	EAttribute getDesignChoices_Name();

	/**
	 * Returns the meta object for the containment reference '{@link spl.DesignChoices#getCm <em>Cm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cm</em>'.
	 * @see spl.DesignChoices#getCm()
	 * @see #getDesignChoices()
	 * @generated
	 */
	EReference getDesignChoices_Cm();

	/**
	 * Returns the meta object for the containment reference '{@link spl.DesignChoices#getFm <em>Fm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fm</em>'.
	 * @see spl.DesignChoices#getFm()
	 * @see #getDesignChoices()
	 * @generated
	 */
	EReference getDesignChoices_Fm();

	/**
	 * Returns the meta object for the containment reference '{@link spl.DesignChoices#getFmap <em>Fmap</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fmap</em>'.
	 * @see spl.DesignChoices#getFmap()
	 * @see #getDesignChoices()
	 * @generated
	 */
	EReference getDesignChoices_Fmap();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.DesignChoices#getDmap <em>Dmap</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dmap</em>'.
	 * @see spl.DesignChoices#getDmap()
	 * @see #getDesignChoices()
	 * @generated
	 */
	EReference getDesignChoices_Dmap();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.DesignChoices#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property</em>'.
	 * @see spl.DesignChoices#getProperty()
	 * @see #getDesignChoices()
	 * @generated
	 */
	EReference getDesignChoices_Property();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.DesignChoices#getVar <em>Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Var</em>'.
	 * @see spl.DesignChoices#getVar()
	 * @see #getDesignChoices()
	 * @generated
	 */
	EReference getDesignChoices_Var();

	/**
	 * Returns the meta object for the containment reference '{@link spl.DesignChoices#getCd <em>Cd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cd</em>'.
	 * @see spl.DesignChoices#getCd()
	 * @see #getDesignChoices()
	 * @generated
	 */
	EReference getDesignChoices_Cd();

	/**
	 * Returns the meta object for the containment reference '{@link spl.DesignChoices#getSc <em>Sc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sc</em>'.
	 * @see spl.DesignChoices#getSc()
	 * @see #getDesignChoices()
	 * @generated
	 */
	EReference getDesignChoices_Sc();

	/**
	 * Returns the meta object for class '{@link spl.variabls <em>variabls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>variabls</em>'.
	 * @see spl.variabls
	 * @generated
	 */
	EClass getvariabls();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.variabls#getInter <em>Inter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inter</em>'.
	 * @see spl.variabls#getInter()
	 * @see #getvariabls()
	 * @generated
	 */
	EReference getvariabls_Inter();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.variabls#getTrans <em>Trans</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Trans</em>'.
	 * @see spl.variabls#getTrans()
	 * @see #getvariabls()
	 * @generated
	 */
	EReference getvariabls_Trans();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.variabls#getGrd <em>Grd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Grd</em>'.
	 * @see spl.variabls#getGrd()
	 * @see #getvariabls()
	 * @generated
	 */
	EReference getvariabls_Grd();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.variabls#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Label</em>'.
	 * @see spl.variabls#getLabel()
	 * @see #getvariabls()
	 * @generated
	 */
	EReference getvariabls_Label();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.variabls#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Entry</em>'.
	 * @see spl.variabls#getEntry()
	 * @see #getvariabls()
	 * @generated
	 */
	EReference getvariabls_Entry();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.variabls#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Trigger</em>'.
	 * @see spl.variabls#getTrigger()
	 * @see #getvariabls()
	 * @generated
	 */
	EReference getvariabls_Trigger();

	/**
	 * Returns the meta object for class '{@link spl.Label <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Label</em>'.
	 * @see spl.Label
	 * @generated
	 */
	EClass getLabel();

	/**
	 * Returns the meta object for the attribute '{@link spl.Label#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.Label#getName()
	 * @see #getLabel()
	 * @generated
	 */
	EAttribute getLabel_Name();

	/**
	 * Returns the meta object for class '{@link spl.Trigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Trigger</em>'.
	 * @see spl.Trigger
	 * @generated
	 */
	EClass getTrigger();

	/**
	 * Returns the meta object for the attribute '{@link spl.Trigger#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.Trigger#getName()
	 * @see #getTrigger()
	 * @generated
	 */
	EAttribute getTrigger_Name();

	/**
	 * Returns the meta object for class '{@link spl.Entry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entry</em>'.
	 * @see spl.Entry
	 * @generated
	 */
	EClass getEntry();

	/**
	 * Returns the meta object for the attribute '{@link spl.Entry#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.Entry#getName()
	 * @see #getEntry()
	 * @generated
	 */
	EAttribute getEntry_Name();

	/**
	 * Returns the meta object for class '{@link spl.Interfc <em>Interfc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interfc</em>'.
	 * @see spl.Interfc
	 * @generated
	 */
	EClass getInterfc();

	/**
	 * Returns the meta object for the attribute '{@link spl.Interfc#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.Interfc#getName()
	 * @see #getInterfc()
	 * @generated
	 */
	EAttribute getInterfc_Name();

	/**
	 * Returns the meta object for class '{@link spl.TransitionalBehavior <em>Transitional Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transitional Behavior</em>'.
	 * @see spl.TransitionalBehavior
	 * @generated
	 */
	EClass getTransitionalBehavior();

	/**
	 * Returns the meta object for the attribute '{@link spl.TransitionalBehavior#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.TransitionalBehavior#getName()
	 * @see #getTransitionalBehavior()
	 * @generated
	 */
	EAttribute getTransitionalBehavior_Name();

	/**
	 * Returns the meta object for class '{@link spl.Guard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guard</em>'.
	 * @see spl.Guard
	 * @generated
	 */
	EClass getGuard();

	/**
	 * Returns the meta object for the attribute '{@link spl.Guard#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.Guard#getName()
	 * @see #getGuard()
	 * @generated
	 */
	EAttribute getGuard_Name();

	/**
	 * Returns the meta object for class '{@link spl.ChoiceModel <em>Choice Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Choice Model</em>'.
	 * @see spl.ChoiceModel
	 * @generated
	 */
	EClass getChoiceModel();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.ChoiceModel#getCh <em>Ch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ch</em>'.
	 * @see spl.ChoiceModel#getCh()
	 * @see #getChoiceModel()
	 * @generated
	 */
	EReference getChoiceModel_Ch();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.ChoiceModel#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraint</em>'.
	 * @see spl.ChoiceModel#getConstraint()
	 * @see #getChoiceModel()
	 * @generated
	 */
	EReference getChoiceModel_Constraint();

	/**
	 * Returns the meta object for class '{@link spl.Choice <em>Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Choice</em>'.
	 * @see spl.Choice
	 * @generated
	 */
	EClass getChoice();

	/**
	 * Returns the meta object for the attribute '{@link spl.Choice#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.Choice#getName()
	 * @see #getChoice()
	 * @generated
	 */
	EAttribute getChoice_Name();

	/**
	 * Returns the meta object for the attribute '{@link spl.Choice#getVarType <em>Var Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Type</em>'.
	 * @see spl.Choice#getVarType()
	 * @see #getChoice()
	 * @generated
	 */
	EAttribute getChoice_VarType();

	/**
	 * Returns the meta object for class '{@link spl.FeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Model</em>'.
	 * @see spl.FeatureModel
	 * @generated
	 */
	EClass getFeatureModel();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.FeatureModel#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Feature</em>'.
	 * @see spl.FeatureModel#getFeature()
	 * @see #getFeatureModel()
	 * @generated
	 */
	EReference getFeatureModel_Feature();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.FeatureModel#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraint</em>'.
	 * @see spl.FeatureModel#getConstraint()
	 * @see #getFeatureModel()
	 * @generated
	 */
	EReference getFeatureModel_Constraint();

	/**
	 * Returns the meta object for class '{@link spl.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature</em>'.
	 * @see spl.Feature
	 * @generated
	 */
	EClass getFeature();

	/**
	 * Returns the meta object for the attribute '{@link spl.Feature#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.Feature#getName()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Name();

	/**
	 * Returns the meta object for the attribute '{@link spl.Feature#getVartype <em>Vartype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vartype</em>'.
	 * @see spl.Feature#getVartype()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Vartype();

	/**
	 * Returns the meta object for class '{@link spl.scwellformedness <em>scwellformedness</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>scwellformedness</em>'.
	 * @see spl.scwellformedness
	 * @generated
	 */
	EClass getscwellformedness();

	/**
	 * Returns the meta object for the attribute '{@link spl.scwellformedness#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source</em>'.
	 * @see spl.scwellformedness#getSource()
	 * @see #getscwellformedness()
	 * @generated
	 */
	EAttribute getscwellformedness_Source();

	/**
	 * Returns the meta object for the attribute '{@link spl.scwellformedness#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target</em>'.
	 * @see spl.scwellformedness#getTarget()
	 * @see #getscwellformedness()
	 * @generated
	 */
	EAttribute getscwellformedness_Target();

	/**
	 * Returns the meta object for class '{@link spl.cdwellformedness <em>cdwellformedness</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>cdwellformedness</em>'.
	 * @see spl.cdwellformedness
	 * @generated
	 */
	EClass getcdwellformedness();

	/**
	 * Returns the meta object for the attribute '{@link spl.cdwellformedness#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source</em>'.
	 * @see spl.cdwellformedness#getSource()
	 * @see #getcdwellformedness()
	 * @generated
	 */
	EAttribute getcdwellformedness_Source();

	/**
	 * Returns the meta object for the attribute '{@link spl.cdwellformedness#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target</em>'.
	 * @see spl.cdwellformedness#getTarget()
	 * @see #getcdwellformedness()
	 * @generated
	 */
	EAttribute getcdwellformedness_Target();

	/**
	 * Returns the meta object for class '{@link spl.FMconstraints <em>FMconstraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FMconstraints</em>'.
	 * @see spl.FMconstraints
	 * @generated
	 */
	EClass getFMconstraints();

	/**
	 * Returns the meta object for the attribute '{@link spl.FMconstraints#getOpr <em>Opr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opr</em>'.
	 * @see spl.FMconstraints#getOpr()
	 * @see #getFMconstraints()
	 * @generated
	 */
	EAttribute getFMconstraints_Opr();

	/**
	 * Returns the meta object for the reference list '{@link spl.FMconstraints#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Operand</em>'.
	 * @see spl.FMconstraints#getOperand()
	 * @see #getFMconstraints()
	 * @generated
	 */
	EReference getFMconstraints_Operand();

	/**
	 * Returns the meta object for class '{@link spl.CMconstraints <em>CMconstraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CMconstraints</em>'.
	 * @see spl.CMconstraints
	 * @generated
	 */
	EClass getCMconstraints();

	/**
	 * Returns the meta object for the attribute '{@link spl.CMconstraints#getOpr <em>Opr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opr</em>'.
	 * @see spl.CMconstraints#getOpr()
	 * @see #getCMconstraints()
	 * @generated
	 */
	EAttribute getCMconstraints_Opr();

	/**
	 * Returns the meta object for the reference list '{@link spl.CMconstraints#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Operand</em>'.
	 * @see spl.CMconstraints#getOperand()
	 * @see #getCMconstraints()
	 * @generated
	 */
	EReference getCMconstraints_Operand();

	/**
	 * Returns the meta object for class '{@link spl.ClassDiagram <em>Class Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Diagram</em>'.
	 * @see spl.ClassDiagram
	 * @generated
	 */
	EClass getClassDiagram();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.ClassDiagram#getRelationship <em>Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relationship</em>'.
	 * @see spl.ClassDiagram#getRelationship()
	 * @see #getClassDiagram()
	 * @generated
	 */
	EReference getClassDiagram_Relationship();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.ClassDiagram#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class</em>'.
	 * @see spl.ClassDiagram#getClass_()
	 * @see #getClassDiagram()
	 * @generated
	 */
	EReference getClassDiagram_Class();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.ClassDiagram#getWf <em>Wf</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Wf</em>'.
	 * @see spl.ClassDiagram#getWf()
	 * @see #getClassDiagram()
	 * @generated
	 */
	EReference getClassDiagram_Wf();

	/**
	 * Returns the meta object for class '{@link spl.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see spl.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the attribute '{@link spl.State#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Entry</em>'.
	 * @see spl.State#getEntry()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_Entry();

	/**
	 * Returns the meta object for the attribute '{@link spl.State#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.State#getName()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_Name();

	/**
	 * Returns the meta object for class '{@link spl.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see spl.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the attribute '{@link spl.Transition#getAlwaysPresent <em>Always Present</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Always Present</em>'.
	 * @see spl.Transition#getAlwaysPresent()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_AlwaysPresent();

	/**
	 * Returns the meta object for the attribute '{@link spl.Transition#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigger</em>'.
	 * @see spl.Transition#getTrigger()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Trigger();

	/**
	 * Returns the meta object for the attribute '{@link spl.Transition#getGrd <em>Grd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Grd</em>'.
	 * @see spl.Transition#getGrd()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Grd();

	/**
	 * Returns the meta object for the attribute '{@link spl.Transition#getTransitionbehavior <em>Transitionbehavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transitionbehavior</em>'.
	 * @see spl.Transition#getTransitionbehavior()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Transitionbehavior();

	/**
	 * Returns the meta object for the attribute '{@link spl.Transition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.Transition#getName()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Name();

	/**
	 * Returns the meta object for the reference '{@link spl.Transition#getTostate <em>Tostate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tostate</em>'.
	 * @see spl.Transition#getTostate()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Tostate();

	/**
	 * Returns the meta object for the reference '{@link spl.Transition#getFromstate <em>Fromstate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Fromstate</em>'.
	 * @see spl.Transition#getFromstate()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Fromstate();

	/**
	 * Returns the meta object for class '{@link spl.Class <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class</em>'.
	 * @see spl.Class
	 * @generated
	 */
	EClass getClass_();

	/**
	 * Returns the meta object for the attribute '{@link spl.Class#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.Class#getName()
	 * @see #getClass_()
	 * @generated
	 */
	EAttribute getClass_Name();

	/**
	 * Returns the meta object for the attribute '{@link spl.Class#getInterfc <em>Interfc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interfc</em>'.
	 * @see spl.Class#getInterfc()
	 * @see #getClass_()
	 * @generated
	 */
	EAttribute getClass_Interfc();

	/**
	 * Returns the meta object for class '{@link spl.Relationship <em>Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relationship</em>'.
	 * @see spl.Relationship
	 * @generated
	 */
	EClass getRelationship();

	/**
	 * Returns the meta object for the attribute '{@link spl.Relationship#getAlwaysPresent <em>Always Present</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Always Present</em>'.
	 * @see spl.Relationship#getAlwaysPresent()
	 * @see #getRelationship()
	 * @generated
	 */
	EAttribute getRelationship_AlwaysPresent();

	/**
	 * Returns the meta object for the attribute '{@link spl.Relationship#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.Relationship#getName()
	 * @see #getRelationship()
	 * @generated
	 */
	EAttribute getRelationship_Name();

	/**
	 * Returns the meta object for the attribute '{@link spl.Relationship#getLab <em>Lab</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lab</em>'.
	 * @see spl.Relationship#getLab()
	 * @see #getRelationship()
	 * @generated
	 */
	EAttribute getRelationship_Lab();

	/**
	 * Returns the meta object for the attribute '{@link spl.Relationship#getInMul <em>In Mul</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>In Mul</em>'.
	 * @see spl.Relationship#getInMul()
	 * @see #getRelationship()
	 * @generated
	 */
	EAttribute getRelationship_InMul();

	/**
	 * Returns the meta object for the attribute '{@link spl.Relationship#getOutMul <em>Out Mul</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Out Mul</em>'.
	 * @see spl.Relationship#getOutMul()
	 * @see #getRelationship()
	 * @generated
	 */
	EAttribute getRelationship_OutMul();

	/**
	 * Returns the meta object for the reference '{@link spl.Relationship#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From</em>'.
	 * @see spl.Relationship#getFrom()
	 * @see #getRelationship()
	 * @generated
	 */
	EReference getRelationship_From();

	/**
	 * Returns the meta object for the reference '{@link spl.Relationship#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To</em>'.
	 * @see spl.Relationship#getTo()
	 * @see #getRelationship()
	 * @generated
	 */
	EReference getRelationship_To();

	/**
	 * Returns the meta object for class '{@link spl.FeatureMapping <em>Feature Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Mapping</em>'.
	 * @see spl.FeatureMapping
	 * @generated
	 */
	EClass getFeatureMapping();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.FeatureMapping#getFmap <em>Fmap</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fmap</em>'.
	 * @see spl.FeatureMapping#getFmap()
	 * @see #getFeatureMapping()
	 * @generated
	 */
	EReference getFeatureMapping_Fmap();

	/**
	 * Returns the meta object for class '{@link spl.Map <em>Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Map</em>'.
	 * @see spl.Map
	 * @generated
	 */
	EClass getMap();

	/**
	 * Returns the meta object for the reference '{@link spl.Map#getFtr <em>Ftr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ftr</em>'.
	 * @see spl.Map#getFtr()
	 * @see #getMap()
	 * @generated
	 */
	EReference getMap_Ftr();

	/**
	 * Returns the meta object for the attribute '{@link spl.Map#getPresence <em>Presence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Presence</em>'.
	 * @see spl.Map#getPresence()
	 * @see #getMap()
	 * @generated
	 */
	EAttribute getMap_Presence();

	/**
	 * Returns the meta object for class '{@link spl.Mapto <em>Mapto</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mapto</em>'.
	 * @see spl.Mapto
	 * @generated
	 */
	EClass getMapto();

	/**
	 * Returns the meta object for the attribute '{@link spl.Mapto#getPresence <em>Presence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Presence</em>'.
	 * @see spl.Mapto#getPresence()
	 * @see #getMapto()
	 * @generated
	 */
	EAttribute getMapto_Presence();

	/**
	 * Returns the meta object for the reference '{@link spl.Mapto#getC <em>C</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>C</em>'.
	 * @see spl.Mapto#getC()
	 * @see #getMapto()
	 * @generated
	 */
	EReference getMapto_C();

	/**
	 * Returns the meta object for the reference '{@link spl.Mapto#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Relation</em>'.
	 * @see spl.Mapto#getRelation()
	 * @see #getMapto()
	 * @generated
	 */
	EReference getMapto_Relation();

	/**
	 * Returns the meta object for the reference '{@link spl.Mapto#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State</em>'.
	 * @see spl.Mapto#getState()
	 * @see #getMapto()
	 * @generated
	 */
	EReference getMapto_State();

	/**
	 * Returns the meta object for the reference '{@link spl.Mapto#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transition</em>'.
	 * @see spl.Mapto#getTransition()
	 * @see #getMapto()
	 * @generated
	 */
	EReference getMapto_Transition();

	/**
	 * Returns the meta object for class '{@link spl.featMapping <em>feat Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>feat Mapping</em>'.
	 * @see spl.featMapping
	 * @generated
	 */
	EClass getfeatMapping();

	/**
	 * Returns the meta object for the attribute '{@link spl.featMapping#getOprLHS <em>Opr LHS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opr LHS</em>'.
	 * @see spl.featMapping#getOprLHS()
	 * @see #getfeatMapping()
	 * @generated
	 */
	EAttribute getfeatMapping_OprLHS();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.featMapping#getMapfrom <em>Mapfrom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Mapfrom</em>'.
	 * @see spl.featMapping#getMapfrom()
	 * @see #getfeatMapping()
	 * @generated
	 */
	EReference getfeatMapping_Mapfrom();

	/**
	 * Returns the meta object for the attribute '{@link spl.featMapping#getOprRHS <em>Opr RHS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opr RHS</em>'.
	 * @see spl.featMapping#getOprRHS()
	 * @see #getfeatMapping()
	 * @generated
	 */
	EAttribute getfeatMapping_OprRHS();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.featMapping#getMapto <em>Mapto</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Mapto</em>'.
	 * @see spl.featMapping#getMapto()
	 * @see #getfeatMapping()
	 * @generated
	 */
	EReference getfeatMapping_Mapto();

	/**
	 * Returns the meta object for class '{@link spl.Mapdec <em>Mapdec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mapdec</em>'.
	 * @see spl.Mapdec
	 * @generated
	 */
	EClass getMapdec();

	/**
	 * Returns the meta object for the attribute '{@link spl.Mapdec#getPresence <em>Presence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Presence</em>'.
	 * @see spl.Mapdec#getPresence()
	 * @see #getMapdec()
	 * @generated
	 */
	EAttribute getMapdec_Presence();

	/**
	 * Returns the meta object for the reference '{@link spl.Mapdec#getChoice <em>Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Choice</em>'.
	 * @see spl.Mapdec#getChoice()
	 * @see #getMapdec()
	 * @generated
	 */
	EReference getMapdec_Choice();

	/**
	 * Returns the meta object for the reference '{@link spl.Mapdec#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see spl.Mapdec#getFeature()
	 * @see #getMapdec()
	 * @generated
	 */
	EReference getMapdec_Feature();

	/**
	 * Returns the meta object for class '{@link spl.MapToEntity <em>Map To Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Map To Entity</em>'.
	 * @see spl.MapToEntity
	 * @generated
	 */
	EClass getMapToEntity();

	/**
	 * Returns the meta object for the attribute '{@link spl.MapToEntity#getPresence <em>Presence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Presence</em>'.
	 * @see spl.MapToEntity#getPresence()
	 * @see #getMapToEntity()
	 * @generated
	 */
	EAttribute getMapToEntity_Presence();

	/**
	 * Returns the meta object for the reference '{@link spl.MapToEntity#getFtr <em>Ftr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ftr</em>'.
	 * @see spl.MapToEntity#getFtr()
	 * @see #getMapToEntity()
	 * @generated
	 */
	EReference getMapToEntity_Ftr();

	/**
	 * Returns the meta object for the reference '{@link spl.MapToEntity#getC <em>C</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>C</em>'.
	 * @see spl.MapToEntity#getC()
	 * @see #getMapToEntity()
	 * @generated
	 */
	EReference getMapToEntity_C();

	/**
	 * Returns the meta object for the reference '{@link spl.MapToEntity#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Relation</em>'.
	 * @see spl.MapToEntity#getRelation()
	 * @see #getMapToEntity()
	 * @generated
	 */
	EReference getMapToEntity_Relation();

	/**
	 * Returns the meta object for the reference '{@link spl.MapToEntity#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State</em>'.
	 * @see spl.MapToEntity#getState()
	 * @see #getMapToEntity()
	 * @generated
	 */
	EReference getMapToEntity_State();

	/**
	 * Returns the meta object for the reference '{@link spl.MapToEntity#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transition</em>'.
	 * @see spl.MapToEntity#getTransition()
	 * @see #getMapToEntity()
	 * @generated
	 */
	EReference getMapToEntity_Transition();

	/**
	 * Returns the meta object for class '{@link spl.DecMapping <em>Dec Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dec Mapping</em>'.
	 * @see spl.DecMapping
	 * @generated
	 */
	EClass getDecMapping();

	/**
	 * Returns the meta object for the attribute '{@link spl.DecMapping#getOprLHS <em>Opr LHS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opr LHS</em>'.
	 * @see spl.DecMapping#getOprLHS()
	 * @see #getDecMapping()
	 * @generated
	 */
	EAttribute getDecMapping_OprLHS();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.DecMapping#getMapfrom <em>Mapfrom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Mapfrom</em>'.
	 * @see spl.DecMapping#getMapfrom()
	 * @see #getDecMapping()
	 * @generated
	 */
	EReference getDecMapping_Mapfrom();

	/**
	 * Returns the meta object for the attribute '{@link spl.DecMapping#getOprRHS <em>Opr RHS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opr RHS</em>'.
	 * @see spl.DecMapping#getOprRHS()
	 * @see #getDecMapping()
	 * @generated
	 */
	EAttribute getDecMapping_OprRHS();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.DecMapping#getMaptoEntity <em>Mapto Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Mapto Entity</em>'.
	 * @see spl.DecMapping#getMaptoEntity()
	 * @see #getDecMapping()
	 * @generated
	 */
	EReference getDecMapping_MaptoEntity();

	/**
	 * Returns the meta object for class '{@link spl.DecisionMapping <em>Decision Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decision Mapping</em>'.
	 * @see spl.DecisionMapping
	 * @generated
	 */
	EClass getDecisionMapping();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.DecisionMapping#getDecisionMap <em>Decision Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Decision Map</em>'.
	 * @see spl.DecisionMapping#getDecisionMap()
	 * @see #getDecisionMapping()
	 * @generated
	 */
	EReference getDecisionMapping_DecisionMap();

	/**
	 * Returns the meta object for class '{@link spl.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see spl.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for the attribute '{@link spl.Property#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.Property#getName()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Name();

	/**
	 * Returns the meta object for the attribute '{@link spl.Property#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see spl.Property#getText()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Text();

	/**
	 * Returns the meta object for enum '{@link spl.VarType <em>Var Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Var Type</em>'.
	 * @see spl.VarType
	 * @generated
	 */
	EEnum getVarType();

	/**
	 * Returns the meta object for enum '{@link spl.Operator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operator</em>'.
	 * @see spl.Operator
	 * @generated
	 */
	EEnum getOperator();

	/**
	 * Returns the meta object for enum '{@link spl.Presence <em>Presence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Presence</em>'.
	 * @see spl.Presence
	 * @generated
	 */
	EEnum getPresence();

	/**
	 * Returns the meta object for class '{@link spl.StateChart <em>State Chart</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Chart</em>'.
	 * @see spl.StateChart
	 * @generated
	 */
	EClass getStateChart();

	/**
	 * Returns the meta object for the attribute '{@link spl.StateChart#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see spl.StateChart#getName()
	 * @see #getStateChart()
	 * @generated
	 */
	EAttribute getStateChart_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.StateChart#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>State</em>'.
	 * @see spl.StateChart#getState()
	 * @see #getStateChart()
	 * @generated
	 */
	EReference getStateChart_State();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.StateChart#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transition</em>'.
	 * @see spl.StateChart#getTransition()
	 * @see #getStateChart()
	 * @generated
	 */
	EReference getStateChart_Transition();

	/**
	 * Returns the meta object for the containment reference list '{@link spl.StateChart#getWf <em>Wf</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Wf</em>'.
	 * @see spl.StateChart#getWf()
	 * @see #getStateChart()
	 * @generated
	 */
	EReference getStateChart_Wf();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SplFactory getSplFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link spl.impl.DesignChoicesImpl <em>Design Choices</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.DesignChoicesImpl
		 * @see spl.impl.SplPackageImpl#getDesignChoices()
		 * @generated
		 */
		EClass DESIGN_CHOICES = eINSTANCE.getDesignChoices();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESIGN_CHOICES__NAME = eINSTANCE.getDesignChoices_Name();

		/**
		 * The meta object literal for the '<em><b>Cm</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_CHOICES__CM = eINSTANCE.getDesignChoices_Cm();

		/**
		 * The meta object literal for the '<em><b>Fm</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_CHOICES__FM = eINSTANCE.getDesignChoices_Fm();

		/**
		 * The meta object literal for the '<em><b>Fmap</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_CHOICES__FMAP = eINSTANCE.getDesignChoices_Fmap();

		/**
		 * The meta object literal for the '<em><b>Dmap</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_CHOICES__DMAP = eINSTANCE.getDesignChoices_Dmap();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_CHOICES__PROPERTY = eINSTANCE.getDesignChoices_Property();

		/**
		 * The meta object literal for the '<em><b>Var</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_CHOICES__VAR = eINSTANCE.getDesignChoices_Var();

		/**
		 * The meta object literal for the '<em><b>Cd</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_CHOICES__CD = eINSTANCE.getDesignChoices_Cd();

		/**
		 * The meta object literal for the '<em><b>Sc</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_CHOICES__SC = eINSTANCE.getDesignChoices_Sc();

		/**
		 * The meta object literal for the '{@link spl.impl.variablsImpl <em>variabls</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.variablsImpl
		 * @see spl.impl.SplPackageImpl#getvariabls()
		 * @generated
		 */
		EClass VARIABLS = eINSTANCE.getvariabls();

		/**
		 * The meta object literal for the '<em><b>Inter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLS__INTER = eINSTANCE.getvariabls_Inter();

		/**
		 * The meta object literal for the '<em><b>Trans</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLS__TRANS = eINSTANCE.getvariabls_Trans();

		/**
		 * The meta object literal for the '<em><b>Grd</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLS__GRD = eINSTANCE.getvariabls_Grd();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLS__LABEL = eINSTANCE.getvariabls_Label();

		/**
		 * The meta object literal for the '<em><b>Entry</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLS__ENTRY = eINSTANCE.getvariabls_Entry();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLS__TRIGGER = eINSTANCE.getvariabls_Trigger();

		/**
		 * The meta object literal for the '{@link spl.impl.LabelImpl <em>Label</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.LabelImpl
		 * @see spl.impl.SplPackageImpl#getLabel()
		 * @generated
		 */
		EClass LABEL = eINSTANCE.getLabel();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LABEL__NAME = eINSTANCE.getLabel_Name();

		/**
		 * The meta object literal for the '{@link spl.impl.TriggerImpl <em>Trigger</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.TriggerImpl
		 * @see spl.impl.SplPackageImpl#getTrigger()
		 * @generated
		 */
		EClass TRIGGER = eINSTANCE.getTrigger();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIGGER__NAME = eINSTANCE.getTrigger_Name();

		/**
		 * The meta object literal for the '{@link spl.impl.EntryImpl <em>Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.EntryImpl
		 * @see spl.impl.SplPackageImpl#getEntry()
		 * @generated
		 */
		EClass ENTRY = eINSTANCE.getEntry();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTRY__NAME = eINSTANCE.getEntry_Name();

		/**
		 * The meta object literal for the '{@link spl.impl.InterfcImpl <em>Interfc</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.InterfcImpl
		 * @see spl.impl.SplPackageImpl#getInterfc()
		 * @generated
		 */
		EClass INTERFC = eINSTANCE.getInterfc();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFC__NAME = eINSTANCE.getInterfc_Name();

		/**
		 * The meta object literal for the '{@link spl.impl.TransitionalBehaviorImpl <em>Transitional Behavior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.TransitionalBehaviorImpl
		 * @see spl.impl.SplPackageImpl#getTransitionalBehavior()
		 * @generated
		 */
		EClass TRANSITIONAL_BEHAVIOR = eINSTANCE.getTransitionalBehavior();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITIONAL_BEHAVIOR__NAME = eINSTANCE.getTransitionalBehavior_Name();

		/**
		 * The meta object literal for the '{@link spl.impl.GuardImpl <em>Guard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.GuardImpl
		 * @see spl.impl.SplPackageImpl#getGuard()
		 * @generated
		 */
		EClass GUARD = eINSTANCE.getGuard();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUARD__NAME = eINSTANCE.getGuard_Name();

		/**
		 * The meta object literal for the '{@link spl.impl.ChoiceModelImpl <em>Choice Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.ChoiceModelImpl
		 * @see spl.impl.SplPackageImpl#getChoiceModel()
		 * @generated
		 */
		EClass CHOICE_MODEL = eINSTANCE.getChoiceModel();

		/**
		 * The meta object literal for the '<em><b>Ch</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHOICE_MODEL__CH = eINSTANCE.getChoiceModel_Ch();

		/**
		 * The meta object literal for the '<em><b>Constraint</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHOICE_MODEL__CONSTRAINT = eINSTANCE.getChoiceModel_Constraint();

		/**
		 * The meta object literal for the '{@link spl.impl.ChoiceImpl <em>Choice</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.ChoiceImpl
		 * @see spl.impl.SplPackageImpl#getChoice()
		 * @generated
		 */
		EClass CHOICE = eINSTANCE.getChoice();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHOICE__NAME = eINSTANCE.getChoice_Name();

		/**
		 * The meta object literal for the '<em><b>Var Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHOICE__VAR_TYPE = eINSTANCE.getChoice_VarType();

		/**
		 * The meta object literal for the '{@link spl.impl.FeatureModelImpl <em>Feature Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.FeatureModelImpl
		 * @see spl.impl.SplPackageImpl#getFeatureModel()
		 * @generated
		 */
		EClass FEATURE_MODEL = eINSTANCE.getFeatureModel();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL__FEATURE = eINSTANCE.getFeatureModel_Feature();

		/**
		 * The meta object literal for the '<em><b>Constraint</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL__CONSTRAINT = eINSTANCE.getFeatureModel_Constraint();

		/**
		 * The meta object literal for the '{@link spl.impl.FeatureImpl <em>Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.FeatureImpl
		 * @see spl.impl.SplPackageImpl#getFeature()
		 * @generated
		 */
		EClass FEATURE = eINSTANCE.getFeature();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__NAME = eINSTANCE.getFeature_Name();

		/**
		 * The meta object literal for the '<em><b>Vartype</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__VARTYPE = eINSTANCE.getFeature_Vartype();

		/**
		 * The meta object literal for the '{@link spl.impl.scwellformednessImpl <em>scwellformedness</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.scwellformednessImpl
		 * @see spl.impl.SplPackageImpl#getscwellformedness()
		 * @generated
		 */
		EClass SCWELLFORMEDNESS = eINSTANCE.getscwellformedness();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCWELLFORMEDNESS__SOURCE = eINSTANCE.getscwellformedness_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCWELLFORMEDNESS__TARGET = eINSTANCE.getscwellformedness_Target();

		/**
		 * The meta object literal for the '{@link spl.impl.cdwellformednessImpl <em>cdwellformedness</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.cdwellformednessImpl
		 * @see spl.impl.SplPackageImpl#getcdwellformedness()
		 * @generated
		 */
		EClass CDWELLFORMEDNESS = eINSTANCE.getcdwellformedness();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CDWELLFORMEDNESS__SOURCE = eINSTANCE.getcdwellformedness_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CDWELLFORMEDNESS__TARGET = eINSTANCE.getcdwellformedness_Target();

		/**
		 * The meta object literal for the '{@link spl.impl.FMconstraintsImpl <em>FMconstraints</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.FMconstraintsImpl
		 * @see spl.impl.SplPackageImpl#getFMconstraints()
		 * @generated
		 */
		EClass FMCONSTRAINTS = eINSTANCE.getFMconstraints();

		/**
		 * The meta object literal for the '<em><b>Opr</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FMCONSTRAINTS__OPR = eINSTANCE.getFMconstraints_Opr();

		/**
		 * The meta object literal for the '<em><b>Operand</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FMCONSTRAINTS__OPERAND = eINSTANCE.getFMconstraints_Operand();

		/**
		 * The meta object literal for the '{@link spl.impl.CMconstraintsImpl <em>CMconstraints</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.CMconstraintsImpl
		 * @see spl.impl.SplPackageImpl#getCMconstraints()
		 * @generated
		 */
		EClass CMCONSTRAINTS = eINSTANCE.getCMconstraints();

		/**
		 * The meta object literal for the '<em><b>Opr</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CMCONSTRAINTS__OPR = eINSTANCE.getCMconstraints_Opr();

		/**
		 * The meta object literal for the '<em><b>Operand</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CMCONSTRAINTS__OPERAND = eINSTANCE.getCMconstraints_Operand();

		/**
		 * The meta object literal for the '{@link spl.impl.ClassDiagramImpl <em>Class Diagram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.ClassDiagramImpl
		 * @see spl.impl.SplPackageImpl#getClassDiagram()
		 * @generated
		 */
		EClass CLASS_DIAGRAM = eINSTANCE.getClassDiagram();

		/**
		 * The meta object literal for the '<em><b>Relationship</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_DIAGRAM__RELATIONSHIP = eINSTANCE.getClassDiagram_Relationship();

		/**
		 * The meta object literal for the '<em><b>Class</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_DIAGRAM__CLASS = eINSTANCE.getClassDiagram_Class();

		/**
		 * The meta object literal for the '<em><b>Wf</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_DIAGRAM__WF = eINSTANCE.getClassDiagram_Wf();

		/**
		 * The meta object literal for the '{@link spl.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.StateImpl
		 * @see spl.impl.SplPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Entry</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__ENTRY = eINSTANCE.getState_Entry();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__NAME = eINSTANCE.getState_Name();

		/**
		 * The meta object literal for the '{@link spl.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.TransitionImpl
		 * @see spl.impl.SplPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Always Present</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__ALWAYS_PRESENT = eINSTANCE.getTransition_AlwaysPresent();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__TRIGGER = eINSTANCE.getTransition_Trigger();

		/**
		 * The meta object literal for the '<em><b>Grd</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__GRD = eINSTANCE.getTransition_Grd();

		/**
		 * The meta object literal for the '<em><b>Transitionbehavior</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__TRANSITIONBEHAVIOR = eINSTANCE.getTransition_Transitionbehavior();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__NAME = eINSTANCE.getTransition_Name();

		/**
		 * The meta object literal for the '<em><b>Tostate</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TOSTATE = eINSTANCE.getTransition_Tostate();

		/**
		 * The meta object literal for the '<em><b>Fromstate</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__FROMSTATE = eINSTANCE.getTransition_Fromstate();

		/**
		 * The meta object literal for the '{@link spl.impl.ClassImpl <em>Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.ClassImpl
		 * @see spl.impl.SplPackageImpl#getClass_()
		 * @generated
		 */
		EClass CLASS = eINSTANCE.getClass_();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS__NAME = eINSTANCE.getClass_Name();

		/**
		 * The meta object literal for the '<em><b>Interfc</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS__INTERFC = eINSTANCE.getClass_Interfc();

		/**
		 * The meta object literal for the '{@link spl.impl.RelationshipImpl <em>Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.RelationshipImpl
		 * @see spl.impl.SplPackageImpl#getRelationship()
		 * @generated
		 */
		EClass RELATIONSHIP = eINSTANCE.getRelationship();

		/**
		 * The meta object literal for the '<em><b>Always Present</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP__ALWAYS_PRESENT = eINSTANCE.getRelationship_AlwaysPresent();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP__NAME = eINSTANCE.getRelationship_Name();

		/**
		 * The meta object literal for the '<em><b>Lab</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP__LAB = eINSTANCE.getRelationship_Lab();

		/**
		 * The meta object literal for the '<em><b>In Mul</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP__IN_MUL = eINSTANCE.getRelationship_InMul();

		/**
		 * The meta object literal for the '<em><b>Out Mul</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP__OUT_MUL = eINSTANCE.getRelationship_OutMul();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP__FROM = eINSTANCE.getRelationship_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP__TO = eINSTANCE.getRelationship_To();

		/**
		 * The meta object literal for the '{@link spl.impl.FeatureMappingImpl <em>Feature Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.FeatureMappingImpl
		 * @see spl.impl.SplPackageImpl#getFeatureMapping()
		 * @generated
		 */
		EClass FEATURE_MAPPING = eINSTANCE.getFeatureMapping();

		/**
		 * The meta object literal for the '<em><b>Fmap</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MAPPING__FMAP = eINSTANCE.getFeatureMapping_Fmap();

		/**
		 * The meta object literal for the '{@link spl.impl.MapImpl <em>Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.MapImpl
		 * @see spl.impl.SplPackageImpl#getMap()
		 * @generated
		 */
		EClass MAP = eINSTANCE.getMap();

		/**
		 * The meta object literal for the '<em><b>Ftr</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP__FTR = eINSTANCE.getMap_Ftr();

		/**
		 * The meta object literal for the '<em><b>Presence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAP__PRESENCE = eINSTANCE.getMap_Presence();

		/**
		 * The meta object literal for the '{@link spl.impl.MaptoImpl <em>Mapto</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.MaptoImpl
		 * @see spl.impl.SplPackageImpl#getMapto()
		 * @generated
		 */
		EClass MAPTO = eINSTANCE.getMapto();

		/**
		 * The meta object literal for the '<em><b>Presence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAPTO__PRESENCE = eINSTANCE.getMapto_Presence();

		/**
		 * The meta object literal for the '<em><b>C</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPTO__C = eINSTANCE.getMapto_C();

		/**
		 * The meta object literal for the '<em><b>Relation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPTO__RELATION = eINSTANCE.getMapto_Relation();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPTO__STATE = eINSTANCE.getMapto_State();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPTO__TRANSITION = eINSTANCE.getMapto_Transition();

		/**
		 * The meta object literal for the '{@link spl.impl.featMappingImpl <em>feat Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.featMappingImpl
		 * @see spl.impl.SplPackageImpl#getfeatMapping()
		 * @generated
		 */
		EClass FEAT_MAPPING = eINSTANCE.getfeatMapping();

		/**
		 * The meta object literal for the '<em><b>Opr LHS</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEAT_MAPPING__OPR_LHS = eINSTANCE.getfeatMapping_OprLHS();

		/**
		 * The meta object literal for the '<em><b>Mapfrom</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEAT_MAPPING__MAPFROM = eINSTANCE.getfeatMapping_Mapfrom();

		/**
		 * The meta object literal for the '<em><b>Opr RHS</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEAT_MAPPING__OPR_RHS = eINSTANCE.getfeatMapping_OprRHS();

		/**
		 * The meta object literal for the '<em><b>Mapto</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEAT_MAPPING__MAPTO = eINSTANCE.getfeatMapping_Mapto();

		/**
		 * The meta object literal for the '{@link spl.impl.MapdecImpl <em>Mapdec</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.MapdecImpl
		 * @see spl.impl.SplPackageImpl#getMapdec()
		 * @generated
		 */
		EClass MAPDEC = eINSTANCE.getMapdec();

		/**
		 * The meta object literal for the '<em><b>Presence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAPDEC__PRESENCE = eINSTANCE.getMapdec_Presence();

		/**
		 * The meta object literal for the '<em><b>Choice</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPDEC__CHOICE = eINSTANCE.getMapdec_Choice();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPDEC__FEATURE = eINSTANCE.getMapdec_Feature();

		/**
		 * The meta object literal for the '{@link spl.impl.MapToEntityImpl <em>Map To Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.MapToEntityImpl
		 * @see spl.impl.SplPackageImpl#getMapToEntity()
		 * @generated
		 */
		EClass MAP_TO_ENTITY = eINSTANCE.getMapToEntity();

		/**
		 * The meta object literal for the '<em><b>Presence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAP_TO_ENTITY__PRESENCE = eINSTANCE.getMapToEntity_Presence();

		/**
		 * The meta object literal for the '<em><b>Ftr</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP_TO_ENTITY__FTR = eINSTANCE.getMapToEntity_Ftr();

		/**
		 * The meta object literal for the '<em><b>C</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP_TO_ENTITY__C = eINSTANCE.getMapToEntity_C();

		/**
		 * The meta object literal for the '<em><b>Relation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP_TO_ENTITY__RELATION = eINSTANCE.getMapToEntity_Relation();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP_TO_ENTITY__STATE = eINSTANCE.getMapToEntity_State();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP_TO_ENTITY__TRANSITION = eINSTANCE.getMapToEntity_Transition();

		/**
		 * The meta object literal for the '{@link spl.impl.DecMappingImpl <em>Dec Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.DecMappingImpl
		 * @see spl.impl.SplPackageImpl#getDecMapping()
		 * @generated
		 */
		EClass DEC_MAPPING = eINSTANCE.getDecMapping();

		/**
		 * The meta object literal for the '<em><b>Opr LHS</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEC_MAPPING__OPR_LHS = eINSTANCE.getDecMapping_OprLHS();

		/**
		 * The meta object literal for the '<em><b>Mapfrom</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEC_MAPPING__MAPFROM = eINSTANCE.getDecMapping_Mapfrom();

		/**
		 * The meta object literal for the '<em><b>Opr RHS</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEC_MAPPING__OPR_RHS = eINSTANCE.getDecMapping_OprRHS();

		/**
		 * The meta object literal for the '<em><b>Mapto Entity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEC_MAPPING__MAPTO_ENTITY = eINSTANCE.getDecMapping_MaptoEntity();

		/**
		 * The meta object literal for the '{@link spl.impl.DecisionMappingImpl <em>Decision Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.DecisionMappingImpl
		 * @see spl.impl.SplPackageImpl#getDecisionMapping()
		 * @generated
		 */
		EClass DECISION_MAPPING = eINSTANCE.getDecisionMapping();

		/**
		 * The meta object literal for the '<em><b>Decision Map</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECISION_MAPPING__DECISION_MAP = eINSTANCE.getDecisionMapping_DecisionMap();

		/**
		 * The meta object literal for the '{@link spl.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.PropertyImpl
		 * @see spl.impl.SplPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__NAME = eINSTANCE.getProperty_Name();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__TEXT = eINSTANCE.getProperty_Text();

		/**
		 * The meta object literal for the '{@link spl.VarType <em>Var Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.VarType
		 * @see spl.impl.SplPackageImpl#getVarType()
		 * @generated
		 */
		EEnum VAR_TYPE = eINSTANCE.getVarType();

		/**
		 * The meta object literal for the '{@link spl.Operator <em>Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.Operator
		 * @see spl.impl.SplPackageImpl#getOperator()
		 * @generated
		 */
		EEnum OPERATOR = eINSTANCE.getOperator();

		/**
		 * The meta object literal for the '{@link spl.Presence <em>Presence</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.Presence
		 * @see spl.impl.SplPackageImpl#getPresence()
		 * @generated
		 */
		EEnum PRESENCE = eINSTANCE.getPresence();

		/**
		 * The meta object literal for the '{@link spl.impl.StateChartImpl <em>State Chart</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see spl.impl.StateChartImpl
		 * @see spl.impl.SplPackageImpl#getStateChart()
		 * @generated
		 */
		EClass STATE_CHART = eINSTANCE.getStateChart();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE_CHART__NAME = eINSTANCE.getStateChart_Name();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_CHART__STATE = eINSTANCE.getStateChart_State();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_CHART__TRANSITION = eINSTANCE.getStateChart_Transition();

		/**
		 * The meta object literal for the '<em><b>Wf</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_CHART__WF = eINSTANCE.getStateChart_Wf();

	}

} //SplPackage
