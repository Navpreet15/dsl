/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.Transition#getName <em>Name</em>}</li>
 *   <li>{@link spl.Transition#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link spl.Transition#getGrd <em>Grd</em>}</li>
 *   <li>{@link spl.Transition#getTransitionbehavior <em>Transitionbehavior</em>}</li>
 *   <li>{@link spl.Transition#getAlwaysPresent <em>Always Present</em>}</li>
 *   <li>{@link spl.Transition#getTostate <em>Tostate</em>}</li>
 *   <li>{@link spl.Transition#getFromstate <em>Fromstate</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getTransition()
 * @model annotation="gmf.link source='fromstate' target='tostate' width='2' label='name, trigger, grd, transitionbehavior' label.pattern='{0}-{1}:{2}/{3}' target.decoration='arrow'"
 * @generated
 */
public interface Transition extends EObject {
	/**
	 * Returns the value of the '<em><b>Always Present</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Always Present</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Always Present</em>' attribute.
	 * @see #setAlwaysPresent(Boolean)
	 * @see spl.SplPackage#getTransition_AlwaysPresent()
	 * @model default="true"
	 * @generated
	 */
	Boolean getAlwaysPresent();

	/**
	 * Sets the value of the '{@link spl.Transition#getAlwaysPresent <em>Always Present</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Always Present</em>' attribute.
	 * @see #getAlwaysPresent()
	 * @generated
	 */
	void setAlwaysPresent(Boolean value);

	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigger</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' attribute.
	 * @see #setTrigger(String)
	 * @see spl.SplPackage#getTransition_Trigger()
	 * @model
	 * @generated
	 */
	String getTrigger();

	/**
	 * Sets the value of the '{@link spl.Transition#getTrigger <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger</em>' attribute.
	 * @see #getTrigger()
	 * @generated
	 */
	void setTrigger(String value);

	/**
	 * Returns the value of the '<em><b>Grd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grd</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grd</em>' attribute.
	 * @see #setGrd(String)
	 * @see spl.SplPackage#getTransition_Grd()
	 * @model
	 * @generated
	 */
	String getGrd();

	/**
	 * Sets the value of the '{@link spl.Transition#getGrd <em>Grd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Grd</em>' attribute.
	 * @see #getGrd()
	 * @generated
	 */
	void setGrd(String value);

	/**
	 * Returns the value of the '<em><b>Transitionbehavior</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitionbehavior</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitionbehavior</em>' attribute.
	 * @see #setTransitionbehavior(String)
	 * @see spl.SplPackage#getTransition_Transitionbehavior()
	 * @model
	 * @generated
	 */
	String getTransitionbehavior();

	/**
	 * Sets the value of the '{@link spl.Transition#getTransitionbehavior <em>Transitionbehavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transitionbehavior</em>' attribute.
	 * @see #getTransitionbehavior()
	 * @generated
	 */
	void setTransitionbehavior(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see spl.SplPackage#getTransition_Name()
	 * @model id="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link spl.Transition#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Tostate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tostate</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tostate</em>' reference.
	 * @see #setTostate(State)
	 * @see spl.SplPackage#getTransition_Tostate()
	 * @model required="true"
	 * @generated
	 */
	State getTostate();

	/**
	 * Sets the value of the '{@link spl.Transition#getTostate <em>Tostate</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tostate</em>' reference.
	 * @see #getTostate()
	 * @generated
	 */
	void setTostate(State value);

	/**
	 * Returns the value of the '<em><b>Fromstate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fromstate</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fromstate</em>' reference.
	 * @see #setFromstate(State)
	 * @see spl.SplPackage#getTransition_Fromstate()
	 * @model required="true"
	 * @generated
	 */
	State getFromstate();

	/**
	 * Sets the value of the '{@link spl.Transition#getFromstate <em>Fromstate</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fromstate</em>' reference.
	 * @see #getFromstate()
	 * @generated
	 */
	void setFromstate(State value);

} // Transition
