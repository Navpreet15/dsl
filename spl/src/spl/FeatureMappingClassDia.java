/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Mapping Class Dia</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.FeatureMappingClassDia#getName <em>Name</em>}</li>
 *   <li>{@link spl.FeatureMappingClassDia#getContainment <em>Containment</em>}</li>
 *   <li>{@link spl.FeatureMappingClassDia#getFeaturename <em>Featurename</em>}</li>
 *   <li>{@link spl.FeatureMappingClassDia#getRelationname <em>Relationname</em>}</li>
 *   <li>{@link spl.FeatureMappingClassDia#getClassname <em>Classname</em>}</li>
 *   <li>{@link spl.FeatureMappingClassDia#getStatename <em>Statename</em>}</li>
 *   <li>{@link spl.FeatureMappingClassDia#getTransitionname <em>Transitionname</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getFeatureMappingClassDia()
 * @model annotation="gmf.node label='featurename, relationname, classname, statename, transitionname' label.pattern='{0}=>{1}:{2}:{3}:{4}'"
 * @generated
 */
public interface FeatureMappingClassDia extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see spl.SplPackage#getFeatureMappingClassDia_Name()
	 * @model id="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link spl.FeatureMappingClassDia#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containment</em>' attribute.
	 * @see #setContainment(String)
	 * @see spl.SplPackage#getFeatureMappingClassDia_Containment()
	 * @model
	 * @generated
	 */
	String getContainment();

	/**
	 * Sets the value of the '{@link spl.FeatureMappingClassDia#getContainment <em>Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containment</em>' attribute.
	 * @see #getContainment()
	 * @generated
	 */
	void setContainment(String value);

	/**
	 * Returns the value of the '<em><b>Featurename</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Featurename</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Featurename</em>' attribute list.
	 * @see spl.SplPackage#getFeatureMappingClassDia_Featurename()
	 * @model
	 * @generated
	 */
	EList<String> getFeaturename();

	/**
	 * Returns the value of the '<em><b>Relationname</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationname</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationname</em>' attribute list.
	 * @see spl.SplPackage#getFeatureMappingClassDia_Relationname()
	 * @model
	 * @generated
	 */
	EList<String> getRelationname();

	/**
	 * Returns the value of the '<em><b>Classname</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classname</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classname</em>' attribute list.
	 * @see spl.SplPackage#getFeatureMappingClassDia_Classname()
	 * @model
	 * @generated
	 */
	EList<String> getClassname();

	/**
	 * Returns the value of the '<em><b>Statename</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statename</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statename</em>' attribute list.
	 * @see spl.SplPackage#getFeatureMappingClassDia_Statename()
	 * @model
	 * @generated
	 */
	EList<String> getStatename();

	/**
	 * Returns the value of the '<em><b>Transitionname</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitionname</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitionname</em>' attribute list.
	 * @see spl.SplPackage#getFeatureMappingClassDia_Transitionname()
	 * @model
	 * @generated
	 */
	EList<String> getTransitionname();

} // FeatureMappingClassDia
