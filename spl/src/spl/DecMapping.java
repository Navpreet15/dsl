/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dec Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.DecMapping#getOprLHS <em>Opr LHS</em>}</li>
 *   <li>{@link spl.DecMapping#getMapfrom <em>Mapfrom</em>}</li>
 *   <li>{@link spl.DecMapping#getOprRHS <em>Opr RHS</em>}</li>
 *   <li>{@link spl.DecMapping#getMaptoEntity <em>Mapto Entity</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getDecMapping()
 * @model annotation="gmf.node label='oprLHS, oprRHS' label.pattern='{0}:{1}'"
 * @generated
 */
public interface DecMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Opr LHS</b></em>' attribute.
	 * The literals are from the enumeration {@link spl.Operator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opr LHS</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opr LHS</em>' attribute.
	 * @see spl.Operator
	 * @see #setOprLHS(Operator)
	 * @see spl.SplPackage#getDecMapping_OprLHS()
	 * @model
	 * @generated
	 */
	Operator getOprLHS();

	/**
	 * Sets the value of the '{@link spl.DecMapping#getOprLHS <em>Opr LHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opr LHS</em>' attribute.
	 * @see spl.Operator
	 * @see #getOprLHS()
	 * @generated
	 */
	void setOprLHS(Operator value);

	/**
	 * Returns the value of the '<em><b>Mapfrom</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Mapdec}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapfrom</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mapfrom</em>' containment reference list.
	 * @see spl.SplPackage#getDecMapping_Mapfrom()
	 * @model containment="true"
	 * @generated
	 */
	EList<Mapdec> getMapfrom();

	/**
	 * Returns the value of the '<em><b>Opr RHS</b></em>' attribute.
	 * The literals are from the enumeration {@link spl.Operator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opr RHS</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opr RHS</em>' attribute.
	 * @see spl.Operator
	 * @see #setOprRHS(Operator)
	 * @see spl.SplPackage#getDecMapping_OprRHS()
	 * @model
	 * @generated
	 */
	Operator getOprRHS();

	/**
	 * Sets the value of the '{@link spl.DecMapping#getOprRHS <em>Opr RHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opr RHS</em>' attribute.
	 * @see spl.Operator
	 * @see #getOprRHS()
	 * @generated
	 */
	void setOprRHS(Operator value);

	/**
	 * Returns the value of the '<em><b>Mapto Entity</b></em>' containment reference list.
	 * The list contents are of type {@link spl.MapToEntity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapto Entity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mapto Entity</em>' containment reference list.
	 * @see spl.SplPackage#getDecMapping_MaptoEntity()
	 * @model containment="true"
	 * @generated
	 */
	EList<MapToEntity> getMaptoEntity();

} // DecMapping
