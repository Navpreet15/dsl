/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fact Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.FactFeature#getName <em>Name</em>}</li>
 *   <li>{@link spl.FactFeature#getContain <em>Contain</em>}</li>
 *   <li>{@link spl.FactFeature#getFfact <em>Ffact</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getFactFeature()
 * @model annotation="gmf.node label='name'"
 * @generated
 */
public interface FactFeature extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see spl.SplPackage#getFactFeature_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link spl.FactFeature#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Contain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contain</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contain</em>' attribute.
	 * @see #setContain(String)
	 * @see spl.SplPackage#getFactFeature_Contain()
	 * @model
	 * @generated
	 */
	String getContain();

	/**
	 * Sets the value of the '{@link spl.FactFeature#getContain <em>Contain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contain</em>' attribute.
	 * @see #getContain()
	 * @generated
	 */
	void setContain(String value);

	/**
	 * Returns the value of the '<em><b>Ffact</b></em>' reference list.
	 * The list contents are of type {@link spl.Feature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ffact</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ffact</em>' reference list.
	 * @see spl.SplPackage#getFactFeature_Ffact()
	 * @model annotation="gmf.link label='Contain'"
	 * @generated
	 */
	EList<Feature> getFfact();

} // FactFeature
