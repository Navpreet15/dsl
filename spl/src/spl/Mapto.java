/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapto</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.Mapto#getPresence <em>Presence</em>}</li>
 *   <li>{@link spl.Mapto#getC <em>C</em>}</li>
 *   <li>{@link spl.Mapto#getRelation <em>Relation</em>}</li>
 *   <li>{@link spl.Mapto#getState <em>State</em>}</li>
 *   <li>{@link spl.Mapto#getTransition <em>Transition</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getMapto()
 * @model annotation="gmf.node label='presence'"
 * @generated
 */
public interface Mapto extends EObject {
	/**
	 * Returns the value of the '<em><b>Presence</b></em>' attribute.
	 * The literals are from the enumeration {@link spl.Presence}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Presence</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Presence</em>' attribute.
	 * @see spl.Presence
	 * @see #setPresence(Presence)
	 * @see spl.SplPackage#getMapto_Presence()
	 * @model
	 * @generated
	 */
	Presence getPresence();

	/**
	 * Sets the value of the '{@link spl.Mapto#getPresence <em>Presence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Presence</em>' attribute.
	 * @see spl.Presence
	 * @see #getPresence()
	 * @generated
	 */
	void setPresence(Presence value);

	/**
	 * Returns the value of the '<em><b>C</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>C</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>C</em>' reference.
	 * @see #setC(spl.Class)
	 * @see spl.SplPackage#getMapto_C()
	 * @model
	 * @generated
	 */
	spl.Class getC();

	/**
	 * Sets the value of the '{@link spl.Mapto#getC <em>C</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>C</em>' reference.
	 * @see #getC()
	 * @generated
	 */
	void setC(spl.Class value);

	/**
	 * Returns the value of the '<em><b>Relation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation</em>' reference.
	 * @see #setRelation(Relationship)
	 * @see spl.SplPackage#getMapto_Relation()
	 * @model
	 * @generated
	 */
	Relationship getRelation();

	/**
	 * Sets the value of the '{@link spl.Mapto#getRelation <em>Relation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relation</em>' reference.
	 * @see #getRelation()
	 * @generated
	 */
	void setRelation(Relationship value);

	/**
	 * Returns the value of the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' reference.
	 * @see #setState(State)
	 * @see spl.SplPackage#getMapto_State()
	 * @model
	 * @generated
	 */
	State getState();

	/**
	 * Sets the value of the '{@link spl.Mapto#getState <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' reference.
	 * @see #getState()
	 * @generated
	 */
	void setState(State value);

	/**
	 * Returns the value of the '<em><b>Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' reference.
	 * @see #setTransition(Transition)
	 * @see spl.SplPackage#getMapto_Transition()
	 * @model
	 * @generated
	 */
	Transition getTransition();

	/**
	 * Sets the value of the '{@link spl.Mapto#getTransition <em>Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transition</em>' reference.
	 * @see #getTransition()
	 * @generated
	 */
	void setTransition(Transition value);

} // Mapto
