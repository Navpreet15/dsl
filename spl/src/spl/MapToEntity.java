/**
 */
package spl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Map To Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.MapToEntity#getPresence <em>Presence</em>}</li>
 *   <li>{@link spl.MapToEntity#getFtr <em>Ftr</em>}</li>
 *   <li>{@link spl.MapToEntity#getC <em>C</em>}</li>
 *   <li>{@link spl.MapToEntity#getRelation <em>Relation</em>}</li>
 *   <li>{@link spl.MapToEntity#getState <em>State</em>}</li>
 *   <li>{@link spl.MapToEntity#getTransition <em>Transition</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getMapToEntity()
 * @model annotation="gmf.node label='presence'"
 * @generated
 */
public interface MapToEntity extends EObject {
	/**
	 * Returns the value of the '<em><b>Presence</b></em>' attribute.
	 * The literals are from the enumeration {@link spl.Presence}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Presence</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Presence</em>' attribute.
	 * @see spl.Presence
	 * @see #setPresence(Presence)
	 * @see spl.SplPackage#getMapToEntity_Presence()
	 * @model
	 * @generated
	 */
	Presence getPresence();

	/**
	 * Sets the value of the '{@link spl.MapToEntity#getPresence <em>Presence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Presence</em>' attribute.
	 * @see spl.Presence
	 * @see #getPresence()
	 * @generated
	 */
	void setPresence(Presence value);

	/**
	 * Returns the value of the '<em><b>Ftr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ftr</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ftr</em>' reference.
	 * @see #setFtr(Feature)
	 * @see spl.SplPackage#getMapToEntity_Ftr()
	 * @model
	 * @generated
	 */
	Feature getFtr();

	/**
	 * Sets the value of the '{@link spl.MapToEntity#getFtr <em>Ftr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ftr</em>' reference.
	 * @see #getFtr()
	 * @generated
	 */
	void setFtr(Feature value);

	/**
	 * Returns the value of the '<em><b>C</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>C</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>C</em>' reference.
	 * @see #setC(spl.Class)
	 * @see spl.SplPackage#getMapToEntity_C()
	 * @model
	 * @generated
	 */
	spl.Class getC();

	/**
	 * Sets the value of the '{@link spl.MapToEntity#getC <em>C</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>C</em>' reference.
	 * @see #getC()
	 * @generated
	 */
	void setC(spl.Class value);

	/**
	 * Returns the value of the '<em><b>Relation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation</em>' reference.
	 * @see #setRelation(Relationship)
	 * @see spl.SplPackage#getMapToEntity_Relation()
	 * @model
	 * @generated
	 */
	Relationship getRelation();

	/**
	 * Sets the value of the '{@link spl.MapToEntity#getRelation <em>Relation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relation</em>' reference.
	 * @see #getRelation()
	 * @generated
	 */
	void setRelation(Relationship value);

	/**
	 * Returns the value of the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' reference.
	 * @see #setState(State)
	 * @see spl.SplPackage#getMapToEntity_State()
	 * @model
	 * @generated
	 */
	State getState();

	/**
	 * Sets the value of the '{@link spl.MapToEntity#getState <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' reference.
	 * @see #getState()
	 * @generated
	 */
	void setState(State value);

	/**
	 * Returns the value of the '<em><b>Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' reference.
	 * @see #setTransition(Transition)
	 * @see spl.SplPackage#getMapToEntity_Transition()
	 * @model
	 * @generated
	 */
	Transition getTransition();

	/**
	 * Sets the value of the '{@link spl.MapToEntity#getTransition <em>Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transition</em>' reference.
	 * @see #getTransition()
	 * @generated
	 */
	void setTransition(Transition value);

} // MapToEntity
