/**
 */
package spl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.Map#getFtr <em>Ftr</em>}</li>
 *   <li>{@link spl.Map#getPresence <em>Presence</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getMap()
 * @model annotation="gmf.node label='presence'"
 * @generated
 */
public interface Map extends EObject {
	/**
	 * Returns the value of the '<em><b>Ftr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ftr</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ftr</em>' reference.
	 * @see #setFtr(Feature)
	 * @see spl.SplPackage#getMap_Ftr()
	 * @model required="true"
	 * @generated
	 */
	Feature getFtr();

	/**
	 * Sets the value of the '{@link spl.Map#getFtr <em>Ftr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ftr</em>' reference.
	 * @see #getFtr()
	 * @generated
	 */
	void setFtr(Feature value);

	/**
	 * Returns the value of the '<em><b>Presence</b></em>' attribute.
	 * The literals are from the enumeration {@link spl.Presence}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Presence</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Presence</em>' attribute.
	 * @see spl.Presence
	 * @see #setPresence(Presence)
	 * @see spl.SplPackage#getMap_Presence()
	 * @model
	 * @generated
	 */
	Presence getPresence();

	/**
	 * Sets the value of the '{@link spl.Map#getPresence <em>Presence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Presence</em>' attribute.
	 * @see spl.Presence
	 * @see #getPresence()
	 * @generated
	 */
	void setPresence(Presence value);

} // Map
