/**
 */
package spl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Choice</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.Choice#getName <em>Name</em>}</li>
 *   <li>{@link spl.Choice#getVarType <em>Var Type</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getChoice()
 * @model annotation="gmf.node label='name, varType' figure='rectangle' label.pattern='{0}:{1}'"
 * @generated
 */
public interface Choice extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see spl.SplPackage#getChoice_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link spl.Choice#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Var Type</b></em>' attribute.
	 * The literals are from the enumeration {@link spl.VarType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Type</em>' attribute.
	 * @see spl.VarType
	 * @see #setVarType(VarType)
	 * @see spl.SplPackage#getChoice_VarType()
	 * @model
	 * @generated
	 */
	VarType getVarType();

	/**
	 * Sets the value of the '{@link spl.Choice#getVarType <em>Var Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Type</em>' attribute.
	 * @see spl.VarType
	 * @see #getVarType()
	 * @generated
	 */
	void setVarType(VarType value);

} // Choice
