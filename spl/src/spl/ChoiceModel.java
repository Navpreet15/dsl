/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Choice Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.ChoiceModel#getCh <em>Ch</em>}</li>
 *   <li>{@link spl.ChoiceModel#getConstraint <em>Constraint</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getChoiceModel()
 * @model annotation="gmf.node label.text='ChoiceModel' label.placement='none' figure='rectangle'"
 * @generated
 */
public interface ChoiceModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Ch</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Choice}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ch</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ch</em>' containment reference list.
	 * @see spl.SplPackage#getChoiceModel_Ch()
	 * @model containment="true"
	 * @generated
	 */
	EList<Choice> getCh();

	/**
	 * Returns the value of the '<em><b>Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link spl.CMconstraints}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint</em>' containment reference list.
	 * @see spl.SplPackage#getChoiceModel_Constraint()
	 * @model containment="true"
	 * @generated
	 */
	EList<CMconstraints> getConstraint();

} // ChoiceModel
