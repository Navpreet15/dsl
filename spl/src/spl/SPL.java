/**
 */
package spl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SPL</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.SPL#getName <em>Name</em>}</li>
 *   <li>{@link spl.SPL#getFm <em>Fm</em>}</li>
 *   <li>{@link spl.SPL#getDm <em>Dm</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getSPL()
 * @model annotation="gmf.node label='name' figure='rectangle'"
 * @generated
 */
public interface SPL extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see spl.SplPackage#getSPL_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link spl.SPL#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Fm</b></em>' containment reference list.
	 * The list contents are of type {@link spl.FeatureModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fm</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fm</em>' containment reference list.
	 * @see spl.SplPackage#getSPL_Fm()
	 * @model containment="true"
	 * @generated
	 */
	EList<FeatureModel> getFm();

	/**
	 * Returns the value of the '<em><b>Dm</b></em>' containment reference list.
	 * The list contents are of type {@link spl.DomainModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dm</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dm</em>' containment reference list.
	 * @see spl.SplPackage#getSPL_Dm()
	 * @model containment="true"
	 * @generated
	 */
	EList<DomainModel> getDm();

} // SPL
