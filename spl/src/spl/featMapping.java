/**
 */
package spl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>feat Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.featMapping#getOprLHS <em>Opr LHS</em>}</li>
 *   <li>{@link spl.featMapping#getMapfrom <em>Mapfrom</em>}</li>
 *   <li>{@link spl.featMapping#getOprRHS <em>Opr RHS</em>}</li>
 *   <li>{@link spl.featMapping#getMapto <em>Mapto</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getfeatMapping()
 * @model annotation="gmf.node label='oprLHS, oprRHS' label.pattern='{0}:{1}'"
 * @generated
 */
public interface featMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Opr LHS</b></em>' attribute.
	 * The literals are from the enumeration {@link spl.Operator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opr LHS</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opr LHS</em>' attribute.
	 * @see spl.Operator
	 * @see #setOprLHS(Operator)
	 * @see spl.SplPackage#getfeatMapping_OprLHS()
	 * @model
	 * @generated
	 */
	Operator getOprLHS();

	/**
	 * Sets the value of the '{@link spl.featMapping#getOprLHS <em>Opr LHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opr LHS</em>' attribute.
	 * @see spl.Operator
	 * @see #getOprLHS()
	 * @generated
	 */
	void setOprLHS(Operator value);

	/**
	 * Returns the value of the '<em><b>Mapfrom</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Map}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapfrom</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mapfrom</em>' containment reference list.
	 * @see spl.SplPackage#getfeatMapping_Mapfrom()
	 * @model containment="true"
	 * @generated
	 */
	EList<Map> getMapfrom();

	/**
	 * Returns the value of the '<em><b>Opr RHS</b></em>' attribute.
	 * The literals are from the enumeration {@link spl.Operator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opr RHS</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opr RHS</em>' attribute.
	 * @see spl.Operator
	 * @see #setOprRHS(Operator)
	 * @see spl.SplPackage#getfeatMapping_OprRHS()
	 * @model
	 * @generated
	 */
	Operator getOprRHS();

	/**
	 * Sets the value of the '{@link spl.featMapping#getOprRHS <em>Opr RHS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opr RHS</em>' attribute.
	 * @see spl.Operator
	 * @see #getOprRHS()
	 * @generated
	 */
	void setOprRHS(Operator value);

	/**
	 * Returns the value of the '<em><b>Mapto</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Mapto}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapto</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mapto</em>' containment reference list.
	 * @see spl.SplPackage#getfeatMapping_Mapto()
	 * @model containment="true"
	 * @generated
	 */
	EList<Mapto> getMapto();

} // featMapping
