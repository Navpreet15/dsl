/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.FeatureMapping#getFmap <em>Fmap</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getFeatureMapping()
 * @model annotation="gmf.node label.text='FeatureMappings' label.placement='none'"
 * @generated
 */
public interface FeatureMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Fmap</b></em>' containment reference list.
	 * The list contents are of type {@link spl.featMapping}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fmap</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fmap</em>' containment reference list.
	 * @see spl.SplPackage#getFeatureMapping_Fmap()
	 * @model containment="true"
	 *        annotation="gmf.compartment layout='list'"
	 * @generated
	 */
	EList<featMapping> getFmap();

} // FeatureMapping
