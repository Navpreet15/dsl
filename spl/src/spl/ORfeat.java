/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ORfeat</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.ORfeat#getOrfeature <em>Orfeature</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getORfeat()
 * @model
 * @generated
 */
public interface ORfeat extends EObject {
	/**
	 * Returns the value of the '<em><b>Orfeature</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Feature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Orfeature</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Orfeature</em>' containment reference list.
	 * @see spl.SplPackage#getORfeat_Orfeature()
	 * @model containment="true" lower="2"
	 * @generated
	 */
	EList<Feature> getOrfeature();

} // ORfeat
