/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decision Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.DecisionMapping#getDecisionMap <em>Decision Map</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getDecisionMapping()
 * @model annotation="gmf.node label.text='DecisionMappings' label.placement='none'"
 * @generated
 */
public interface DecisionMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Decision Map</b></em>' containment reference list.
	 * The list contents are of type {@link spl.DecMapping}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decision Map</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decision Map</em>' containment reference list.
	 * @see spl.SplPackage#getDecisionMapping_DecisionMap()
	 * @model containment="true"
	 *        annotation="gmf.compartment layout='list'"
	 * @generated
	 */
	EList<DecMapping> getDecisionMap();

} // DecisionMapping
