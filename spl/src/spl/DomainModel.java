/**
 */
package spl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Domain Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.DomainModel#getName <em>Name</em>}</li>
 *   <li>{@link spl.DomainModel#getCd <em>Cd</em>}</li>
 *   <li>{@link spl.DomainModel#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getDomainModel()
 * @model annotation="gmf.node label='name' figure='rectangle'"
 * @generated
 */
public interface DomainModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see spl.SplPackage#getDomainModel_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link spl.DomainModel#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Cd</b></em>' containment reference list.
	 * The list contents are of type {@link spl.ClassDiagram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cd</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cd</em>' containment reference list.
	 * @see spl.SplPackage#getDomainModel_Cd()
	 * @model containment="true"
	 * @generated
	 */
	EList<ClassDiagram> getCd();

	/**
	 * Returns the value of the '<em><b>Sc</b></em>' containment reference list.
	 * The list contents are of type {@link spl.StateChart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sc</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sc</em>' containment reference list.
	 * @see spl.SplPackage#getDomainModel_Sc()
	 * @model containment="true"
	 * @generated
	 */
	EList<StateChart> getSc();

} // DomainModel
