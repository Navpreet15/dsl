/**
 */
package spl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.Relationship#getAlwaysPresent <em>Always Present</em>}</li>
 *   <li>{@link spl.Relationship#getName <em>Name</em>}</li>
 *   <li>{@link spl.Relationship#getLab <em>Lab</em>}</li>
 *   <li>{@link spl.Relationship#getInMul <em>In Mul</em>}</li>
 *   <li>{@link spl.Relationship#getOutMul <em>Out Mul</em>}</li>
 *   <li>{@link spl.Relationship#getFrom <em>From</em>}</li>
 *   <li>{@link spl.Relationship#getTo <em>To</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getRelationship()
 * @model annotation="gmf.link source='from' target='to' target.decoration='arrow' width='2' label='name, lab, inMul, outMul' label.pattern='{0}:{1}:{2}:{3}'"
 * @generated
 */
public interface Relationship extends EObject {
	/**
	 * Returns the value of the '<em><b>Always Present</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Always Present</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Always Present</em>' attribute.
	 * @see #setAlwaysPresent(Boolean)
	 * @see spl.SplPackage#getRelationship_AlwaysPresent()
	 * @model default="true"
	 * @generated
	 */
	Boolean getAlwaysPresent();

	/**
	 * Sets the value of the '{@link spl.Relationship#getAlwaysPresent <em>Always Present</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Always Present</em>' attribute.
	 * @see #getAlwaysPresent()
	 * @generated
	 */
	void setAlwaysPresent(Boolean value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see spl.SplPackage#getRelationship_Name()
	 * @model id="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link spl.Relationship#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Lab</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lab</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lab</em>' attribute.
	 * @see #setLab(String)
	 * @see spl.SplPackage#getRelationship_Lab()
	 * @model
	 * @generated
	 */
	String getLab();

	/**
	 * Sets the value of the '{@link spl.Relationship#getLab <em>Lab</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lab</em>' attribute.
	 * @see #getLab()
	 * @generated
	 */
	void setLab(String value);

	/**
	 * Returns the value of the '<em><b>In Mul</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Mul</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Mul</em>' attribute.
	 * @see #setInMul(int)
	 * @see spl.SplPackage#getRelationship_InMul()
	 * @model default="1"
	 * @generated
	 */
	int getInMul();

	/**
	 * Sets the value of the '{@link spl.Relationship#getInMul <em>In Mul</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Mul</em>' attribute.
	 * @see #getInMul()
	 * @generated
	 */
	void setInMul(int value);

	/**
	 * Returns the value of the '<em><b>Out Mul</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Mul</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Mul</em>' attribute.
	 * @see #setOutMul(int)
	 * @see spl.SplPackage#getRelationship_OutMul()
	 * @model default="1"
	 * @generated
	 */
	int getOutMul();

	/**
	 * Sets the value of the '{@link spl.Relationship#getOutMul <em>Out Mul</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Out Mul</em>' attribute.
	 * @see #getOutMul()
	 * @generated
	 */
	void setOutMul(int value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' reference.
	 * @see #setFrom(spl.Class)
	 * @see spl.SplPackage#getRelationship_From()
	 * @model required="true"
	 * @generated
	 */
	spl.Class getFrom();

	/**
	 * Sets the value of the '{@link spl.Relationship#getFrom <em>From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(spl.Class value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' reference.
	 * @see #setTo(spl.Class)
	 * @see spl.SplPackage#getRelationship_To()
	 * @model required="true"
	 * @generated
	 */
	spl.Class getTo();

	/**
	 * Sets the value of the '{@link spl.Relationship#getTo <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' reference.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(spl.Class value);

} // Relationship
