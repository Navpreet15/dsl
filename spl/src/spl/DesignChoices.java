/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Design Choices</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.DesignChoices#getName <em>Name</em>}</li>
 *   <li>{@link spl.DesignChoices#getCm <em>Cm</em>}</li>
 *   <li>{@link spl.DesignChoices#getFm <em>Fm</em>}</li>
 *   <li>{@link spl.DesignChoices#getFmap <em>Fmap</em>}</li>
 *   <li>{@link spl.DesignChoices#getDmap <em>Dmap</em>}</li>
 *   <li>{@link spl.DesignChoices#getProperty <em>Property</em>}</li>
 *   <li>{@link spl.DesignChoices#getVar <em>Var</em>}</li>
 *   <li>{@link spl.DesignChoices#getCd <em>Cd</em>}</li>
 *   <li>{@link spl.DesignChoices#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getDesignChoices()
 * @model
 * @generated
 */
public interface DesignChoices extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see spl.SplPackage#getDesignChoices_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link spl.DesignChoices#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Cm</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cm</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cm</em>' containment reference.
	 * @see #setCm(ChoiceModel)
	 * @see spl.SplPackage#getDesignChoices_Cm()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ChoiceModel getCm();

	/**
	 * Sets the value of the '{@link spl.DesignChoices#getCm <em>Cm</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cm</em>' containment reference.
	 * @see #getCm()
	 * @generated
	 */
	void setCm(ChoiceModel value);

	/**
	 * Returns the value of the '<em><b>Fm</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fm</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fm</em>' containment reference.
	 * @see #setFm(FeatureModel)
	 * @see spl.SplPackage#getDesignChoices_Fm()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FeatureModel getFm();

	/**
	 * Sets the value of the '{@link spl.DesignChoices#getFm <em>Fm</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fm</em>' containment reference.
	 * @see #getFm()
	 * @generated
	 */
	void setFm(FeatureModel value);

	/**
	 * Returns the value of the '<em><b>Fmap</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fmap</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fmap</em>' containment reference.
	 * @see #setFmap(FeatureMapping)
	 * @see spl.SplPackage#getDesignChoices_Fmap()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FeatureMapping getFmap();

	/**
	 * Sets the value of the '{@link spl.DesignChoices#getFmap <em>Fmap</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fmap</em>' containment reference.
	 * @see #getFmap()
	 * @generated
	 */
	void setFmap(FeatureMapping value);

	/**
	 * Returns the value of the '<em><b>Dmap</b></em>' containment reference list.
	 * The list contents are of type {@link spl.DecisionMapping}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dmap</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dmap</em>' containment reference list.
	 * @see spl.SplPackage#getDesignChoices_Dmap()
	 * @model containment="true"
	 * @generated
	 */
	EList<DecisionMapping> getDmap();

	/**
	 * Returns the value of the '<em><b>Property</b></em>' containment reference list.
	 * The list contents are of type {@link spl.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' containment reference list.
	 * @see spl.SplPackage#getDesignChoices_Property()
	 * @model containment="true"
	 * @generated
	 */
	EList<Property> getProperty();

	/**
	 * Returns the value of the '<em><b>Var</b></em>' containment reference list.
	 * The list contents are of type {@link spl.variabls}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var</em>' containment reference list.
	 * @see spl.SplPackage#getDesignChoices_Var()
	 * @model containment="true"
	 * @generated
	 */
	EList<variabls> getVar();

	/**
	 * Returns the value of the '<em><b>Cd</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cd</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cd</em>' containment reference.
	 * @see #setCd(ClassDiagram)
	 * @see spl.SplPackage#getDesignChoices_Cd()
	 * @model containment="true"
	 * @generated
	 */
	ClassDiagram getCd();

	/**
	 * Sets the value of the '{@link spl.DesignChoices#getCd <em>Cd</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cd</em>' containment reference.
	 * @see #getCd()
	 * @generated
	 */
	void setCd(ClassDiagram value);

	/**
	 * Returns the value of the '<em><b>Sc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sc</em>' containment reference.
	 * @see #setSc(StateChart)
	 * @see spl.SplPackage#getDesignChoices_Sc()
	 * @model containment="true"
	 * @generated
	 */
	StateChart getSc();

	/**
	 * Sets the value of the '{@link spl.DesignChoices#getSc <em>Sc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sc</em>' containment reference.
	 * @see #getSc()
	 * @generated
	 */
	void setSc(StateChart value);

} // DesignChoices
