/**
 */
package spl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decisionto Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.DecisiontoFeature#getDecisionname <em>Decisionname</em>}</li>
 *   <li>{@link spl.DecisiontoFeature#getFeaturename <em>Featurename</em>}</li>
 *   <li>{@link spl.DecisiontoFeature#getContainfeature <em>Containfeature</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getDecisiontoFeature()
 * @model annotation="gmf.node label='decisionname, featurename, containfeature' label.pattern='{0}=>{1}{2} FM'"
 * @generated
 */
public interface DecisiontoFeature extends EObject {
	/**
	 * Returns the value of the '<em><b>Decisionname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decisionname</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decisionname</em>' attribute.
	 * @see #setDecisionname(String)
	 * @see spl.SplPackage#getDecisiontoFeature_Decisionname()
	 * @model
	 * @generated
	 */
	String getDecisionname();

	/**
	 * Sets the value of the '{@link spl.DecisiontoFeature#getDecisionname <em>Decisionname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decisionname</em>' attribute.
	 * @see #getDecisionname()
	 * @generated
	 */
	void setDecisionname(String value);

	/**
	 * Returns the value of the '<em><b>Featurename</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Featurename</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Featurename</em>' attribute.
	 * @see #setFeaturename(String)
	 * @see spl.SplPackage#getDecisiontoFeature_Featurename()
	 * @model
	 * @generated
	 */
	String getFeaturename();

	/**
	 * Sets the value of the '{@link spl.DecisiontoFeature#getFeaturename <em>Featurename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Featurename</em>' attribute.
	 * @see #getFeaturename()
	 * @generated
	 */
	void setFeaturename(String value);

	/**
	 * Returns the value of the '<em><b>Containfeature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containfeature</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containfeature</em>' attribute.
	 * @see #setContainfeature(String)
	 * @see spl.SplPackage#getDecisiontoFeature_Containfeature()
	 * @model
	 * @generated
	 */
	String getContainfeature();

	/**
	 * Sets the value of the '{@link spl.DecisiontoFeature#getContainfeature <em>Containfeature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containfeature</em>' attribute.
	 * @see #getContainfeature()
	 * @generated
	 */
	void setContainfeature(String value);

} // DecisiontoFeature
