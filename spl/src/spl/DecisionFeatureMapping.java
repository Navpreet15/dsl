/**
 */
package spl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decision Feature Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.DecisionFeatureMapping#getFeaturename <em>Featurename</em>}</li>
 *   <li>{@link spl.DecisionFeatureMapping#getDecisionname <em>Decisionname</em>}</li>
 *   <li>{@link spl.DecisionFeatureMapping#getClassname <em>Classname</em>}</li>
 *   <li>{@link spl.DecisionFeatureMapping#getContainclass <em>Containclass</em>}</li>
 *   <li>{@link spl.DecisionFeatureMapping#getRelationname <em>Relationname</em>}</li>
 *   <li>{@link spl.DecisionFeatureMapping#getContainrelation <em>Containrelation</em>}</li>
 *   <li>{@link spl.DecisionFeatureMapping#getStatename <em>Statename</em>}</li>
 *   <li>{@link spl.DecisionFeatureMapping#getContainstate <em>Containstate</em>}</li>
 *   <li>{@link spl.DecisionFeatureMapping#getTransitionname <em>Transitionname</em>}</li>
 *   <li>{@link spl.DecisionFeatureMapping#getContaintransition <em>Containtransition</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getDecisionFeatureMapping()
 * @model annotation="gmf.node label='decisionname,featurename, classname, relationname,  statename, transitionname, \nContainrelation, Containtransition, Containclass, Containstate' label.pattern='{0}&{1}=>{2} {3} CD: {4} {5} CD:{6} {7} SC: {8} {9} SC'"
 * @generated
 */
public interface DecisionFeatureMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Featurename</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Featurename</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Featurename</em>' attribute.
	 * @see #setFeaturename(String)
	 * @see spl.SplPackage#getDecisionFeatureMapping_Featurename()
	 * @model
	 * @generated
	 */
	String getFeaturename();

	/**
	 * Sets the value of the '{@link spl.DecisionFeatureMapping#getFeaturename <em>Featurename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Featurename</em>' attribute.
	 * @see #getFeaturename()
	 * @generated
	 */
	void setFeaturename(String value);

	/**
	 * Returns the value of the '<em><b>Decisionname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decisionname</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decisionname</em>' attribute.
	 * @see #setDecisionname(String)
	 * @see spl.SplPackage#getDecisionFeatureMapping_Decisionname()
	 * @model
	 * @generated
	 */
	String getDecisionname();

	/**
	 * Sets the value of the '{@link spl.DecisionFeatureMapping#getDecisionname <em>Decisionname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decisionname</em>' attribute.
	 * @see #getDecisionname()
	 * @generated
	 */
	void setDecisionname(String value);

	/**
	 * Returns the value of the '<em><b>Classname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classname</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classname</em>' attribute.
	 * @see #setClassname(String)
	 * @see spl.SplPackage#getDecisionFeatureMapping_Classname()
	 * @model
	 * @generated
	 */
	String getClassname();

	/**
	 * Sets the value of the '{@link spl.DecisionFeatureMapping#getClassname <em>Classname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Classname</em>' attribute.
	 * @see #getClassname()
	 * @generated
	 */
	void setClassname(String value);

	/**
	 * Returns the value of the '<em><b>Containclass</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containclass</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containclass</em>' attribute.
	 * @see #setContainclass(String)
	 * @see spl.SplPackage#getDecisionFeatureMapping_Containclass()
	 * @model
	 * @generated
	 */
	String getContainclass();

	/**
	 * Sets the value of the '{@link spl.DecisionFeatureMapping#getContainclass <em>Containclass</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containclass</em>' attribute.
	 * @see #getContainclass()
	 * @generated
	 */
	void setContainclass(String value);

	/**
	 * Returns the value of the '<em><b>Relationname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationname</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationname</em>' attribute.
	 * @see #setRelationname(String)
	 * @see spl.SplPackage#getDecisionFeatureMapping_Relationname()
	 * @model
	 * @generated
	 */
	String getRelationname();

	/**
	 * Sets the value of the '{@link spl.DecisionFeatureMapping#getRelationname <em>Relationname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relationname</em>' attribute.
	 * @see #getRelationname()
	 * @generated
	 */
	void setRelationname(String value);

	/**
	 * Returns the value of the '<em><b>Containrelation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containrelation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containrelation</em>' attribute.
	 * @see #setContainrelation(String)
	 * @see spl.SplPackage#getDecisionFeatureMapping_Containrelation()
	 * @model
	 * @generated
	 */
	String getContainrelation();

	/**
	 * Sets the value of the '{@link spl.DecisionFeatureMapping#getContainrelation <em>Containrelation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containrelation</em>' attribute.
	 * @see #getContainrelation()
	 * @generated
	 */
	void setContainrelation(String value);

	/**
	 * Returns the value of the '<em><b>Statename</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statename</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statename</em>' attribute.
	 * @see #setStatename(String)
	 * @see spl.SplPackage#getDecisionFeatureMapping_Statename()
	 * @model
	 * @generated
	 */
	String getStatename();

	/**
	 * Sets the value of the '{@link spl.DecisionFeatureMapping#getStatename <em>Statename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Statename</em>' attribute.
	 * @see #getStatename()
	 * @generated
	 */
	void setStatename(String value);

	/**
	 * Returns the value of the '<em><b>Containstate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containstate</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containstate</em>' attribute.
	 * @see #setContainstate(String)
	 * @see spl.SplPackage#getDecisionFeatureMapping_Containstate()
	 * @model
	 * @generated
	 */
	String getContainstate();

	/**
	 * Sets the value of the '{@link spl.DecisionFeatureMapping#getContainstate <em>Containstate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containstate</em>' attribute.
	 * @see #getContainstate()
	 * @generated
	 */
	void setContainstate(String value);

	/**
	 * Returns the value of the '<em><b>Transitionname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitionname</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitionname</em>' attribute.
	 * @see #setTransitionname(String)
	 * @see spl.SplPackage#getDecisionFeatureMapping_Transitionname()
	 * @model
	 * @generated
	 */
	String getTransitionname();

	/**
	 * Sets the value of the '{@link spl.DecisionFeatureMapping#getTransitionname <em>Transitionname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transitionname</em>' attribute.
	 * @see #getTransitionname()
	 * @generated
	 */
	void setTransitionname(String value);

	/**
	 * Returns the value of the '<em><b>Containtransition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containtransition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containtransition</em>' attribute.
	 * @see #setContaintransition(String)
	 * @see spl.SplPackage#getDecisionFeatureMapping_Containtransition()
	 * @model
	 * @generated
	 */
	String getContaintransition();

	/**
	 * Sets the value of the '{@link spl.DecisionFeatureMapping#getContaintransition <em>Containtransition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containtransition</em>' attribute.
	 * @see #getContaintransition()
	 * @generated
	 */
	void setContaintransition(String value);

} // DecisionFeatureMapping
