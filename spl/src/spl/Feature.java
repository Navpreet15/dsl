/**
 */
package spl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link spl.Feature#getName <em>Name</em>}</li>
 *   <li>{@link spl.Feature#getVartype <em>Vartype</em>}</li>
 * </ul>
 *
 * @see spl.SplPackage#getFeature()
 * @model annotation="gmf.node label='name, vartype' figure='ellipse' label.pattern='{0}:{1}'"
 * @generated
 */
public interface Feature extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see spl.SplPackage#getFeature_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link spl.Feature#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Vartype</b></em>' attribute.
	 * The literals are from the enumeration {@link spl.VarType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vartype</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vartype</em>' attribute.
	 * @see spl.VarType
	 * @see #setVartype(VarType)
	 * @see spl.SplPackage#getFeature_Vartype()
	 * @model
	 * @generated
	 */
	VarType getVartype();

	/**
	 * Sets the value of the '{@link spl.Feature#getVartype <em>Vartype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vartype</em>' attribute.
	 * @see spl.VarType
	 * @see #getVartype()
	 * @generated
	 */
	void setVartype(VarType value);

} // Feature
