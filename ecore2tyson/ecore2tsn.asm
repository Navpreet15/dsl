<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="ecore2tsn"/>
		<constant value="links"/>
		<constant value="NTransientLinkSet;"/>
		<constant value="col"/>
		<constant value="J"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="OclParametrizedType"/>
		<constant value="#native"/>
		<constant value="Collection"/>
		<constant value="J.setName(S):V"/>
		<constant value="OclSimpleType"/>
		<constant value="OclAny"/>
		<constant value="J.setElementType(J):V"/>
		<constant value="TransientLinkSet"/>
		<constant value="A.__matcher__():V"/>
		<constant value="A.__exec__():V"/>
		<constant value="self"/>
		<constant value="__resolve__"/>
		<constant value="1"/>
		<constant value="J.oclIsKindOf(J):B"/>
		<constant value="18"/>
		<constant value="NTransientLinkSet;.getLinkBySourceElement(S):QNTransientLink;"/>
		<constant value="J.oclIsUndefined():B"/>
		<constant value="15"/>
		<constant value="NTransientLink;.getTargetFromSource(J):J"/>
		<constant value="17"/>
		<constant value="30"/>
		<constant value="Sequence"/>
		<constant value="2"/>
		<constant value="A.__resolve__(J):J"/>
		<constant value="QJ.including(J):QJ"/>
		<constant value="QJ.flatten():QJ"/>
		<constant value="e"/>
		<constant value="value"/>
		<constant value="resolveTemp"/>
		<constant value="S"/>
		<constant value="NTransientLink;.getNamedTargetFromSource(JS):J"/>
		<constant value="name"/>
		<constant value="__matcher__"/>
		<constant value="A.__matchecore2CD():V"/>
		<constant value="A.__matchEClass2Class():V"/>
		<constant value="A.__matchEReference2Relation():V"/>
		<constant value="__exec__"/>
		<constant value="ecore2CD"/>
		<constant value="NTransientLinkSet;.getLinksByRule(S):QNTransientLink;"/>
		<constant value="A.__applyecore2CD(NTransientLink;):V"/>
		<constant value="EClass2Class"/>
		<constant value="A.__applyEClass2Class(NTransientLink;):V"/>
		<constant value="EReference2Relation"/>
		<constant value="A.__applyEReference2Relation(NTransientLink;):V"/>
		<constant value="__matchecore2CD"/>
		<constant value="ecore"/>
		<constant value="EC"/>
		<constant value="IN"/>
		<constant value="MMOF!Classifier;.allInstancesFrom(S):QJ"/>
		<constant value="TransientLink"/>
		<constant value="NTransientLink;.setRule(MATL!Rule;):V"/>
		<constant value="NTransientLink;.addSourceElement(SJ):V"/>
		<constant value="cd"/>
		<constant value="ClassDiagram"/>
		<constant value="TS"/>
		<constant value="NTransientLink;.addTargetElement(SJ):V"/>
		<constant value="NTransientLinkSet;.addLink2(NTransientLink;B):V"/>
		<constant value="9:5-11:31"/>
		<constant value="__applyecore2CD"/>
		<constant value="NTransientLink;"/>
		<constant value="NTransientLink;.getSourceElement(S):J"/>
		<constant value="NTransientLink;.getTargetElement(S):J"/>
		<constant value="3"/>
		<constant value="EClass"/>
		<constant value="class"/>
		<constant value="EReference"/>
		<constant value="relationship"/>
		<constant value="10:12-10:13"/>
		<constant value="10:12-10:20"/>
		<constant value="10:3-10:20"/>
		<constant value="11:18-11:19"/>
		<constant value="11:18-11:30"/>
		<constant value="11:3-11:30"/>
		<constant value="link"/>
		<constant value="__matchEClass2Class"/>
		<constant value="ic"/>
		<constant value="oc"/>
		<constant value="Class"/>
		<constant value="17:3-21:4"/>
		<constant value="__applyEClass2Class"/>
		<constant value="DefaultValue"/>
		<constant value="interfc"/>
		<constant value="EReferences"/>
		<constant value="18:12-18:14"/>
		<constant value="18:12-18:19"/>
		<constant value="18:4-18:19"/>
		<constant value="19:15-19:17"/>
		<constant value="19:15-19:30"/>
		<constant value="19:4-19:30"/>
		<constant value="20:20-20:22"/>
		<constant value="20:20-20:34"/>
		<constant value="20:4-20:34"/>
		<constant value="__matchEReference2Relation"/>
		<constant value="ir"/>
		<constant value="oref"/>
		<constant value="Relationship"/>
		<constant value="28:3-34:4"/>
		<constant value="__applyEReference2Relation"/>
		<constant value="LowerBound"/>
		<constant value="inMul"/>
		<constant value="UpperBound"/>
		<constant value="outMul"/>
		<constant value="EReferenceType"/>
		<constant value="to"/>
		<constant value="EOpposite"/>
		<constant value="from"/>
		<constant value="29:12-29:14"/>
		<constant value="29:12-29:19"/>
		<constant value="29:4-29:19"/>
		<constant value="30:13-30:15"/>
		<constant value="30:13-30:26"/>
		<constant value="30:4-30:26"/>
		<constant value="31:14-31:16"/>
		<constant value="31:14-31:27"/>
		<constant value="31:4-31:27"/>
		<constant value="32:9-32:11"/>
		<constant value="32:9-32:26"/>
		<constant value="32:4-32:26"/>
		<constant value="33:11-33:13"/>
		<constant value="33:11-33:23"/>
		<constant value="33:11-33:38"/>
		<constant value="33:4-33:38"/>
	</cp>
	<field name="1" type="2"/>
	<field name="3" type="4"/>
	<operation name="5">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<push arg="7"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="9"/>
			<pcall arg="10"/>
			<dup/>
			<push arg="11"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="12"/>
			<pcall arg="10"/>
			<pcall arg="13"/>
			<set arg="3"/>
			<getasm/>
			<push arg="14"/>
			<push arg="8"/>
			<new/>
			<set arg="1"/>
			<getasm/>
			<pcall arg="15"/>
			<getasm/>
			<pcall arg="16"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="24"/>
		</localvariabletable>
	</operation>
	<operation name="18">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<load arg="19"/>
			<getasm/>
			<get arg="3"/>
			<call arg="20"/>
			<if arg="21"/>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<dup/>
			<call arg="23"/>
			<if arg="24"/>
			<load arg="19"/>
			<call arg="25"/>
			<goto arg="26"/>
			<pop/>
			<load arg="19"/>
			<goto arg="27"/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="19"/>
			<iterate/>
			<store arg="29"/>
			<getasm/>
			<load arg="29"/>
			<call arg="30"/>
			<call arg="31"/>
			<enditerate/>
			<call arg="32"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="33" begin="23" end="27"/>
			<lve slot="0" name="17" begin="0" end="29"/>
			<lve slot="1" name="34" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="35">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
			<parameter name="29" type="36"/>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<load arg="19"/>
			<load arg="29"/>
			<call arg="37"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="6"/>
			<lve slot="1" name="34" begin="0" end="6"/>
			<lve slot="2" name="38" begin="0" end="6"/>
		</localvariabletable>
	</operation>
	<operation name="39">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<pcall arg="40"/>
			<getasm/>
			<pcall arg="41"/>
			<getasm/>
			<pcall arg="42"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="5"/>
		</localvariabletable>
	</operation>
	<operation name="43">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<push arg="44"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="46"/>
			<enditerate/>
			<getasm/>
			<get arg="1"/>
			<push arg="47"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="48"/>
			<enditerate/>
			<getasm/>
			<get arg="1"/>
			<push arg="49"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="50"/>
			<enditerate/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="33" begin="5" end="8"/>
			<lve slot="1" name="33" begin="15" end="18"/>
			<lve slot="1" name="33" begin="25" end="28"/>
			<lve slot="0" name="17" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="51">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="52"/>
			<push arg="53"/>
			<findme/>
			<push arg="54"/>
			<call arg="55"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="56"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="44"/>
			<pcall arg="57"/>
			<dup/>
			<push arg="33"/>
			<load arg="19"/>
			<pcall arg="58"/>
			<dup/>
			<push arg="59"/>
			<push arg="60"/>
			<push arg="61"/>
			<new/>
			<pcall arg="62"/>
			<pusht/>
			<pcall arg="63"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="64" begin="19" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="33" begin="6" end="26"/>
			<lve slot="0" name="17" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="65">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="66"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="33"/>
			<call arg="67"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="59"/>
			<call arg="68"/>
			<store arg="69"/>
			<load arg="69"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="70"/>
			<call arg="30"/>
			<set arg="71"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="72"/>
			<call arg="30"/>
			<set arg="73"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="74" begin="11" end="11"/>
			<lne id="75" begin="11" end="12"/>
			<lne id="76" begin="9" end="14"/>
			<lne id="77" begin="17" end="17"/>
			<lne id="78" begin="17" end="18"/>
			<lne id="79" begin="15" end="20"/>
			<lne id="64" begin="8" end="21"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="59" begin="7" end="21"/>
			<lve slot="2" name="33" begin="3" end="21"/>
			<lve slot="0" name="17" begin="0" end="21"/>
			<lve slot="1" name="80" begin="0" end="21"/>
		</localvariabletable>
	</operation>
	<operation name="81">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="70"/>
			<push arg="53"/>
			<findme/>
			<push arg="54"/>
			<call arg="55"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="56"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="47"/>
			<pcall arg="57"/>
			<dup/>
			<push arg="82"/>
			<load arg="19"/>
			<pcall arg="58"/>
			<dup/>
			<push arg="83"/>
			<push arg="84"/>
			<push arg="61"/>
			<new/>
			<pcall arg="62"/>
			<pusht/>
			<pcall arg="63"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="85" begin="19" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="82" begin="6" end="26"/>
			<lve slot="0" name="17" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="86">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="66"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="82"/>
			<call arg="67"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="83"/>
			<call arg="68"/>
			<store arg="69"/>
			<load arg="69"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="38"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="87"/>
			<call arg="30"/>
			<set arg="88"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="89"/>
			<call arg="30"/>
			<set arg="73"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="90" begin="11" end="11"/>
			<lne id="91" begin="11" end="12"/>
			<lne id="92" begin="9" end="14"/>
			<lne id="93" begin="17" end="17"/>
			<lne id="94" begin="17" end="18"/>
			<lne id="95" begin="15" end="20"/>
			<lne id="96" begin="23" end="23"/>
			<lne id="97" begin="23" end="24"/>
			<lne id="98" begin="21" end="26"/>
			<lne id="85" begin="8" end="27"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="83" begin="7" end="27"/>
			<lve slot="2" name="82" begin="3" end="27"/>
			<lve slot="0" name="17" begin="0" end="27"/>
			<lve slot="1" name="80" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="99">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="72"/>
			<push arg="53"/>
			<findme/>
			<push arg="54"/>
			<call arg="55"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="56"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="49"/>
			<pcall arg="57"/>
			<dup/>
			<push arg="100"/>
			<load arg="19"/>
			<pcall arg="58"/>
			<dup/>
			<push arg="101"/>
			<push arg="102"/>
			<push arg="61"/>
			<new/>
			<pcall arg="62"/>
			<pusht/>
			<pcall arg="63"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="103" begin="19" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="100" begin="6" end="26"/>
			<lve slot="0" name="17" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="104">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="66"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="100"/>
			<call arg="67"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="101"/>
			<call arg="68"/>
			<store arg="69"/>
			<load arg="69"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="38"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="105"/>
			<call arg="30"/>
			<set arg="106"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="107"/>
			<call arg="30"/>
			<set arg="108"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="109"/>
			<call arg="30"/>
			<set arg="110"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="111"/>
			<get arg="109"/>
			<call arg="30"/>
			<set arg="112"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="113" begin="11" end="11"/>
			<lne id="114" begin="11" end="12"/>
			<lne id="115" begin="9" end="14"/>
			<lne id="116" begin="17" end="17"/>
			<lne id="117" begin="17" end="18"/>
			<lne id="118" begin="15" end="20"/>
			<lne id="119" begin="23" end="23"/>
			<lne id="120" begin="23" end="24"/>
			<lne id="121" begin="21" end="26"/>
			<lne id="122" begin="29" end="29"/>
			<lne id="123" begin="29" end="30"/>
			<lne id="124" begin="27" end="32"/>
			<lne id="125" begin="35" end="35"/>
			<lne id="126" begin="35" end="36"/>
			<lne id="127" begin="35" end="37"/>
			<lne id="128" begin="33" end="39"/>
			<lne id="103" begin="8" end="40"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="101" begin="7" end="40"/>
			<lve slot="2" name="100" begin="3" end="40"/>
			<lve slot="0" name="17" begin="0" end="40"/>
			<lve slot="1" name="80" begin="0" end="40"/>
		</localvariabletable>
	</operation>
</asm>
