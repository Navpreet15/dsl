import java.io.File;
import java.io.IOException;
import java.util.Collections;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.Command;
import edu.mit.csail.sdg.alloy4compiler.parser.CompModule;
import edu.mit.csail.sdg.alloy4compiler.parser.CompUtil;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Options;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Solution;
import edu.mit.csail.sdg.alloy4compiler.translator.TranslateAlloyToKodkod;
import spl.*;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.Command;
import edu.mit.csail.sdg.alloy4compiler.parser.CompModule;
import edu.mit.csail.sdg.alloy4compiler.parser.CompUtil;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Options;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Solution;
import edu.mit.csail.sdg.alloy4compiler.translator.TranslateAlloyToKodkod;
import kodkod.engine.Solution;



public class Feedback {

	public static void main(String[] args) {
		
		try {
			CompModule model = CompUtil.parseEverything_fromFile(null,null, "/Users/NAVWA/OneDrive/papers/re18-spldc/alloy/bankspldc.als");
			for(int i=0; i<model.getAllCommands().size(); i++)
			{	
			Command cmd=model.getAllCommands().get(i);
			A4Solution solution= TranslateAlloyToKodkod.execute_command(null, model.getAllReachableSigs(), cmd, new A4Options());
			if(solution.satisfiable())
			{
			solution.writeXML("bank"+i+".xml");
			System.out.println("The counterexample for "+solution.getOriginalCommand()+ " is saved as"+ "bank"+i+".xml");
			}
			else{
				System.out.println(solution.getOriginalCommand()+"is a valid assertion");
			}
			}
			for(int i=0; i<model.getAllCommands().size(); i++){
				
				// TODO Auto-generated method stub
				Command cmd=model.getAllCommands().get(i);
				A4Solution solution= TranslateAlloyToKodkod.execute_command(null, model.getAllReachableSigs(), cmd, new A4Options());
				if(solution.satisfiable()){
				File xfile = new File("bank"+i+".xml");	
				DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
				try {
					DocumentBuilder Builder=factory.newDocumentBuilder();
					try {
						Document doc = Builder.parse(xfile);
						getfeedback(doc, model);
					} catch (SAXException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
				} catch (ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
				}
			
		} catch (Err e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void getfeedback(Document doc, CompModule model){
		SplFactory factory=SplFactory.eINSTANCE;
		 // Create a resource set.
		  ResourceSet resourceSet = new ResourceSetImpl();

		  // Register the default resource factory -- only needed for stand-alone!
		  resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
		    Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		  for(int k=0; k<model.getAllCommands().size();k++){
			  Command cmd=model.getAllCommands().get(k);
			try {
				A4Solution solution= TranslateAlloyToKodkod.execute_command(null, model.getAllReachableSigs(), cmd, new A4Options());
				if(solution.satisfiable())
				{
				// Get the URI of the model file.
				  org.eclipse.emf.common.util.URI fileURI = org.eclipse.emf.common.util.URI.createFileURI(new File("bank"+k+".spl").getAbsolutePath());

				  // Create a resource for this file.
				  Resource resource = resourceSet.createResource(fileURI);
				  DesignChoices dc =factory.createDesignChoices();
				  dc.setName("DesignChoice");
				  resource.getContents().add(dc);
				  ChoiceModel cmodel=factory.createChoiceModel();
				  cmodel.setName("Counterexample Decisions");
				  SPL pl=factory.createSPL();
				  pl.setName("Counterexample Product line");
				  resource.getContents().add(pl);
				  dc.getCm().add(cmodel);
				  dc.getSpl().add(pl);
				  FeatureModel fmodel = factory.createFeatureModel();
				  fmodel.setName("counterexample configuration");
				  pl.getFm().add(fmodel);
				  DomainModel dm=factory.createDomainModel();
				  dm.setName("CounterProduct");
				  pl.getDm().add(dm);
				  //ClassDiagram cd =factory.createClassDiagram();
				  //StateChart sc=factory.createStateChart();
				  //dm.getCd().add(cd);
				  //dm.getSc().add(sc);
				  // Add the book and writer objects to the contents.

				  // Save the contents of the resource to the file system.
				
				NodeList items = doc.getElementsByTagName("skolem");
				for (int i = 0; i<items.getLength(); i++)
				{
					Element getskolem= (Element)items.item(i);
					Element getatom = (Element)getskolem.getElementsByTagName("atom").item(0);
					NodeList product = doc.getElementsByTagName("field");
					//System.out.println(product.getLength());
					for(int j=0; j< product.getLength(); j++)
					{
						Element p = (Element)product.item(j);
						getfeatures(doc , p, product, factory, getatom, resource, fmodel);
						getchoices(doc, p, product, factory, getatom, resource, cmodel);
						getDomainModel(doc, p, product, factory, getatom, resource, dm);
					}
				}
				
				try {
					resource.save(null);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
				  
			} catch (Err e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		  }
				
		  
	}
	
	
	public static void getfeatures(Document doc, Element p, NodeList product, SplFactory factory, Element getatom, Resource resource, FeatureModel fmodel){
		// Getting the required features
		if (p.getAttribute("label").contentEquals("config"))
		{
			NodeList tuplecon = p.getElementsByTagName("tuple");
			//System.out.println(tuplecon.getLength());
			for (int q=0; q<tuplecon.getLength(); q++)
			{
				Element getTuplecon = (Element)tuplecon.item(q);
				NodeList atmcon = getTuplecon.getElementsByTagName("atom");
				Element getconfig = (Element)atmcon.item(0);
				if(getconfig.getAttribute("label").contentEquals(getatom.getAttribute("label")))
				{
					System.out.println("The feature configuration that is responsilble for the property violation is:");
					//System.out.println(getatom.getAttribute("label"));
					Element fm = (Element)atmcon.item(1);
					//System.out.println(fm.getAttribute("label"));
					for(int r=0; r< product.getLength(); r++)
					{
						Element f = (Element)product.item(r);
						if (f.getAttribute("label").contentEquals("feature"))
						{
							NodeList tuplef = f.getElementsByTagName("tuple");
							//System.out.println(tuplef.getLength());
							for (int s=0; s<tuplef.getLength(); s++)
							{
								Element getTuplef = (Element)tuplef.item(s);
								NodeList atmf = getTuplef.getElementsByTagName("atom");
								Element getfeat = (Element)atmf.item(0);
								if(getfeat.getAttribute("label").contentEquals(fm.getAttribute("label")))
								{
									Element feature = (Element)atmf.item(1);
									Feature ft = factory.createFeature();
									ft.setName(feature.getAttribute("label"));
									System.out.println(feature.getAttribute("label"));
									fmodel.getFeature().add(ft);
									//resource.getContents().add(ft);
								}	
							}
						}
					}
				}
			}
		}
	}
	
	public static void getchoices(Document doc,Element p, NodeList product, SplFactory factory, Element getatom, Resource resource, ChoiceModel cmodel){
		if (p.getAttribute("label").contentEquals("spl"))
		{ 	
			NodeList tuple = p.getElementsByTagName("tuple");
			//System.out.println(tuple.getLength());
			for (int k=0; k<tuple.getLength(); k++)
			{
				Element getTuple = (Element)tuple.item(k);
				NodeList atm = getTuple.getElementsByTagName("atom");
				Element getSPL = (Element)atm.item(1);
				if(getSPL.getAttribute("label").contentEquals(getatom.getAttribute("label")))
				{
					Element designchoice = (Element)atm.item(0);
					//System.out.println(designchoice.getAttribute("label"));	
					for(int l=0; l< product.getLength(); l++)
					{
						Element cm = (Element)product.item(l);
						if (cm.getAttribute("label").contentEquals("cm"))
						{
							NodeList tupled = cm.getElementsByTagName("tuple");
							for(int m =0; m<tupled.getLength(); m++)
							{
								Element getTupled = (Element)tupled.item(m);
								NodeList atmd = getTupled.getElementsByTagName("atom");
								//System.out.println(atmd.getLength());
								Element getcm = (Element)atmd.item(0);
								if(getcm.getAttribute("label").contentEquals(designchoice.getAttribute("label")))
								{
									Element choicemodel = (Element)atmd.item(1);
									//System.out.println(choicemodel.getAttribute("label"));	
									for(int n=0; n< product.getLength(); n++)
									{
										Element c = (Element)product.item(n);
										if (c.getAttribute("label").contentEquals("choice"))
										{
											NodeList tuplec = c.getElementsByTagName("tuple");
											System.out.println("The design choice that are possible reason for violation are:");
											for(int o =0; o<tuplec.getLength(); o++)
											{
												Element getTuplec = (Element)tuplec.item(o);
												NodeList atmc = getTuplec.getElementsByTagName("atom");
												//System.out.println(atmc.getLength());
												Element getc = (Element)atmc.item(0);
												if(getc.getAttribute("label").contentEquals(choicemodel.getAttribute("label")))
												{
													Element choice = (Element)atmc.item(1);
													Choice ch=factory.createChoice();
													ch.setName(choice.getAttribute("label"));
													System.out.println(choice.getAttribute("label"));
													cmodel.getCh().add(ch);
													//resource.getContents().add(ch);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	public static void getDomainModel(Document doc, Element p, NodeList product, SplFactory factory, Element getatom, Resource resource, DomainModel dm){
		if (p.getAttribute("label").contentEquals("dm"))
		{
			NodeList tuplecon = p.getElementsByTagName("tuple");
			for (int q=0; q<tuplecon.getLength(); q++)
			{
				Element getTuplecon = (Element)tuplecon.item(q);
				NodeList atmcon = getTuplecon.getElementsByTagName("atom");
				Element getconfig = (Element)atmcon.item(0);
				if(getconfig.getAttribute("label").contentEquals(getatom.getAttribute("label")))
				{
					System.out.println("The Product that violates the property is:");
					//System.out.println(getatom.getAttribute("label"));
					Element fm = (Element)atmcon.item(1);
					//System.out.println(fm.getAttribute("label"));
					for(int r=0; r< product.getLength(); r++)
					{
						Element f = (Element)product.item(r);
						if (f.getAttribute("label").contentEquals("sc"))
						{
							NodeList tuplef = f.getElementsByTagName("tuple");
							for (int s=0; s<tuplef.getLength(); s++)
							{
								Element getTuplef = (Element)tuplef.item(s);
								NodeList atmf = getTuplef.getElementsByTagName("atom");
								Element getfeat = (Element)atmf.item(0);
								if(getfeat.getAttribute("label").contentEquals(fm.getAttribute("label")))
								{
									Element Statechart = (Element)atmf.item(1);
									StateChart sc=factory.createStateChart();
									dm.getSc().add(sc);
									//System.out.println(Statechart.getAttribute("label"));
									for(int l=0; l< product.getLength(); l++)
									{
										Element stt = (Element)product.item(l);
										if (stt.getAttribute("label").contentEquals("state"))
										{
											NodeList tupltr = stt.getElementsByTagName("tuple");
											for (int m=0; m<tupltr.getLength(); m++)
											{
												Element getTupltr = (Element)tupltr.item(m);
												NodeList atmtr = getTupltr.getElementsByTagName("atom");
												Element gettrans = (Element)atmtr.item(0);
												if(gettrans.getAttribute("label").contentEquals(Statechart.getAttribute("label")))
												{
													Element trans = (Element)atmtr.item(1);
													System.out.println(trans.getAttribute("label"));
													State st = factory.createState();
													st.setName(trans.getAttribute("label"));
													sc.getState().add(st);
												}
											}
										}
									}
									for(int t=0; t< product.getLength(); t++)
									{
										Element tr = (Element)product.item(t);
										if (tr.getAttribute("label").contentEquals("transition"))
										{
											NodeList tupltr = tr.getElementsByTagName("tuple");
											for (int m=0; m<tupltr.getLength(); m++)
											{
												Element getTupltr = (Element)tupltr.item(m);
												NodeList atmtr = getTupltr.getElementsByTagName("atom");
												Element gettrans = (Element)atmtr.item(0);
												if(gettrans.getAttribute("label").contentEquals(Statechart.getAttribute("label")))
												{
													Element trans = (Element)atmtr.item(1);
													System.out.println(trans.getAttribute("label"));
													Transition trs = factory.createTransition();
													trs.setName(trans.getAttribute("label"));
													//sc.getTransition().add(trs);
													for(int a=0; a< product.getLength(); a++)
													{
														Element sr = (Element)product.item(a);
														if (sr.getAttribute("label").contentEquals("source"))
														{
															NodeList tuplsr = sr.getElementsByTagName("tuple");
															for (int n=0; n<tuplsr.getLength(); n++)
															{
																Element getTuplsrc = (Element)tuplsr.item(n);
																NodeList atmsr = getTuplsrc.getElementsByTagName("atom");
																Element getsourc = (Element)atmsr.item(0);
																if(getsourc.getAttribute("label").contentEquals(trans.getAttribute("label")))
																{
																	Element sourc = (Element)atmsr.item(1);
																	System.out.println(sourc.getAttribute("label"));
																	State sour = factory.createState();
																	sour.setName(sourc.getAttribute("label"));
																	trs.setFromstate(sour);
																	//sc.getTransition().add(trs);
																}
															}
														}
														for(int b=0; b< product.getLength(); b++)
														{
															Element trg = (Element)product.item(b);
															if (trg.getAttribute("label").contentEquals("target"))
															{
																NodeList tuplsr = sr.getElementsByTagName("tuple");
																for (int n=0; n<tuplsr.getLength(); n++)
																{
																	Element getTuplsrc = (Element)tuplsr.item(n);
																	NodeList atmsr = getTuplsrc.getElementsByTagName("atom");
																	Element getsourc = (Element)atmsr.item(0);
																	if(getsourc.getAttribute("label").contentEquals(trans.getAttribute("label")))
																	{
																		Element sourc = (Element)atmsr.item(1);
																		System.out.println(sourc.getAttribute("label"));
																		State target = factory.createState();
																		target.setName(sourc.getAttribute("label"));
																		//sc.getState().add(target);
																		trs.setTostate(target);
																		sc.getTransition().add(trs);
																	}
																}
															}
														}
													}
												}
												
											}
										}
									}
								
								}
							}
						}
					}
					
					
				}
			}
			
		}
	}
}
		
				
	


