import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.Node;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import spl.*;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xml.type.internal.DataValue.URI;
import org.eclipse.*;

public class parsefile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File xfile = new File("a.xml");	
		DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder Builder=factory.newDocumentBuilder();
			try {
				Document doc = Builder.parse(xfile);
				SplFactory sfactory=SplFactory.eINSTANCE;
				//System.out.println(doc.getDocumentElement().getNodeName());
				NodeList items = doc.getElementsByTagName("skolem");
					for (int i = 0; i<items.getLength(); i++)
					{
						Element getskolem= (Element)items.item(i);
						Element getatom = (Element)getskolem.getElementsByTagName("atom").item(0);
						NodeList product = doc.getElementsByTagName("field");
						//System.out.println(product.getLength());
						for(int j=0; j< product.getLength(); j++)
						{
							Element p = (Element)product.item(j);
							//System.out.println(p.getAttribute("label"));
							if (p.getAttribute("label").contentEquals("spl"))
							{ 	
								NodeList tuple = p.getElementsByTagName("tuple");
								//System.out.println(tuple.getLength());
								for (int k=0; k<tuple.getLength(); k++)
								{
									Element getTuple = (Element)tuple.item(k);
									NodeList atm = getTuple.getElementsByTagName("atom");
									Element getSPL = (Element)atm.item(1);
									if(getSPL.getAttribute("label").contentEquals(getatom.getAttribute("label")))
									{
										Element designchoice = (Element)atm.item(0);
										//System.out.println(designchoice.getAttribute("label"));	
										for(int l=0; l< product.getLength(); l++)
										{
											Element cm = (Element)product.item(l);
											if (cm.getAttribute("label").contentEquals("cm"))
											{
												NodeList tupled = cm.getElementsByTagName("tuple");
												for(int m =0; m<tupled.getLength(); m++)
												{
													Element getTupled = (Element)tupled.item(m);
													NodeList atmd = getTupled.getElementsByTagName("atom");
													//System.out.println(atmd.getLength());
													Element getcm = (Element)atmd.item(0);
													if(getcm.getAttribute("label").contentEquals(designchoice.getAttribute("label")))
													{
														Element choicemodel = (Element)atmd.item(1);
														//System.out.println(choicemodel.getAttribute("label"));	
														
														for(int n=0; n< product.getLength(); n++)
														{
															Element c = (Element)product.item(n);
															if (c.getAttribute("label").contentEquals("choice"))
															{
																NodeList tuplec = c.getElementsByTagName("tuple");
																System.out.println("The design choice that are possible reason for violation are:");
																for(int o =0; o<tuplec.getLength(); o++)
																{
																	Element getTuplec = (Element)tuplec.item(o);
																	NodeList atmc = getTuplec.getElementsByTagName("atom");
																	//System.out.println(atmc.getLength());
																	Element getc = (Element)atmc.item(0);
																	if(getc.getAttribute("label").contentEquals(choicemodel.getAttribute("label")))
																	{
																		Element choice = (Element)atmc.item(1);
																		Choice ch= sfactory.createChoice();
																		ch.setName(choice.getAttribute("label"));
																		System.out.println(choice.getAttribute("label"));
																		
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
							
							// Getting the required features
							if (p.getAttribute("label").contentEquals("config"))
							{
								NodeList tuplecon = p.getElementsByTagName("tuple");
								//System.out.println(tuplecon.getLength());
								for (int q=0; q<tuplecon.getLength(); q++)
								{
									Element getTuplecon = (Element)tuplecon.item(q);
									NodeList atmcon = getTuplecon.getElementsByTagName("atom");
									Element getconfig = (Element)atmcon.item(0);
									if(getconfig.getAttribute("label").contentEquals(getatom.getAttribute("label")))
									{
										System.out.println("The feature configuration that is responsilble for the property violation is:");
										//System.out.println(getatom.getAttribute("label"));
										Element fm = (Element)atmcon.item(1);
										//System.out.println(fm.getAttribute("label"));
										for(int r=0; r< product.getLength(); r++)
										{
											Element f = (Element)product.item(r);
											if (f.getAttribute("label").contentEquals("feature"))
											{
												NodeList tuplef = f.getElementsByTagName("tuple");
												//System.out.println(tuplef.getLength());
												for (int s=0; s<tuplef.getLength(); s++)
												{
													Element getTuplef = (Element)tuplef.item(s);
													NodeList atmf = getTuplef.getElementsByTagName("atom");
													Element getfeat = (Element)atmf.item(0);
													if(getfeat.getAttribute("label").contentEquals(fm.getAttribute("label")))
													{
														Element feature = (Element)atmf.item(1);
														Feature ft = sfactory.createFeature();
														ft.setName(feature.getAttribute("label"));
														System.out.println(feature.getAttribute("label"));
													}	
												}
											}
										}
									}
								}
							}
						}	
					}
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}





