
// BASIC DEFINITIONS
abstract sig Feature{}

one sig  Withdraw ,  Deposit extends Feature{}


// FEATURE MODEL DEFINITION
abstract sig FeatureModel{
	feature: some Feature
}

//Domain Model Definition
abstract sig Interface{}



abstract sig Class{
interface: lone Interface,
}








//abstract sig Attribute{}
//one sig Illuminated, label extends Attribute{}
//Defining boolean datatype
abstract sig Bool{}
one sig True extends Bool{}
one sig False extends Bool{}

one sig HelpButton extends Class{
Illuminated: Bool,
label: String }


fact{HelpButton.Illuminated = True and HelpButton.label = "HELP"}

abstract sig Label{}

abstract sig Relationship{
from: one Class,
to: one Class,
label: one Label,
inMul:one Int,
outMul: one Int}





abstract sig ClassDiagram{
class : some Class,
relationship: some Relationship
}

//List all the actions
abstract sig Entry{}
abstract sig Trigger{}
abstract sig TransitionBehavior{}
abstract sig Guard{}





one sig  PinValid ,  AmountAvailable ,  PaperRoll extends Guard{}


abstract sig State{
entry : set Entry}
one sig  InsertCard ,  InsertAmount ,  DepositMoney ,  EnterPin ,  WithdrawMoney ,  PrintReceipt extends State{}


//List all the guards



abstract sig Transition{
source: one State,
target: one State,
trigger: set Trigger,
tb : set TransitionBehavior,
guard : lone Guard
}

one sig  T1 ,  T2 ,  T3 ,  T5 ,  T6 ,  T4 extends Transition{}


abstract sig StateChart{
state: some State,
transition : some Transition}


//Entries Depending on th Design Choices

 fact {(T1.source = InsertCard) and (T1.target =EnterPin)
  
}
 fact {(T2.source = EnterPin) and (T2.target =InsertAmount)
 and (T2.guard= PinValid)  
}
 fact {(T3.source = InsertAmount) and (T3.target =WithdrawMoney)
 and (T3.guard= AmountAvailable)  
}
 fact {(T5.source = DepositMoney) and (T5.target =PrintReceipt)
 and (T5.guard= PaperRoll)  
}
 fact {(T6.source = WithdrawMoney) and (T6.target =PrintReceipt)
 and (T6.guard= PaperRoll)  
}
 fact {(T4.source = InsertAmount) and (T4.target =DepositMoney)
  
}

fact{T1.guard= none}
fact{T4.guard= none}
fact{T1.trigger= none}
fact{T2.trigger= none}
fact{T3.trigger= none}
fact{T5.trigger= none}
fact{T6.trigger= none}
fact{T1.tb= none}
fact{T2.tb= none}
fact{T3.tb= none}
fact{T5.tb= none}
fact{T6.tb= none}


fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
 T4 not in p.dm.sc.transition=> T5 not in p.dm.sc.transition }
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
 T3 not in p.dm.sc.transition=> T6 not in p.dm.sc.transition }




fact{all sc: StateChart | T1 in sc.transition}
fact{all sc: StateChart | T2 in sc.transition}
fact{all sc: StateChart | T4 in sc.transition}




abstract sig DomainModel{
cd : one ClassDiagram,
sc: one StateChart
}



// Metamodel well-formedness constraints
fact {
	all d: ClassDiagram | all r: Relationship | r in d.relationship => 
	(r.from in d.class) and (r.to in d.class)
}


//Product Definition
abstract sig Product{
dm: one DomainModel,
config: one FeatureModel}

//Well formdness rules
fact{all f: FeatureModel | f in Product.config}
fact{all d: DomainModel | d in Product.dm}

//Product line definition
abstract sig SPL{
product : some Product
}

fact {all p: Product | p in SPL.product}


//Feature Mapping
fact {all p: Product | Withdraw in p.config.feature =>
((T3 in p.dm.sc.transition) )
else( (T3 not in p.dm.sc.transition) )}
fact {all p: Product | Deposit in p.config.feature =>
((T4 in p.dm.sc.transition) )
else( (T4 not in p.dm.sc.transition) )}


//SPLDCs
//Choices
abstract sig Choice{}
one sig  Multitask ,  Receipt extends Choice {}


//ChoiceModel definition
abstract sig ChoiceModel{
choice : set Choice}

//Design Choices Definition
abstract sig DesignChoices{
cm : one ChoiceModel,
spl : one SPL
}

fact {all c: ChoiceModel | c in DesignChoices.cm}
fact{all s: SPL | s in DesignChoices.spl}

//spldc definition
one sig SPLDC{
dc : some DesignChoices
}

fact {all d: DesignChoices | d in SPLDC.dc}

//Decision Mapping

fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
Multitask in dec.cm.choice => 
((Deposit in p.config.feature))}
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
Multitask in dec.cm.choice => 
((Withdraw in p.config.feature))}

fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
((Receipt in dec.cm.choice) and (Deposit in p.config.feature))  => 
T5 in p.dm.sc.transition }
fact { all dec: DesignChoices | all s: dec.spl | all p: s.product | 
((Receipt in dec.cm.choice) and (Withdraw in p.config.feature))  => 
T6 in p.dm.sc.transition }



//symmetry breaking constraints
fact {all c1, c2 : ClassDiagram | c1.relationship = c2.relationship => c1 = c2}
fact {all s1, s2 : StateChart | (s1.state = s2.state) and (s1.transition = s2.transition)
=> s1=s2 }
fact {all f1, f2 : FeatureModel | f1.feature = f2.feature => f1= f2}
fact {all d1, d2 : DomainModel | (d1.cd = d2.cd) and (d1.sc = d2.sc) => d1=d2}
fact {all t1, t2 : Relationship | (t1.from = t2.from) and (t1.to = t2.to) =>
t1=t2}
fact {all p1, p2 : Product | (p1.config = p2.config) and (p1.dm=p2.dm)=>
p1 = p2}

fact{all c1, c2 : ChoiceModel | c1.choice = c2.choice =>
c1 = c2}
fact {all dc1, dc2 : DesignChoices | dc1.cm = dc2.cm =>
dc1=dc2}



assert NoGuardsNA {all pl: SPL | all p: pl.product|all dm: p.dm | all t: dm.sc.transition | t.guard =none }
assert NoGuardsNS {all pl: SPL | some p: pl.product|all dm: p.dm | all t: dm.sc.transition | t.guard =none }
assert NoGuardsPA {some pl: SPL | all p: pl.product|all dm: p.dm | all t: dm.sc.transition | t.guard =none }
assert NoGuardsPS {some pl: SPL | some p: pl.product|all dm: p.dm | all t: dm.sc.transition | t.guard =none }

check NoGuardsNA for 10
check NoGuardsNS for 10
check NoGuardsPA for 10
check NoGuardsPS for 10

