package mergemodels;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.xtext.example.mydsl.tyson.Choice;
import org.xtext.example.mydsl.tyson.ChoiceModel;
import org.xtext.example.mydsl.tyson.Class;
import org.xtext.example.mydsl.tyson.ClassDiagram;
import org.xtext.example.mydsl.tyson.DecMapping;
import org.xtext.example.mydsl.tyson.DecisionMapping;
import org.xtext.example.mydsl.tyson.DesignChoices;
import org.xtext.example.mydsl.tyson.Feature;
import org.xtext.example.mydsl.tyson.FeatureMapping;
import org.xtext.example.mydsl.tyson.FeatureModel;
import org.xtext.example.mydsl.tyson.Map;
import org.xtext.example.mydsl.tyson.MapToEntity;
import org.xtext.example.mydsl.tyson.Mapdec;
import org.xtext.example.mydsl.tyson.Mappings;
import org.xtext.example.mydsl.tyson.Mapto;
import org.xtext.example.mydsl.tyson.Operator;
import org.xtext.example.mydsl.tyson.Presence;
import org.xtext.example.mydsl.tyson.TysonFactory;
import org.xtext.example.mydsl.tyson.featMapping;

public class merging {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Generate the output file using FILEURI, ResourceSet, tFactory
				//creating instance of tyson factory
						TysonFactory tfactory = TysonFactory.eINSTANCE;
						// Create a resource set.
						  ResourceSet tresourceSet = new ResourceSetImpl();
						  // Register the default resource factory -- only needed for stand-alone!
						  tresourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
						  Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
						  org.eclipse.emf.common.util.URI tfileURI = org.eclipse.emf.common.util.URI.createFileURI(new File("model"+".ts").getAbsolutePath());
						  // Create a resource for this file.
						  Resource tresource = tresourceSet.createResource(tfileURI);
						  
						  // Create the root element
						  DesignChoices dc =tfactory.createDesignChoices();
						  tresource.getContents().add(dc);
				
						  
						  
				
				// Read Model m1
				
				getfeatureModel(tfactory, dc, tresource, tresourceSet);
				
				//Read model m2
				
				getchoiceModel(tfactory, dc, tresource, tresourceSet);
				
				
				//Read model m3
				getclassDiagram(tfactory, dc, tresource, tresourceSet);
				
				//genfeaturemap(tresource, tfactory, dc);
				
				
				 try {
						tresource.save(null);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				 
				 Mappings mappings = tfactory.createMappings();
				 
				 genfeaturemap(tresource, tfactory, dc, mappings);
				 gendecisionmap(tresource, tfactory, dc, mappings);
				 
				 dc.setM(mappings);
				 
				 try {
						tresource.save(null);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				 
					  System.out.println(tfileURI+ " generated");
					
					
				

			}
	
	
	
	private static void gendecisionmap(Resource tresource, TysonFactory tfactory, DesignChoices dc, Mappings mappings) {
		// TODO Auto-generated method stub
		DecisionMapping dmap = tfactory.createDecisionMapping();
		mappings.getDmap().add(dmap);
		ChoiceModel cm = dc.getCm();
		int randomNum = ThreadLocalRandom.current().nextInt(0, cm.getCh().size() + 1);
		Choice c = cm.getCh().get(randomNum);
		
		
		
		DecMapping dm = tfactory.createDecMapping();
		dmap.getDecisionMap().add(dm);
		Mapdec mdec = tfactory.createMapdec();
		
		dm.getMapfrom().add(mdec);
		mdec.setChoice(c);
		mdec.setPresence(Presence.INCLUDE);
		dm.setOprLHS(Operator.AND);
		
		
		
		int randomNum1 = ThreadLocalRandom.current().nextInt(0, dc.getCd().getClass_().size() + 1);
		Class cl = dc.getCd().getClass_().get(randomNum1);
		MapToEntity mentity = tfactory.createMapToEntity();
		
		mentity.setC(cl);
		dm.setOprRHS(Operator.OR);

		
		
		
		/* Things to do:
		 * 1. Randomly selecting Operators
		 *2. Randomly selecting number of features in a feature mapping
		 *3. Randomly selecting number of classes in RHS in both feature mapping and decision mapping
		 *4. Randomly selecting number of choices in the Decision Mapping
		 *5. Randomly selecting number of feature mappings
		 *6. Randomly selecting number of decision mappings */
	}
	
	
	
	
	
	
	
	
	
	
	
	private static void genfeaturemap(Resource tresource, TysonFactory tfactory, DesignChoices dc, Mappings mappings) {
		// TODO Auto-generated method stub
		
		
		
		FeatureMapping fmap = tfactory.createFeatureMapping();
		mappings.getFmap().add(fmap);
		// generate random integer 
		FeatureModel fm = dc.getFm();
		int randomNum = ThreadLocalRandom.current().nextInt(0, fm.getFeature().size());
		// select that indexed feature from the feature model
		Feature f = fm.getFeature().get(randomNum);
		
		// Add selected faeture to map
		/// Create instance of featmapping
		featMapping featmap = tfactory.createfeatMapping();
		fmap.getFmap().add(featmap);
		
		// create instance of map
		Map m = tfactory.createMap();
		
		// add random feature to Map
		m.setFtr(f);
		m.setPresence(Presence.INCLUDE);
		featmap.setOprLHS(Operator.AND);
		
		featmap.getMapfrom().add(m);
		
		// select random operator LHS
		
		
		// generate random number 
		int randomNum1 = ThreadLocalRandom.current().nextInt(0, dc.getCd().getClass_().size());
		Class c = dc.getCd().getClass_().get(randomNum1);
		
		Mapto mt = tfactory.createMapto();
		
		mt.setC(c);
		featmap.setOprRHS(Operator.AND);
		featmap.getMapto().add(mt);
		
		
		
		
	//  select that numbered class/ relationship from cd
		
	}
	
	
	
	
	
			
			public static void getfeatureModel(TysonFactory tfactory, DesignChoices dc, Resource tresource, ResourceSet tresourceSet)
			{
				 File f =  new File("E:\\Studies\\SPLOTstuff\\splot\\splot\\featuremodels\\aircraft_fm.ts");
				  URI fileURI = URI.createFileURI(f.getAbsolutePath());
				// Read m1 file
				Resource t1resource = tresourceSet.getResource(fileURI, true); 
				TysonFactory factory = TysonFactory.eINSTANCE;
				DesignChoices dcs = (DesignChoices) t1resource.getContents().get(0);
				FeatureModel fm = dcs.getFm();
				
				//FeatureModel outfm = EcoreUtil.copy(fm);
				dc.setFm(fm);
				//tresource.getContents().add(fm);
				
				/*for(int i=0; i< fm.getFeature().size(); i++)
				{
					Feature infeature = fm.getFeature().get(i);
					Feature feature  = tfactory.createFeature();
					feature.setName(infeature.getName());
					
					for(int j=0; j< infeature.getChild().size(); j++)
					{
						Feature incld = infeature.getChild().get(j);
						Feature outcld = tfactory.createFeature();
						
						
					}
					
					
				}
				*/
				
				
				// Read All Features from feature Model
				
				// Create Feature for output
				
				// Set Feature Name
				
				
					// Read Features of children
				
					// For each children create feature.child and add it to the output
				
				
			}
			
			
			public static void getchoiceModel(TysonFactory tfactory, DesignChoices dc, Resource tresource, ResourceSet tresourceSet)
			{
				 File f =  new File("E:\\Studies\\SPLOTstuff\\splot\\splot\\Choicemodels\\car_fm.ts");
				  URI fileURI = URI.createFileURI(f.getAbsolutePath());
				// Read m1 file
				Resource t1resource = tresourceSet.getResource(fileURI, true); 
				TysonFactory factory = TysonFactory.eINSTANCE;
				DesignChoices dcs = (DesignChoices) t1resource.getContents().get(0);
				ChoiceModel cm = dcs.getCm();
				dc.setCm(cm);
			}
			
			
			
			public static void getclassDiagram(TysonFactory tfactory, DesignChoices dc, Resource tresource, ResourceSet tresourceSet)
			{
				File f =  new File("E:\\Studies\\ZooMetamodels\\domainmodels\\ADELFE.ts");
				  URI fileURI = URI.createFileURI(f.getAbsolutePath());
				// Read m1 file
				Resource t1resource = tresourceSet.getResource(fileURI, true); 
				TysonFactory factory = TysonFactory.eINSTANCE;
				DesignChoices dcs = (DesignChoices) t1resource.getContents().get(0);
				ClassDiagram cd = dcs.getCd();
				dc.setCd(cd);
			}
			
		}
